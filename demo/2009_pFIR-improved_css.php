<?php
header('Content-Type: text/css');
header('Cache-Control: no-cache');
$u='2009_pFIR-improved_gif.php?_='.mt_rand(0,100000000);
?>
#test { background-color:black; color:white; height:300px; padding:20px; }
#title1.FIRed{
  background-image:url(<?php echo $u; ?>);
  background-position:left bottom;
  background-repeat:no-repeat;
  text-indent:-9999em;
  width:142px;
}
#title2.FIRed{
  background-image:url(<?php echo $u; ?>);
  background-position:-142px bottom;
  background-repeat:no-repeat;
  text-indent:-9999em;
  width:119px;
}
#title1.FIR3{
  background-image:url(<?php echo $u; ?>&wait=3);
  background-position:left bottom;
  background-repeat:no-repeat;
  text-indent:-9999em;
  width:142px;
}
#title2.FIR3{
  background-image:url(<?php echo $u; ?>&wait=3);
  background-position:-142px bottom;
  background-repeat:no-repeat;
  text-indent:-9999em;
  width:119px;
}
#title1.FIR6{
  background-image:url(<?php echo $u; ?>&wait=6);
  background-position:left bottom;
  background-repeat:no-repeat;
  text-indent:-9999em;
  width:142px;
}
#title2.FIR6{
  background-image:url(<?php echo $u; ?>&wait=6);
  background-position:-142px bottom;
  background-repeat:no-repeat;
  text-indent:-9999em;
  width:119px;
}
