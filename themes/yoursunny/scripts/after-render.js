const cheerio = require('cheerio');
const css = require('css');
const {slugify} = require('transliteration');

hexo.extend.filter.register('after_post_render', function(post){
  const path = post.path.replace(hexo.source_dir, '')
               .replace('_posts/', '').replace(/\.[0-9a-z]+$/, '');

  const $ = cheerio.load('', {decodeEntities: false});
  const $article = $('<article/>');
  $article.append(post.content);

  $article.find('img').addClass('pure-img');
  $article.find('table:not(:has(td.gutter))').addClass('pure-table pure-table-bordered');
  $article.find('a').each(function(){
    const $a = $(this);
    const href = $a.attr('href');
    if (href.includes('//amzn.to/') ||
        (href.includes('amazon.com/') && href.includes('tag=yoursunny-20'))) {
      $a.after(' <small class="afflink-amzn" title="As an Amazon Associate I earn from qualifying purchases.">(paid link)</small>');
    }
  });

  post.article_class = 'article-' + slugify(path);
  if (post.scoped_style) {
    const cssParsed = css.parse(post.scoped_style, {silent: true});
    cssParsed.stylesheet.rules = cssParsed.stylesheet.rules.filter((rule) => rule.type == 'rule');
    cssParsed.stylesheet.rules.forEach((rule) => {
      rule.selectors = rule.selectors.map((sel) => '.' + post.article_class + ' ' + sel);
    });
    post.scoped_style = css.stringify(cssParsed, {compress: true});
  }

  post.content = $article.html();
  post.excerpt = $.html($article.children().slice(0, 5)
                        .not('script').not(':empty').not('p.code-caption'));
});
