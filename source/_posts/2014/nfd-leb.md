---
title: Deploy NDN Forwarding Daemon in Low End Box
lang: en
date: 2014-09-22
tags:
- NDN
- hosting
---

**Named Data Networking (NDN)** is a [future Internet architecture](https://named-data.net/project/execsummary/) designed as a *distribution network*.
To access NDN network from a Linux or OSX machine, one can install [NDN Platform](https://named-data.net/codebase/platform/), a collection of software packages including the protocol stack and critical applications.
[NDN Forwarding Daemon (NFD)](https://docs.named-data.net/NFD/current/), a core component of the architecture, serves as a software router and runs both on network routers as well as on end hosts to communicate with routers.

NDN Platform has new version releases periodically, and binary packages are provided with each platform release.
However, the development of NDN software, including NFD, happens much faster than platform releases.
If one wants to run bleeding edge software, those packages must be built from source code available on [GitHub](https://github.com/named-data).

As a geeky [low end box](https://lowendbox.com/) user, I'm thinking: **can I run NDN platform on a Linux box with a small amount of memory?**
The box I'm talking about is an OpenVZ container from [LowEndSpirit](https://web.archive.org/web/20140923055605/http://lowendspirit.com/locations.html) UK location, with **only 128MB memory and no swap space**.
To make the challenge more interesting, I want to avoid `apt-get`, and run bleeding edge version built from source code.

## Building on the Box

I quickly installed compilers and dependencies (such as `libboost-all-dev` which takes several minutes to download) with `apt-get`, and cloned the git repositories for NFD and other essential NDN Platform packages.
Given that the box has small memory and slow CPU, I can expect the compilation process to take a few hours, just like 8 years ago when I was compiling Apache on a library computer.

The wait does not take too long: the build process ends after a few seconds, because it doesn't work at all.
**ndn-cxx and NFD cannot build with only 128MB memory**.
You need [2GB memory](https://named-data.net/codebase/platform/documentation/ndn-platform-development-guidelines/) to build them from source.
I can guess one reason is the extensive usage of Boost.Asio which involves lots of C++ templates, and gcc cannot handle all those identifiers within 128MB memory.

## "Cross-" Compiling

Cross-compiling is a technique often used to build software for an embedded device with limited resources.
For example, Android applications are not compiled on Android phones; instead, they are typically built on more powerful computers, and then loaded into the phones.

I can use the same technique to build NFD for the low end box: **find a machine with more memory, and copy the binaries into the box**.
Except that, it's unnecessary to *cross-*compile, because the box is x86 architecture, and it's easy to find another machine with x86 architecture.

So, I provisioned an x86 virtual machine with 2GB memory on a powerful server.
**This machine must have same operating system as the target box**, and dependencies should have the same version.
`apt-get` can keep the dependencies up to date.
This means, each time I run a system update, I'd run it on both machines.

Now it follows the regular build process.
I turned off all debugging options, because I don't expect any useful debugging in 128MB memory.

## Installation

Copying the binaries into the box turns out to be easy: Waf build system has a `--destdir` option which allows me to specify where to install the binaries.
The default is `/` so that binaries are installed to the local machine.

All I need to do is to **mount the root directory of the target box via `sshfs`, and specify `--destdir=/target`**.
Alternatively, I can ask Waf to install into an empty directory, and then copy the contents into the target box.

This process works.
There's still a problem: the binaries are huge, because they still contain some sort of debugging symbols.
The solution is: run `strip` on each binary, after they are built but before installation.

## Configuration

Hopefully NFD is up and running on the low end box now.

The default configuration comes optimized for a machine with 1GB memory and it keeps 500MB worth of packets in the cache.
I don't have 500MB of memory, so I lowered the `tables.cs_max_packets` configuration option.

## Next Steps

Finally we have the software router running on the low end box.
Next step would be to connect it to other routers, such as the [NDN Testbed](https://named-data.net/ndn-testbed/).
I'll cover this topic in the [next post](/t/2014/nfd-connect/).
