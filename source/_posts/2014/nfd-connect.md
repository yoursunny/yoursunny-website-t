---
title: Get NFD Connected
lang: en
date: 2014-10-19
edited: 2021-06-01
tags:
- NDN
---

UPDATE 2021-06-01:
`nfdc` command syntax changed since NFD 0.6.0.
This article has been updated to include the new syntax.

**Named Data Networking (NDN)** is a potential [future Internet architecture](https://named-data.net/project/execsummary/) designed as a *distribution network*.
My [last post](/t/2014/nfd-leb/) described how to deploy NDN Forwarding Daemon (NFD) on a low end box.
Now it's time to get it connected.

The procedures and experiences in this post are applicable to any NDN node.
If you aren't using a low end box, you may follow the [official guide](https://docs.named-data.net/NFD/current/INSTALL.html) to install binary packages or compile from source.
This post assumes you have `ndn-cxx`, `nfd`, and [`ndnping`](https://github.com/named-data/ndn-tools) installed.
You need access to two machines with NFD running; they are referred to as "local" and "remote".

## Connect to Another Machine

After installing NFD on your machine, you can connect to any other machine running NFD.
Although NDN can run natively above Ethernet, there isn't a global scale native NDN network yet because NDN is still in its early stage.
Instead, NDN can run as an *overlay network* above traditional IP network.
You can specify the IP address and port number of the remote NFD, so that NDN packets are encapsulated into UDP or TCP packets and sent to the remote NFD.

Before going further, ensure NFD is started on both local and remote machines:

```bash
nfd-start
```

To establish a connection, you may type the following command:

```bash
# NFD ≤0.5.1
nfdc create udp4://192.0.2.1:6363

# NFD ≥0.6.0
nfdc face create udp4://192.0.2.1:6363
```

In this command:

* `nfdc` is the executable for NFD control tool
* `create` or `face create` means "create a face", where the term "face" is a generalization of "network interface" concept
* `udp4` means "IPv4 and UDP"
* `192.0.2.1` is the IP address of the remote machine; obviously you must change it to an actual IP address where another NFD instance is running
* `6363` is the port number of NFD on the remote machine; this is the default port number, and you usually don't need to change it

After this command, a *tunnel* is established from the local NFD to the remote NFD.
Logically, a tunnel is similar to a VPN connection, where the two machines are directly connected at NDN network layer.
A tunnel could cross many IP and Ethernet layer links, but those are invisible to NDN software router.

To verify a tunnel is established, run `nfd-status -f` (NFD ≤0.5.1) or `nfdc face list` (NFD ≥0.6.0) to see a list of active faces.
In the output you should see a line with the IP address and port number of the remote NFD.

## Send an Interest

In NDN architecture, all communications are receiver driven.
A *consumer* application sends an *Interest* packet, which indicates what content it wants to retrieve; the Interest packet contains a *Name* to specify *what* content is wanted, but doesn't not say *where* it is.
The network must figure out where the content is; the Interest packet is forwarded to the *producer* application, which is able to provide the content.
The producer application replies with a *Data* packet that contains the content wanted by the consumer; the Data packet contains the same Name as the Interest (or more precisely, the Data Name could have a longer Name, but it must have the Interest Name as its prefix).
The network then delivers the Data packet back to the consumer; this is achieved by matching Name and using forwarding states.

The command to send an Interest is:

```bash
ndnping -c 1 /A
```

In this command,

* `ndnping` is the executable for NDN reachability test tool, which is able to send Interests
* `-c 1` means sending one Interest only
* `/A` is the prefix of Interest(s)

This command would send an Interest with Name `/A/ping/<random-number>`.
However, most likely the response would be "Timeout", because after the command sends the Interest, the local NFD cannot figure out where the content with this Name may be retrieved, so that it would drop the packet.

To tell the local NFD where the content could be retrieved, a route in the routing table is needed.
To add a route to the routing table, type this command:

```bash
# NFD ≤0.5.1
nfdc register /A udp4://192.0.2.1:6363

# NFD ≥0.6.0
nfdc route add /A udp4://192.0.2.1:6363
```

In this command,

* `nfdc` is the executable for NFD control tool
* `register` or `route add` means "register a route"
* `/A` is the Name prefix; it tells NFD that all contents with this Name prefix is available on the remote NFD
* `udp4://192.0.2.1:6363` is the overlay protocol, IP address, and port number of the remote NFD; you must change it to actual IP address and port number where another NFD instance is running

To verify a route is added, run `nfd-status -r` (NFD ≤0.5.1) or `nfdc route list` (NFD ≥0.6.0) to see the routing table.
You should see a line with the Name prefix `/A`.

After adding the route, run `ndnping -c 1 /A` again, and the Interest will go to the remote NFD.
The response is most likely still "Timeout", but you can confirm an Interest indeed went out by observing traffic with Wireshark or `tcpdump`.

## Complete the Communication

So, the Interest went out to the remote NFD, but why does `ndnping -c 1 /A` return "Timeout"?
This is because the remote NFD doesn't know where the content is.

To have a successful NDN communication, two applications are needed: a *consumer* that sends *Interest* packets, and a *producer* that replies with *Data* packets to satisfy those Interests.
We already have `ndnping` as a consumer, but we are missing a producer to provide the contents.

To run a producer, log on to the remote machine, and type this command:

```bash
ndnpingserver /A
```

In this command,

* `ndnpingserver` is the executable for NDN reachability test tool server side, which is a producer that accepts Interests from `ndnping` and responds with Data packets
* `/A` is the Name prefix this producer would provider contents

This command will not return immediately.
Instead, it will stay in the terminal to wait for incoming Interests and respond to them.
Don't close that terminal.

Go back to the local machine, and run `ndnping -c 1 /A` again.
If everything is configured correctly, the output shall say "Content" with a round trip time measurement.
Viola!

If it's still "Timeout", try executing `ndnping -c 1 /A` on another terminal of the remote machine; if it's also "Timeout", check whether `ndnpingserver /A` is still running.
Also type `nfd-status -r` (NFD ≤0.5.1) or `nfdc route list` (NFD ≥0.6.0) on the local machine to inspect the routing table, and execute `nfdc register /A udp4://192.0.2.1:6363` (NFD ≤0.5.1) or `nfdc route add /A udp4://192.0.2.1:6363` (NFD ≥0.6.0) again if the route has disappeared.

## Connect to the NDN Testbed

The [NDN Testbed](https://named-data.net/ndn-testbed/) is a backbone NDN network with [software routers in North America, Europe, and Asia](https://web.archive.org/web/20230213021522if_/https://ndndemo.arl.wustl.edu/topology2.png).
The NDN Testbed is a shared resource created for research purposes; joining the testbed [requires an intention of productive research](https://named-data.net/ndn-testbed/policies-connecting-nodes-ndn-testbed/).

Connecting to a backbone router in the NDN Testbed is no different from connecting to any other machine.
The following command connects you to a router:

```bash
# NFD ≤0.5.1
nfdc register /ndn udp4://hobo.cs.arizona.edu

# NFD ≥0.6.0
nfdc route add /ndn udp4://hobo.cs.arizona.edu
```

Now you can retrieve any content from the network, such as:

```bash
ndnping /ndn/edu/arizona
```

This command executes the NDN reachability test tool, which measures the round trip time between you and the Arizona router.

## Next Steps

Now you have connected your local NFD to another NFD instance on a remote machine, and successfully retrieved some contents across this two-node network.
You are able to connect your local NFD to the NDN Testbed, a backbone NDN network, and retrieved some contents from the NDN Testbed.

The next step is to allow others on the backbone network reach you.
I'll cover this topic in my [next post](/t/2016/nfd-prefix/).
