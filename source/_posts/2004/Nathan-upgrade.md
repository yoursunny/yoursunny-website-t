---
title: "Nathan's Upgrade"
lang: en
date: 2004-03-20
tags:
- fiction
---

(Degner was sitting at his desk, reading something.)

**aside** Mr Degner is a clerk at the airport.

(The telephone rang.)

**Degner** (Pick up the receiver.) Hello! This is Degner speaking.

**Fury** Mr Degner, I'm Tome Fury. You probably don't remember meeting me and my family. Back in November, we were going to Miami and the flight was overbooked.

**Degner** I'm sorry that I can't recall the occasion.

**Fury** You made an announcement asking for volunteers willing to give up their seats for free tickets and a later flight. My wife, Ann, went up to your desk and told you that our family would be willing to go later. There were four of us---myself, my wife, our daughter, Mariah, and our son, Nathan.

**Degner** I'm sorry, but I can't recall it now. Could you give me more details?

**Fury** Well, we gave you our tickets. But about 15 minutes later, you came back and said that you wouldn't need our seats after all.

**aside** Mr Degner was unsure just where his former customer was going with his phone call. Was he angry that his family didn't get their free tickets?

**Fury** So you gave our tickets back and told us that you had given us an upgrade to first class. Then you thanked us for our willingness to be bumped. I know that this was something you didn't have to do. Now do you remember us?

**aside** Degner still couldn't to recall it, but as his customer seemed not to be angry, he said.

**Degner** Yes, I think I vaguely recall meeting you. That was what I should do, and it was just a routine procedure. I'm glad you enjoyed those seats so much.

**Fury** (excitedly) Oh, we did! That flight to Miami was wonderful, Ann and Mariah sat next to each other, and Nathan and I were right across from them. We laughed and talked all the way to Florida. It was just fantastic.

**Degner** I'm really glad that you and your family were happy with the seats. Thank you for taking the time to call ... (move the telephone dial near, about to hang up)

**Fury** There's something more. Just after we got home from our vacation...  (start to weep) Nathan was out riding his bike and ... (pause, sobbing, sadly) The driver of the car didn't even see ... (stop, sobbing)

**Degner** (welled up with tears, wait in silence)

**Fury** (a moment later) That trip was the last week the four of us were together. We'll always remember that flight to Miami, all of us sitting in those first-class seats. We were so happy and Nathan had so much fun. You helped make that time special, Mr. Degner. Ann and I just wanted to say how much we appreciate the gift you gave us.

**Degner** (breathe in deeply) Mr Fury, you have my deepest sympathies on the terrible loss of your son. And thank you for sharing this story with me.

**Fury** Thank you again for your upgrading us to the first-class and making that time special. Goodbye!

**Degner** Bye. (hang up the phone)

**aside** Mr Degner hadn't realized how much their upgrade had meant to them on their final trip with their son, Nathan. It was, after all, just a routine procedure.

**aside**  A few months later, Fury visited Degner at the airport.

(Fury walk to the desk with a suitcase in his hand.)

**Fury** Glad to meet you, Mr Degner. I'm Tom Fury, Nathan's father. Do you still remember me?

**Degner** Sure. I will always remember you. And thank you again for the phone call.

**Fury** You're welcome. (take out a photo from his suitcase) This is a photograph of Nathan. It's a small gift for you.

**Degner** Oh, thank you very much.

**aside** Nathan's picture has been with Mr Degner at work ever since. He keeps it as a monument to a boy who is no longer with us. It is a constant reminder that we never know when even the smallest gesture can touch others in an extraordinary way.

adapted from STORYTELLER, JANUARY 2004, STUDIO CLASSROOM
