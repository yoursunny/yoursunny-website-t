---
title: "Thought Provoker #3"
lang: en
date: 2020-02-23
tags:
- commentary
- geocaching
image: AnnapolisRock.jpg
---

The title comes from a geocache named [Thought Provoker #3](https://coord.info/GC7BK8G).
Inside the container there is a thought provoking question:

> If you could travel across the United States of America (and had no other obligations, financial or time constraints) what mode of transportation would you use and why?
> Walk/run, ride a bike, ride a motorcycle, hitchhike, drive a car, drive a RV/motorhome, take trains, or something else?

Here are my answers.

## First Choice: Geocaching Express 6000

The Geocaching Express 6000 GPS Receiver is a powerful GPS receiver, as shown in Geocaching International Film Festival (GIFF) 2019:

<iframe width="560" height="315" src="https://www.youtube.com/embed/GLE55sRJ-WI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Upon entering the coordinates, the GPS receiver would instantly create a teleportation portal to the destination.
If I own a Geocaching Express 6000, I can reach anywhere in the world with one jump through the portal, for both geocaching and general traveling.
This would be the most efficient way to travel across the United States of America.

Unfortunately, my attempt to purchase the Geocaching Express 6000 has been unsuccessful:

<iframe width="560" height="315" src="https://www.youtube.com/embed/Lu17gZ2CrAI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Second Choice: Hiking and Self-Driving Vehicle

I experienced [my first hike](/t/2017/hiking-Arizona/) in 2011, and have been enjoying hiking ever since.

![hiking the Appalachian Trail](AT.jpg)

In April 2019, I had the opportunity to hike on a section of the Appalachian Trail.
During the 6-hour hike, I can hear the birds and the stream, I can smell the mud and the flowers, I feel connected to the nature.
Occasionally, there's a view point allowing a peek into the vast farm lands.

![view at Annapolis Rock](AnnapolisRock.jpg)

After the trip, I [read up on the Appalachian Trail](https://www.appalachiantrail.org/), and realized its vastness: the Appalachian National Scenic Trail is the longest hiking-only footpath in the world.
The trail travels across 14 states, and it would take 6 months to hike through the entire trail.
I also learned that A.T. is not the only long trail in USA, and I met [one person](https://coord.info/PR29V0D) who hiked 10 long trails.

Hiking through those trails sounds interesting, but I understand that I do not have the necessary physical shape for this challenge.
If I hike for a whole day, I would need at least two days of resting.

I hear self-driving vehicles are coming soon ™.
When they become available, a combination of hiking and self-driving vehicle would be my preferred way of traveling across the United States of America.

In the morning, I can have the vehicle drop me off at a trail head, and then start the hike.
While I'm on the trails, the vehicle can go the the grocery store and take care of the supplies.
At end of the day, the vehicle shall meet me at the next trail head, and I can rest on the vehicle.
This way, I can see the most amazing sights, and bypass less interesting sections of the trails.

The vehicle shall have a sleeping compartment, so that I can rest at night or during my off days.
It also needs a shower and a robotic laundry system, so that I can stay clean.

Additionally, I need a drone that can carry 20kg of load.
This would allow me to do the good deed and pick up plastic bottles along the waterways, then have the drone carrying them out.

## Third Choice: Amtrak, Rental Cars, and Bus Pass of Every City

This is the most practical option to travel across the United States of America that is available today.

![sleeping car on Amtrak Texas Eagle](Amtrak.jpg)

A roomette on Amtrak is a stress-free way to travel for long distances.
In 2017, I [took the Amtrak train](/t/2017/cross-country-move/) to move from Tucson AZ to Gaithersburg MD, and thoroughly enjoyed the trip.

![rental car in Fairfield PA](GC68DKJ.jpg)

With a local area, I would [drive rental cars](/t/2017/behind-the-wheel/).
I found the rural roads between farm lands relaxing.
During the weekends of 2019, I drove to three state capitols: Annapolis MD, Harrisburg PA, and Richmond VA.
My driving skill has been improving: during the [Oregon vacation](/t/2019/Oregon-vacation-b/), I'm able to drive 4 hours per day on consecutive days without feeling overwhelmed.

![Deuce on The Strip in Las Vegas NV](Deuce.jpg)

If I'm visiting downtown of a major city, I still prefer to use public transit.
Although transit is typically slower than driving, I do not have the deal with navigation and parking.
This also reduces the environmental impact of my travels.
This was the option I chose when I visited [downtown Portland](/t/2019/Oregon-vacation-6/) during my Oregon vacation.

[Bicycling](/t/2017/on-two-wheels/) remains an option, but only for cities with flat terrain such as Tucson AZ or Hagerstown MD.
I would not want to bike on the hilly terrain of San Francisco CA or Arlington VA.

![Hawaii State Capitol](Hawaii.jpg)

Trains, cars, and buses would not enable me to reach Hawaii, one of the most beautiful places of the United States of America.
No problem, because [I have been there](/t/2017/academic-papers/), via Hawaiian Airlines.
If I'm going there again, can I [use a sailboat](https://routesofchange.org/blog/pacific-ocean-crossing-part-1-san-francisco-to-hawaii)?
