---
title: Geocaching in 2020
lang: en
date: 2020-12-31
tags:
- geocaching
- Maryland
image: GC5HGAY.jpg
---

I [started geocaching](/t/2017/start-geocaching/) in 2013 when I [lived in Tucson, Arizona](/t/2017/six-years-in-arizona/).
I [moved](/t/2017/cross-country-move/) to Montgomery County, Maryland in late 2017, and became a Premium Member on Geocaching.com at the beginning of 2018 so that I can find thousands of Premium-only caches around here.

## The Normal Months

Since 2019, I hooked up with [Georick402](https://www.mdgps.org/forum/viewtopic.php?f=50&t=9465), Maryland's top hider, to go geocaching together on weekends.
I have no transportation, and he has no brain, so I solve the mystery caches, and he drives me to them; it's a win-win partnership.
This relationship, of course, continued into 2020.

![2020-01-26 yoursunny and Georick402 at GC2HT7Q](GC2HT7Q.jpg)

In January and February, we went out together almost every weekend.
We completed a few high-difficulty Multi caches such as the [Woodrow Wilson Bridge Challenge](https://coord.info/GC2HT7Q), found Virginia's top-rated Letterbox hybrid [Roo's Runaway](https://coord.info/GC34J72), took the [Historic White's Ferry](https://mocoshow.com/blog/breaking-whites-ferry-ceasing-operations-immediately/), and attended Leap Day event(s) on February 29.

![2020-02-22 Historic White's Ferry](WhitesFerry.jpg)

Apart from that, I went to Washington DC and found my first Adventure Lab.
I also made plans to [hide two caches every quarter](https://www.mdgps.org/forum/viewtopic.php?f=1&t=10073#p127851).

## The CAM Trip

Maryland announced its first Coronavirus case on March 05.
A week later, schools were shuttered, [store shelves were empty](/t/2020/quarantine-foods/), and social distancing signs began to appear.
Nevertheless, there were few other restrictions, and outdoor exercises such as geocaching were deemed essential activities.
Maryland Geocaching Society's annual [CAM (Cache Across Maryland)](https://www.mdgps.org/forum/viewtopic.php?f=35&t=10078#p127970) started as usual, with a big warning on safety and well-being of CAM participants.
I called Georick402, briefly discussed the risks, and decided on a March 21 trip to Garrett County in Western Maryland, which had zero Coronavirus case at the time.

![2020-03-21 me at Hoye-Crest, Maryland's high point](HoyeCrest.jpg)

I found 19 geocaches on this trip.
Moreover, I climbed up to Hoye-Crest, the highest natural point in Maryland at an elevation of 1020 meters.

As a safety precaution, we both brought hand sanitizer, and there were Lysol wipes prepared in the car.
Although I usually prefer fancy restaurants and indoor dining, I opted for fast food and drive through, to reduce contact with restaurant workers and other diners.
When we run into other geocachers on the trail, we also kept our distancess.

![2020-03-21 a group of geocachers on Kindness Demonstration Trail (GC8J83J)](Kindness.jpg)

It was a long but exciting trip, and my partner [made a list of numbers](https://coord.info/GL10ZJT4C):

* 0 confirmed corona virus cases in Garret County
* 0 DNF's
* 1 [Highpoint](https://coord.info/GC20C8)
* 1 [smallest church in the lower 48 states](https://coord.info/GC2GZ9G)
* 2 of my caches we did maintenance on
* 2 new 2020 CAM caches
* 6 o'clock AM - left my house
* 8 o'clock PM - got home
* 10 other geocachers we ran into
* 11 finds for me
* 12 times I've now been to Maryland's Highpoint (and I keep saying I'm not going back)
* 14 hours on the road
* 19 finds for yoursunny (as I had already found 8 of them)
* 23 degrees *(-5 ℃)* - the coldest temperature (not counting wind chill)
* 45 degrees *(7 ℃)* - the warmest temperature
* 348 feet *(106 m)* our lowest elevation
* 410 miles *(660 km)* put on the geo-mobile
* 1,248 snow flakes
* 3,360 feet highest elevation
* 30,198 steps
* ~~46,248 tears yoursunny cried coming home, saying it was too much, before he finally fell asleep.~~

This trip, as it turns out, would become the last trip for a long time.

## The SLUMP Goal

A classic geocaching challenge is to maintain a **streak**, i.e. finding at least one geocache every day, for *X* consecutive days.
The opposite of a streak, i.e. not finding any geocache for *X* consecutive days, is called a **slump**.
My longest streak was 7 days, in December 2018, when I bought a weekly bus pass and went geocaching every day.
My longest slump was 44 days, in the summer of 2016, when [the Arizona heat](/t/2017/heat-and-monsoon/) kept me inside.
I like the number "7", but I don't like "44" and wanted a *whole* number such as 50 or 75.
Nevertheless, I couldn't see myself not geocaching for more than 44 days.

The governor announced a stay-at-home order on March 30.
After that, bus services were significantly reduced, and the Maryland geocaching reviewer [stopped publishing](https://www.mdgps.org/forum/viewtopic.php?f=8&t=10110#p128160) new geocaches except those near hiking trails.
I got an idea: what's a better time to collect some slump days?

Being on a **slump goal**, I cannot find any geocaches, or can I?
I still can, as long as I don't sign the log.
Thus, I started walking around the neighborhood, checking on those nearby caches that I have been saving for "emergencies", so that I would be able to locate them easily when I need them in a pinch.
I can also solve puzzles, and maintain my own hides if they are within walking distance.
I even bought a new [iPhone SE](https://amzn.to/2KQm8OS) and the [Cachly app](https://www.cach.ly/) to celebrate [Blue Switch Day](https://www.geocaching.com/blog/2020/04/new-souvenir-blue-switch-day-2020/).

![2020-06-20 me in front of I-270 at Watkins Mill Road Interchange](GC8TWEY.jpg)

On June 20, a new cache published just minutes from my apartment.
I ran to this cache and got [FTF](https://coord.info/GL11R7481).
My SLUMP ended at 90 days, a good number.

## The 2000

The Coronavirus situation got worse, and local authorities started to encourage people to wear masks.
I purchased the [UA SPORTSMASK](https://amzn.to/3hymQwf) from Under Armour, so that I can continue going outside for my essential activities.
There were not many geocaches to find and I don't want to dip into my "emergency" backups, so that I kept checking on the same caches over and over, without logging them as found.
Other than that, I would imagine the places I would go when we can travel freely again, and my audio ended up on [PodCacher show 719.0](https://www.podcacher.com/show-719-0-memory-lane-geocaching/) (timestamp 32:28).

![2020-08-27 me wearing UA SPORTSMASK](SPORTSMASK.jpg)

Looking at the numbers, my 2000 milestone is coming up.
I need to select a good cache for this milestone.
More importantly, it needs to have a unique icon, because I [made plans](https://coord.info/GLXT1FC8) to collect 8 different icons in my milestones.
In a normal year, it would be easy to take the Metrorail to Washington DC and log an EarthCache around Tidal Basin, but I don't feel safe doing so in a pandemic.
Then I noticed a [CITO event](https://coord.info/GC8YY9H): this could be the 8th icon I'm looking for.

Georick402 agreed to drive me there, with the condition that both of us must wear masks and the car window rolls down.
20 geocachers attended this event wearing masks, and picked up a good amount of trash from the trail.
I logged this CITO event as [my 2000th milestone](https://coord.info/GL12PM9F6).

![milestones in my Geocaching.com and Project-GC profiles](milestone.png)

## The Rental Car

In October, I realized that the last time I drove a car was 10 months ago [to Richmond](https://twitter.com/yoursunny/status/1203780376398831617?s=20).
Seeing the somewhat lowered COVID-19 case counts, I decided to rent a car and practice my driving skills.
Typically, I would pick up a rental car on Saturday and return it on Monday, and arrange one day for museums and the other day for geocaching.
With most of the museum either closed or enforcing strict visitation limits, I planned both days for geocaching.

![2020-10-11 CAT ROCK elevation 1562 feet](GC7XYZF.jpg)

I booked a car on the weekend of October 10-11, which happens to be [International EarthCache Day](https://www.geocaching.com/blog/2020/09/new-souvenir-international-earthcache-day-2020/).
Finding an EarthCache on this weekend would give me a souvenir in the Geocaching profile.
I climbed up to Cat Rock in [Cunningham Falls State Park](https://dnr.maryland.gov/publiclands/pages/western/cunningham.aspx), and found the [EarthCache](https://coord.info/GC7XYZF) at the top.
I logged this cache as my 2011th find, but it would appear as my 2000th milestone on [Project-GC](https://project-gc.com/ProfileStats/yoursunny) that does not count Adventure Labs.
Other than that, I performed some cache maintenance, and enjoyed driving on the relaxing rural roads.

## The D/T Average

I'm finding fewer geocaches this year, but the *quality* of the geocaches I do find are better than previous year.
After the October trip, I noticed that the average Difficulty and Terrain ratings of my finds this year were both over 2.000, which is the first time since I started in 2013.
To solidity the lead, I decided to take a rental car again on the weekend of November 07-08, with a focus on high Terrain caches.

I spent the Saturday driving to five [C&O Canal Locks in Northern Montgomery County](https://coord.info/GC8N5J7) to play an Adventure Lab.
On Sunday, I drove 2 hours to [Rocks State Park](https://dnr.maryland.gov/publiclands/Pages/central/Rocks/Falling-Branch.aspx) in Harford County.
I visited Maryland's second highest free-falling waterfall, [Kilgore Falls](https://www.onlyinyourstate.com/maryland/waterfall-hike-md/), and found 6 geocaches with a Terrain rating of 3.0 or higher.

![2020-11-08 me at Kilgore Falls](GC5HGAY.jpg)

After this trip, the average Difficulty and Terrain ratings of the year were 2.082 and 2.077.
As long as I don't find too many easy caches for the last few weeks, I would end the year with average D and T both above 2.000.
But there's still one imperfection: the numbers were so close, but they were different.
Can I make them the same?

My calculation suggests that I need to find one or more caches so that the sum of their Terrain points is 1 more than the sum of their Difficulty points.
The Geocaching app supports filtering geocaches by their Difficulty/Terrain ratings, so that I searched for a few combinations, and decided on finding [Georick's Geotrail Cache # 13](https://coord.info/GC5F4ZC) (D1.5 T2.5) as my last cache of the year.
On December 24, the day [Last 2020 souvenir](https://www.geocaching.com/blog/2020/11/our-sun-sets-to-rise-again-with-the-last-2020-and-first-2021-souvenirs/) opened, I walked two hours in the pouring rain (but warmer temperature), crawled in the mud, and found this cache.

![Project-GC "average difficulty per year" and "average terrain per year" charts](DT.png)

## The Conclusion

Today is December 31, the last day of the year.
I haven't seen Georick402 since September, but I heard his interview on [PodCacher show 735.0](https://www.podcacher.com/show-735-0-the-journey-of-becoming-a-geocacher/) (timestamp 12:17).

![Project-GC "finds by month" chart](2020.png)

This year I have 196 finds.
Despite the pandemic, I have five achievements in 2020:

* I finished a 90-day SLUMP goal.
* I collected the eighth unique icon on my milestones.
* For the first time since I started, the average Difficulty of the year is above 2.000.
* For the first time since I started, the average Terrain of the year is above 2.000.
* My average D and average T of 2020 have the same number **2.079**, as confirmed by my [Project-GC profile](https://web.archive.org/web/20201231202530if_/https://cdn2.project-gc.com/ProfileStatsImage/yoursunny).

The pandemic has not disappeared, but geocaching must carry on.
