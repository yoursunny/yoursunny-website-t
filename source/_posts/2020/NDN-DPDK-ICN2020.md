---
title: "NDN-DPDK: NDN Forwarding at 100 Gbps on Commodity Hardware"
lang: en
author: "Junxiao Shi, Davide Pesavento, Lotfi Benmohamed"
date: 2020-09-30
tags:
- NDN
- Linux
---

Presented at: [7th ACM Conference on Information-Centric Networking (ICN 2020)](https://conferences.sigcomm.org/acm-icn/2020/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/iOvmIQz4nvQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Since the Named Data Networking (NDN) data plane requires name-based lookup of potentially large tables using variable-length hierarchical names as well as per-packet state updates, achieving high-speed NDN forwarding remains a challenge.
In order to address this gap, we developed a high-performance NDN router capable of reaching forwarding rates higher than 100 Gbps while running on commodity hardware.
In this paper we present our design and discuss its tradeoffs.
We achieved this performance through several optimization techniques that include adopting better algorithms and efficient data structures, as well as making use of the parallelism offered by modern multi-core CPUs and multiple hardware queues with user-space drivers for kernel bypass.
Our open-source forwarder is the first software implementation of NDN to exceed 100 Gbps throughput while supporting the full protocol semantics.
We also present the results of extensive benchmarking carried out to assess a number of performance dimensions and to diagnose the current bottlenecks in the packet processing pipeline for future scalability enhancements.
Finally, we identify future work which includes hardware-assisted ingress traffic dispatching, dynamic load balancing across forwarding threads, and novel caching solutions to accommodate on-disk content stores.

Read full paper at ACM Digital Library: [NDN-DPDK: NDN Forwarding at 100 Gbps on Commodity Hardware](https://dl.acm.org/doi/10.1145/3405656.3418715)

![NDN-DPDK logo](https://fastly.jsdelivr.net/gh/usnistgov/ndn-dpdk@1586ca4294ccb7e1d24844fce7bf6c1856009eb6/docs/NDN-DPDK-logo.svg)

NDN-DPDK software available on GitHub: [NDN-DPDK repository](https://github.com/usnistgov/ndn-dpdk)
