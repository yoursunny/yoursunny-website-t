---
title: I Earned $100 with One Tweet
lang: en
date: 2020-08-10
tags:
- commentary
---

Apple Card is a new credit card product offered by Apple.
In February, Apple Card ran an ad campaign on Twitter:

> Apple Card has no fees.
>
> Not Apple Card has every possible fee.
>
> So if your credit card is not Apple Card, maybe it should be.

Upon seeing this advertisement, I thought: Apple Card is not the only credit card that has no fees; my oldest credit card, **Discover Card**, also does not charge fees.
Moreover, Discover Card has higher rewards in certain categories than Apple Card.
Thus, I [tweeted a reply](https://twitter.com/yoursunny/status/1230075837682155522) under their ad, mimicking their grammar:

![Apple Card has no fees and measly 2% rewards unless you buy from Apple. Discover Card has no fees and up to 5% rewards in rotating category, which is grocery stores this quarter. So if your credit card is not Discover Card, maybe it should be.](tweet.png)

Twitterverse thinks I'm [hilarious](https://twitter.com/corykeane/status/1230345393717882882).
They liked and retweeted my reply, so that it quickly became the top reply under the Apple Card ad, and everyone who expands the ad would see it.

![Tweet Analytics: 17497 impressions, 942 total engagements](analytics.png)

In the next few weeks, two people used my [referral link](https://bit.ly/itrefer) to apply for a Discover Card and got approved.

!["$50 refer a friend credit" line item in credit card statement](credit.png)

For each successful referral, I can earn $50 in rebates.
And that's how I earned $100 with one tweet.

P.S.
If you want a Discover Card, use my [referral link](https://bit.ly/itrefer).
