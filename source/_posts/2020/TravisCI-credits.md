---
title: Travis CI Pricing Change - Usage based Billing
lang: en
date: 2020-11-03
tags:
- commentary
- git
discuss:
- https://dev.to/yoursunny/travis-ci-pricing-change-usage-based-billing-5gba
---

## "Welcome to Travis CI!"

This morning I waked up to an email titled *Welcome to Travis CI!*.

[Travis CI](https://travis-ci.com/) is a cloud-based Continuous Integration platform that automatically compiles and tests my code whenever I push a commit to GitHub, and warns me by email if there is an error.
I started using Travis CI in 2014, so it feels weird to receive a "welcome" email today.

The email reads:

> You are on Free.
>
> You have 1,000 credits left - these will begin counting down automatically as soon as you run your first build.
> You can use your credits to build on both private and open-source repositories using Linux, macOS, and Windows OS.
> 1,000 credits will be replenished automatically monthly. Additional Credits purchase is not available for Free Plan.

## 10,000 Credits

Logging into my account, I see a red warning message at the top:

> Builds have been temporarily disabled for public repositories due to a negative credit balance.
  Please go to the Plan page to replenish your credit balance or alter your Consume paid credits for OSS setting.

On the *Plan* page, it indicates that I am on the Free Plan with 10,000 credits, replenished monthly:

![Travis CI Plan page](plan.png)

The *Plan Select* page describes the Free Plan differently:

![Travis CI Plan Select page](select.png)

There are a lot of inconsistencies:

information source | initial credit | monthly credit
-------------------|----------------|---------------
email              | 1000           | 1000
*Plan* page        | 10000          | 10000
*Plan Select* page | 10000          | 0

On Travis CI blog, an article [The new pricing model for travis-ci.com](https://blog.travis-ci.com/2020-11-02-travis-ci-new-billing) says:

> For those of you who have been building on public repositories (on travis-ci.com, with no paid subscription), we will upgrade you to our trial (free) plan with a 10K credit allotment (which allows around 1000 minutes in a Linux environment).

I guess there are two different free plans, and I'm on the latter one:

* "Trial" Free Plan: one-time 10K credits for new users.
* "Open Source" Free Plan: monthly 10K credits for existing users who have been building on public repos.

## Are They Enough?

My biggest question is, are 10K credits enough for my projects?
To answer this question, I looked into my recent usage:

repository            | builds in 3 months | minutes per build | minutes per month
----------------------|--------------------|-------------------|------------------
NDNts                 | 54                 | 18                | 324
NDNph                 | 3                  | 9                 | 9
DefinitelyTyped       | 5                  | 3                 | 5
ndn-python-repo       | 3                  | 58 + 16 on macOS  | 58 + 16 on macOS
ndn-cxx-breaks        | 3                  | 123               | 123
NDNts-NFD-status-page | 9                  | 2                 | 6

At the rate of [10 credits/min on Linux and 50 credits/min on macOS](https://docs.travis-ci.com/user/billing-overview/#usage---credits), these builds would consume 6,050 credits monthly.
I'm not in trouble yet.

## The Way(s) Out

GitHub Actions, backed by software giant Microsoft Corporation, has no usage limits for open source projects.
I migrated [NDN-DPDK](/t/2020/NDN-DPDK-ICN2020/) and [NFD nightly](/t/2020/NFD-nightly/) to GitHub Actions.
It seems that I should move more of my own projects over there.

[ndn-cxx-breaks](https://ndn-cxx-breaks.ndn.today/) is now suspended.
I can't have visitors click the button a few times and use up all my credits.
It will come back when I have it moved to GitHub or another service.

![GitHub Apps - repository access](access.png)

For the projects I fork and contribute to, such as ndn-python-repo, it is unnecessary to trigger Travis CI builds before I send a pull request.
To achieve that, I can go to [Installed GitHub Apps](https://github.com/settings/installations) page, and change *repository access* to only allow the repositories I want to build.

The end is nigh.
