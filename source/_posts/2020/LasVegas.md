---
title: Wandering in Las Vegas
lang: en
date: 2020-10-31
tags:
- travel
- geocaching
image: Circus.jpg
---

After being busy with virtual conferences and hackathons, I finally got some time to return to my virtual travels.
This week I'm going to Las Vegas, the entertainment capital of the world.

![Welcome to Fabulous Las Vegas sign, 2013-12-17](Fabulous.jpg)

## Am I in Las Vegas?

Christmas 2011, after visiting [San Diego](/t/2020/SanDiego/) and [Los Angeles theme parks](/t/2020/LosAngeles/), two schoolmates and I drove 6 hours to Las Vegas.
We walked around the vicinity of our hotel, and saw what appears to be *Statue of Liberty* in front of a hotel.
From what I remember from watching [The Amazing Race](https://amzn.to/31EDvra), this statue should be in New York.
Although my geographic knowledge was limited at the time, I knew we were not in New York.
Instead, we were in front of New York-New York Hotel and Casino, designed to evoke the New York City skyline.
This *Statue of Liberty* is a smaller replica of the real thing.

![Statue of Liberty replica in front of New York-New York Hotel and Casino, 2011-12-26](Liberty.jpg)

A few years later, I found out that we were not in Las Vegas, either.
Las Vegas is an incorporated city in the center of Clark County, Nevada.
New York-New York Hotel and Casino is situated along South Las Vegas Boulevard, to the south of the City of Las Vegas.
It is out of Las Vegas city limits, and belongs to the unincorporated town of Paradise, Nevada.
Nevertheless, we did enter the City of Las Vegas that evening, and contributed a few dollars to the slot machines.

## The Slot Lesson

Speaking of the slot machines, my second trip started with them.
It was December 2013, the week before Christmas.
With classes coming to an end and the professor going out of town, I wanted a get-away from the university campus.
Being a poor grad student, I selected Las Vegas because the hotel rooms are dirt cheap there.

As soon as I step off the flight in McCarran Airport, I was greeted by several rows of slot machines.
I skipped all of them, found the exit, skipped signs pointing to limos and taxis, found the city bus terminal, and took a bus to [Circus Circus Hotel & Casino](https://www.circuscircus.com/).
I booked 2 nights at this hotel, with a total price of only $49.18 - an unimaginable price in any other major city.

According to Circus Circus website, they are offering a free "slot lesson" at 14:00 that day.
I entered the casino on time, but couldn't find a classroom.
I asked a casino employee about the slot lesson; after verifying that I'm of legal gambling age, he agreed to teach me.
The first question was, "what do you want to play?", but I didn't understand this question.
He then explained "we have penny machines, quarter machines, dollar machines, etc", and I selected "5 cents at a time".

He took me to a nickel machine, instructed me to insert a dollar bill into the machine, and showed me the buttons.
I pressed the **BET** button, and the fruits started rolling.
A few seconds later, the machine lighted up and played music.
He said "congratulations, you won seven dollars".
I thought it was a special effect as part of the "slot lesson", but he reassured me that the winnings were real, and it had nothing to do with him standing beside me.

He then showed me the button to withdraw the winnings.
I pressed **CASH OUT** button, and a piece of paper came out of the machine.
However, it wasn't a stack of dollar bills, but a "cashout voucher".
The instructor explained that I can either use this voucher on another slot machine, or redeem it for cash at an ATM.

![Circus Circus cashout voucher, 2013-12-17](cashout.jpg)

Given that I have $7.95 now, my instructor asked whether I want to try a dollar machine.
I agreed to this plan and inserted my voucher.
The instructor pointed to the voucher printer, and I saw a voucher coming out of the machine, despite that I did not press the **CASH OUT** button.
Then, he said: a dollar machine cannot keep track of smaller denominations, so that it would reject the dimes and pennies onto a cashout voucher, but keep the dollars in the machine; sometimes people would grab the voucher and forget about the dollars, and leave without playing.
This was new information to me.

Enough of a lesson, so I thanked my instructor, and he wished me good luck.
I continued playing for a while, and enjoyed the graphical designs and music on both modern and classic-looking slot machines.

## Vegas Shows

Being the entertainment capital of the world, there are more forms of entertainment in Las Vegas Valley than the slot machines.

I went to [Vegas! The Show](https://vegastheshow.com/), a theatrical production that tells the story about Las Vegas through music and dance, performed nightly at Saxe Theater in Planet Hollywood.
I couldn't understand the storyline, but [a few acts were entertaining](https://twitter.com/yoursunny/status/412439412816228352), and the music was very loud.

![Rainstorm at Miracle Mile Shops, 2013-12-17](MiracleMile.jpg)

After the show, I went window shopping at [Miracle Mile Shops](https://www.miraclemileshopslv.com/), just outside of the theater.
It is an indoor shopping mall with a [Los Angeles](/t/2020/LosAngeles/) theme.
Suddenly, I heard thunder, the sky became dark, and it started to rain.
The water wasn't enough for [#KeepCalmAndDrenched](https://twitter.com/hashtag/KeepCalmAndGetDrenched), but the sound effect of this [rainstorm show](https://www.visitlasvegas.com/listing/rainstorm-%26-fountain-at-miracle-mile-shops/35573/) was realistic.

<iframe width="560" height="315" src="https://www.youtube.com/embed/xzW9HUbjffY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

A larger water feature is [Fountains of Bellagio](https://bellagio.mgmresorts.com/en/entertainment/fountains-of-bellagio.html), which has been the largest musical fountain in the world until 2010.
1200 nozzles and 4500 lights created a vast choreographed performance, synchronized with music played through loud speakers.
When I visited, music of the day was [The Most Wonderful Time of the Year](https://amzn.to/31STJwH).
To this date, I would think about the fountains whenever I hear this song.

<iframe width="560" height="315" src="https://www.youtube.com/embed/vh5R2F1hFL0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Not far from Bellagio is [Mirage Volcano](https://mirage.mgmresorts.com/en/amenities/volcano.html).
Instead of water, it uses fireballs risen from a volcano-shaped rockery, complete with orange lights and a hissing soundtrack.

![courtroom in Mob Museum, 2013-12-18](courtroom.jpg)

I ended my second trip at downtown Las Vegas, before heading to the Greyhound station.
I visited the [Mob Museum](https://themobmuseum.org/) that tells the story about organized crime.
After the museum closed for the day, I spent the last two hours at [Fremont Street Experience](https://vegasexperience.com/).
It's like [Tucson's 2nd Saturdays](/t/2017/Tucson-festivals/), except that it occurs every night.
There are three concert stages, two ziplines, and a giant canopy with LED screens.

![Fremont Street Experience, 2013-12-18](Fremont.jpg)

## Snowflakes of Cash

Las Vegas is a wonderful city, but I wasn't planning for a third trip, because there are other places worth visiting.
Then, in January 2014, I received an unexpected email:

> Congratulations!
> As one of the ten (10) additional tickets selected to replace any unresponsive winners in The Snowflakes of Cash Drawing on December 30, 2013, you have won a 3 night Complimentary stay and $1000.
> All complimentary stays must be redeemed prior to March 31, 2014 or are otherwise null and void.
> Your $1000 will be paid during the 3 night complimentary stay.
>
> Slot Marketing Supervisor, Circus Circus Hotel and Casino

I vaguely recalled that, I signed up for a player card at the hotel during my previous visit in order to get a discount breakfast, and with the player card I received one raffle ticket that I filled out and put into a dropbox.
It was totally unexpected that I won a prize with this one raffle ticket.

![Circus Circus Hotel and Casino, 2014-03-19](Circus.jpg)

After a few rounds of emails and phone calls, I confirmed that it wasn't a scam, and booked the trip during the university Spring Break in March 2014.
Upon arrival at the hotel, I received one of the better rooms that contains a king-sized bed and a flatscreen television.
Then, I was escorted to the "VIP service" room of the casino.
Upon hearing that I won the prize with only one raffle ticket, the manager told me that I was "incredibly lucky" because many people did not win even if they deposited hundreds of tickets after spending many hours playing slots.

The prize was paid in seven $100 bills, with the rest being held for tax.
I was freaking out when I was handed the cash, because this was only the second time I handled so much money (the first time being when I [initially arrived in Tucson](/t/2017/first-days-in-Tucson/)).
I immediately found [a taxi](/t/2017/hailing-taxis/), paid $5 of tips to the cabbie for a safe transport to the bank, and deposited the money into an ATM.

After that, I walked around, watched the Mirage Volcano again, and stuffed myself with crab legs at one of the [expensive seafood buffets](https://mirage.mgmresorts.com/en/restaurants/cravings.html).

## Highest Thrill Ride and Geocache

I [started geocaching](/t/2017/start-geocaching/) shortly before my second trip.
I found a total of 8 geocaches in Nevada.
Some were parking garage caches and lamp post caches, but the most memorable was the Virtual cache [Watch that first step...](https://coord.info/GC68E7).
It is located at the top of Stratosphere, the tallest tower west of the Mississippi River.

![Stratosphere, 2014-03-21](Strat.jpg)

There are four amusement rides on top of the tower, which are some of the world's highest.
While not a requirement for the geocache, my ticket included a ride of my choice.
I looked around:

* **SkyJump** is a controlled bungee-jumping that plummets 261 meters in a few seconds.
  Hell no!
* **Insanity** is a device hanging over the edge and spinning for 2 minutes.
  I'm not afraid of heights, but my fragile brain cannot take the rotational force.
* **Big Shot** is a rapid ascent up into the sky.
  It's the highest ride, but the least scary.
  However, this ride was nonoperational during my visit.
* **X-Scream** is an 8-seat roller coaster on a straight 21-meter track.
  It also goes over the edge, but it does not spin and is relatively quick.
  Thus, I chose to ride X-Scream.

![X-Scream Ride, 2014-03-21](X-Scream.jpg)

Seated, strapped in, and the ride started.
I was propelled forward, downward, backward, and upward.
I could hear the wind and feel my feet hanging in the air high above the ground.
However, I wasn't too scared because I carefully observed the ride beforehand and knew what would happen.

## Afterword

I calmly got off X-Scream without screaming, and my pants remained dry.
Using the small fortune I made from that raffle ticket, I returned to Tucson by flight instead of an overnight Greyhound bus.
At the airport, I played slots for the last time, and won $4; this was also the only slot winning during this trip, after at least $26 of losses earlier.

During my two solo trips, I joined five sightseeing groups with travel agencies, which I will write about in future articles.
The sightseeing tours include:

* [Grand Canyon West](https://web.archive.org/web/20140102235119/http://www.grandcanyontourcompany.com/buswrhelo.html)
* [Hoover Dam](https://web.archive.org/web/20131211080030/http://www.grandcanyontourcompany.com/hooverdlx.htm)
* [Area 51](https://web.archive.org/web/20140823014041/http://www.adventurephototours.com/area51.php)
* [Bryce Canyon and Zion Canyon](https://web.archive.org/web/20140301155016/http://www.adventurephototours.com/brycezion.htm)
* [Red Rock Canyon](https://web.archive.org/web/20140223014127/http://pinkjeeptours.com/las-vegas/tour/red-rock-national-conservation-area)

The $1000 prize money was considered "gambling winnings" and taxed at 30%.
The remaining $700 was spent in tours, buffets, and transportation.
Moreover, an error in tax filing of this income delayed my 2014 federal tax return for almost two years.
A tour guide once said: if you leave Vegas with one more dollar in your pocket, you are a winner.
For me, this did not happen.

After these three trips, I never returned to Las Vegas, and never played on another slot machine.

At UA Spring Fling 2016, I [played on](https://twitter.com/yoursunny/status/718936120155840517) the RCSFUN G-Force ride.
It has a rotational motion similar to Insanity on top of The Strat, but without the tower's height.
I kept my eyes open, but my face turned pale after the ride.
