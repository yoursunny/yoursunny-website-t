---
title: Wandering in Los Angeles
lang: en
date: 2020-08-09
tags:
- travel
- geocaching
image: Disneyland.jpg
---

This week, I virtually travel to Los Angeles, California, a city that I visited many times.

## The Airport

[Los Angeles International Airport (LAX)](https://www.flylax.com/) is one of the [busiest airports in the United States](https://en.wikipedia.org/wiki/List_of_the_busiest_airports_in_the_United_States).
At a distance of 90 minutes flight time, LAX is the nearest major international airport from Tucson [where I attended grad school](/t/2017/six-years-in-arizona/).
Therefore, I often find myself transiting in LAX.

![LAX sign, 2019-03-06](LAX.jpg)

Since I [entered United States for the first time](/t/2017/first-days-in-Tucson/) nine year ago, I transited through LAX 10 times, including 8 flights to/from China and 2 flights to/from [Honolulu, Hawaii](/t/2017/academic-papers/).
Occasionally I have a long connection time. During that time,

* I can sit at the baggage claim, work on my laptop, and watch passengers coming off the next few flights eagerly waiting for their luggage.
* I can take the free "A bus" that loops around LAX's eight terminals, twice.
* I can stand next to the window and watch a swarm of more than 20 vehicles servicing a large international flight.

![vehicles servicing a Japan Airlines airliner, 2013-05-16](JAL.jpg)

## Theme Parks

My first *proper* trip to Los Angeles was 2011 Christmas.
It was on the same road trip, right after we visited [San Diego](/t/2020/SanDiego/).
We could not afford a hotel, so we booked three nights in Hollywood Youth Hostel, for $19 per bed per night.
The hostel has a prime location: across the street is the famous Kodak Theatre, home of the Academy Awards ceremony.
We also saw the stars on the Hollywood Walk of Fame and the handprints in front of Grauman's Chinese Theatre.

The main destination of this trip, however, was the two theme parks in Los Angeles area: Universal Studios and Disneyland.

### Universal Studios Hollywood

[Universal Studios Hollywood](https://www.universalstudioshollywood.com/) is located in Universal City, California, 5 KM north of Kodak Theatre.
The theme park features several rollercoaster rides related to cartoon films produced by Universal, but I have not watched these cartoons so that I do not have a feeling.
I found the educational programs more interesting, because I can learn a bit on how films are made.

During the Studio Tour, I learned that film production is all about what the camera sees.
The buildings seen in a film probably have the front side, while the other side is styled as a completely different building.
Likewise, many vehicles have only one side decorated, because the camera would never see the other side.

![building facades seen at Universal Studios, 2011-12-24](StudioTour.jpg)

The most memorable part was the Special Effects Show.
Near the end of that show, a few audience members were recruited onto the stage to participate in a demo.
One guy was asked go behind a curtain and climb a ladder, with his shadow projected onto the curtain.
Suddenly, with dramatic music, it was seen that the ladder has collapsed, and the volunteer has fallen onto the ground, making a loud noise.
The host said to his wife: I'm sorry for your loss, we'll make up by giving you a few drink coupons.
A few moments later, the husband walked out of the curtain, unharmed.
It turns out that the collapsing ladder was an illusion, because anything is possible in a film, and what you see may or may not be the reality.

![six audience members standing in front of a green curtain, 2011-12-24](SpecialEffects.jpg)

### Disneyland

The other theme park in Los Angeles area is [Disneyland](https://disneyland.disney.go.com/).
It is located in Anaheim, California, a 30-minute drive from Hollywood.
We went there on Christmas Day, together with all the kids that did not end up on the naughty list, along with their parents.

![Disneyland let the memories begin sign, 2011-12-25](Disneyland.jpg)

Disneyland is a little magic kingdom.
It has its own city hall, fire department, market house, and railroads.
Musical parades, led by the Mickey Mouse, happen from time to time.

![musical parade in Disneyland, 2011-12-25](parade.jpg)

There are so many rides, with long lines that would require hours of waiting.
But we soon figured out the trick: the Single Rider Pass, which entitles us to skip most of the line and go straight to the ride platform.
Whenever there's an empty seat after arranging the families, the conductor would ask a single rider to fill that seat.
We had to ride separately from each other and cannot choose a "better" seat, but it saved a lot of time.

![me at Space Mountain, 2011-12-25](SpaceMountain.jpg)
![Disneyland single rider pass, 2011-12-25](SingleRider.jpg)

The theme of these rides was about various Disney cartoons.
Once again, I have not watched the cartoons, and could not recognize any Disney character other than the Mickey Mouse.
Thus, I was more interested in the mechanical devices that control the rides, than the experiences on the rides.

![ride control panel, 2011-12-25](panel.jpg)

## Chinatown and Downtown

My second trip to Los Angeles was in June 2012.
I brought [my roommate](/t/2017/roommates-or-not/) John on an overnight [Amtrak train](/t/2017/cross-country-move/) from Tucson to Los Angeles Union Station, then [took a taxi](/t/2017/hailing-taxis/) to have breakfast in Chinatown.
Los Angeles Chinatown has many restaurants serving authentic Chinese food.
Both of us are from China, and we were excited to have a taste from our home country.

![colorful buildings in Los Angeles Chinatown, 2012-06-01](Chinatown.jpg)

After breakfast, we went on a self-guided tour of various historic buildings in Chinatown.
Most of them are churches or other religious gathering places, some having beautiful murals.
Then, we walked south into the downtown area, and took a guided tour of Cathedral of Our Lady of the Angels.

![Cathedral of Our Lady of the Angels, 2012-06-01](Cathedral.jpg)

There are even more historic buildings in Los Angeles downtown.
Following a travel guide that I downloaded, we saw the fine crafted staircases in the Bradbury Building, the shoebox exterior design of the Walt Disney Concert Hall, and the 77-year-old *Los Angeles Times* building.

![Bradbury Building, 2012-06-01](Bradbury.jpg)

John was especially excited to see the STAPLES Center, home of the Los Angeles Lakers, his favorite basketball team.
While I only played [a little basketball](/t/2005/gzjs/) myself, I certainly have heard about Kobe Bryant, one of the best basketball players.
I'm sure that John would love to see Kobe playing, but there wasn't a game that night, so he had to settle with a few photos with the sculptures.

![STAPLES Center, 2012-06-01](STAPLES.jpg)

## The Beach

Both Los Angeles and San Diego are coastal cities on the eastern shore of the Pacific Ocean.
I wasn't able to find a sandy beach in San Diego, but I found one here in Los Angeles County: Santa Monica State Beach.

![me doing pull-ups on gymnastics rings, 2012-06-05](pullup-rings.jpg)
![me doing pull-ups on a chin-up bar, 2012-06-05](pullup-bar.jpg)
![me doing pull-ups on a rope, 2012-06-05](pullup-rope.jpg)

My first visit to the beach was in 2012 with John, after a one-hour bus ride from our motel in the downtown area.
We did not have [Crocs](https://amzn.to/33EnCCV) or swimsuit, so we could not go swimming in the ocean.
However, we played a long time on the Muscle Beach, and I dipped my toe in the ocean.
Polyester pants will be dry after 10 minutes in the sun.

![writing "yoursunny.com" on the beach, 2014-01-13](writing.jpg)

In January 2014, I came to Santa Monica beach again.
This time, I wrote my name on the beach, and rode the rollercoaster in the Pacific Park.
[A tsunami](https://pacpark.com/foxs-9-1-1-drama-series-premieres-with-a-tsunami-hitting-the-santa-monica-pier/) did not occur during my ride.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XESth1AhiXE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Around UCLA

I had three meeting trips in University of California, Los Angeles.
Unlike [my San Diego trips](/t/2020/SanDiego/), I did not add extra vacation days to any of these trips.
The main reason is that, my 2011 and 2012 trips gave me a negative impression on the traffic and bus system in Los Angeles.

The first meeting was in January 2014.
I had a morning flight and an afternoon of spare time.
After the rollercoaster ride, I took a bus along the Pacific Coast Highway to the [Getty Villa](https://www.getty.edu/), a fine arts museum.
I do not understand arts, but I relaxed along the long pool in the Outer Peristyle garden.

![Getty Villa, 2014-01-13](GettyVilla.jpg)

I chose a late flight for the second meeting in September 2015.
Not wanting to deal with the bus, I took a shared van straight to the hotel.
On meeting days, I still waked up early to walk around, find geocaches, and burn off the calories from the delicious catered foods.

![Westwood skyline, 2015-09-28](Westwood.jpg)

For my third trip in March 2019, I'm departing from Washington, DC instead of Tucson, Arizona, which is a much longer flight.
By this time, my bad memories about the traffic have faded, replaced by my desire to find more geocaches.
I managed to book an early flight arriving LAX around noon, and made plans to visit the main street of El Segundo, a small city just south of the airport.
However, [Alaska Airlines messed up my flight](https://twitter.com/yoursunny/status/1103305558696841216), and I did not arrive at LAX until after 21:00.
Consequently, I could not follow through my El Segundo plans on that day, although the compensation of that 9-hour flight delay led to my [7-day Oregon vacation](/t/2019/Oregon-vacation-1/).

I still found a few geocaches during the last two trips, including two caches "four years in the making".
One of them is [Baking Soda](https://coord.info/GCHZZ2), a virtual cache located in Pierce Brothers Westwood Village Memorial Park & Mortuary.
My first attempt on this cache was in 2015, but the park was closed.
In 2019, I forgot the 2015 experience, and again encountered a locked gate at 07:30.
However, I learned that it opens daily at 08:00, so that I managed to enter the park and find the geocache on the last day when the meeting start time was moved to 09:00.

![closed gate of Pierce Brothers Westwood Village Memorial Park & Mortuary, 2019-03-07](gate.jpg)
![inside Pierce Brothers Westwood Village Memorial Park & Mortuary, 2019-03-09](cemetary.jpg)

The other is [Water you looking at?](https://coord.info/GC2FT2V).
It is a simple nano hidden on a water valve, and the find was uneventful.
What's interesting is that, I found the "same" cache twice.
After my 2014 find, the original cache was archived, and a new CO resurrected this cache with [a reloaded edition](https://coord.info/GC6PK0H) in 2016.
When I returned to UCLA in 2019, I once again found and logged this cache.

## Final Words

Los Angeles, California is a city that I have mixed feeling about.
I don't like Los Angeles because of the heavy traffic, although I had to go there many times.
On the other hand, the weather and the sunset view are as good as San Diego, and there's a beautiful beach in Santa Monica.
Finally, don't forget California's favorite: In-N-Out burger (pro tip: ask for double meat and double veggies).

![In-N-Out Burger, 2019-03-06](InNOut.jpg)
![sunset at LAX, 2019-03-10](sunset.jpg)
