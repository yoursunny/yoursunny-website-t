---
title: Geo-snatching
lang: en
author: "You Might Be A Geocacher If......"
date: 2020-01-13
tags:
- geocaching
---

*Geo-snatching* is the act of finding loopholes instead of geocaches.

This includes:

* armchair geocaching;
* hacking your smartphone's location so you can log Adventure Labs or solve Whereigo cartridges without visiting the location;
* logging an FTF on a friend's cache that you helped place;
* finding your own caches by adopting it to a sock puppet account (aka [rule 52612](https://www.facebook.com/groups/183002578433642/permalink/2745927168807824/?comment_id=2747544025312805&reply_comment_id=2747619801971894));
* other general tomfoolery.

The term geo-snatching was invented by [gsmX2](https://www.youtube.com/channel/UC-QAD0G35uYFKedu1zorcMg) on [this Facebook post](https://www.facebook.com/groups/183002578433642/permalink/2302125813187964/).
This article explains what geo-snatching or geosnatching means in geocaching, and how a geo-snatcher or geosnatcher differs from an honest geocacher.
Content of this article does not necessarily reflect blog owner's views.
