---
title: NDNts Demo at NDN Community Meeting 2020
lang: en
date: 2020-09-10
tags:
- NDNts
- NDN
---

[NDN Community Meeting](https://www.nist.gov/news-events/events/2020/09/ndn-community-meeting) is an annual event that brings together a large community of researchers from academia, industry, and government, as well as users and other parties interested in the development of [Named Data Networking (NDN)](https://named-data.net) technology.
Having no [peer review process](https://www.reddit.com/r/atheism/comments/5pejl7/protest_sign_today_what_do_we_want_evidence_based/), it is a prime opportunity to showcase my personal projects to the community.
I [demo'ed](https://twitter.com/yoursunny/status/1042550814466826240) my [ndn-js home surveillance camera](/t/2017/how-homecam-works/) at NDNcomm 2018.
This time, I decide to demo my flagship product, [NDNts: NDN Libraries for the Modern Web](/p/NDNts/).

## The Demo Video

NDNts is [a set of libraries](/t/2019/NDNts-intro/) with many different features, where do I start?
I decide to select a subset of *unique* features that are not found in any other library:

* The **Endpoint** API that enhances *face* by automatically handling repetitive tasks such as Interest retransmissions and packet signing/verification, so that app developers can focus on the application logic.
* An implementation of **trust schemas**.
* NDN Certificate Management protocol implementation, including a graphical user interface for the certificate authority component.

I also threw in two web applications:

* A webpage to display [NLSR status](https://nlsr-status.ndn.today/) on the NDN testbed.
* The new [HomeCam](https://twitter.com/yoursunny/status/1279885701409116165) built with NDNts.

<iframe width="560" height="315" src="https://www.youtube.com/embed/4RiD0JpZa50" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I made PowerPoint slides and figured out how to generate a presentation video.
However, not many people had time to view the video, because they only received the link (to an NDN-based video streaming service) a few hours before the conference.

## The Online Session

This year's NDNcomm is held virtually using the Blue Jeans Meetings app.
Six poster/demo presenters were each allocated a separate room in the app.
Right before the 1-hour poster/demo session, we also had the opportunity to each give a 3-minute pitch with a few slides.
I think I have the most number of slides among all the pitches, but I practiced enough so that it has the correct duration.

At exactly 16:00 EDT, I opened my demo room, ready for visitations.
I got 2 attendees right away, and more people joined later.

Some of the discussions are summarized below.

## Naming a Browser

Prof [Alex Afanasyev](https://users.cs.fiu.edu/~afanasyev/) used to say:

> Name is the secret sauce of NDN.

When I demo'ed the HomeCam app, Prof [Lixia Zhang](https://web.cs.ucla.edu/~lixia/) asked how the browser is named.
My answer is: the browser gets a (supposedly unique) random identifier.
This isn't a satisfying answer, and I'm told that developers ran into a similar problem in naming smart home gadgets.

As a web application, I could let the user to authenticate via Twitter, GitHub, or similar services, and then acquire an NDN name prefix related to their identity.
However, naming an anonymous user is still an open question.

## Crypto in the Browser

ndn-js, the original NDN libraries for browsers, implements crypto operations in JavaScript using [CryptoJS](https://code.google.com/archive/p/crypto-js/), which was the only choice before the adoption of [Web Crypto API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API) in browsers.
Web Crypto was later added to ndn-js as an option, but the CryptoJS implementation remains due to their backwards compatibility guarantees.

NDNts `@ndn/keychain` package uses Web Crypto API for crypto operations, because NDNts only targets the latest platforms.
In Node.js, [`@peculiar/webcrypto`](https://www.npmjs.com/package/@peculiar/webcrypto) implements Web Crypto API by wrapping the native `crypto` library, so that I can still use the same code to do crypto.
However, when it comes to the PKCS#8 encryption in the [SafeBag](https://docs.named-data.net/ndn-cxx/0.7.0/specs/safe-bag.html) object, I find it difficult to do in Web Crypto, and had to invoke Node `crypto` library directly, which means this feature would not work in the browser.

[Jeff Thompson](https://github.com/jefft0), the main developer of ndn-js, shared some insights on his experience developing crypto features in the browser: unsurprisingly, he implemented PKCS#8 encryption in JavaScript.
[Zhiyi Zhang](https://zhiyi-zhang.com/), a PhD candidate specializing in security and privacy, [made a crypto library in WebAssembly](https://github.com/Zhiyi-Zhang/PS-Signature-and-EL-PASSO) so that it can work in browsers.

We all know that [JavaScript cryptography considered harmful](https://web.archive.org/web/20111127053119/http://www.matasano.com/articles/javascript-cryptography/), although the widespread deployment of HTTPS has improved the situation of secure code delivery.
But I have to ask about the risk of timing attacks.
Jeff's reaction was: timing attacks are overrated!

While theoretically possible, a timing attack is not the top vulnerability of a web application, because there are bigger fishes out there.
The bigger fish, in Jeff's opinion, is the lack of secure memory: if JavaScript code ever handles crypto key material, there is no effective method to [cleanse](https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-rtlsecurezeromemory) memory.
Filling an `ArrayBuffer` with zeros is not guaranteed to happen, and the browser would not zeroize memory after garbage collection.

NDNts normally uses Web Crypto with non-extractable keys.
As long as the browser cleanses crypto memory correctly, I'm safe.
However, I cannot say the same for `@peculiar/webcrypto`, which internally [stores the key in a `Buffer`](https://github.com/PeculiarVentures/webcrypto/blob/47976a9b35fcdbb42e045cb3ba5b9626bed620a5/src/keys/key.ts#L5).
As for WebAssembly based crypto, it *might* have better control over timing attacks, but its memory is also exposed because `WebAssembly.Memory` is just an `ArrayBuffer`.

[Davide Pesavento](https://twitter.com/DavidePesa), who has been studying security protocols, pointed out that timing attacks should not be ignored, at least in system designs.
If you design something that will always have timing attacks, it's bad.

## Trust Schema + Common Name Library

[Kathie Nichols](https://pollere.net/) introduced the idea of *Versatile Security Toolkit* (VerSec) in NDNcomm 2019 and I liked it a lot, so that I further developed the idea and made it into NDNts `@ndn/trust-schema` package.
However, the current implementation lacked a key part: I do not have a compiler to turn the textual trust schema rules into (binary) packets that can be signed, retrieved, and verified.

A lesser known part of VerSec idea is:

> use the schema at run-time (`schemer.hpp`) to access Name components so that changes in Names don't require changes in application code

My understanding is that, the trust schema could provide patterns to construct names, and the application just needs to fill in each variable with some name components.
It's like constructing a string with a JavaScript template.
If I ever want to change the URI structure of my blog (bad idea), I only need to change that JavaScript template (in reality, the Hexo configuration).

```js
const consts = {
  domain: "yoursunny.com",
  directory: "/t",
};

function getBlogPostUri(args) {
  const { domain, directory, date, slug } = { ...consts, ...args };
  return `https://${domain}${directory}/${date.getFullYear()}/${slug}/`;
}

console.log(getBlogPostUri({
  date: new Date(2020, 9, 10),
  slug: "NDNts-NDNcomm2020",
  title: "NDNts Demo at NDN Community Meeting 2020",
}));
```

This led to my **schematized everything** idea.
The "trust" schema can be used to determine signing keys and construct names, why not task it to do more?
For example,

* If a packet name matches pattern X, it should be encrypted with RSA-OAEP and the key name can be constructed with pattern Y.
* If an object name matches pattern X, it should be compressed with gzip then versioned and segmented, and a FLIC manifest should be generated at a name constructed with pattern Y.
* If a stored packet name matches pattern X, it should be replicated across three servers.

Lixia brought up the [Common Name Library (CNL)](https://named-data.net/publications/ndncnl/) they are trying to resurrect.
CNL provides an API like this:

```py
image = Namespace("/foo/someimage/42")
image.setFace(face)
image.setDecryptor(decryptor)

def onSegmentedObject(handler, obj):
  print("Got image")

SegmentedObjectHandler(image, onSegmentedObject).objectNeeded()
```

Lixia thinks schema and CNL should be complementary, but the schema should be limited to handle security only.
My opinion is that, a more generalized schema would make the combination even more powerful.
While CNL supports serialization, signing, and encryption features, these settings must be specified in code.
Having a generalized schema, these settings can be specified in the schema instead, so that the retrieval code is a lot simpler:

```js
const image = await retrieve({
  schema,
  name: new Name("/foo/someimage/42"),
});
```

## Sync or PubSub

One feature that NDNts does not have is a proper data synchronization or pub-sub mechanism.
The `@ndn/repo-external` package has an implementation of the "pubsub" protocol in [ndn-python-repo](https://ndn-python-repo.readthedocs.io/), but it can support only one subscriber, so that it does not qualify as a true pub-sub protocol that should allow multiple subscribers.

Since 2013, there have been [11 NDN synchronization protocols](https://named-data.net/sync-protocol-related-publications/) in the publications list.
However, none is implementable:

* Most of these protocols exist only as publications, but do not have a working implementation.
* PSync is one of the few that has a working implementation, but the actual protocol (wire format) is still unstable (e.g. v0.2.0 changed the wire format by adding data compression).
* ChronoSync is the only protocol that has a [written protocol specification](https://docs.named-data.net/ndn-ccl-api/chrono-sync2013.html) and working implementations, but the protocol has known shortcomings in a broadcast environment.

Lixia mentioned that they are going to do a *cleanup*: they are going to select one of their best sync protocols, and produce a written specification and a working implementation.
I hope so!
Once this happens, NDNts (and possibly my other libraries) is going to add support for this protocol.

## Final Words

My demo session lasted about 80 minutes, with 8 unique visitors.
Given the negative reactions I often receive in the past, this is a better turnout than I expected.
It ended with me showing [my Twitter profile](https://twitter.com/yoursunny), with my bio being:

> stallion coder; push-up specialist

I was not asked to do any push-ups during this session.
