---
title: Quarantine Foods
lang: en
date: 2020-05-16
tags:
- life
- Maryland
- food
image: tuckey-potato.jpg
---

The year 2020 started normal.
Every weekday, I go to office.
Three times a week, I pump those muscles at the gym.
Every four days, I visit the grocery store to pick up a loaf of bread, along with other ingredients.
Every Friday evening, I have a video chat with my family.

## 🕒 12 Timezones Away — 3 Timezones Away — Oops, It's Here

The first sign of problem occurred on Chinese New Year's Day.
Traditionally, my mother's extended family would have an annual gathering during the holidays.
However, my mother informed me that their gathering has been canceled, because there's a Coronavirus outbreak in parts of China.
I thought, well, this doesn't affect me, because I live in Maryland USA, 12 timezones away from China.

The next week, China entered a lock down due to the spreading virus.
My father found himself relying more on [grocery delivery services](/t/2017/shocking-changes-China/).
A difference from what I observed in 2017 is that, the gated community is not allowing delivery persons to enter the neighborhood, and my father has to collect his deliveries at the neighborhood entrance.
At the same time, I heard on Twitter that the Coronavirus has reached the Chinese community in Seattle, 3 timezones from me.

In early March, [the Coronavirus reached Maryland](https://governor.maryland.gov/2020/03/05/governor-larry-hogan-declares-state-of-emergency-expands-statewide-response-to-novel-coronavirus/).
A week later, [public schools are being closed](https://news.maryland.gov/msde/state-superintendent-salmon-announces-temporary-closure-of-maryland-public-schools/).
This is when things started to get serious.

## 🧻 Empty Shelves

My normal grocery shopping schedule is once every four days, which coincides with how fast I consume a loaf of bread.
March 13, the Friday before school closure, happened to be my grocery shopping day.

The store was unusually crowded on that day.
I'm unable to find any bread.
I went for my "backup" breakfast option: pasta.
I think I grabbed the last two boxes of "normally shaped" pasta.

![Giant Foods, 2020-03-13, pasta shelf](shelf-pasta.jpg)

The meat department had no beef or pork, and the only chicken left was feet and livers.
Many Americans are too scared to think about these specialty proteins.
I bought one tray of chicken feet, and later cooked a pot of braised chicken feet.

![Giant Foods, 2020-03-13, meat fridge](shelf-meat.jpg)

Two days later, I returned to the store and found some organic breads, but not from my favorite brand.

![Giant Foods, 2020-03-15, bread shelf](shelf-bread.jpg)

I'm glad that I didn't need any [toilet paper](https://www.bbc.com/news/world-australia-51731422).

On March 30, I went to an Asian market and bought some dumplings.
It's two weeks into the pandemic, social distancing signs were up, store employees were wearing masks, and there's a plexiglass barrier between shoppers and the cashier.

Later that day, Maryland Governor Larry Hogan issued a [Stay At Home Order](https://governor.maryland.gov/2020/03/30/as-covid-19-crisis-escalates-in-capital-region-governor-hogan-issues-stay-at-home-order-effective-tonight/).
I decided that, I should stop my normal grocery shopping routine, and move to online shopping as much as possible.
To this date, I haven't stepped into a grocery store.

## 🥕 Imperfect Foods

I have a membership at [Imperfect Foods](https://bit.ly/imperfectsignup).
Every other Friday, I can choose to receive a box of oddly-shaped produce that is "too ugly" for the grocery store and "would otherwise go to waste".
The produce is said to be 30% cheaper than grocery stores, but after adding a $5 delivery fee, prices are roughly the same.
Therefore, I often skip the deliveries, in favor of the "perfect" foods from real grocery stores.

![Imperfect Foods, 2020-04-03, produce items](imperfect-produce.jpg)

During the Coronavirus pandemic, this [Imperfect Foods](https://bit.ly/imperfectsignup) membership becomes my lifeline.
I found myself ordering more produce, and adding not-so-cheap protein items such as sausage and free range eggs.
Every box is filled to the brim, and I once ordered so much that they have to send the items in two boxes.

![Imperfect Foods, 2020-05-08, protein items](imperfect-protein.jpg)

However, from time to time, I would receive an email on Friday morning:

![A slight change in your Imperfect box this week](imperfect-change.png)

When the box arrives in the afternoon, more often than not, I would find several more items missing.
Although I eventually would receive refunds of the turnips that did not turn up, such packing errors have been problematic for my meal plans.

## 🍠 Tuckey's Farm

Gaithersburg has a [Main Street Farmers and Artists Market](https://www.gaithersburgmd.gov/recreation/farmers-markets) that opens year round, Saturdays from 09:00 to 14:00.
During the pandemic, this market was closed abruptly, together with most other city facilities.
But there's one slight problem: the corps are still growing in the field, and they have to go somewhere.
Thus, a [Gaithersburg Farmers Market delivery service](https://www.gaithersburgmd.gov/recreation/farmers-markets/farmers-market-deliveries) began.

![Tuckey's Farm, 2020-04-11, apples, leeks, lettuce, and cabbage](tuckey-apple.jpg)

Among a handful of vendors, **Tuckey's Farm** is the only vendor that offers produce items and has the prices published on the webpage.
However, I am having trouble understanding the units.
In grocery stores, produce items are priced either by weight (e.g. cabbage is $0.49 per pound) or by quantity (e.g. banana is $0.19 each).
The list from Tuckey's Farm looks like this:

* Apples- $5.00/quart or $10.00/half peck
  * Apple varieties available- Gala, Yellow Delicious, Nittany, Fuji, Pink Lady, Gold Rush, Granny Smith and Mutsu
* Sweet Potatoes- orange or white- $5.00/quart
* Red Beets- $5.00/quart
* Shallots- $4.00/half pint
* Leeks- $1.00 each or 6 for $5.00
* Free Range Brown Eggs- $5.00/dozen

I know that a "pint" is the bigger box of blueberries, but what is a "quart" or a "half peck"?
So I emailed them to ask.
The answer reminds me of grandma's basket:

> A peck or half peck is an old english measurement of volume.
> There are 2 1/2 - 3 quarts of apples in a half peck, so it is a better deal than buying two quarts.
> The number of apples really varies depending on their size.
> You would probably get 11 or 12 really large apples in a half peck, but 15-20 smaller ones.
> Gala are the smallest apples we have.
> Fuji are quite large.

![Tuckey's Farm, 2020-04-11, potatoes, sweet potatoes, and red beets](tuckey-potato.jpg)

I placed the order on Monday, paid via PayPal, and received the delivery on Saturday.
It turns out that a quart equals:

* 6 sweet potatoes.
* 7 potatoes.
* 8 pink lady apples.
* 11 red beets.

During the next few days, the sweet potatoes went into the [air fryer](https://amzn.to/3cBDfMW) and became sweet potato fries for breakfast.
The apples and beets went into the [blender](https://amzn.to/2X3VxQy) and became smoothies for evening snack.

![homemade sweet potato fries](sweet-potato-fries.jpg)

The potatoes, however, stayed in the box.
I tried to make French fries in the air fryer, but they tasted bad without excessive salt and tomato ketchup.
Until one day, I found the [recipe of mashed potatoes](https://www.allrecipes.com/recipe/24771/basic-mashed-potatoes/).
It needs potatoes, and milk.
Where can I get milk?

## 🍏 Whole Foods, Whole Paycheck

Selections are limited at the farmers market.
There's no milk, and the only protein item is eggs.
To solve the milk problem, I turned to **Whole Foods Market**.

I've been a [Prime member](https://amzn.to/3bECrFN) since 2018, mainly for watching movies.
When I visit Whole Foods Market last year, I noticed that there were dozens of *suspicious* shoppers who kept looking at their phones while putting items into their baskets.
Recently I found that [Whole Foods Market has an Amazon storefront](https://amzn.to/2TetVHf) that offers delivery services.
I decide to give it a try.

![Whole Foods Market, 2020-04-28, chicken, beef, tofu, and milk](wfm-protein.jpg)

I entered my address, filled my cart, then:

> No delivery windows available.
> New windows are released throughout the day.

The next day, some items disappeared from my cart because they were sold out.
I put in some new items, saw an available delivery time, and proceeded to enter the instructions on how to open the apartment gate.
Then, the message showed up once again:

> No delivery windows available.
> New windows are released throughout the day.

This has been very frustrating.
I kept refreshing, and finally completed the checkout process.

Typically, they would start putting together the order within an hour, and then the delivery arrives after another hour.
Items are packaged in sturdy bags.
Some of the bags are not recyclable, and I'm unhappy about that.

![Whole Foods Market, 2020-04-05, plastic bags](wfm-bags.jpg)

Nevertheless, most items sold at Whole Foods Market are at my fingertips.
As long as I'm able to get a delivery window, meals are no longer a problem.
I can have chicken breast, chicken legs, chicken thighs, ground beef, cubed beef, duck breast, shrimp, and fish.
I can also get my favorite [Seeduction Bread](https://amzn.to/2Zc79n3) and have normal breakfasts for the next 4 days.

Whole Foods also sells my favorite cake: the tiramisu.
I would devour the moist delicacy layer by layer, and let the whipped cream get all over my whiskers and hands.
I feel as bold and powerful as the [#ChaosRaccoon](https://twitter.com/JoeSondow/status/1198828320357572608).

![Whole Foods Market, 2020-05-06, tiramisu cake slice](wfm-tiramisu.jpg)

Whole Foods delivery is free for orders above $35.
There is absolutely no chance to place an order below $35 though, because Whole Foods only sell the highest quality stuff, so that everything costs 20% more than other grocery stores.
It is nicknamed ["Whole Paycheck"](https://www.urbandictionary.com/define.php?term=whole%20paycheck) for a reason.

## 🥬 Other Suppliers

I tried several other suppliers.

[Territory Grocery](https://territorygrocery.com/) is a brand of Territory Foods, a meal plan company that delivers microwave dinners in single-use plastics.
Their small produce box sells for $35, and I don't get to choose what goes inside.

I tried my luck once.
The box contained:

* 0.5lb collard greens
* 1 sweet potato
* 1 lemon
* 2 garlic cloves
* 2 potatoes
* 2 oranges
* 2 broccolis
* 3 apples
* 3 tomatoes
* a spoiled mess of cilantro

I could get the same items from Imperfect Foods for less than $15.
Territory Grocery is not a good value.

Then, there's **Walmart** and **Target** for non-perishable items such as rice and spices.
My favorite brown rice is out of stock.
Shipping estimate for a bottle of olive oil is more than a week.
I'm only able to get some Biscoff Cookies.

## 🛑 No End in Sight

Recently, I found myself eating less ice cream.
When I walk through the aisles of a grocery store, my hand would automatically grab a pint of these sugary food.
Nowadays, Whole Foods is the only place selling ice cream, and I have to make a conscious effort to search for it.
As a result, my more-than-weekly ice cream habit has subdued.
This aligns well with the lower calorie burning due to decreased physical activities.

The pandemic is still ongoing.
Although [Maryland is reopening](https://governor.maryland.gov/2020/05/13/stage-one-governor-hogan-announces-gradual-reopenings-with-flexible-community-based-approach/), Montgomery County [remains closed](https://twitter.com/yoursunny/status/1243649114052743169) for in-person business.
I'll have to continue shopping online for a while.

I can see progress is being made.
Starting in May, Whole Foods is showing many more delivery windows than April, and they stopped using the silver plastic bags in favor of recyclable paper bags.
Tuckey's Farm is doing deliveries not only on Saturdays, but also on Tuesdays, because they are having more customers.
Target brought back the brown rice, and restored 2-day shipping.
The quarantine life will go on.
