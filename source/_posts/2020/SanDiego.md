---
title: Wandering in San Diego
lang: en
date: 2020-07-26
tags:
- travel
- geocaching
image: Maritime.jpg
---

I have [wanderlust](https://www.merriam-webster.com/dictionary/wanderlust#examples) but the [Coronavirus lockdown](/t/2020/quarantine-foods/) is preventing me from traveling this year.
It would be fun to dig out old photos and look back on my past travels.
Let me start with one of my favorite cities: San Diego, California.

## December 2011

My first time in San Diego was a road trip with a few Chinese students I met during [my first days in Tucson](/t/2017/first-days-in-Tucson/).
We visited [SeaWord San Diego](https://seaworld.com/san-diego/), where I enjoyed their famous **Shamu** show, in which I got splashed by the killer whales.

![whale performance in SeaWorld San Diego, 2011-12-22](SeaWorld.jpg)

During the same trip, we also visited [USS Midway Museum](https://www.midway.org/), an aircraft carrier docked in San Diego harbor.
I learned how these little planes take off, and more importantly how they can land safely with the help of arresting gear.

![USS Midway, 2011-12-23](Midway.jpg)

In front of USS Midway Museum, we saw the original foam-and-urethane [Unconditional Surrender](https://en.wikipedia.org/wiki/Unconditional_Surrender_%28sculpture%29) sculpture.
It turns out that this particular statue was torn down 4 months later, and replaced in 2013 by a bronze statue.

![Unconditional Surrender sculpture, San Diego original installation, 2011-12-23](UnconditionalSurrender.jpg)

## October 2012

A year later, I attended my first [academic conference](/t/2017/academic-papers/), hosted by University of California, San Diego (UCSD).
UCSD campus is located in La Jolla, a small town 16 miles north from San Diego.
During the conference, I had plenty of time to explore the campus.
Their library has a unique shape:

![Geisel Library, 2012-10-12](GeiselLibrary.jpg)

I had one afternoon of free time, and I used that time to visit [Maritime Museum of San Diego](https://sdmaritime.org/), located next door from USS Midway.
Instead of one big aircraft carrier, this museum consists of several sailing ships and a submarine.
It was my first time crawling into the narrow pathways of a submarine.

![me at Maritime Museum, 2012-10-10](Maritime.jpg)

## November 2013

The academic conference at UCSD became a recurring event, and the second edition was in 2013.
I didn't have a [driver's license](/t/2017/behind-the-wheel/) at that time, but I gained enough confidence in San Diego's public transit system during the previous trip, so that I decided to fly to San Diego two days early, and take the bus to visit some downtown attractions.

[Balboa Park](https://www.balboapark.org/) is the largest park in downtown San Diego.
The center of Balboa Park has fifteen museums.
I was fascinated by the [model railroad](https://youtu.be/VtlpPutFOA4) that moves and sounds just like real trains (later I found a [similar museum](/t/2017/Tucson-museums/) in Tucson).
At [Fleet Science Center](https://www.fleetscience.org/), I learned [how a wheel works](https://youtu.be/hZgd9fPnwzI), and followed my colorful shadow made by three projectors of different colors:

![my colorful shadow at Fleet Science Center, 2013-11-10](FleetScienceCenter.jpg)

San Diego Zoo is also located in Balboa Park.
I visited the zoo on the second day, viewed every animal except the pandas.
My favorite is the [meerkat](https://youtu.be/wCiODKEdcks).

I had a bit more money than the previous years, so that I rewarded myself with the [most expensive meal](https://www.quora.com/What-is-the-most-expensive-meal-you-have-eaten-How-good-did-it-taste/answers/69690626) I've had: a Hornblower dinner cruise.
It was a 3-hour cruise in the San Diego bay, where I got to see the [San Diego skyline](https://youtu.be/lZxa3smUU-A) and the mighty Coronado Bridge:

![Coronado Bridge, night view, 2013-11-10](CoronadoBridge.jpg)

## February 2015

The next UCSD conference trip was in 2015.
There were two differences from previous trips:

* I brought my classmate on this trip.
* I [became a geocacher](/t/2017/start-geocaching/), which completely changed the way I travel.

I convinced my classmate to *walk* out of the airport instead of taking a taxi.
He was excited to see the sea as soon as we left the airport.
We had lunch together, then took a trolley to a natural attraction I selected: Sunny Jim's Cave.

![Sunny Jim's Cave, 2015-02-04](LaJollaCave.jpg)

Why did I choose this place?
It has [an EarthCache](https://coord.info/GC52XC8), a type of geocache that educates us about geographical features, such as the seven sea caves carved in the 75-million-year-old sandstone sea cliff.
I got a smiley face on Geocaching.com, while my classmate also learned something unique in the area.

On conference days, I get up early to find geocaches.
One of the caches I found was [Eucalyptus Maze](https://coord.info/GC916), which turns out to be one of the oldest caches in San Diego county.

<iframe width="600" height="400" allowfullscreen style="border-style:none;" src="/t/demo/pannellum.htm#panorama=/t/2020/SanDiego/EucalyptusMaze.jpg&autoLoad=true&yaw=170&pitch=30&minPitch=-60&maxPitch=80"></iframe>

## March 2016

My fifth trip to San Diego, and fourth conference at UCSD, was March 2016.
This time, I was traveling with two classmates on the same flight.
I introduced a few attractions to them, and they wanted to visit the Maritime Museum.
Since I have been to that museum in 2012, I didn't go with them.

Instead, I went geocaching in downtown San Diego.
I walked more than 8 miles that day, searched for 14 geocaches and found 10.
I saw the old fire house, the ships, parks, kites in the sky, and bridge above the bay.
I ended my trip in SeaPort Village, and took a bus to UCSD.

![SeaPort Village, 2016-03-19](SeaportVillage.jpg)

The conference that year expanded from two days to four days, including two days of hackathon.
I was [running a 4-person project](https://2nd-ndn-hackathon.named-data.net/ndn-cxx-logging/).
From time to time, I grab one of my strong boys for a run in the Eucalyptus forest, to "clear my mind" from all the coding.
I also had the chance to see ["the supercomputer"](https://twitter.com/yoursunny/status/712468053959811072) at San Diego Supercomputer Center.

## Why I Like San Diego

2016 was my last time visiting San Diego.
After that, UCSD no longer hosts the academic conference I've been attending.
During my five short visits, San Diego gave me many good memories, so that it becomes one of my favorite cities.

![Sunset Cliff, 2013-11-09](SunsetCliff.jpg)

San Diego has the **perfect weather**: sunny, low humidity, and rarely rains.
Moreover, I can see the ocean, and the beautiful sunsets.

San Diego has a **walkable** downtown.
I can *walk* from the airport to downtown in an hour, and the walk is along the beautiful harbor instead of noisy roadways.
In how many other cities can you do that?

![San Diego Trolley, 2012-10-10](trolley.jpg)

Going beyond downtown, the **public transit system makes sense**.
After just two trips, I can remember the major routes.
In 2013, I chose a hotel 5 miles away, so that I can walk along the sea cliff every morning.
I took the 07:30 bus, and arrived at UCSD on time each day.

![Del Mar, 2013-11-12](DelMar.jpg)

Now it's 2020 and I live on the East Coast.
I wish I have a chance to visit the sunny San Diego, California again.
