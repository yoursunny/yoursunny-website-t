---
title: NDNts Nightly Build
lang: en
date: 2020-08-22
edited: 2024-03-06
tags:
- NDNts
- NDN
- JavaScript
---

**NDNts nightly build** is a set of NPM-compatible tarballs compiled automatically from the development branch of [NDNts](/p/NDNts/), Named Data Networking (NDN) libraries for the modern web.
They are built by the Continuous Integration (CI) system and uploaded to the NDNts nightly build website: [**ndnts-nightly.ndn.today**](https://ndnts-nightly.ndn.today).
Homepage of that website displays a list of URIs of available tarballs.

This article was latest updated on 2024-03-06 to reflect latest changes.

## How to install NDNts nightly build

You can find available tarballs on NDNts nightly build website: [**ndnts-nightly.ndn.today**](https://ndnts-nightly.ndn.today/).

To install a tarball as a local dependency within the current project, you can execute something like:

```shell
$ npm install https://ndnts-nightly.ndn.today/packet.tgz
+ @ndn/packet@0.0.20200822-nightly.a471902
added 5 packages from 3 contributors and audited 5 packages in 1.612s
found 0 vulnerabilities
```

To install a tarball as a global command, you can execute something like:

```shell
$ npm install -g https://ndnts-nightly.ndn.today/keychain-cli.tgz
/home/sunny/.nvm/versions/node/v14.8.0/bin/ndntssec -> /home/sunny/.nvm/versions/node/v14.8.0/lib/node_modules/@ndn/keychain-cli/cli.cjs
+ @ndn/keychain-cli@0.0.20200822-nightly.a471902
updated 19 packages in 13.327s
```

## Caveats of NDNts nightly build

NDNts nightly build website is not a NPM registry.
It only serves the latest version, and does not preserve previous versions.
Whenever the CI job uploads a new version, it overwrites the previous versions.

NPM stores a SHA512 digest of the tarball in the `package-lock.json` file.
When you (re)install packages with a `package-lock.json` file present, it would download the tarballs from the NDNts nightly build website, and notice that their SHA512 digest does not match the records in `package-lock.json`.
Then, the installation fails with an error like this:

```shell
$ npm install
npm ERR! code EINTEGRITY
npm ERR! sha512-mtyR9BXk5/liMRGHkVILpRGE2+PtDDCuUz49PzW8TwR17UnrTWMFmAb4uIefDQqW1jdCTOOX9kPzkfeK6f1RjQ==
  integrity checksum failed when using sha512: wanted
  sha512-mtyR9BXk5/liMRGHkVILpRGE2+PtDDCuUz49PzW8TwR17UnrTWMFmAb4uIefDQqW1jdCTOOX9kPzkfeK6f1RjQ==
  but got
  sha512-Q6oePXREf3J3wpwIbZ5j7ZtVRuNpuxStmnQwcQvjhSeT9ZZ2xoJnkjdYsmzfPkZnrD1QaWWMBQelcUR8EukBCQ==.
  (1494 bytes)
```

To workaround this issue, you should delete the lockfile and previously downloaded NDNts packages before executing `npm install`.

```bash
# command for NPM
rm -rf package-lock.json node_modules/\@ndn

# command for PNPM
rm -rf node_modules/@ndn node_modules/.pnpm/@ndn* pnpm-lock.yaml \
  ~/.local/share/pnpm/store/v3/@ndn*
```

I understand that these steps are quite unusual.
In the future, I'll provide a procedure that allows building and locally hosting a specific version of NDNts, to make NDNts nightly build easier to use.

UPDATE 2021-01-02: [self-hosted NDNts nightly build](/t/2021/NDNts-nightly-selfhost/) guide is now published.
