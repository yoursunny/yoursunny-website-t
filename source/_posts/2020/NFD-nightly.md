---
title: NFD nightly packages
lang: en
date: 2020-07-25
tags:
- NDN
- git
- Debian
- Ubuntu
- RaspberryPi
---

**NOTICE**
Instructions in this article are outdated.
Please see latest information in [NFD nightly usage](/t/2021/NFD-nightly-usage/).

[NDN Forwarding Daemon (NFD)](https://docs.named-data.net/NFD/current/) is the reference implementation of Named Data Networking (NDN) forwarding plane.
The software is continuously developed, but binary releases happen rather infrequently.
Recently, I made a workflow to build NFD and related software automatically.

Download page: [nfd-nightly.ndn.today](https://nfd-nightly.ndn.today)

## Instructions

Which platform should I choose?

* The platform identifier has two parts: Linux distribution and CPU architecture.
  Both parts must match your machine.
* Linux distribution:
  * **bionic** is Ubuntu 18.04.
  * **buster** is Debian 10. This includes Raspberry Pi OS.
* CPU architecture:
  * **amd64** works on Intel and AMD 64-bit machines.
  * **armv7** works on ARMv7 machines, such as [Beaglebone Black](https://amzn.to/2J5sSoZ) and [Raspberry Pi](https://amzn.to/2CL0hUQ) with 32-bit OS.
  * **armv6** works on [Raspberry Pi Zero W](https://amzn.to/2DjtzEz) only.

Which packages should I install?

* **libndn-cxx_*.deb**: ndn-cxx library.
* **libndn-cxx-dev_*.deb**: development headers for ndn-cxx.
  * This is needed if you want to compile software that uses ndn-cxx.
  * Depends on: `libndn-cxx`
* **ndnsec_*.deb**: `ndnsec` command for managing ndn-cxx KeyChain.
  * Depends on: `libndn-cxx`
* **nfd_*.deb**: NDN Forwarding Daemon (NFD) and `nfdc` management utility.
  * Depends on: `libndn-cxx`, `ndnsec`
* **ndnpeek_*.deb**: `ndnpeek` and `ndnpoke` commands for sending a single packet.
  * Depends on: `nfd`
* **ndnping_*.deb**: `ndnping` and `ndnpingserver` commands for testing reachability.
  * Depends on: `nfd`
* **ndnchunks_*.deb**: `ndncatchunks` and `ndnputchunks` commands for file transfers.
  * Depends on: `nfd`
* **ndn-dissect_*.deb**: `ndn-dissect` command for analyzing packet structure.
  * Depends on: `libndn-cxx`
* Other packages are generally not needed.

How to download and install?

1. Choose a platform, and find the latest build that offers this platform.
2. Download the **zip** files for ndn-cxx, NFD, and ndn-tools.
3. Put all the **deb** files in the same directory.
4. Execute `sudo dpkg -i` followed by the packages you want to install.
   It is normal for this command to report dependency errors.
5. Execute `sudo apt-get install -f` to install missing dependencies from APT sources.

(2021-01-18 update) Can I install with `apt-get`?

* Yes! See [this article](/t/2021/NFD-nightly-apt/).

Why am I seeing "page not found" after clicking a download link?

* You must be logged into GitHub before you can download.

## Q&A

Why should I use **deb** packages instead of building from source code?

* It is faster to install **deb** packages than trying to build from source code.
* You can uninstall cleanly with `sudo apt-get remove` command.
* [`sudo ./waf install` can conflict with packages](https://wiki.debian.org/DontBreakDebian#A.27make_install.27_can_conflict_with_packages) and break your system.

What are the differences between [ppa:named-data/ppa](https://launchpad.net/~named-data/+archive/ubuntu/ppa) and NFD-nightly?

* Update frequency:
  * PPA: after each NFD release, which occurs a few times per year.
  * NFD-nightly: weekly.
* Supported platforms:
  * PPA: recent Ubuntu version.
  * NFD-nightly: Debian and Ubuntu.
* Format:
  * PPA: signed **deb** packages, automatic upgrade with `apt-get` command.
  * NFD-nightly: unsigned **deb** packages, manual install with `dpkg` command.
* Quality:
  * PPA: production.
  * NFD-nightly: experimental.

How are these packages built?

* They are built on [GitHub Actions](https://docs.github.com/en/actions) using Docker containers.
* Non-amd64 architectures are compiled in [Balena](https://www.balena.io/docs/reference/base-images/base-images-ref/) images.

Why do I have to be logged into GitHub to download?

* The **zip** files are stored as "artifacts" on GitHub.
* Anonymous downloads of artifacts are [disallowed](https://github.community/t/public-read-access-to-actions-artifacts/17363/11) on GitHub.

The package isn't working?

* If you are seeing "Illegal Instruction" error, [open an issue](https://github.com/yoursunny/NFD-nightly/issues) to report.
  * Be sure to include: "build number" of the download, filename of the **deb** package, and information about your platform.
* Other errors are not caused by NFD-nightly. Report on [nfd-dev mailing list](https://named-data.net/codebase/platform/support/mailing-lists/) instead.

How to request a new platform?

* I can add newer Debian or Ubuntu releases.
  * NFD needs to be compatible with the default compiler and Boost versions installed from APT.
* I cannot create packages for Linux distributions that do not use APT.
  * I'm a geocacher and I cannot stand the word `dnf` ![DNF](https://www.geocaching.com/images/logtypes/3.png).
* I can add more CPU architecture, if [Balena](https://www.balena.io/docs/reference/base-images/base-images-ref/) has an image for that.
* [Open an issue](https://github.com/yoursunny/NFD-nightly/issues) to discuss.

How to request a new package?

* I can add additional packages (e.g. NLSR).
* They need to have packaging scripts in [named-data/ppa-packaging](https://github.com/named-data/ppa-packaging).
  * If not, [contribute to ppa-packaging](https://gerrit.named-data.net/q/project:ppa-packaging) first.
* [Open an issue](https://github.com/yoursunny/NFD-nightly/issues) to discuss.
