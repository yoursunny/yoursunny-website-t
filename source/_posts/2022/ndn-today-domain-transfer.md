---
title: Domain Transfer, WHOIS Privacy, DNSSEC, and the Absence of Push-ups
lang: en
date: 2022-04-03
edited: 2022-04-05
tags:
- DNS
- hosting
discuss:
- https://lowendspirit.com/discussion/4004/domain-transfer-whois-privacy-dnssec-and-the-absence-of-push-ups/p1
- https://lowendtalk.com/discussion/178345/domain-transfer-whois-privacy-dnssec-and-the-absence-of-push-ups/p1
---

## Annual Domain Transfer for Profit

Since [my first domain name](/t/2006/yoursunny-com-domain/) in 2006, I have purchased several domain names for my various websites.
A few years ago, I discovered a secret in the domain registration business: many registrars offer a cheaper price for domain transfer than domain renewal, as a means to attract new customers.
Therefore, if I transfer my domain every year to a different registrar, I would pay less than renewing the domain at the same registrar.

DNS services for a domain used to be associated with the registrar.
When I transfer a domain away, the DNS server of the old registrar would stop responding to queries regarding my domain, and the DNS server of the new registrar does not yet have any records about the IP addresses of my web server.
Therefore, a domain transfer would usually cause the website to become inaccessible for a day or two.
Typically, I [post a tweet](https://twitter.com/yoursunny/status/539286944002621440) when a domain transfer is about to happen, so that my readers could know why my website is down.

Nowadays, I'm using Cloudflare DNS for most of my domains.
Cloudflare DNS server is independent from the domain registrar, so that my website continues to resolve correctly throughout a domain transfer, as long as neither registrars modify the name server delegation records.
In case the new registrar automatically updates the delegation records to their DNS servers, I have to quickly login to the control panel and change it back to Cloudflare, which would then cause a brief downtime of the website.
Having done so for many years, I am accustomed to this process.

## Transfer of ndn.today

I registered **ndn.today** domain name in 2020, to host several of my personal projects related to Named Data Networking, which include the popular [NDN push-ups](https://pushups.ndn.today) page.
Later that year, I transferred this domain from NameCheap to Porkbun.
After entering the [Auth-Code](https://en.wikipedia.org/wiki/Auth-Code) at the new registrar and accepting the transfer request at the old registrar, the domain moved over, and Cloudflare continues to resolve the domain so that there's no website downtime.

Many domain registrars offer free WHOIS privacy services, which conceal my name, street address, and email from the public WHOIS database.
I do not consider WHOIS privacy essential because [my information is public](/m/), but I kept it enabled so that I could receive fewer spam email.
During the above domain transfer, WHOIS information changed from [*Withheld for Privacy ehf*](https://withheldforprivacy.com/) to [*Private by Design, LLC*](https://private.design/), which are the WHOIS privacy services of NameCheap and Porkbun respectively.
Despite the change, I am still the domain owner as recorded in the registrar's control panel.

Fast forward to March 30, 2022, it's less than two months before the expiration date of **ndn.today**, so it's time to yo-yo the domain again.
[Tldes.com](https://tldes.com/today) indicates that [one.com](https://www.one.com/en/domain/prices) has the cheapest domain transfer offer for `.today` at $2.22, and they are an IANA-accredited registrar.
Following the usual procedure, I unlocked the domain, entered the Auth-Code, paid the invoice, and accepted the transfer request.
The domain moved over, and I went to bed.

The next morning, I received an alert:

> Hi,
>
> The monitor web RSpec (<https://rspec.ndn.today>) is currently DOWN (Connection Timeout).
>
> UptimeRobot will alert you when it is back up.

Usually, such an alert indicates a problem with the hosting server.
I SSH'ed into the server, but did not find any issues.
Since I was busy with coding that day, I ignored the alert thinking it would resolve by itself in a few hours.

In the evening of March 31, I recalled this UptimeRobot alert, and discovered that I cannot open the [NDN push-ups](https://pushups.ndn.today) website anymore.

![This site can't be reached. pushups.ndn.today's server IP address could not be found. Try: Checking the connection. ERR_NAME_NOT_RESOLVED](name-not-resolved.png)

I started to realize, the alert was caused by a DNS problem, not a server issue.
I queried the domain with two online WHOIS lookup tools, one shows the name server delegation to be Cloudflare as expected, the other one shows:

```text
Updated Date: 2022-03-31T13:03:53Z
Creation Date: 2020-05-25T19:09:17Z
Registry Expiry Date: 2023-05-25T19:09:17Z
Registrar: One.com A/S
Registrar IANA ID: 1462
Domain Status: transferPeriod https://icann.org/epp#transferPeriod
Registry Registrant ID: REDACTED FOR PRIVACY
Registrant Name: REDACTED FOR PRIVACY
Registrant Organization: Private by Design, LLC
Name Server: ns01.one.com
Name Server: ns02.one.com
DNSSEC: signedDelegation
```

Apparently, one.com automatically changed the name server delegation to their own.
Since I haven't entered any DNS records in one.com DNS control panel, their DNS server cannot resolve my website IP address.

## Domain Owner: Private by Design, LLC

I haven't encountered an automatic name server update since 2014, but I know how to deal with it: login to the new registrar's control panel, and change the name server back to Cloudflare.
I filled up the form, clicked submit, and then received a message:

> You have name server changes pending approval.
> A request has been sent to the registrant of the domain for approval.
> If not approved within three days, the request will fail.

Looking through my email inbox, I received nothing.
After poking around, I found the issue: according to one.com, the domain is now owned by *Private by Design, LLC*, Porkbun's WHOIS privacy service.
Therefore, they sent the request to `whoisprivacy@porkbun.com`, but I have no way to access that mailbox.

I attempted to modify the domain contact.
However, that form also triggers an automated email to `whoisprivacy@porkbun.com` that I cannot access.
I contacted Porkbun chat support, and they told me the validation code contained in one of the emails.
I submitted this validation code at one.com, but it did not allow me to immediately update the domain contact.

## Road to Recovery

One.com is the current registrar and they have the authority and responsibility to resolve the problem.
I contacted their chat support, *Support Robot* gave a 7-minute estimate for a support agent, but nobody picked up after 15 minutes.
I tried again after a few hours and got to someone, who asked me to fill up a [general owner change PDF form](https://one-docs.com/ownerchange/general-owner-change.pdf) and submit by email.
Seeing that the form should be accompanied by a photo ID such as my driver's license, I felt uneasy to send sensitive documents via unencrypted email.
Upon raising my concern, the support agent *Angel* said that I can also [use the ticket system](https://help.one.com/hc/en-us/requests/new), much better!

I prepared and signed the form, scanned my driver's license, and submitted both via the ticket system.
The same *Angel* replied to the ticket, saying that she's going to forward the form to their *Hostmasters*, who would process the form within 24 hours.
Sigh.

The next morning, on April 1, I received another email:

> Owner details update for domain ndn.today
>
> We have declined your request of Mar 31, 2022 to update the owner details for your domain ndn.today.

I started to ~~panic~~ [<abbr title="Personal Meltdown and Stupidness">PMS</abbr>](https://lowendtalk.com/discussion/comment/3290066/#Comment_3290066): I submitted the PDF form according to instructions, and why is it declined?
A different chat support agent checked the status of my ticket, and assured me that the PDF form is still being processed, and the decline message came from my earlier attempt in the control panel.

## DNSSEC Thwarts the Temporary Measure

As I'm waiting for the hostmaster to process the "owner change", I thought about a temporary solution: I can enter server IP addresses in the control panel, so that one.com DNS server could resolve the records.
I have been using Cloudflare CDN for some of my sites under this domain, mainly for the convenience of TLS termination.
Moving them off the CDN means that I have to do TLS termination on my own server, but I'm experienced with this: [Caddy server has automatic TLS](/t/2021/yoursunny-com-caddy/) and can obtain certificates automatically.

I inserted A and AAAA records in one.com DNS control panel, and configured reverse proxies in the `Caddyfile`:

```text
https://pushups.ndn.today {
  reverse_proxy https://pushups.netlify.app
}
```

[Certificate Transparency](https://certificate.transparency.dev/) notifications started rolling in, suggesting that my reverse proxy has successfully obtained TLS certificates for the subdomains.
I couldn't access the websites myself at that time, but I'm confident that [DNS propagation delay](https://www.pickaweb.co.uk/kb/why-propagation-delay-occures-and-causes-inaccessibility-of-website/) would eventually resolve itself.
[Certificates are issued by ZeroSSL](https://crt.sh/?id=6454998598) instead of the usual Let's Encrypt, but it's not a matter of concern.

It's now April 2, two days after the initial UptimeRobot alert, I still cannot access my websites.
I queried my domain on [DNS lookup & Propagation Check](https://dnspropagation.net/A/pushups.ndn.today), and it gave mixed results: some DNS servers can resolve the domain and some cannot.
While DNS does have negative caching, such caching is normally short-lived, so that DNS caching is not the only one to blame.

![worldwide DNS propagation map, 5 green ticks and 14 red crosses](propagation.png)

The reason of failed DNS resolution lies in DNSSEC.
Following a recommendation from Cloudflare, I enabled DNSSEC for my domain, so that unauthorized DNS servers cannot respond to queries with bogus responses.
Setting up DNSSEC is a two-step process:

1. Cloudflare DNS server generated a signing key pair, and would use it to sign every response under my domain.
2. Using a form on the Porkbun control panel, I submitted the digest of the public key to the `.today` registry, in what's called a [DS record](https://www.cloudflare.com/learning/dns/dns-records/dnskey-ds-records/).

When a DNS resolver receives the DS record, it would retrieve the public key from the delegated name server (i.e. Cloudflare), and check that the public key matches the digest and the records are signed by this public key.
Right now, the name server delegation is pointing to `ns01.one.com`, but the DS record contains the digest of Cloudflare's public key.
Since one.com does not own the corresponding private key, it would not be able to come up with a valid signature.
Consequently, DNS resolvers would reject its responses and refuse to resolve my domain.

[VeriSign Labs DNSSEC Analyzer](https://dnssec-analyzer.verisignlabs.com/) confirms my suspicion:

![Found 1 DS records for ndn.today in the today zone; No DNSKEY records found; ns01.one.com is authoritative for pushups.ndn.today; No RRSIGs found](dnssec.png)

[Not every DNS resolver validates DNSSEC signatures](https://blog.apnic.net/2020/03/02/dnssec-validation-revisited/), which explains why some DNS servers can resolve the domain and some cannot.
I checked Caddy server logs, and it suggests that Let's Encrypt could not issue certificates due to DNS resolution failure, so that Caddy automatically switched to ZeroSSL as a fallback.
This implies that Let's Encryption is using a DNSSEC-validating resolver, while ZeroSSL is using a non-validating resolver.

Now, my domain is in limbo.
If a viewer is using a non-validating DNS resolver, they can visit my website and see my push-ups.
If a viewer is using a DNSSEC-validating resolver, there would be no push-ups for them.

## Summary & Final Words

I regularly transfer domains between registrars to take advantage of lower pricing.
This time, I transferred a domain from Porkbun to one.com without disabling WHOIS privacy service.
The new registrar considered Private by Design, LLC, Porkbun's WHOIS privacy service, to be the domain owner, and restricted me from accessing most features in the control panel.
Paperwork for re-assigning the domain owner to me is still being processed after two days.

The new registrar automatically changed name server delegation to their own.
I inserted DNS records to one.com DNS server as a temporary measure, but these records are being rejected by DNSSEC-validating resolvers because one.com does not possess the signing keys.
Consequently, **ndn.today** domain has been inaccessible for three days and counting, and half of the world population is unable to watch my push-ups.

**UPDATE 2022-04-05**:
Shortly after this article was first published, one.com approved the domain owner change request and re-assigned the domain under my own name.
I updated the name server back to Cloudflare right away, and my websites are fully recovered as of this writing.
According to UptimeRobot, total downtime was about 77 hours and 49 minutes.
Needless to say, I wasted so much time trying to fix my domain, the time that I could otherwise spend doing push-ups.
