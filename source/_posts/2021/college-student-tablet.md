---
title: 请问大学生买平板电脑的意义大吗？
lang: zh
date: 2021-01-17
tags:
- academic
- Android
- Windows
discuss:
- https://shuiyuan.sjtu.edu.cn/t/topic/15422/20?u=yoursunny
---

我从上海交通大学本科、到亚利桑那大学研究生，都是坚持纸质笔记，而不是用电脑作笔记。
我的笔记只在课堂当场书写、课后做作业时完善，之后就记住了。
考试前夕是不看的。

本科阶段（2005-2009年），我买不起平板电脑，也从未见过同学使用。
每天晚上，我捧着[笔记本电脑](https://twitter.com/yoursunny/status/601229627671973888)，到D楼空调教室使用无线网络。

研究生阶段（2011-2017年），我先后用过4台平板电脑：

* [昂达](https://twitter.com/yoursunny/status/540208326647091200)，Android，7in
* [联想](https://twitter.com/yoursunny/status/687337135184175104)，Android，10in
* 苹果，iPad Mini 2，8in
* [RCA](https://twitter.com/yoursunny/status/681610002533105665)，Windows 10，10in

平板电脑的主要用途有：

* 50% 看电视
* 25% 看论文PDF，尤其是在坐车的时候
* 15% 刷微博
* 10% 给家人打电话

Windows平板是带有键盘、HDMI接口的。
前往外地参加会议时，可以替代笔记本电脑，减少背包重量。

![photo credit: Susmit Shannigrahi](https://fastly.jsdelivr.net/gh/4th-ndn-hackathon/4th-ndn-hackathon.github.io@a64958f52f0d6e27e7ef3ad24b54783cb1511f99/images/20170325_095105.jpg)

图为本人在孟菲斯大学参加第4届命名数据网络黑客马拉松时，正在使用Windows平板电脑。
我向主办方借用了一台显示器。
桌面上是Linux界面，因为不论我用哪一台电脑，我都会通过VNC连接到办公室的Linux电脑上进行编程。
