---
title: Ranked 10000 in Code Jam 2021 Qualification, Perl + Scala + Dart
lang: en
date: 2021-03-28
tags:
- competition
- PHP
- Go
---

I spent the Saturday competing in [Google Code Jam](https://g.co/codejam) 2021 qualification round, and earned a well-rounded rank: exactly 10000.

![yoursunny rank 10000](rank.png)

## Code Jam is about Algorithms

My [first time competing in Code Jam](https://www.go-hero.net/jam/12/name/yoursunny) was 2012.
I heard about Code Jam earlier that year, from a flyer posted by the ACM student chapter at University of Arizona.
Since then, I participated in Code Jam every year, and 2021 is my 10th year competing in Code Jam.

Code Jam isn't about code or jam, but about *mathematics* and *algorithms*.
Although I'm a good network programmer and write a lot of production quality code, I struggle at algorithms especially when I'm under a time pressure.
Nevertheless, I continued competing in this annual event, so that I can remember the last bits of algorithms, and occasionally learn a few new tricks by reading through the post-round analyses.

## Code Jam is about Programming Languages

When I started out in Code Jam, I coded in PHP.
Despite all the negative words people have on PHP, I chose PHP because:

* PHP is a scripting language that does not require compiling, so that I don't have to run the compiler.
* PHP uses dynamic typing, so that I can avoid the hassle of checking and converting types.
* PHP has thousands of useful functions in its standard library, so that I can easily deal with input and output data.

In 2017, I discovered a statistics board about [contestants using multiple languages](https://www.go-hero.net/jam/17/multilang).
This discovery led to me trying to get onto that board.
Given the qualification round has 27 hours and I have plenty of time, I decided to [try a few new programming languages](https://www.go-hero.net/jam/17/name/yoursunny).
I decided to avoid PHP in that round, solved two problems with Ruby and Perl, and also used C and C++ that I'm already familiar with from other contexts.

Code Jam submission system had an upgrade in 2018, which broke that statistics board.
Nevertheless, I continued the pattern of using different programming languages, especially in the qualification round.
In the years since, **I have used 18 different languages in Code Jam**:

year | round         | languages
-----|---------------|----------------------------------
2017 | Qualification | C, Ruby, Perl, C++, Python, CPLEX
2017 | Round 1A      | Python, PHP, C++
2018 | Practice      | bash, C, Python, C++
2018 | Qualification | Go, C++, Python, PHP
2018 | Round 1A      | C, PHP, Python
2018 | Round 1B      | Go, Python
2018 | Round 1C      | C++, Python, PHP
2018 | Round 2       | PHP, bash, Python
2019 | Qualification | Rust, Lua, Python, Go
2019 | Round 1A      | bash, C++
2020 | Qualification | Julia, F#, Kotlin, R, D
2020 | Round 1A      | Python, D
2020 | Round 1B      | Python
2021 | Qualification | Perl, Scala, Dart

## Try a New Programming Language at Code Jam

So, how to try an unfamiliar, exoteric, and wacky programming languages at Code Jam?

First of all, you should not be under a time pressure.
You can learn new languages at the qualification round where there's plenty of time, or in the practice system.

Check the [Code Jam Platform](https://codingcompetitions.withgoogle.com/codejam/faq#platform) specification for a list of acceptable programming language and their versions.
In the past, Code Jam used to allow contestants to download the input, run the solution on our own computers, and then upload the output.
Since 2018, it's switched to a server-based execution environment that allows a shortlist of 34 programming languages.
We can never use [Whitespace](https://en.wikipedia.org/wiki/Whitespace_%28programming_language%29) and [Shakespeare](https://en.wikipedia.org/wiki/Shakespeare_Programming_Language) again.

Read about the languages at [Learn X in Y minutes](https://learnxinyminutes.com/).
This is my favorite website for getting a taste of programming languages.
They have a page on each language, covering the basic syntax, operators, and control flows.
When I decide to try a particular language at Code Jam, I have that page open as my main reference.

A challenging part of using a new language at Code Jam is figuring out how to do I/O.
Typically, the reference page at learnxinyminutes.com contains enough information about how to print stuff, but very little about how to parse the input.
I found myself Googling these a lot:

* how to read a line from stdin in *%LANGUAGE%*?
* how to split a line into words in *%LANGUAGE%*?
* how to convert string to integer in *%LANGUAGE%*?

I would write the code for parsing input and printing output first, before trying to implement the algorithm.

[Learn X in Y minutes](https://learnxinyminutes.com/) describes themselves as offering scenic programming language tours.
In my opinion, trying a new language at Code Jam would be akin to a quick hands-on workshop of that programming language.

## *%LANGUAGE%* May Not Work for Every Problem

There isn't a universal programming language suitable for every Code Jam problem.
Even the most popular ones, C++ and Python, have their shortcomings.
The wacky languages have even more restrictions that may limit their usefulness, or at least significantly increase the effort required to get them working for a particular problem.
There has been a few instances where I decided to give up solving a problem with my first chosen language, and rewrite in a different (familiar or unfamiliar) language.

I often have trouble with **functional programming languages**, such as Haskell and OCaml.
They require a different way of thinking, but I haven't been able to keep up.
Nevertheless, I successfully used Scala, a function programming language, this year in the *Moons and Umbrellas* problem.
Its `match` syntax is well suited for the dynamic programming required to earn a "bonus point" of that problem.

Older programming languages are also difficult to use, because they are typically strict in syntax and limited in standard library features.
I haven't been able to figure out Pascal, and I felt confusing when I tried to use Perl.

Nevertheless, there are still dozens of programming languages I haven't explored in the Code Jam system.
I'll **have fun at Code Jam, one language at a time**!

<script>
(() => {
  const LANGUAGES = ["C", "Ruby", "Perl", "C++", "Python", "CPLEX", "PHP", "bash", "Go", "Rust", "Lua", "Julia", "F#", "Kotlin", "R", "D", "Scala", "Dart"];
  const randomLanguage = () => LANGUAGES[Math.floor(LANGUAGES.length * Math.random())];
  const $elements = [];
  document.querySelectorAll("em").forEach(($em) => {
    if (!$em.textContent.includes("%LANGUAGE%")) {
      return;
    }
    const $element = document.createElement("span");
    $element.textContent = randomLanguage();
    $em.replaceWith($element);
    $elements.push($element);
  });
  if ($elements.length > 0) {
    setInterval(() => {
      for (const $element of $elements) {
        $element.textContent = randomLanguage();
      }
    }, 3000);
  }
})();
</script>
