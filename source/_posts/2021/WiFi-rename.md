---
title: Rename WiFi Interface on Ubuntu 20.04
lang: en
date: 2021-03-04
tags:
- WiFi
- Ubuntu
- RaspberryPi
discuss:
- https://dev.to/yoursunny/rename-wifi-interface-on-ubuntu-20-04-2448
---

During an experiment, I need to use three WiFi interfaces on a Raspberry Pi running Ubuntu 20.04.
In addition to Raspberry Pi's internal WiFi interface, I added two USB WiFi adapters.
Three network interfaces showed up in the system (`ip link` command), and they are named wlan0, wlan1, and wlan2 by default.

I often need to capture packets with `tcpdump`, and I often have to be type these interface names manually.
It isn't easy to remember the purpose of each network interface, so I wanted to rename the interfaces to reflect their role in my application.
However, this isn't as easy as it sounds.

## 🚫 Netplan

Ubuntu 20.04 configures network interfaces using [Netplan](https://netplan.io), so my first thought was: I can write a Netplan configuration that matches network interfaces with their MAC addresses, and assigns the desired name to each network interface.

The config file would look like this:

```yaml
network:
  version: 2
  wifis:
    uplink:
      optional: true
      match:
        macaddress: ba:fe:de:f0:b9:e4
      set-name: uplink
      access-points:
        home:
          password: rP8jKHJ64
      dhcp4: true
      dhcp6: true
      accept-ra: true
```

However, this method would not work:

```shell
$ sudo netplan apply
ERROR: uplink: networkd backend does not support wifi with match:, only by interface name
```

## 🚫 70-persistent-net.rules

Many online guides refer to a file `/etc/udev/rules.d/70-persistent-net.rules`, which is processed by udev during boot.
The file should have been created automatically by the system upon discovery of the network interfaces, and I just need to modify the interface names to the desired names.

However, this file does not exist in Ubuntu 20.04, so it's another dead end.

## ✔️ systemd.link

After carefully reading [Debian's NetworkInterfaceNames guide](https://wiki.debian.org/NetworkInterfaceNames), I found the correct way: [systemd.link](https://www.freedesktop.org/software/systemd/man/systemd.link.html).

To rename WiFi interfaces, I can create a systemd configuration file for each network interface:

```bash
echo '[Match]
MACAddress=ba:fe:de:f0:b9:e4
[Link]
Name=uplink' | sudo tee /etc/systemd/network/10-uplink.link
```

After rebooting, the network interface is renamed to what I wanted.
