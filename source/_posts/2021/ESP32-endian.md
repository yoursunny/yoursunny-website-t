---
title: Is ESP32 Big Endian or Little Endian?
lang: en
date: 2021-01-05
tags:
- ESP32
- ESP8266
- nRF52
- Arduino
discuss:
- https://dev.to/yoursunny/is-esp32-big-endian-or-little-endian-4lfk
- https://www.reddit.com/r/esp32/comments/ks40o2/is_esp32_big_endian_or_little_endian/
---

I'm programming network protocols on the Espressif ESP32 microcontroller, and I want to know: **is ESP32 big endian or little endian?**
Unfortunately, search results have only videos and forum posts and PDF; the answer, if present, is buried deep in pages and pages of discussions and irrelevant content.
So I quickly wrote a little program to determine the endianness of ESP32.

## The Straight Answer: ESP32 is Little Endian

I have determined that: the Tensilica Xtensa LX6 microprocessor in **ESP32 is little endian**.

ESP32 is little endian.
Many other processors are little endian, too:

* Intel and AMD x86 and x86_64 processors are little endian.
* Raspberry Pi and Beaglebone Black are little endian, although the underlying ARM processor may operate as big endian.
* ESP8266 is little endian. It has Tensilica Xtensa L106 microprocessor, similar to the ESP32
* nRF52 series is little endian. This includes the Adafruit Bluefruit nRF52832.

## Arduino Sketch to Determine Endianness

I used this Arduino program to determine the endianness of ESP32 CPU:

```arduino
void setup() {
  Serial.begin(115200);
  Serial.println();

  uint32_t x = 0x12345678;
  const uint8_t* p = reinterpret_cast<const uint8_t*>(&x);
  Serial.printf("%02X%02X%02X%02X\n", p[0], p[1], p[2], p[3]);
}

void loop() {
}
```

The program should print `12345678` on a big endian machine, or `78563412` on a little endian machine.
ESP32 prints:

> 78563412

So ESP32 is little endian.
