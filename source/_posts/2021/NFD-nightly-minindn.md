---
title: Use NFD nightly with Mini-NDN
lang: en
date: 2021-11-15
tags:
- NDN
- Ubuntu
- Mininet
---

[Mini-NDN](https://minindn.memphis.edu/) is a network emulation tool that enables testing, experimentation, and research on the Named Data Networking (NDN) platform.
It uses container technology to emulate a small-to-medium NDN network topology.
Each container represents a network node, in which it runs NDN Forwarding Daemon (NFD), NLSR routing daemon, and other NDN programs.
Virtual Ethernet adapters are added between containers to simulated network links.

During the recent [12th NDN hackathon](https://12th-ndn-hackathon.named-data.net/hacks.html#7-mini-ndn-improvements), I worked with my buddy [Saurab Dulal](https://dulalsaurab.github.io/) to improve Mini-NDN.
One of our achievements was a shiny new Mini-NDN installation script.
The new script can install NDN software binary packages from [named-data PPA](https://launchpad.net/~named-data/+archive/ubuntu/ppa), which saves time significantly compared to the alternative of compiling from source code.

However, a drawback of named-data PPA is that, the binary packages available there are only updated after each NFD release, which occurs a few times a year.
If it has been several months since the release, the PPA packages would be *ancient*.
They would not include the latest features, improvements, and bug fixes in the NDN codebase.

If you want to use up-to-date NDN software, but do not want to wait for the software to compile from source, I can offer another option: install the weekly automated builds from [NFD-nightly](https://nfd-nightly.ndn.today).
This article explains how to do that.

## Operating System

Mini-NDN works best in Ubuntu 20.04 operating system.
Therefore, this is the operating system you should use.

Ubuntu 20.04 is available on [Emulab](https://www.emulab.net) and many other computer networking testbeds.
If you want to use a virtual machine, Vagrant [bento/ubuntu-20.04](https://app.vagrantup.com/bento/boxes/ubuntu-20.04) image is recommended.

## Install NDN Software from NFD-nightly

Mini-NDN installation script recognizes existing NDN software installations.
If a compatible installation is found, it can use the existing packages instead of reinstalling them.
This is why we could use [NFD-nightly](https://nfd-nightly.ndn.today) packages in Mini-NDN.

For this to work, you should install NDN software from NFD-nightly, before downloading and installing Mini-NDN.
The following commands installs NFD-nightly packages and prepares them for use with Mini-NDN:

```bash
# enable NFD-nightly APT repository
echo "deb [arch=amd64 trusted=yes] https://nfd-nightly-apt.ndn.today/ubuntu focal main" \
  | sudo tee /etc/apt/sources.list.d/nfd-nightly.list

# update package lists
sudo apt update

# install NDN software from NFD-nightly
sudo apt install libndn-cxx-dev libpsync-dev nfd nlsr \
  ndn-tools ndn-traffic-generator infoedit

# stop and disable NDN service daemons
sudo systemctl disable --now nfd
sudo systemctl disable --now nlsr
sudo systemctl disable --now ndnping
```

Normally, NFD and other service processes are configured to autostart on the host computer.
These services are not needed by Mini-NDN, and they could interfere with Mini-NDN experiments.
Therefore, the last group of commands stop and disable these services.

## Download and Install Mini-NDN

With NFD-nightly packages installed, we are ready to download and install Mini-NDN.

```bash
# clone Mini-NDN git repository
git clone --recursive https://github.com/named-data/mini-ndn.git

# run the installation script
cd mini-ndn
./install.sh
```

The installation script prints what it plans to do and asks for your confirmation.
As you can see, it will use existing NDN software packages that were installed from NFD-nightly.

```text
Will download to /users/sjx/mini-ndn/dl
Will install compiler and build tools
Will use existing ndn-cxx
Will use existing NFD
Will use existing PSync
Will use existing NLSR
Will use existing NDN Essential Tools
Will use existing NDN Traffic Generator
Will use existing infoedit
Will download and install Mininet (default branch)
Will download and install Mininet-WiFi (default branch)
Will compile with 48 parallel jobs
Will install Mini-NDN package
Press ENTER to continue or CTRL+C to abort
```

If everything looks all right, press ENTER key to confirm.
The installation process would take 5~15 minutes depending on available CPU and memory, because the script still needs to compile Mininet and Mininet-WiFi dependencies.
If you do not need any WiFi emulation, you can skip Mini-WiFi and reduce installation time with this command:

```bash
./install.sh --no-wifi
```

## Check NFD Version in Mini-NDN

You can start Mini-NDN example experiment, and then type `a nfdc --version` to see NFD version:

```shell
~/mini-ndn$ sudo python examples/mnndn.py
Using topology file /usr/local/etc/mini-ndn/default-topology.conf
Starting NFD on nodes
Starting NLSR on nodes
*** Starting CLI:
mini-ndn> a nfdc --version
0.7.1-37-gf28a061c
```

## Conclusion

This article explains how to install NFD, NLSR, and other NDN software from [NFD nightly APT repository](https://nfd-nightly.ndn.today) on Ubuntu 20.04, and then use this version in Mini-NDN emulation tools.
The benefit of doing so is to take advantage of the latest features, improvements, and bug fixes in the NDN codebase, without waiting a long time to compile NDN software from source code.

This article was last verified on 2021-11-15 using a Cloudlab Utah c6525-100g PC.
