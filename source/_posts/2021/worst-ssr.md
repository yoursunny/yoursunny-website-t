---
title: The Worst Server-Side Rendering Pipeline
lang: en
date: 2021-01-16
tags:
- web
- joke
discuss:
- https://twitter.com/yoursunny/status/1316354222313680897
- https://dev.to/yoursunny/comment/187cp
---

My server side rendering pipeline:
I use nginx to invoke PHP to invoke Node.js to invoke Puppeteer to invoke Chromium.
Client side receives a screenshot of the webpage.
They can never steal my super secret HTML and JavaScript code again.

## How to have hyperlinks?

If the whole webpage is a screenshot picture, how to have hyperlinks you ask?

```html
<A HREF=browse.php>
  <IMG SRC=page.bmp WIDTH=640 HEIGHT=480 ISMAP>
</A>
```

The [`ISMAP` attribute](https://www.tutorialspoint.com/html/using_html_ismap.htm) creates a server-side image map.
You click anywhere and the server receives coordinates, like this:

```http
GET /browse.php?10,27 HTTP/1.1
Host: ssr.example.com
```

## Want a mobile site?

Sure.

I can use Selenium to control an Android emulator.
The screencast is streamed as MJPEG straight to your device!
Bandwidth consumption isn't a problem.
This is what 5G is for.

## I'm concerned with accessibility

We are fully ADA compliant.

There's a 800 phone number on the page in text format.
Blind personnel can call this number to speak to a live operator, who would read the page to them.
This is a friendlier service than Text-To-Speech systems.

User can interact with the page by spoken or keypad commands.
That's web browsing without a computer screen.

## Why are you trying to keep the HTML and JavaScript "super secret"?

I'm too ashamed to let others find out that the page is made with Microsoft FrontPage Express and Flash and ASP and jQuery, so it has to be kept secret.

---

## What others are saying

[@\_cluxter\_](https://twitter.com/_cluxter_/status/1316366844530438146):
You do realize that it's just a matter of time before someone really consider this as something cool and next gen and implements it?

[@ropsue](https://twitter.com/ropsue/status/1317429281786646529):
oh, perfect! And as an additional benefit we can go BACK to pixel perfect cross browser requirements

[@dadaistoven](https://twitter.com/dadaistoven/status/1316439474516242432):
This is sheer genius

[@\_skris](https://twitter.com/_skris/status/1316468471396352000):
I was there. I was there when it all began.
\*before someone creates this framework/engine for real\*

[@anirbanroy2002](https://twitter.com/anirbanroy2002/status/1316519662256091136):
Then search engines like Google will have to depend on Optical Character Recognition to index and rank pages!

[Peyton McGinnis](https://dev.to/sergix/comment/19egk):
This is probably the most over-engineered backend stack I've ever heard of.

[Ilya Buligin](https://dev.to/ilyabuligin/comment/187dd):
Can you show me this site? It's sound amazing :D

[Madza](https://dev.to/madza/comment/19fpe):
Hahah, made my day 🤣🤣👍
