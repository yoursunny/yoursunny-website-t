---
title: 漫步在北京 (下) | Wandering in Beijing (2)
date: 2021-03-06
tags:
- travel
- China
image: wall.jpg
---

北京历史悠久，景点众多，一篇文章写不下。
所以，接着[上篇](/t/2021/Beijing1/)，我来继续回顾我在北京看到的风景名胜。

Beijing has a long history and more attractions than what I can fit in one article.
Following the [previous part](/t/2021/Beijing1/), let me continue revisiting what I've experienced in Beijing.

## 北京的长城 | Great Wall

万里长城是中国古代建造的最为宏大的工程之一，也是中华民族精神的象征。
长城在春秋战国时期即已修筑，不过北京附近的长城大多数是明代建造的。
八达岭是北京最著名、交通比较方便的一段长城。
我在2013年6月16日游览了八达岭长城。

Great Wall of China is one of the most magnificent projects in ancient China.
It has existed since more than 2500 years ago, but most remaining Great Wall sections near Beijing were built in the Ming Dynasty.
Badaling is the most visited section of Great Wall because it's conveniently accessible by public transit.
I visited Badaling on 16 June 2013.

**八达岭长城**位于北京市延庆区，离市区的距离比较远。
游览长城那天，我一早赶往北京北站，乘坐早晨6点的“和谐长城号”列车，一个半小时后到达八达岭。
买好门票、在【不到长城非好汉】碑前留影后，我便开始攀登长城。

**Great Wall - Badaling** is located in Yanqing District of Beijing, which has quite some distance from the city center.
I had to get up early and take a train at 6 AM, arriving at Badaling Station just after 07:30.
After buying a ticket and taking a mandatory selfie in front of the "He who has never been to the Great Wall is not a true man" inscription, I started my climb onto the Great Wall.

![【不到长城非好汉】碑 "He who has never been to the Great Wall is not a true man" 2013-06-16](haohan.jpg)

我在小学课本上看到过长城的样子，就像一条两侧砌有砖墙的走廊。
到了现场我才知道，长城与走廊有很大的不同：长城是沿着八达岭山脉建造的，所以地面也像山路一样高低起伏。
登长城的两个月前，[我在登山时扭伤了脚踝](/t/2017/hiking-Arizona/)，还没有完全恢复。
加上登长城那天下雨，我只好倍加小心、慢慢行走。

I've seen a few sketches of the Great Wall on my elementary school textbook.
As I remember, it's just like a corridor with brick walls on both sides.
Only until I'm at the Great Wall did I know, the structure of the Great Wall has much difference from a corridor: it is built on a mountain range, so that the "corridor" is not flat, but has an uneven surface just like the mountain underneath.
[I injured my ankle when a rock attacked me in Sabino Canyon](/t/2017/hiking-Arizona/) two months before, so I had to walk slowly and carefully, especially when it's raining and the bricks on the Great Wall was slippery.

![长城砖墙 Great Wall 2013-06-16](wall.jpg)

长城两侧都有砖墙，但是两侧的砖墙是不一样的。
北侧的砖墙有一人多高，上部每隔几米都有一个一人宽的垛口，而下部还有一个长方形的小洞。
南侧的砖墙则比较低矮，也没有各类孔洞。
这是因为，长城是军事防御工事，敌人是从北方来袭的，而南侧是己方。
所以北侧的砖墙设有垛口，称为“瞭望口”；下面的小洞称为“射眼”，士兵可以射箭攻击来袭的敌人。
《[关于长城，你应该知道的硬知识](https://www.sohu.com/a/259736392_528910)》对此有详细的介绍。

There are, indeed, brick walls on both sides, but the two walls are different.
The northern wall is higher than a person, and contains various holes.
The southern wall is lower and has no holes.
This is because the Great Wall used to be a defense installation: enemies would come from the north.
The holes allow soldiers to gather intelligence and to shoot with bows and arrows.

## 北京的山水 | Mountains and Lakes

除了八达岭长城以外，北京还有几处登山胜地。
**八大处公园**位于石景山区，有1300米的山路，山上有8处寺庙。
我去的那天也是下雨，不过这里的路比长城好走。

Apart from Badaling, Beijing has several other hiking spots.
**Badachu Park**, located in west Beijing, has a rocky trail of 1300 meters, linking 8 temples.
I went there on 22 June 2013, another rainy day, but luckily this trail is easier to navigate than the Great Wall.

![香山公园 Fragrant Hills 2013-06-23](FragrantHills.jpg)

**香山公园**位于海淀区，名字里有“山”，公园里也有一座海拔575米的山峰。
不过，香山的主要景点不是山，而是山下的“静翠湖”。
湖面上有荷花，湖边有假山、小桥、亭台。
香山是清朝乾隆皇帝的皇家园林，而毛主席曾居住在香山脚下的双清别墅、并在那里指挥了解放战争中的渡江战役，所以香山也属于“皇帝去过的地方”与“毛主席去过的地方”。

**Fragrant Hills Park** is named after a "hill", but its main attraction is not the hill, but a lake.
The lake is beautiful: there are lotus in the water, and bridges, rockeries, and pavilions around the lake.
Fragrant Hills was an imperial garden in Qing Dynasty, and Chairman Mao commanded one battle prior to the founding of China, so that this park also qualifies for "where the kings have been" and "where Chairman Mao has been".

![盛开的荷花 lotus blossom 2011-07-27](lotus.jpg)
![我正在俯卧撑 me doing push-ups 2011-07-27](push-ups.jpg)

我是2013年6月23日去的香山，不是秋季、没有看到著名的“香山红叶”，荷花也还没开。
但是，我在**北海公园**看到了湖面上盛开的荷花。
那是2011年7月27日，正值盛夏。
我似乎还在公园的一角做了俯卧撑？

It's said that Fragrant Hills Park is prettiest in autumn when the leaves turn red, but I visited in late spring, when the leaves are still green and the lotus is not yet open.
On the other hand, when I visited **Beihai Park**, another imperial garden, I saw a full blossom of lotus on that lake.
Moreover, I took what might be the earliest photograph of me doing [push-ups](/t/2021/NDN-video-reality/).

## 北京的美食 | Beijing Foods

我在北京一般都是行色匆匆、快餐应付，不过我吃过几次地道的北京菜。

When it comes to meals, I'm usually busy with sightseeing and have to eat fast food, but I enjoyed Beijing cuisine several times.

2010年第一次出差时，公司楼下有一家**全聚德烤鸭**店。
我从小就知道北京烤鸭，所以点名要吃全聚德烤鸭。
半只烤鸭，五个人吃，每人只有两小块，蘸着甜面酱吃，稍稍感受其鲜嫩的肉质。
后来我到了美国留学，偶尔也会去中餐馆点半只烤鸭，一个人分两餐吃。

During my first business trip in 2010, there's a Quanjude restaurant near the office building.
I know about **Peking roast duck** since I was a child, so I wanted to eat the roast duck at Quanjude, a restaurant famous for this dish.
The five of us ordered half a duck, so there's only two pieces for each person.
Dipped in the sweet sauce, I can slightly feel the smooth texture and aroma of the duck.
Now I'm in USA, but I still order [Peking roast duck from Chinese restaurants](/t/2017/Tucson-restaurants/) occasionally.

此外，我还在北京吃过**东来顺饭庄**的羊肉汤，以及路边早餐车上的**煎饼馃子**。
可以带离北京的美食，则当属**茯苓夹饼**：两层薄薄的烙饼，夹着一层蜂蜜做的馅料，是一种北京的传统小吃。
我每次去北京都要买几袋回来，爸爸妈妈去北京出差时也会帮我带一点。

I also had **lamb soup** in Dong Lai Shun, and **mung bean flour pancake** from a street vendor.
When I leave Beijing, I would pick up some **Fuling jiabing**, a snack made from honey and nuts wrapped in two paper-thin pancakes.
Knowing my love for this snack, my parent would also buy some for me when they go to Beijing.

## 北京的住宿 | Hotels

我在北京住过公司推荐的四星级酒店，也住过廉价的旅馆。
离天安门广场步行只有5分钟的地方，就有一家汉庭海友客栈，那是我住过的最小的客房：开门一张单人床，比火车卧铺大一点点。

I stayed in various hotels in Beijing, including the 4-star Crown Plaza and cheap motels.
There is a motel 5-minute from Tiananmen Square, where I experienced the smallest room I ever stayed: the room has a twin-size bed with a narrow aisle, only slightly larger than [Amtrak roomette](/t/2017/cross-country-move/).

![汉庭海友客栈(前门店) Hanting Motel 2013-06-28](Hanting.jpg)

而印象最深刻的，则是海淀区的**西山书院**。
那是2013年，我到工业区做科研，看着地图订了这家宾馆。
地图上这家宾馆就在路边，到了现场才知道，宾馆建在山顶、地图标记的地方是上山的入口。
我打电话联系后，只好拖着行李艰难上山。

The most memorable place was **Xishan Villa** in Haidian District.
I was assigned for a research project in the industrial park, and I selected this hotel as it's one of the closest according to maps.
What I didn't know was that, the hotel is located atop of a hill, and the map marker indicates the entrance to the road leading to the hotel.
After making a few phone calls, I had to drag my luggage up this hill.

西山书院是为企业集中培训设计的，可以让大家远离“世俗”。
我入住的时候，宾馆里几乎没有客人，餐厅的营业时间、供应品种也相当有限。
我只好每天上下山，走路一个多小时往返，早餐只能在山下小店买馒头充饥。

Xishan Villa is designed for company retreats, where people can stay away from the crowded city and relax in the quiet mountain.
When I stayed there, there's no guest other than myself, and the dining hall had limited hours and supplies.
Every day, the trip between my hotel room and the laboratory was a 1-hour hike, and my only breakfast option is buying steamed pork buns from a street vendor.

## 北京的交通 | Transportation

根据2010年人口普查，北京有1670万人口，是中国的第二大城市。
所以，北京的交通拥堵也可想而知。
不过，我每次去北京，旅游主要安排在周末，工作日出行则尽量错开上下班高峰，所以堵车对我基本没有影响。

Beijing is the second largest city in China, with a population of 16 million according to 2010 census.
Consequently, traffic congestion is a serious problem.
However, I arranged my trips during weekends or off-peak hours, to lessen the stress from traffic.

北京的公共交通相当完善，2011年时建有9条地铁线路。
北京的三个火车站都可以乘坐地铁到达，而首都国际机场也有轻轨通达。
乘坐地铁只要投币2元，就可以不论距离随便乘坐，列车频次也足够我的出行需求。
只有一次我在地铁上遇到下班高峰，拥挤程度略低于上海地铁。

Public transportation is complete in Beijing, with 9 subway lines as of 2011.
All three major train stations are reachable by subway, and there's a lightrail line to the airport.
Each subway ride costs 2 coins regardless of distance, and the trains are frequent enough for my needs.
I only encountered rush hour once on the subway, but it's less crowded than Shanghai subway.

但是，北京并不是一个适合步行的城市。
虽然所有的道路都有人行道、红绿灯，但是除了紫禁城、天安门广场以外，步行前往其他景点太远。

However, I do not consider Beijing a "walkable" city.
Although Beijing is well equipped with sidewalks and pedestrian traffic lights, most attractions other than the Forbidden City and Tiananmen Square are too far to walk.

## 后记 | Final Words

我去过北京3次，其中旅游共9天，还有很多地方没有去过。
如果我再去北京，还想去看看十三陵、慕田峪长城，以及国家博物馆尚未去过的展区。

I've been to Beijing 3 times so far, with 9 days sightseeing.
There are many attractions that I missed, such as Ming tombs, Great Wall Mutianyu section, and additional exhibits in the National Museum.
I want to visit Beijing again if I have a chance.
