---
title: Wandering in Bay Area
lang: en
date: 2021-08-12
tags:
- travel
- geocaching
image: CableCar.jpg
---

As the pandemic subdues, real world travels are resuming, but I can still do virtual travels.
This article remembers my six trips to [San Francisco Bay Area](https://en.wikipedia.org/wiki/San_Francisco_Bay_Area), a populous region in Northern California.

![San Francisco seen from Twin Peaks, 2012-06-05](TwinPeaks.jpg)

## San Francisco as a Tourist

My first visit to the Bay Area was in June 2012, with my roommate John.
We took a train to [Los Angeles](/t/2020/LosAngeles/), and then travelled to San Francisco on [Chinatown bus tour](https://web.archive.org/web/20120617190822/http://www.seagullholiday.com/en/journey.aspx?JourneyID=1170).

![Golden Gate Bridge seen from east, 2012-06-05](GoldenGate.jpg)

The bus tour squeezed all the major tourist attractions within one day:

* Golden Gate Bridge
* Fisherman Wharf
* Lombard Street
* Palace of Fine Arts
* St Mary Cathedral
* Twin Peaks
* Castro District

My favorite is [Lombard Street](https://en.wikipedia.org/wiki/Lombard_Street_%28San_Francisco%29), which is known as "the crookedest street in the world" (but [not the only one](https://www.pairrec.com/blog/vermont-crooked-street)).
Standing on the sidewalk, I can see a series of eight switchbacks decorated with trees and flowers, with vehicles driving through these sharp turns slowly and carefully.

![Lombard Street seen from bottom, 2012-06-05](Lombard.jpg)

## Computer History Museum

San Francisco Bay Area is home to Silicon Valley, a global center for high technology and innovation.
During my final year at [grad school](/t/2017/six-years-in-arizona/), I was invited to several on-site job interviews at tech companies in this region.
I wasn't able to get into those highly competitive positions, but I received five free trips to the Bay Area.

Typically, an interview is a two-day trip.
On the first day, if I pick an early morning flight, I can arrive in the Bay Area around lunch time, and have an afternoon for sightseeing.
The interview itself starts in the morning of the second day, and then I fly back to Tucson in the evening.

One of the places I visited in one of these afternoons is [Computer History Museum](https://computerhistory.org/), a museum that explores the evolution of our technological world.
In its main exhibit **Revolution: The First 2000 Years of Computing**, I learned that:

* My grandma's abacus (算盘) is a form of computer.
* Some computers are analog, not digital.
* Not every program is on a disk or punched cards: they could be wires on a patch panel.
* Some memory device do not support random access: delay lines and magnetic drums are serial access.
* Some floppy disks are larger than 5.25-inch.
* You need to place your telephone handset on a MODEM, instead of plugging in the telephone wire.

[![abacus, Computer History Museum, 2017-06-15](abacus.jpg)](https://www.computerhistory.org/revolution/calculators/1/1)
[![EAI patch panel, Computer History Museum, 2017-06-15](EAI.jpg)](https://www.computerhistory.org/revolution/analog-computers/3/152)
[![ADC-300 modem, Computer History Museum, 2017-06-15](MODEM.jpg)](https://www.computerhistory.org/revolution/networking/19/371)
[![IBM 1401, Computer History Museum, 2017-06-15](IBM1401.jpg)](https://computerhistory.org/exhibits/ibm1401/)

My favorite cabinet in this museum is [the step-by-step process](https://www.computerhistory.org/revolution/digital-logic/12/288) transforming a semiconductor crystal into an integrated circuit.
I'm also excited to see a few "big" computers, such as [IBM System/360](https://www.computerhistory.org/revolution/mainframe-computers/7/161) and [UNIVAC](https://www.computerhistory.org/revolution/early-computer-companies/5/106).
There are two functional room-sized computers, [PDP-1](https://computerhistory.org/exhibits/pdp-1/) and [IBM 1401](https://computerhistory.org/exhibits/ibm1401/), at the museum.
However, there wasn't a demo scheduled on the day of my visit so I couldn't see these computers in action.

## Hiller Aviation Museum

Another Bay Area museum I visited is [Hiller Aviation Museum](https://www.hiller.org/).
It is an aircraft history museum, with a collection of about 50 aircraft.

Most aviation museums start with a (replica of) Wright Flyer, but I saw something older at this museum: an [1869 Aviator](https://www.hiller.org/event/avitor/).
The Aviator had no pilot and was a lighter-than-air flight, but it's 34 years older than Wright Flyer.

My favorite exhibit at this museum is the cockpit of a [Boeing 747-100](https://www.hiller.org/event/boeing-747/) airliner.
The docent let me sit in the captain's seat, and then introduced to me some of the controls.

![me in Boeing 747 cockpit, Hiller Aviation Museum, 2016-11-13](B747.jpg)

Hiller Aviation Museum is located adjacent to San Carlos Airport, a real-life general aviation airport.
After I left the museum, I went to the end of SQL Runway 30, [watching airplanes taking off and landing](https://youtu.be/obY4m3svEVI).

## BART, Caltrain, and Cable Car

San Carlos is a general aviation airport for light airplanes.
As for major airports for commercial traffic, I've been to three: San Francisco (SFO), San Jose (SJC), and Oakland (OAK).
If I was not provided with a rental car, I prefer mass transportation rather than taxis to reduce my carbon footprint.

I can leave SFO airport by [Bay Area Rapid Transit (BART)](https://www.bart.gov/), a subway system with trains going to downtown San Francisco every 15 minutes.
I figured out BART pretty quickly, because it's very similar to the subway systems in Shanghai and Beijing.

SJC and OAK airports did not have BART service when I visited in 2017, but they are close to [Caltrain](https://www.caltrain.com/), a commuter rail system that runs around the San Francisco Peninsula, with trains every 30 minutes.
Caltrain is full of traps to a visitor like me: I need a Clipper card for fare payment, and I must tap the card at the station before boarding a train and tap again at the destination after exiting the train.
Forgetting to tap or double tapping would result in either a fine or being charged the maximum fare.

![Powell & Mason line cable car, 2017-02-20](CableCar.jpg)

Rants aside, the most unique ground transportation system in the Bay Area has to be the [San Francisco cable car system](https://www.sfmta.com/getting-around/muni/cable-cars), the world's last manually operated cable car system.
I rode the cable car in the rain, visited the [Cable Car Museum](http://www.cablecarmuseum.org/) to learn its history and operations, and then stood at a street corner to watch how gripman starts and stops the cable car at a hilltop station.
It's an interesting experience to see, learn, and ride this national historic landmark.

<iframe width="560" height="315" src="https://www.youtube.com/embed/7mbiHgTtm8Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## EarthCache in Bay Area

I [became a geocacher](/t/2017/start-geocaching/) in 2013, which changed how I travel.
I tried to include a bit of geocaching in each of my four Bay Area trips in 2016 and 2017.

![Don Edwards San Francisco Bay National Wildlife Refuge, 2016-11-13](DonEdwards.jpg)

The first park I visited is [Don Edwards San Francisco Bay National Wildlife Refuge](https://www.fws.gov/refuge/don_edwards_san_francisco_bay/).
I selected this park because it has an unusual shape on Google Maps: dozens of thin curved lines surrounding dozens of small "lakes".
When I arrived on site, I understood how the shape came to be: the refuge stretches along a marshy shoreline, where each "lake" is a pond, and each curved line is a levee that separates adjacent ponds and provides a hiking trail.
There are 4 EarthCaches in this park, asking me to view and measure various sediments deposited in the levees and surrounding area.
These are the first geocaches I logged in Northern California.
I also hiked in the adjacent [Coyote Hills Regional Park](https://www.ebparks.org/parks/coyote_hills/) on the same day, and found my first physical geocache in Northern California.

![Coyote Hills Regional Park, 2016-11-13](CoyoteHills.jpg)

Apart from these, I also visited the oldest passenger railroad station in California, and found out what's behind Facebook's iconic 👍 sign.

![Sun microsystems sign, 2017-06-21](Sun.jpg)
![Facebook thumbs up sign, 2017-06-21](Facebook.jpg)

## Goodbye, Bay Area

After six visits to the San Francisco Bay Area, I [moved to east coast](/t/2017/cross-country-move/), and now lives very far from Northern California.
Nevertheless, I still remember my short few days in the Bay Area, especially when I watch [Silicon Valley HBO](https://www.hbo.com/silicon-valley).
