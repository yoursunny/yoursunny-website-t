---
title: NFD nightly went APT
lang: en
date: 2021-01-18
tags:
- NDN
- git
- Debian
- Ubuntu
- RaspberryPi
---

**NOTICE**
Instructions in this article are outdated.
Please see latest information in [NFD nightly usage](/t/2021/NFD-nightly-usage/).

Last year, I started building [NFD nightly packages](/t/2020/NFD-nightly/) in GitHub Actions.
So far, installation is a manual procedure: the user must manually download the ZIP files from [nfd-nightly.ndn.today](https://nfd-nightly.ndn.today), decompress them, and figure out the dependency among various `.deb` packages.
Starting today, I'm publishing NFD nightly packages in an APT repository, and you can install them with `apt-get` command.

## Add the NFD nightly APT repository

Add the repository with the command that matches your platform:

```bash
# Ubuntu 18.04 (bionic), amd64 (laptops and servers)
echo "deb [trusted=yes] https://nfd-nightly-apt.ndn.today/ubuntu bionic main" \
  | sudo tee /etc/apt/sources.list.d/nfd-nightly.list

# Ubuntu 20.04 (focal), amd64 (laptops and servers)
echo "deb [trusted=yes] https://nfd-nightly-apt.ndn.today/ubuntu focal main" \
  | sudo tee /etc/apt/sources.list.d/nfd-nightly.list

# Debian 10 (buster), amd64 (laptops and servers)
echo "deb [trusted=yes] https://nfd-nightly-apt.ndn.today/debian buster main" \
  | sudo tee /etc/apt/sources.list.d/nfd-nightly.list

# Debian 10 (buster), armv7 (Raspberry Pi 3 or 4)
echo "deb [trusted=yes] https://nfd-nightly-apt.ndn.today/debian buster main" \
  | sudo tee /etc/apt/sources.list.d/nfd-nightly.list

# Debian 10 (buster), armv6 (Raspberry Pi Zero W)
echo "deb [trusted=yes] https://nfd-nightly-apt.ndn.today/raspberrypi buster main" \
  | sudo tee /etc/apt/sources.list.d/nfd-nightly.list
```

If your operating system and CPU architecture combination is not listed, it is not supported by NFD nightly.
See the [previous post](/t/2020/NFD-nightly/) on how to request a new platform.

## Install NFD nightly packages

First, you should update the package lists:

```bash
sudo apt update
```

Then, you can execute `apt install` command to install desired packages, for example:

```bash
sudo apt install libndn-cxx-dev nfd ndnping ndnpeek ndn-dissect
```

## Switch between NFD installation methods

There are several methods to install NFD and related programs, including:

* NFD nightly APT repository (this article)
* NFD nightly [downloads](/t/2020/NFD-nightly/)
* [named-data PPA](https://launchpad.net/~named-data/+archive/ubuntu/ppa)
* [source code](https://docs.named-data.net/NFD/current/INSTALL.html)

Packages created with different methods are possibly incompatible with each other.
Therefore, if you want to switch between installation methods, it's recommended to delete the previous installation.

```bash
# delete packages from NFD nightly APT repository
sudo apt purge libndn-cxx
sudo rm -f /etc/apt/sources.list.d/nfd-nightly.list

# delete packages from NFD nightly downloads
sudo apt purge libndn-cxx

# delete packages from named-data PPA
sudo apt purge libndn-cxx
sudo rm -f /etc/apt/sources.list.d/named-data-ubuntu-ppa-*.list

# delete source code installation
# (execute in each source code directory)
sudo ./waf uninstall
```
