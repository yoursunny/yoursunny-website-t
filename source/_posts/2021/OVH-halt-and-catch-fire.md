---
title: "OVH Strasbourg: Halt and Catch Fire, Data Uploaded to the Cloud"
lang: en
date: 2021-03-12
tags:
- hosting
image: smoke.jpg
discuss:
- https://dev.to/yoursunny/ovh-strasbourg-halt-and-catch-fire-data-uploaded-to-the-cloud-4j19
- https://lowendtalk.com/discussion/170343/down-ovh-sbg-lots-and-lots-of-tears/p1
---

2021-03-10, an OVH Cloud data center in Strasbourg, France caught on fire.
Thousands of servers have been destroyed by fire.
Thousands more are currently unavailable due to power cut, and will remain offline for several more days.

Data stored in those servers have been uploaded to the cloud via black smoke.

![a building is burning with black smoke rising to the sky](smoke.jpg)

According to Hacker News, a Dev mistakenly invoked the [Halt and Catch Fire](https://en.wikipedia.org/wiki/Halt_and_Catch_Fire_%28computing%29) instruction on an Uninterrupted Power Supply unit, causing this incident.

Chairman of OVH cloud advised clients to activate their [disaster recovery plans](/t/2021/disaster-recovery-no-tears/), such as restoring off-site backups to a new cloud server.
Some clients are crying because they did not have backups, or they stored their backups on another machine in the same data center.
Other clients experienced no downtime because they designed their systems for *datacenter scale redundancy*.

[![datacenter scale](https://imgs.xkcd.com/comics/datacenter_scale_2x.png)](https://www.explainxkcd.com/wiki/index.php/1737:_Datacenter_Scale)
