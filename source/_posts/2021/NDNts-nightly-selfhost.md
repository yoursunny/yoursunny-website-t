---
title: Self-Hosted NDNts Nightly Build
lang: en
date: 2021-01-02
edited: 2024-03-06
tags:
- NDNts
- NDN
- JavaScript
---

**NDNts nightly build** is a set of NPM-compatible tarballs compiled automatically from the development branch of [NDNts](/p/NDNts/), Named Data Networking (NDN) libraries for the modern web, distributed on [https://ndnts-nightly.ndn.today](https://ndnts-nightly.ndn.today) website.
Users can install NDNts nightly build following [these instructions](/t/2020/NDNts-nightly/).

However, this website only stores the latest version of NDNts packages.
This has been causing installation conflicts when NPM tries to look for previous versions.
Moreover, as I have declared, [I don't care much about backwards compatibility](/t/2019/NDNts-intro/).
With NPM, all published versions are stored indefinitely, so you can continue using an older version without being affected by breaking changes.
On the other hand, once a new nightly build is uploaded, the previous version is overwritten and no longer available for downloads.
You are then forced to cope with the breaking changes I introduce from time to time, possibly at higher frequency than you would like to.

Today, I'm introducing two methods for self-hosting NDNts nightly build.
Both methods allow you to build a specific version of NDNts codebase from a checkout of the [NDNts monorepo](https://github.com/yoursunny/NDNts), and generate a set of tarballs that you can host locally on a server under your control.
Afterwards, you can install NDNts packages from this server, without relying on my website and without being affected by my breaking changes.

## Self-Hosted NDNts on an HTTP Server

> This section was latest updated on 2024-03-06 to reflect latest changes.

The following steps allow you to create and host tarballs of one specific version of NDNts.

```bash
# clone NDNts monorepo
git clone https://github.com/yoursunny/NDNts.git
# you may checkout a specific branch / commit

# install dependencies
corepack pnpm install

# you can make code changes; you should commit these changes but you do not have to push

# compile TypeScript into JavaScript
corepack pnpm build

# generate packages
NDNTS_PUBLISH_URI=http://localhost:8000 bash mk/publish-nightly.sh

# start a webserver
cd mk/nightly-output
python3 -m http.server
```

You can then access this website at [http://localhost:8000](http://localhost:8000).
It would be similar as [https://ndnts-nightly.ndn.today](https://ndnts-nightly.ndn.today).

You are able to control when to upgrade the NDNts packages on this webserver to a new version, but when you upgrade, [the same caveats](/t/2020/NDNts-nightly/) of NDNts nightly build apply.

## Self-Hosted NDNts on a Private NPM Registry

> This section is outdated as of 2024-03-06.

It is also possible to build and publish NDNts into a self-hosted NPM registry.
Multiple versions of NDNts can be stored in the private NPM registry, and you can switch between them without causing conflicts.
We will be using [Verdaccio](https://verdaccio.org/), a lightweight open source private NPM proxy registry.

```bash
# install Verdaccio
npm install -g verdaccio
# for localhost usage, it is unnecessary to change any configuration
# please read Verdaccio documentation on how to securely deploy it on a server
# in particular, you should setup HTTPS and restrict package publishing to admins

# start Verdaccio
verdaccio

# execute the next steps in another console window

# backup existing .npmrc file
[[ -f ~/.npmrc ]] && mv ~/.npmrc ~/npmrc-backup

# select Verdaccio registry
npm set registry http://localhost:4873

# create an account on Verdaccio
# you can use any username, password, and email address
npm login

# clone NDNts monorepo
git clone https://github.com/yoursunny/NDNts.git
# you may checkout a specific branch / commit

# install dependencies
npm run bootstrap

# you can make code changes; you should commit these changes but you do not have to push

# build and publish NDNts
bash mk/publish-release.sh -alpha.1
# the published version number is the last commit date plus the specified version suffix
# if last commit was made on 2021-01-02, this command would create version 0.0.20210102-alpha.1
# if you publish more than once on the same day, you must provide a different version suffix

# (optional) revert the git commit generated for the version
git reset --hard HEAD^1

# restore original .npmrc file
[[ -f ~/npmrc-backup ]] && mv ~/npmrc-backup ~/.npmrc
```

The published packages are stored in `$HOME/.config/verdaccio` directory.
You'll notice that Verdaccio is also storing other NPM packages here, because it is a caching registry.
You may delete those packages after stopping Verdaccio, and only keep the `@ndn` directory.

Then, you can install NDNts packages from this private NPM registry:

```bash
# select Verdaccio for @ndn scope only
npm config set @ndn:registry http://localhost:4873

# install a package, such as @ndn/packet
npm install @ndn/packet

# check package version
jq '.dependencies["@ndn/packet"]' package.json
# you should see the version you published earlier
```
