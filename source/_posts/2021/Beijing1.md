---
title: 漫步在北京 (上) | Wandering in Beijing (1)
date: 2021-02-21
tags:
- travel
- China
image: PalaceMuseum.jpg
---

北京是中国的首都，我在2010、2011、2013年去过三次。
在2021年第一次“虚拟旅游”中，我来回顾一下我在北京看到的风景名胜。

Beijing is China's capital.
I visited Beijing three times, in 2010, 2011, and 2013.
I'd like to revisit my sightseeing trips to Beijing in first "virtual travel" of 2021.

## 运动员去过的地方 | Where the Athletes Have Been

2008年，北京举办了夏季奥林匹克运动会，其中主要的两个体育场馆“鸟巢”、“水立方”特别令人瞩目。
2010年8月7日，“大约”是奥运会开幕两周年，我来到北京奥林匹克公园参观鸟巢、水立方。

Beijing is the host city of 2008 Summer Olympics.
The most notable place of 2008 Olympics was "Bird Nest" and "Water Cube".
On August 7, 2010, approximately two years since the opening ceremony of 2008 Olympics, I came to Beijing Olympic Green to visit these two venues.

![国家体育场内部球场、跑道、看台 interior of National Stadium 2010-08-07](NationalStadium.jpg)

鸟巢，即**国家体育场**，是2008年北京奥运会的主体育场、开闭幕式举办地。
其名称“鸟巢”是来源于钢结构搭建成的造型，在晚上开启红橙色灯光时远观特别漂亮。
鸟巢里面则是草地足球场、塑胶跑道、一排排的看台，与其他体育场差不多，主要的特征只有跑道上及运动员入口处的“Beijing 2008”字样。

The Bird's Nest, aka **Beijing National Stadium**, is the main stadium of 2008 Olympics, and the venue of both the opening ceremony and the closing ceremony.
Its name, the Bird's Nest, comes after the unique shape constructed by steel beams, which looks beautiful at night from afar with red and orange lighting.
Inside the stadium is a grassy soccer field, running tracks, and rows of seating, which are similar as other stadiums.
The only distinguishing feature is the "Beijing 2008" symbol painted on the tracks and the athlete entrances.

![我站在国家游泳中心比赛泳池前的看台栏杆前 me standing in front of the main swimming pool of National Aquatics Center 2010-08-07](NationalAquatics.jpg)

水立方，即**国家游泳中心**，位于鸟巢的西面，是2008年北京奥运会的水上项目比赛场馆。
建筑外表由3000多个膜气枕组成，也是在晚上开启蓝色灯光时最好看。
水立方里面可以看到奥运会游泳、跳水项目比赛使用过的泳池，平静的水面上没有一丝波纹，但是我可以想像“飞鱼”菲尔普斯在池中比赛的场景。

Water Cube, aka **Beijing National Aquatics Center**, located to the west of the Bird's Nest, is the venue for aquatics competitions in 2008 Olympics.
The building surface consists of more than 3000 "bubbles", which looks the prettiest at night when lit in blue.
Inside the Water Cube, I can see the pools for swimming and diving events.
The water is calm and waveless, but I can imagine Michael Phelps flying through this pool during the Games.

![玲珑塔 Ling Long Pagoda 2013-06-28](LingLong.jpg)

奥林匹克公园的北侧还有一座**玲珑塔**，在奥运会期间提供电视演播、转播功能。
到了晚上，玲珑塔上不但有灯光，而且灯光的颜色会不断变换，是我最喜欢的建筑。

On the north side of the Olympic Green lies the Ling Long Pagoda, which was a television studio during the Games.
In the evening, the pagoda is lit with changing colors, making it my favorite building in the area.

## 皇帝去过的地方 | Where the Kings Have Been

北京自1620年、元朝的时候就是中国的首都。
在北京，我可以看到许多皇帝去过的地方。

Beijing has been China's capital since 1620 in Yuan Dynasty.
In Beijing, I can trace the footsteps of China's former kings.

![从景山公园拍摄的故宫博物院 Palace Museum photographed from north in Jingshan Park 2011-07-30](PalaceMuseum.jpg)

最著名的地方当属紫禁城，现称**故宫博物院**，是明清两朝、二十四位皇帝的皇宫。
故宫博物院里有几十个宫殿，总计有8704间房，比世界上最大的酒店（7351间）还多。

The most famous of all has to be the Forbidden City, which is the imperial palace of 24 kings in Chinese history.
Now known as **Palace Museum**, there are dozens of buildings and a total of 8704 rooms, more than the world's largest hotel (7351 rooms).

<iframe width="560" height="315" src="https://www.youtube.com/embed/-9BdCXjd84o" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

博物院里收藏了大量文物，其中我最喜欢的是位于奉先殿的“钟表馆”，里面陈列了数百件老式机械钟表。
我在钟表馆参观时正逢每天两次的演示时间。
讲解员给一座宝塔形的台钟上发条后，只见宝塔下的十多个金色的小人开始旋转，整个宝塔徐徐升起，钟内响起了叮叮当当的报时声，最后宝塔又慢慢降落。
我和周围的十多位观众看了演示，都啧啧称奇。

Palace Museum has a huge number of historical artifacts.
My favorite is an exhibit of mechanical clocks and watches.
I was lucky enough to see a demonstration of a pagoda-shaped desk clock.
After the docent winds up the mainspring, dozens of gold-plated figurines rotates around the pagoda like a carousel, the whole pagoda rises up, and out comes the chimes announcing the time.
A minute later, the rotation slows down, and the pagoda slowly descends to its original position.
The crowd was amazed at the demonstration of this well-maintained timepiece.

![我站在天坛公园祈年殿前 me in front of The Hall of Prayer for Good Harvests 2011-07-26](TempleOfHeaven.jpg)

除此之外，我还在北京的皇家园林**颐和园**观看荷花，在北京的皇家祭坛**天坛**体会顶天立地的感觉。

Apart from the Forbidden City, I viewed lotus blossom in the imperial garden **Summer Palace**, and imagined the heavenly feeling in the **Temple of Heaven**.

## 毛主席去过的地方 | Where Chairman Mao Has Been

1949年10月1日，毛泽东主席在天安门城楼上宣布中华人民共和国成立，天安门广场上升起了第一面五星红旗。
从此，**天安门广场**成了中国的象征。
我三次去北京，都去过天安门广场。

The founding ceremony of People's Republic of China was held on 01 October 1949.
On that day, Chairman Mao Zedong proclaimed the founding of the nation, and the flag of China was raised for the first time in Tiananmen Square.
Since then, **Tiananmen Square** became a symbol of China.
I visited Tiananmen Square during all three visits to Beijing.

天安门广场的北端设有一根国旗杆，每天日出、日落的时候会进行升旗、降旗仪式。
我并未打算去观摩升旗仪式，但是有一天我在早晨四点多就醒了，一看离升旗时间还有一小时，我决定乘坐出租车去看升旗。
待我经过安检、进入广场时，离升旗只有10分钟了，广场上人满为患。
我只能在远处听着国歌、看着五星红旗冉冉升起，虽然心里激动，但是看不到仪仗队的英姿。

There is a flag pole on the north end of Tiananmen Square.
The flag of China is raised and lowered every day at sunrise and sunset.
Attending a flag raising ceremony wasn't in my plan as I usually sleep late, but one day I woke up at 4AM in a hotel room, and I thought "why not"?
So I [hailed in a taxi](/t/2017/hailing-taxis/) right away, and arrived at Tiananmen Square in time for the 05:19 AM flag raising ceremony.
By the time I arrived and passed security checks, there were already thousands of visitors in front of me, and I was only able to get a spot far from the flag pole.
I felt excited when the national anthem was played and the flag rose to the top, but I couldn't see the color guard in action.

![降旗仪式 flag lowering ceremony 2011-07-26](flag.jpg)

降旗仪式我看过两次。
由于没有音乐，观看降旗仪式的游客较少。
我可以站在前排，看到国旗护卫队战士迈着整齐划一的脚步，干净利落的执行降旗任务。

I've been to flag lowering ceremonies twice.
There were fewer tourists in attendance, which allowed me to stand in the front row.
I witnessed 38 color guard officers walk in formation, and carefully performed the important task.

![开国大典话筒 microphone used during the founding ceremony of China 2013-06-29](microphone.jpg)
![中央人民政府牌匾 Central People's Government placard 2013-06-29](CentralPeoplesGovernment.jpg)

我没有进入天安门城楼、毛主席纪念堂参观。
但是，我在**国家博物馆**看到了毛主席在开国大典上用过的话筒、以及最早的“中央人民政府”牌匾。

I didn't enter the Tiananmen gate or the Chairman Mao Memorial Hall due to time constraints.
However, I visited the **National Museum of China**, where the microphone used by Chairman Mao at the founding ceremony of the nation is on display, as well as the original placard of the Central People's Government.

## 未完待续 | To Be Continued

在[下一篇文章](/t/2021/Beijing2/)里，我将回顾北京的长城、山水，以及我在北京街头看到的民俗文化、美食小吃。

In my [next article](/t/2021/Beijing2/), I'll revisit the parks, remember my experience climbing the Great Wall of China, and review the folk culture and local eats that I witnessed on Beijing streets.
