---
title: NFD nightly APT repository
lang: en
date: 2021-10-30
tags:
- NDN
- Debian
- Ubuntu
- RaspberryPi
---

This article contains instructions of **NFD nightly APT repository**, which provides automated builds of [NDN Forwarding Daemon (NFD)](https://docs.named-data.net/NFD/current/) and related software.

This article was latest updated on 2021-10-30 to reflect latest changes.

## Instructions

To install NDN software from NFD nightly APT repository:

1. If you have previously installed NDN software from named-data PPA or source code, you need to delete them to avoid conflict.
   See *switch between installation methods* section below.

2. Visit <https://nfd-nightly.ndn.today> webpage, choose your operating system and CPU architecture, and you'll get a setup command.

   Run this setup command in the terminal, which adds NFD-nightly as a package source.

3. Update package lists:

    ```bash
    sudo apt update
    ```

4. Install desired packages, such as:

    ```bash
    sudo apt install nfd ndnping ndnpeek infoedit
    ```

   You can see available packages on <https://nfd-nightly.ndn.today> webpage.

## Switch between installation methods

There are several methods to install NFD and related programs, including:

* NFD nightly APT repository (this article)
* [named-data PPA](https://launchpad.net/~named-data/+archive/ubuntu/ppa)
* [source code](https://docs.named-data.net/NFD/current/INSTALL.html)

Packages created with different methods are possibly incompatible with each other.
Therefore, if you want to switch between installation methods, it's recommended to delete the previous installation.

```bash
# delete packages from NFD nightly APT repository
sudo apt purge libndn-cxx
sudo rm -f /etc/apt/sources.list.d/nfd-nightly.list

# delete packages from named-data PPA
sudo apt purge libndn-cxx
sudo rm -f /etc/apt/sources.list.d/named-data-ubuntu-ppa-*.list

# delete source code installation
# (run in each source code directory)
sudo ./waf uninstall
```

## Q&A

Why should I use **deb** packages instead of building from source code?

* It is faster to install **deb** packages than trying to build from source code.
* You can uninstall cleanly with `sudo apt-get remove` command.
* [`sudo ./waf install` can conflict with packages](https://wiki.debian.org/DontBreakDebian#A.27make_install.27_can_conflict_with_packages) and break your system.

What are the differences between [ppa:named-data/ppa](https://launchpad.net/~named-data/+archive/ubuntu/ppa) and NFD-nightly?

* Update frequency:
  * PPA: after each NFD release, which occurs a few times per year.
  * NFD-nightly: weekly.
* Supported platforms:
  * PPA: recent Ubuntu versions.
  * NFD-nightly: Debian and Ubuntu (LTS only).
* Quality:
  * PPA: production.
  * NFD-nightly: experimental.
* Docker friendly:
  * PPA: no, [`sudo` should not be used in Docker container](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#user).
  * NFD-nightly: yes.
* Dependencies:
  * PPA: outdated and excessive (Python 2 and `libboost-all-dev`).
  * NFD-nightly: lean (Python 3 and specific Boost libraries).
* ndn-cxx dependency:
  * PPA: depends on `>=` ndn-cxx version, which may cause runtime errors due to ABI breakage.
  * NFD-nightly: depends on `==` ndn-cxx version, which ensures ABI compatibility.

How are these packages built?

* They are built on [GitHub Actions](https://docs.github.com/en/actions) using Docker containers.
* Non-amd64 architectures are compiled in [Balena](https://www.balena.io/docs/reference/base-images/base-images-ref/) images.

A package isn't working?

* Download errors (404 not found, connection timeout, SSL handshake error, etc):
  * Wait a few minutes, run `sudo apt update` again.
  * This is likely a temporary server issue.
    Alert me on Twitter, mention `@yoursunny`.
* Installation error (`dpkg` fails to configure package):
  * This is possibly a bug in installation script, which originates from [ppa-packaging](https://github.com/named-data/ppa-packaging) but some are patched in NFD-nightly.
  * Report via [GitHub Issues](https://github.com/yoursunny/NFD-nightly/issues), and be sure to include information about your platform.
* "Illegal Instruction" while running a program:
  * This is most likely a bug in NFD-nightly build process.
  * Report via [GitHub Issues](https://github.com/yoursunny/NFD-nightly/issues), and be sure to include information about your platform.
* Other runtime errors (packet drop, prefix registration rejected, etc):
  * These are not caused by NFD-nightly.
  * Report on [nfd-dev mailing list](https://named-data.net/codebase/platform/support/mailing-lists/).

How to request a new platform?

* I can add newer Debian or Ubuntu releases.
  * NFD needs to be compatible with the default compiler and Boost versions installed from APT.
* I cannot create packages for Linux distributions that do not use APT.
  * I'm a geocacher and I cannot stand the word `dnf` ![DNF](https://www.geocaching.com/images/logtypes/3.png).
* I can add more CPU architecture, if [Balena](https://www.balena.io/docs/reference/base-images/base-images-ref/) has an image for that.
* [Open an issue](https://github.com/yoursunny/NFD-nightly/issues) to discuss.

How to request a new package?

* I can add packages if packaging scripts are available.
  * If not, [contribute to ppa-packaging](https://gerrit.named-data.net/q/project:ppa-packaging) first.
* [Open an issue](https://github.com/yoursunny/NFD-nightly/issues) to discuss.
