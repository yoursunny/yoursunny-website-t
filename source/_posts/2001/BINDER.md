---
title: BINDER手抄报
lang: zh
date: 2001-08-12
tags:
- commentary
---

我从1996年开始在家里的一个装订夹上挂出手抄报，取名“BINDER”。
手抄报的内容涵盖我的学习生活，时事评论，文学作品，以及我编写的电脑软件。
现存121页手抄报，起于2000年1月6日，止于2001年8月12日。
我在2017年10月5日用iPad将现存的155页手抄报扫描入电脑，发布在此。

![BINDER page 001](001.gif)

![BINDER page 002](002.gif)

![BINDER page 003](003.gif)

![BINDER page 004](004.gif)

![BINDER page 005](005.gif)

![BINDER page 006](006.gif)

![BINDER page 007](007.gif)

![BINDER page 008](008.gif)

![BINDER page 009](009.gif)

![BINDER page 010](010.gif)

![BINDER page 011](011.gif)

![BINDER page 012](012.gif)

![BINDER page 013](013.gif)

![BINDER page 014](014.gif)

![BINDER page 015](015.gif)

![BINDER page 016](016.gif)

![BINDER page 017](017.gif)

![BINDER page 018](018.gif)

![BINDER page 019](019.gif)

![BINDER page 020](020.gif)

![BINDER page 021](021.gif)

![BINDER page 022](022.gif)

![BINDER page 023](023.gif)

![BINDER page 024](024.gif)

![BINDER page 025](025.gif)

![BINDER page 026](026.gif)

![BINDER page 027](027.gif)

![BINDER page 028](028.gif)

![BINDER page 029](029.gif)

![BINDER page 030](030.gif)

![BINDER page 031](031.gif)

![BINDER page 032](032.gif)

![BINDER page 033](033.gif)

![BINDER page 034](034.gif)

![BINDER page 035](035.gif)

![BINDER page 036](036.gif)

![BINDER page 037](037.gif)

![BINDER page 038](038.gif)

![BINDER page 039](039.gif)

![BINDER page 040](040.gif)

![BINDER page 041](041.gif)

![BINDER page 042](042.gif)

![BINDER page 043](043.gif)

![BINDER page 044](044.gif)

![BINDER page 045](045.gif)

![BINDER page 046](046.gif)

![BINDER page 047](047.gif)

![BINDER page 048](048.gif)

![BINDER page 049](049.gif)

![BINDER page 050](050.gif)

![BINDER page 051](051.gif)

![BINDER page 052](052.gif)

![BINDER page 053](053.gif)

![BINDER page 054](054.gif)

![BINDER page 055](055.gif)

![BINDER page 056](056.gif)

![BINDER page 057](057.gif)

![BINDER page 058](058.gif)

![BINDER page 059](059.gif)

![BINDER page 060](060.gif)

![BINDER page 061](061.gif)

![BINDER page 062](062.gif)

![BINDER page 063](063.gif)

![BINDER page 064](064.gif)

![BINDER page 065](065.gif)

![BINDER page 066](066.gif)

![BINDER page 067](067.gif)

![BINDER page 068](068.gif)

![BINDER page 069](069.gif)

![BINDER page 070](070.gif)

![BINDER page 071](071.gif)

![BINDER page 072](072.gif)

![BINDER page 073](073.gif)

![BINDER page 074](074.gif)

![BINDER page 075](075.gif)

![BINDER page 076](076.gif)

![BINDER page 077](077.gif)

![BINDER page 078](078.gif)

![BINDER page 079](079.gif)

![BINDER page 080](080.gif)

![BINDER page 081](081.gif)

![BINDER page 082](082.gif)

![BINDER page 083](083.gif)

![BINDER page 084](084.gif)

![BINDER page 085](085.gif)

![BINDER page 086](086.gif)

![BINDER page 087](087.gif)

![BINDER page 088](088.gif)

![BINDER page 089](089.gif)

![BINDER page 090](090.gif)

![BINDER page 091](091.gif)

![BINDER page 092](092.gif)

![BINDER page 093](093.gif)

![BINDER page 094](094.gif)

![BINDER page 095](095.gif)

![BINDER page 096](096.gif)

![BINDER page 097](097.gif)

![BINDER page 098](098.gif)

![BINDER page 099](099.gif)

![BINDER page 100](100.gif)

![BINDER page 101](101.gif)

![BINDER page 102](102.gif)

![BINDER page 103](103.gif)

![BINDER page 104](104.gif)

![BINDER page 105](105.gif)

![BINDER page 106](106.gif)

![BINDER page 107](107.gif)

![BINDER page 108](108.gif)

![BINDER page 109](109.gif)

![BINDER page 110](110.gif)

![BINDER page 111](111.gif)

![BINDER page 112](112.gif)

![BINDER page 113](113.gif)

![BINDER page 114](114.gif)

![BINDER page 115](115.gif)

![BINDER page 116](116.gif)

![BINDER page 117](117.gif)

![BINDER page 118](118.gif)

![BINDER page 119](119.gif)

![BINDER page 120](120.gif)

![BINDER page 121](121.gif)
