---
title: P2P流量监控Web平台的设计与实现
author: 石君霄 指导教师：邹福泰
lang: zh
date: 2008-04-15
tags:
- PHP
- academic
scoped_style: |
  .abstract_cn_title {
    font-size:14pt;
    font-family:"黑体";
    text-align:center;
  }
  .abstract_cn_content {
    font-size:10.5pt;
    font-family:"宋体";
    text-indent:2em;
  }
  .keyword_cn {
    margin-bottom:1em;
  }
  .keyword_cn_title {
    font-size:12pt;
    font-family:"黑体";
    padding-right:2em;
  }
  .keyword_cn_content {
    font-size:10.5pt;
    font-family:"宋体";
  }
  .abstract_en_title {
    font-size:14pt;
    font-family:"Times New Roman";
    text-align:center;
  }
  .abstract_en_content {
    font-size:10.5pt;
    font-family:"Times New Roman";
    text-indent:2em;
  }
  .keyword_en {
    margin-bottom:1em;
  }
  .keyword_en_title {
    font-size:12pt;
    font-family:"Times New Roman";
    font-weight:bold;
    padding-right:2em;
  }
  .keyword_en_content {
    font-size:10.5pt;
    font-family:"Times New Roman";
  }
  .num {
    padding-left:2em;
    padding-right:1em;
  }
  .ref_title,.ref_content,.thanks_title,.thanks_content {
    font-size:10.5pt;
    font-weight:normal;
    font-family:"宋体";
  }
  .ref_title,.thanks_title {
    margin-top:1em;
  }
---

<h2 class="abstract_cn_title">摘要</h2>
<p class="abstract_cn_content">P2P点对点传输的不断增长占用大量骨干网带宽，影响关键业务的正常运行。本文首先介绍了主要的P2P协议识别技术，讨论了改进思路。然后安装了一个基于iptables和l7filter的P2P流量监控节点，并用AJAX和PHP编写了一个Web监控平台以便通过Web管理监控节点、在图形界面中查看P2P流量日志，组成了一套P2P流量监控系统。</p>
<p class="keyword_cn">
<span class="keyword_cn_title">关键词</span>
<span class="keyword_cn_content">P2P,流量,监控,网络,l7filter</span>
</p>

<h2 class="abstract_en_title">ABSTRACT</h2>
<p class="abstract_en_content">Fast growth of Peer-to-Peer traffic are taking more and more backbone network bandwidth, and may slow down key businesses. In this paper, major P2P identification techniques and a way to enhance them are introduced at first. Then we set up a P2P traffic detection router based on iptables and l7filter. A Web application is designed and implemented with AJAX and PHP, so that a network administrator can manage the router or view router logs in this Web application. The router and the Web application make up a P2P detection system.</p>
<p class="keyword_en">
<span class="keyword_en_title">KEYWORDS</span>
<span class="keyword_en_content">P2P,traffic,detection,network,l7filter</span>
</p>

<div class="body">

<h2><span class="num">1.</span>绪论</h2>
<h3><span class="num">1.1</span>国内外概况</h3>
<p>随着互联网技术的不断发展，P2P点对点传输逐渐成为资源分享的主要方式。但是，在某些网络中，需要阻断P2P传输，以节省带宽、保证关键应用的正常运行。美国某ISP在国际出口上阻断了P2P流量，大大降低了国际出口的压力，降低了运营成本。</p>
<p>市面上可以找到的“聚生网管”、“P2P终结者”等软件可以实现P2P流量的阻断，但是它们依赖于arp欺骗，严重影响网络稳定，而且对于进行IP-MAC绑定的用户无法生效。</p>
<h3><span class="num">1.2</span>选题意义</h3>
<p>P2P流量占骨干网流量的40%以上,它打破了传统的流量峰值模型,并可能长时间高度堵塞网络。对于带宽敏感的业务Voip,Video等业务造成较大影响，用户的Qos无法保证，出口带宽消耗无限增长。因此，对于P2P流量的监控显得特别重要。</p>
<h3><span class="num">1.3</span>目的和研究范围</h3>
<p>本课题针对目前P2P软件过多占用带宽的问题，探讨对于P2P流量的识别技术，进而可对P2P流量进行有效的管理，开发出相应的P2P流量监控软件。软件通过布署在网络出口处的监控程序，实时监控网络中的P2P流量，并对P2P异常流量进行控制，以保障网络业务的正常运行。</p>

<h2><span class="num">2.</span>研究内容和方法</h2>

<h3><span class="num">2.1</span>P2P技术简介</h3>
<p>P2P，即peer-to-peer，是近年来互联网上最为流行的资源分享方式。传统的以服务器为中心的资源分享，资源的提供完全依赖于网络服务器，热门资源所在的服务器不堪重负。在P2P的分享方式中，服务器只需维护拥有资源的用户列表，而资源在用户之间传输，有效降低了热门资源所在服务器的压力。同时，P2P的出现，大大降低了用户分享自己所拥有资源的难度——发布资源不再需要租用服务器，而只要打开P2P软件制作“种子”，然后在论坛、博客等地公布这个很小的“种子”文件就行了。</p>
<p>P2P技术可以用于文件的分享，也可以用于实时资源的分享，例如网络电视的传输和播放。</p>
<p>常用的P2P软件和协议有：BitTorrent,eDonkey,eMule,PPLive,PPStream,QQLive,SecureP2P等。</p>

<h3><span class="num">2.2</span>P2P监控技术简介</h3>
<p>在路由器或网关上，常用的P2P流量监控技术有：</p>
<h4><span class="num">2.2.1</span>应用层字符串匹配</h4>
<p>在应用层数据中匹配特征字符串，如果含有已知的P2P协议的特征字符串，判定为P2P流量。这种方法的优点是简单明了、增加新协议、误报率低，缺点是无法适应加密的P2P协议。</p>
<h4><span class="num">2.2.2</span>群算法</h4>
<p>使用K-Means、DBSCAN、AutoClass等群算法识别应用程序在网络中传输的离散特征，将某种特征判定为P2P流量。</p>
<h4><span class="num">2.2.3</span>传输层识别</h4>
<p>两两节点之间只维持一个TCP连接、大量节点连接某节点的同一个端口，在排除DNS、SMTP等服务后，判定为P2P流量。这种方法的优点是处理效率高（不需要解包至应用层）、能适应加密的P2P协议，缺点是误判率高。</p>
<h4><span class="num">2.2.4</span>本项目的设计考虑</h4>
<p>P2P流量检测技术通常可分为基于流量特征的识别方法(TLI)和基于深层数据包识别方法(DPI)。从P2P流量识别的技术现状来看，基于应用数据分析技术的深层数据分析方法DPI由于具有准确性高、健壮性好、具有分类功能，且过去的P2P系大都未加密，因此是P2P流量识别的主要方法。但是，基于DPI技术也面临诸如如何提高检测算法的性能、如何支持对加密数据的分析、如何更新P2P应用特征库等问题。同样，基于流量特征的P2P流量识别方法虽然具有性能高、可扩展性好的有点，但由于准确性差，因此在实际应用中也面临诸多困难。此外，现有方法都以离线数据分析为主，缺乏P2P流量的实时识别能力。从本质来看，基于流量特征的检测属于启发式方法，而深层数据分析属于精确匹配方法。如果能够结合这两种方法的优点，就有可能设计出一个准确、高效的P2P流量实时识别算法来。因此，本项目最初的目的是希望能够实现DPI和基于流量特征这两种方式的结合。其要点是：</p>
<ol>
<li>基于流量特征的方式先处理，因为它处理在transfer layer的端口和IP对，可通过连接流的五元组信息进行分析抽取，开销相对比较少。</li>
<li>基于DPI，要深入解析应用层格式，不同的应用有不同的格式匹配模式，这个处理开销较大。</li>
<li>因此，先对连接流对进行分析处理，就大概可filter掉90%的流量，然后剩下的10%的可疑流量用DPI处理。这样的方式具有较好的实用性，也能够对一些加密流进行探测。</li>
</ol>

<h3><span class="num">2.3</span>研究方法</h3>
<p>我们搭建了一个小型的实验环境，包括一台Linux计算机作为网关、两台Windows计算机运行P2P软件、一台Web服务器作为监控中心。</p>

<h2><span class="num">3.</span>研究过程和结果</h2>

<h3><span class="num">3.1</span>节点的安装调试</h3>
<p>经过查阅资料，决定采用iptables以及l7filter作为网关节点的监控软件。l7filter选择的是内核版，所以需要重新编译Linux内核。</p>
<p>节点安装完毕后，可以实现：在控制台上书写iptables配置命令，即可调用l7filter识别BitTorrent和eDonkey两种P2P协议的数据包，然后用iptables的DROP即可阻断这两种协议的通信。</p>

<h3><span class="num">3.2</span>P2P协议分析</h3>
<p>通过抓包软件或阅读协议公开的文档，了解各类P2P协议的特征字符串，书写相应的l7filter规则文件。</p>

<h3><span class="num">3.3</span>Web监控中心的设计与制作</h3>
<p>本人在这个项目里主要负责Web监控中心部分，所以详细谈谈这一部分。</p>

<h4><span class="num">3.3.1</span>为什么需要一个Web监控中心</h4>
<p>随着网络规模的扩大，网络管理的复杂度迅速增加。iptables作为开源、免费的软件防火墙，在很多地方得到了广泛的应用。然而，iptables是一个Linux操作系统下的命令行工具，操作起来有一定的难度。iptables的配置命令并不直观，手工输入不但麻烦、而且容易出错。因此，需要一个图形化的界面来操作iptables及l7filter</p>
<p>同时，在一个规模稍大的网络中，只部署一个P2P流量监控网关节点往往是不够的，往往需要部署多个这样的节点。如果用ssh登录各节点、或者在各节点上安装本地的图形界面，就不能在一个集中的地方管理所有的节点，这也是不合适的。</p>
<p>因此，一个统一的、可以同时管理多个节点的P2P流量监控中心，是P2P流量监控系统的一个重要组成部分。</p>

<h4><span class="num">3.3.2</span>需求分析</h4>
<p>Web监控中心的操作人员是网络管理员，与Web监控中心有关的外部实体是监控节点。网络管理员可以创建、修改、删除节点，可以输入规则、查看日志；监控节点可以从Web监控中心下载规则，可以上传日志到Web监控中心。</p>
<p>经过需求分析，画出用例图如下：<br/>
<img src="use-case.png" width="660" height="495" alt="用例图"/></p>

<h4><span class="num">3.3.3</span>开发框架选择和数据结构设计</h4>
<p>为了降低系统安装成本，选用了免费的PHP作为服务端框架；为了改善用户体验，使用AJAX技术制作客户端，并在客户端引入Prototype和script.aculo.us两个框架以加速开发进度。</p>
<p>在Web监控中心需要存储的数据有：管理员登录认证信息，节点型号，P2P协议，节点，规则，日志。概念数据模型如下：<br/>
<img src="cdm.png" width="700" height="529" alt="概念数据模型"/></p>
<p>为了提高查询效率，节点型号、P2P协议两项很少修改的数据写入.php文件、在程序中包含进来。对于需要修改的其他几项数据存入关系型数据库；由于规则、日志可能数据量很大，选用了MySQL这款高效、免费的数据库服务软件。</p>

<h4><span class="num">3.3.4</span>规则的设计</h4>
<h5><span class="num">3.3.4.1</span>每条规则的生效条件</h5>
<p>在现实网络环境中，可能会遇到以下情况：</p>
<ul>
<li>办公室里允许经理使用P2P工具，而不允许其他员工使用——需要根据用户网络地址区分</li>
<li>允许通过BitTorrent下载，而不允许通过迅雷下载——根据协议区分</li>
<li>工作日白天需要运行各种关键应用，对P2P予以阻断，而晚上和周末不再限制——根据每日时间段、每周星期几区分</li>
<li>平时不允许使用P2P的部门，在奥运会的时候允许观看PPStream网络电视——根据日期区分</li>
</ul>
<p>考虑到上述各种情况，对每条规则设计了下列三维的生效条件，三维同时匹配这条规则才能生效：</p>
<ol>
<li>P2P协议种类：正在使用的协议与此协议匹配，规则生效</li>
<li>用户网络地址：根据精确的IP地址，或者CIDR范围方式设置</li>
<li>时间：可以根据每日的时间段、每周的星期几、年月日表示的日期三级范围设置；同时满足三级范围才能匹配</li>
</ol>
<h5><span class="num">3.3.4.2</span>规则的操作目标</h5>
<p>每条规则匹配后有两种操作目标：</p>
<ul>
<li>accept：接受这个数据包，允许该规则匹配的数据包通过网关</li>
<li>drop：阻断这个数据包，不允许该规则匹配的数据包通过网关</li>
</ul>
<h5><span class="num">3.3.4.3</span>规则的排序</h5>
<p>iptables把每个数据包根据每条规则依次匹配，因此规则输入的次序是重要的。在规则输入界面需要提供规则排序的功能。</p>

<h4><span class="num">3.3.5</span>节点与中心的通信</h4>
<p>监控节点与Web监控中心需要进行通信，通信的目的是下载规则和上传日志。</p>
<h5><span class="num">3.3.5.1</span>通信方法设计原则</h5>
<ul>
<li>安全性：P2P监控节点是一种防火墙，防火墙应该是不可侵犯的；因此管理节点所需的通信必须是安全的。</li>
<li>有效性：必须保证对节点的管理操作能及时生效，没有太大的延迟。</li>
<li>节省带宽：P2P监控节点需要进行带内管理，即管理通信需要占用网络带宽；在保证安全有效的前提下，管理操作不应该过多占用网络带宽。</li>
</ul>
<h5><span class="num">3.3.5.2</span>通信方法的设计</h5>
<p>节点与中心之间的通信方法，可以看成一种“协议”。为了达到上面的三条设计原则，必须在监控节点和监控中心两端分别部署一个“协议栈”。监控中心的sync.php脚本文件负责处理与节点的通信事宜；监控节点上使用的则是一个SHELL脚本sync.sh，调用几个php命令行脚本进行处理。</p>
<p>对安全性，通信方法中有三重保障：</p>
<ol>
<li>验证节点的存在性：节点访问sync.php时要声明自己的guid唯一编号，此编号必须在监控中心中存在</li>
<li>验证节点的网络地址：sync.php要验证访问来源地址与监控中心记录的节点网络地址一致</li>
<li>对称加密：数据经过一个128位对称密钥以Rijndael算法加密，节点和中心都知道这个密钥，而其他人不知道；这样就无法假冒节点、无法假冒中心、也无法进行中间人攻击</li>
</ol>
<p>为了使管理操作及时生效，节点应该定期向中心上传日志、并定期从中心下载规则。为此，在节点上使用cron服务，定时调用sync.sh。调用的时间间隔在10～30分钟为宜，时间太长影响有效性，时间太短则浪费带宽。</p>
<p>为了进一步节省带宽占用，数据在加密前先进行deflate压缩，有效减小了传送数据的字节数。</p>
<h5><span class="num">3.3.5.3</span>上传日志</h5>
<p>节点只负责通过POST方式请求sync.php，把iptables输出的日志压缩加密后发送给中心。中心对数据解密解压缩后，用正则表达式匹配整个日志文件，找到属于P2P流量的日志信息，逐条录入数据库，并跳过字节数、数据包数为0的记录。</p>
<h5><span class="num">3.3.5.4</span>下载规则</h5>
<p>目前的监控节点要求为每个IP地址生成一条规则，若通过CIDR方式指定地址则日志不够精确。要求网络管理员为每一个IP地址输入一条规则是不可能的——重复、机械的操作，显然不应该由人工来完成。程序将自动把用CIDR地址段定义的规则展开成若干条单个IP地址的规则。</p>
<p>节点通过GET方式请求sync.php，监控中心只负责把所有规则以逗号分隔形式压缩加密后一次性发送给节点。监控节点上的脚本进行解密、解压缩，把CIDR展开成单个IP地址，然后将当前时间与规则的生效时间进行比较，来配置iptables和l7filter。一次性发送所有规则（只有结束日期在当前日期之前的规则不发送），这种方法虽然会占用较多的带宽，但使得节点在无法连接上监控中心时仍然可以正常工作。CIDR展开会使规则长度膨胀数百倍，因此这个步骤应该在节点进行。</p>
<h5><span class="num">3.3.5.5</span>监控节点端“协议栈”脚本功能小结</h5>
<p>节点上的“协议栈”的文件和功能如下：</p>
<ol>
<li>sync.sh：导出iptables日志，调用1.php上传日志，用wget下载规则，调用2.php解密解压规则，调用3.php进行CIDR展开和判断时间，运行规则脚本配置iptables</li>
<li>1.php：压缩、加密、上传日志；为了方便节点的安装配置，没有使用curl库，而是直接打开TCP连接，向连接写入数据</li>
<li>2.php：使用mcrypt解密、解压缩规则，生成逗号分隔的规则文件</li>
<li>3.php：读取逗号分隔的规则文件，匹配时间，作CIDR展开，生成iptables配置SHELL脚本rule.sh</li>
<li>conf.php：集中存储常量定义</li>
<li>ip_parser.php：IP解析器库，可以解析文本格式的IP地址或CIDR地址，判定地址有效性，计算网络号、子网掩码、CIDR地址等形式，求一个地址是否在一个子网内</li>
</ol>

<h4><span class="num">3.3.6</span>监控中心的界面设计</h4>
<h5><span class="num">3.3.6.1</span>安全性的保证</h5>
<p>为了保证安全性，网络管理员必须使用HTTPS协议访问监控中心，这样就在传输层自动加密。如果需要进一步增强安全性，还可以配置HTTPS的双向认证，网络管理员与监控中心需要互相出示客户端证书和服务器证书才能操作。</p>
<p>进行任何管理操作前，网络管理员必须使用用户名和密码登录。监控中心为有效的登录生成一个随机字符串，记录到数据库，并且设置一个含有该字符串的cookie，以后的操作就以这个cookie为登录凭据。</p>
<h5><span class="num">3.3.6.2</span>节点的创建、修改、删除</h5>
<img src="edit-node.png" width="989" height="395" alt="节点编辑界面"/>
<h5><span class="num">3.3.6.3</span>规则编辑</h5>
<img src="edit-rule.png" width="989" height="348" alt="规则编辑界面"/>
<p>需要提供规则生效条件、操作目标的输入，以及规则排序功能。协议、操作目标使用下拉框，以防止输入错误；协议下拉框中只显示该节点型号所支持的协议。为使排序任务更加直观方便，设计的操作方法是使用鼠标上下拖动规则前的[排序]链接，这条规则就会随鼠标移动；当5秒钟内不再拖动时，保存排序结果到服务器。</p>
<h5><span class="num">3.3.6.4</span>日志查看</h5>
<img src="view-log.png" width="972" height="240" alt="日志查看界面"/>
<p>允许根据日期查询日志。对显示的日志，只需点击监控节点、时间、操作、协议、网络地址、包计数、字节计数、备注等字段名就可以升序或降序排列。</p>

<h3><span class="num">3.4</span>系统调试</h3>
<h4><span class="num">3.4.1</span>监控节点的调试</h4>
<p>经测试，通过在监控节点控制台上输入命令，可以成功封禁BitTorrent协议。<br/>
实施封禁之前，BT正常下载：<br/><img src="bt-normal.png" width="640" height="454" alt="BT正常下载"/><br/>
实施封禁之后，BT速度为0：<br/><img src="bt-blocked.png" width="640" height="454" alt="BT速度为0"/></p>
<h4><span class="num">3.4.2</span>监控中心的调试</h4>
<p>用Firefox 2.0浏览器，能够正常登录并操作监控中心Web界面的所有功能。在VMware Server虚拟机中安装Ubuntu 7.10，运行sync.sh可以正确获取规则，也可以正确上传模拟的日志。日志能够在Web界面中正确显示、查询、排序。</p>

<h2><span class="num">4</span>结论</h2>
<p>现在的系统仅仅是在l7filter中增加了几个协议，并且创建了一个Web管理界面。</p>
<p>我们最主要的障碍是在于开源软件比较难调试，在探索一些细节难点这方面花费了很多工夫，因而目标并没有完成。不过相信这也是有启示的，也积累了一些经验。今后有兴趣的同学可以考虑继续把它完善下去。</p>

</div>

<h2 class="ref_title">参考文献</h2>
<ol class="ref_content">
<li>PHP Manual, <a href="https://www.php.net/manual/en/">http://cn.php.net/manual/en/</a></li>
<li>Prototype Documentation, <a href="http://www.prototypejs.org/api">http://www.prototypejs.org/api</a></li>
</ol>

<h2 class="thanks_title">谢辞</h2>
<p class="thanks_content">感谢邹福泰老师的指导和提供实验环境，感谢一同参加项目的李凯同学提供了样例规则文本。整个项目的完成离不开他们的力量，在此向他们表示衷心的谢意。</p>
