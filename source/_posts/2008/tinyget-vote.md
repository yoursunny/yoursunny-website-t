---
title: 用TinyGet投票
lang: zh
date: 2008-12-08
tags:
- Windows
- Fiddler
---

前几天，有人托我在东方网的某个评选活动中帮他投票，票数多多益善——也就是所谓的“刷票”。

![投票活动页面](r0si1j.png)

打开投票页面，先手工投几票，发现这个活动的投票不需要登录，对于IP地址、投票频率都没有任何限制。

## 用Fiddler2截取投票HTTP报文

> Fiddler is a Web Debugging Proxy which logs all HTTP(S) traffic between your computer and the Internet. Fiddler allows you to inspect all HTTP(S) traffic, set breakpoints, and "fiddle" with incoming or outgoing data. Fiddler includes a powerful event-based scripting subsystem, and can be extended using any .NET language.
>
> Fiddler is freeware and can debug traffic from virtually any application, including Internet Explorer, Mozilla Firefox, Opera, and thousands more.

打开[Fiddler2](http://www.fiddler2.com)，将浏览器的代理服务器设置为Fiddler2监听的端口（默认为127.0.0.1:8888）。然后为候选人投一票，Fiddler2里有了反应：

![投票报文](25an255.png)

## 保存请求信息

Fiddler2右上部分的窗格内就是投票提交的HTTP请求报文，其中：

* POST是请求的方法
* /public/suopiao/suopiao1134/InfoSubmit_1134.asp是页面URI地址
* User-Agent……Content-Type这几行是头部信息
* Mark_Field……1134这些是请求主体数据

现在将头部信息（不含POST那一行）和请求主体数据分别保存在两个文本文件里：

![保存的报文](10yguxi.png)

## 用TinyGet投一票

TinyGet是[IIS 6.0 Resource Kit Tools](https://web.archive.org/web/20091227111937/http://support.microsoft.com/kb/840671)中的一个HTTP调试与压力测试工具。

> TinyGet version 5.2 is a command-line HTTP client that supports multiple threads and looping. It provides detailed output and response validation, so you can use it to test or troubleshoot HTTP client-to-server communication. With TinyGet, you can customize each test request by configuring many different factors, including the authentication method, HTTP version, and output format.
>
> You can use TinyGet as a simple stress tool to test the performance of your site under different loads.

使用下列命令行调用TinyGet：

```powershell
TinyGet.exe -d -j:POST -frh:headers.txt -frb:body.txt -srv:appl.eastday.com -uri:/public/suopiao/suopiao1134/InfoSubmit_1134.asp
```

各参数的含义如下：

* `-d` 显示服务器响应内容
* `-j:POST` 用POST方法发出请求
* `-frh:headers.txt` 从headers.txt文件中读取头部信息
* `-frb:body.txt` 从body.txt文件中读取请求主体数据
* `-srv:appl.eastday.com` 指定服务器域名
* `-uri:/public/suopiao/suopiao1134/InfoSubmit_1134.asp` 指定URI

![TinyGet投一票](9t1mib.png)

从回显看，已经成功投了一票。

## 用TinyGet刷票

刚才只投了一票，而“刷票”则需要大量投票。估计现在有人会说：写个循环，调用TinyGet 1000次，就可以投1000票了！这样做是可行的，但不是效率最高的。

TinyGet本身就支持多线程和每个线程内的循环，使用内置多线程和循环，比从外部循环调用，效率会高很多——省去了进程创建、命令行解析、文件读取等操作。

命令行如下：

```powershell
TinyGet.exe -j:POST -frh:headers.txt -frb:body.txt -srv:appl.eastday.com -uri:/public/suopiao/suopiao1134/InfoSubmit_1134.asp -x:50 -l:10
```

各参数含义如下：

* `-x:50` 用50个线程并发请求
* `-l:10` 每个线程循环10次

投票数量应为x和l参数的乘积，也就是50×10=500票。注意关闭-d参数，也就是不回显服务器响应。

效果：

![刷票效果](29lyzo5.png)
