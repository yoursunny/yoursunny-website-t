---
title: dbMySQL数据源访问类
lang: zh
date: 2008-02-01
tags:
- PHP
---

dbMySQL类，用于PHP访问MySQL数据库，而编程更加方便、安全性更高。

```php
<?php
class dbMySQL {//数据源访问类
	private $conn;//连接
	private $prefix;//表名前缀
	private $sql=NULL;//上一条SQL语句
	private $result=NULL;//结果
	private $rows=0;//行数
	private $rows_got=0;//已提取行数
	//$db=new dbMySQL(主机,用户名,密码,数据库名,表名前缀,是否永久连接);
	function __construct($host,$username,$password,$dbname,$prefix='',$pconnect=TRUE) {//连接
		$connect_function=$pconnect?'mysql_pconnect':'mysql_connect';
		$this->conn=@$connect_function($host,$username,$password) or die('不能连接到MySQL数据源服务');
		mysql_query("SET NAMES 'utf8'",$this->conn);
		@mysql_select_db($dbname,$this->conn) or die('MySQL数据源中数据库不存在');
		$this->prefix=$prefix;
	}
	private function close() {//如果上一个结果存在，清除它
		if ($this->result!=NULL) mysql_free_result($this->result);
	}
	public function execute_sql($SQL) {//执行
		//echo $SQL;
		$this->close();
		$this->sql=$SQL;
		mysql_query($SQL,$this->conn);
		$err=mysql_error($this->conn); if ($err!='') die($err);
		$this->result=NULL;
		$this->rows=mysql_affected_rows($this->conn);
		$this->rows_got=0;
	}
	public function execute($SQL,$p=NULL,$prefix='###') {//带参数执行
		$s=str_replace($prefix,$this->prefix,$SQL);
		if ($p==NULL) { $this->execute_sql($s); return; }
		foreach ($p as $i => $v) {
			if (ctype_digit($i{0}) && !is_numeric($v)) die('SQL数值型参数错误 '.$i.'=>'.$v);
			$vv=ctype_lower($i{0})?"'".mysql_escape_string($v)."'":$v;
			$s=str_replace('?'.$i,$vv,$s);
		}
		$this->execute_sql($s);
	}
	//带参数执行、查询说明
	//数组$p "a"->"ppp"
	//SELECT * FROM j WHERE m=?a 解析为 SELECT * FROM j WHERE m='ppp'ִ
	//参数名称第一位是小写字母，作为字符型参数，编码并加单引号
	//参数名称第一位是数字，作为数字型参数，如果不是数字就出错
	//参数名称第一位是其他情况，直接带入SQL表达式
	//$prefix表示前缀占位符，默认###，FROM ###kk在前缀是pr_时解析为FROM pr_kk
	public function query_sql($SQL) {//查询
		//echo $SQL;
		$this->close();
		$this->sql=$SQL;
		$this->result=mysql_query($SQL,$this->conn);
		$err=mysql_error($this->conn); if ($err!='') die($err);
		$this->rows=mysql_num_rows($this->result);
		$this->rows_got=0;
	}
	public function query($SQL,$p=NULL,$prefix='###') {//带参数查询
		$s=str_replace($prefix,$this->prefix,$SQL);
		if ($p==NULL) { $this->query_sql($s); return; }
		foreach ($p as $i => $v) {
			if (ctype_digit($i{0}) && !is_numeric($v)) die('SQL数值型参数错误 '.$i.'=>'.$v);
			$vv=ctype_lower($i{0})?"'".mysql_escape_string($v)."'":$v;
			$s=str_replace('?'.$i,$vv,$s);
		}
		$this->query_sql($s);
	}
	public function read() {//提取一行为关联数组，如果已提取完则返回NULL
		if ($this->result==NULL) return NULL;
		if ($this->rows_got==$this->rows) return NULL;
		++$this->rows_got;
		return mysql_fetch_assoc($this->result);
	}
	public function num_rows() {//总行数
		return $this->rows;
	}
	public function eof() {//是否提取完了？
		return ($this->rows_got==$this->rows);
	}
}
?>
```

## dbMySQL优点

* 高安全性：强制检查类型、自动转义，避免SQL注入漏洞
* 方便编程：面向对象语法，不必写mysql_fetch_assoc等长长的函数名
* 方便编程：仿reader机制，直接从对象读出数据，不必再写$resultID
* 支持表名前缀：允许在一个数据库中运行多个应用

## dbMySQL样例代码

```php
<?php
require_once 'dbMySQL.php';
$db=new dbMySQL('localhost','root','password','database','pr_',FALSE);

//插入记录
$db->execute('INSERT INTO ###statistics (t,ip) VALUES (NOW(),?ip)',array('ip'=>$_SERVER['REMOTE_ADDR']));
//实际执行 INSERT INTO pr_statistics (t,ip) VALUES (NOW(),'59.78.20.24')

//查询记录
$db->query('SELECT * FROM ###statistics ORDER BY t DESC LIMIT 0,?1n',array('1n'=>$_GET['n']));
//实际执行SELECT * FROM pr_statistics ORDER BY t DESC LIMIT 0,10

while (!$db->eof()) {
    $row=$db->read();
    echo $row['ip'].'<br/>';
}
?>
```

## dbMySQL类库函数列表

说明 | 定义 | 备注
-|-|-
构造函数 | function __construct(主机名$host,用户名$username,密码$password,库名$dbname,表名前缀$prefix='',是否永久连接$pconnect=TRUE) | new dbMySQL(...)创建新对象
直接执行SQL语句 | function execute_sql(语句$SQL) | 无结果；不处理转义、表名前缀；无安全性，不推荐使用
执行SQL语句 | function execute(语句$SQL,参数$p=NULL,表名前缀替代符$prefix='###') | 无结果
直接执行SQL查询 | function query_sql(语句$SQL) | 有结果；不处理转义、表名前缀；无安全性，不推荐使用
执行SQL查询 | function query(语句$SQL,参数$p=NULL,表名前缀替代符$prefix='###') | 有结果
提取结果 | function read() | 提取一行结果为关联数组；已提取完时返回NULL
总行数 | function num_rows() | 返回刚才的语句影响行数、或查询结果行数
是否提取完 | function eof() | 如果已提取完返回true

### 带参数执行、查询说明

数组`$p=array("a"=>>"ppp")`，`SELECT * FROM j WHERE m=?a` 解析为 `SELECT * FROM j WHERE m='ppp'`

* 参数名称第一位是小写字母，作为字符型参数，编码并加单引号
* 参数名称第一位是数字，作为数字型参数，如果不是数字就出错
* 参数名称第一位是其他情况，直接带入SQL表达式

$prefix表示前缀占位符，默认`###`，`FROM ###kk`在前缀是`pr_`时解析为`FROM pr_kk`
