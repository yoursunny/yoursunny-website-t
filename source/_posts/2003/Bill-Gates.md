---
title: Bill Gates
lang: en
date: 2003-03-15
---

William Henry Gates III is one of the famous persons in the 20th century.
He is one of the few founders of the IT industry.

Gates was born on October 28, 1955.
When he was a child, there was very,very few computers in the world, and of course computers were too expensive for anyone to buy.

In the spring of 1968, the Lakeside Prep School, where Bill Gates studied, decided to buy some computer time from General Electric for their students.
Bill Gates, Paul Allen and some other students of Lakeside soon became interested in this machine, and they stayed in front of the computer day and night.
They had used up all of the school's computer time.
Then another company offered computer time at a lower rate, so that they were able to use computer in Computer Center Corporation.
Soon Bill, Allen became hackers.
They found some bugs of the computer, and they even modified the record of their computer time.
But soon CCC found out this, and they were banned from the computer.

However the CCC's business was beginning to suffer due to the bugs.
The Lakeside Programmers Group got a job at CCC of looking for bugs of their computer system.
Their "pay" was free computer time.
In 1970 CCC went out of business.
The group had to find a new way to get computer time.
It wasn't easy but they'd made it.
Then they worked for several companies in the similar way.

In the fall of 1973 Bill Gates went to Harvard University.
Gates spent many long nights in the university's computer room and slept in classes the next day.

In December 1974, Allen was on his way to visit Gates.
He saw a magazine said that the first microcomputer, Altair, was invented.
He bought the magazine.
They realized that that was a great opportunity.
Then they wrote a BASIC for Altair and sold it to MITS, the producer of Altair.

Gates thought the software market had been born.
Within a year he dropped out of the University and Microsoft was formed.

Much of Gates's success rests on his ability to translate technical visions into market strategy, and to blend creativity with technical acumen.
Gates has accumulated great wealth from his holdings of Microsoft stock.
His willingness to back new technologies such as Microsoft Windows, Windows NT, and workgroup applications has kept Microsoft at the forefront of computer hardware and software evolution.

## References

1. [Bill Gates Before Microsoft](https://ei.cs.vt.edu/~history/Gates.Mirick.html)
2. "Gates, William Henry III" article in Microsoft ENC96ENC CD-ROM
