---
title: 校园一角
lang: zh
date: 2003-03-08
tags:
- life
- China
---

七中校园的西南角是个荷花池。在学校征集名称中，由于《荷塘月色》的出名，这荷池被取名为“自清池”。

我第一次去自清池时，是学了《荷塘月色》以后。
在此之前，我虽已知道有这个池，但一直没有时间去参观。
而在读过《荷塘月色》以后，我下决心要去看看这“自清池”。

那天清晨，我比平时早起了十分钟，“荣幸”的当了食堂的“开门客”（第一个买早餐的人）后，六点五十分来到了自清池前。
那天有一层薄雾，太阳还刚刚出来，荷花池周围笼罩着一层神秘的面纱。

很不幸，这层面纱使我没能领略荷花池的芳容。
尽管我捧着语文课本朗读着这篇著名的《荷塘月色》，我还是没有能力揭开这层面纱。
我决定改天再次拜访自清池。

第二次去是在期中考试期间。
由于是交叉考试，不能在教室复习，而宿舍里的棋牌大战和食堂里的人声鼎沸都不是适合复习的环境，这荷花池当之无愧成了闹中取静的好地方。
语文考试的前一天下午，我步行在池上的柳青亭与艾青亭之间，朗读着《荷塘月色》，时而又望着池中央竖起的小山上刻着的“自清池”三个大字尝试着背诵这篇文章。
语文考试过后，我再也没有心思去踱着方步读散文了，而是找了条石凳坐下来背公式、背单词。
有时候，复习累了，我也会眺望一下南端草坪上正在嬉闹打滚的中预年级的小朋友们。

第三次去已是期末考试期间了，那是一个夜晚，已经考完了语文。我并没有体会到《荷塘月色》的意境，因为这个“自清池”与朱自清先生当前走到的“荷塘”毕竟有着太多的区别。
夜晚的自清池十分宁静，月亮的倒影在水中清晰可见。
可能是因为即将考物理，当时我一下子想起了这个“虚像”在水底四十万公里的远处。

第四次去已是在寒假里了，那天是一小组同学活动，我去得比较早，就是荷花池看看。
我还带了台数码相机想要拍下这儿的美景，可惜忘了装电池，就无法拍照了。
这天，我看见了池里有着成群结对的金鱼。

最近一次去荷花池（其实不如说是走过）时，天正下着大雨。
我匆匆往宿舍跑，要去拿把伞，不过我还是没忘记瞥了一眼荷花池。
大雨中的荷花池并没有失去她一贯的美景，而是更为欢畅的游鱼增添了荷花池的美丽。
虽然我没有时间品味《荷塘月色》的意境，但是我没有忘记荷花池的美景。

荷花池，也就是学校最美的一个“角”，是同学们休息的好去处，也是产生诗意的地方。
