---
title: How do I deploy my website on NDN?
lang: en
date: 2011-12-05
tags:
- NDN
---

As a website owner, I'm always thinking about making my website available on more channels, so I enabled IPv6 several years ago.
NDN comes to my eye as a new "content distribute model" and "communicate protocol".
It's natural for me to think about: how do I deploy my website on NDN?

Like most small websites, my website runs mainly on HTTP.
Visitors issue GET requests to retrieve pages and resources, which is essentially static but may change at any time.
They issue POST requests to interact with web applications, or play with JavaScript-based web applications.
I used to provide file downloading through FTP or BitTorrent (but it's now on SkyDrive).
I'm collecting statistics with Google Analysis, and an accurate report is important to me.
To sum up, I have 5 scenarios:

1. retrieve pages and resources;
2. server-based web applications;
3. JavaScript-based web application;
4. file downloading;
5. analysis.

**File downloading** is the easiest one on NDN.
Once a file is published, I would not modify it without using a new name.
So, publishing files under a NDN prefix is sufficient.
By adding a segmentation component in data names, visitors can download a piece of a file, which is similar to BitTorrent.

**Retrieving pages and resources** is similar to file downloading, except that pages may change at any time.
I don't know when a page will change, so I cannot specify an expiry time when the page was served.
In HTTP, user agents can include a If-Modified-Since header, and server would respond with 304 status code if page is not modified without sending the content.
In NDN, I can add a version component in data names, so user agents can INTEREST locally-cached version number with RightmostRightSibling annotation.

How to deal with "default page"?
You can visit `http://example.com/` and you'll get `index.htm` or some other file chosen by author.
My solution is that user agents must insert an empty segment to specify the "default page".

There is no way to receive cookies on server side.
If cookies and other headers are important, refer to discussion on server-based web applications below.

**Server-based web applications** require POST method.
A simple scheme can solve this problem.

1. user agent INTEREST a script on server and include URI\_P in a segment, server replies with URI\_R.
2. server INTEREST URI\_P which is published by user agent, user agent sends the payload.
3. user agent INTEREST URI\_R, server replies with response to the POST request.

**JavaScript-based web applications**: security of JavaScript relies heavily on Same-Origin Policy.
User agents should treat each publisher certificate + name prefix as a origin, rather than name prefix only.

For **analysis**, one approach is: let JavaScript generate a unique ID on the client for each origin (publisher certificate + name prefix) and store it in localStorage or cookies.
Whenever a page is loaded or an event is fired, INTEREST a special analysis name that includes unique ID, timestamp and page or event descriptor, so the analysis server will receive it.

This report describes some aspects of publishing a website to NDN.
There is a long way to go.
