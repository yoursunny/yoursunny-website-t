---
title: 纯真IP数据库批量查询
lang: zh
date: 2011-10-05
tags:
- PHP
---

纯真IP数据库（qqwry.dat）批量查询PHP脚本，qqwry.lib.php。此脚本适合一次性查询大量IP地址的归属地，比如在几万行的网站日志文件中添加IP归属地。

下载[纯真IP数据库批量查询PHP脚本](https://github.com/yoursunny/code2014/tree/qqwry)

使用前请[获取纯真IP数据库](https://github.com/WisdomFusion/qqwry.dat)存储qqwry.dat于同一目录。

## 接口使用示例

```php
require 'qqwry.lib.php';
$qqwry=new QQWRY;
$ip_arr=array();
$ip_arr[]=ip2long('8.8.8.8');
$ip_arr[]=ip2long('10.0.0.1');
$results=$qqwry->query($ip_arr);
foreach ($results as $ip=>$record) {
    echo long2ip($ip).' '.$record->get_c().' '.$record->get_a()."\n";
}
```

## 批量替换脚本使用示例

```bash
php5 replace-cli.php input.txt output.txt
```

格式是`$ip ($c $a)`；找到的IPv4地址均会按此格式替换。其中，$ip表示IP地址，$c表示国家/地区记录，$a表示城市/接入商记录。若需要其他格式，请修改replace-cli.php代码。
