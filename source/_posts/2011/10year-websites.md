---
title: 那些年，我做过的网站
lang: zh
date: 2011-12-16
tags:
- web
scoped_style: |
  .siteinfo { clear:both; overflow:hidden; width:600px; border:dashed #090 2px; padding:20px; margin-top:20px; }
  .siteinfo h3 { float:left; }
  .siteinfo ul { float:left; clear:left; width:220px; }
  .siteinfo img { float:right; border:solid #99f 1px; margin-bottom:5px; }
---

2001年，我制作了第一个个人主页。10年过去了，让我来回顾一下，10年来我做过的网站。

以下列举了34个网站，只包含目前仍然在线、或者能够找到截图/源码的站点，且不包含某些非公开项目；实际做过的网站，大约在60个左右。部分截图来自[Internet Archive Wayback Machine](https://web.archive.org)。

<div class="siteinfo">
<h3>2001 个人主页</h3>
<ul>
<li>内容：自我介绍，我的第一张数码照片</li>
<li>技术：FrontPage</li>
<li>发布：通过电子邮件发给读者</li>
</ul>
<img src="2001_sjx.jpg" width="300" height="159" alt="个人主页 截图"/>
</div>

<div class="siteinfo">
<h3>2002 FredSoft</h3>
<ul>
<li>内容：VB编程作品</li>
<li>技术：手写HTML4</li>
<li>发布：学校空间</li>
</ul>
<img src="2002_FredSoft.jpg" width="300" height="178" alt="FredSoft 截图"/>
</div>

<div class="siteinfo">
<h3>2002 个人文件夹</h3>
<ul>
<li>内容：链接到子文件夹</li>
<li>技术：手写HTML4、CSS、JavaScript</li>
<li>发布：本地计算机个人使用，Windows 98的folder.htt功能</li>
</ul>
<img src="2002_folder.jpg" width="300" height="228" alt="个人文件夹 截图"/>
</div>

<div class="siteinfo">
<h3>2003 国际帅哥组织</h3>
<ul>
<li>内容：宣传我和班内另一帅哥；好吧，我承认这个组织是我们编造的</li>
<li>技术：FrontPage</li>
<li>发布：freewebs.com免费空间，icmo.2ya.com二级域名</li>
</ul>
<img src="2003_ICMO.jpg" width="300" height="206" alt="国际帅哥组织 截图"/>
</div>

<div class="siteinfo">
<h3>2003 七宝中学社团文化节：学校抗非典</h3>
<ul>
<li>内容：非典型肺炎知识和新闻</li>
<li>技术：Dreamweaver</li>
<li>发布：光盘提交到竞赛组委会</li>
</ul>
<img src="2003_SARS.jpg" width="300" height="193" alt="学校抗非典 截图"/>
</div>

<div class="siteinfo">
<h3>2004 个人主页</h3>
<ul>
<li>内容：门户</li>
<li>技术：手写HTML4、JavaScript</li>
<li>发布：虎翼网免费空间fredsoft.51.net</li>
</ul>
<img src="2004_sjx.jpg" width="300" height="200" alt="个人主页 截图"/>
</div>

<div class="siteinfo">
<h3>2005 数学知识应用竞赛：为您选择手机资费套餐</h3>
<ul>
<li>内容：通过数学方法选择最便宜的手机预付费套餐</li>
<li>技术：手写JavaScript</li>
<li>发布：光盘提交到竞赛组委会，『阳光家园』子栏目</li>
</ul>
<img src="2005_mobilefee.jpg" width="300" height="136" alt="为您选择手机资费套餐 截图"/>
</div>

<div class="siteinfo">
<h3>2005 阳光家园</h3>
<ul>
<li>内容：VB与网页编程作品，以及歌曲录音等</li>
<li>技术：手写HTML4</li>
<li>发布：china.com免费空间，sunnyland.1a.cn二级域名</li>
</ul>
<img src="2005_sunnyland.jpg" width="300" height="197" alt="阳光家园 截图"/>
</div>

<div class="siteinfo">
<h3>2005 It's Me!</h3>
<ul>
<li>内容：自我介绍、联系方式，以及歌曲录音</li>
<li>技术：手写HTML4</li>
<li>发布：freewebs.com免费空间 freewebs.com/13167142048</li>
</ul>
<img src="2005_ItsMe.jpg" width="300" height="235" alt="It's Me! 截图"/>
</div>

<div class="siteinfo">
<h3>2006 你的阳光</h3>
<ul>
<li>内容：个人照片、学习资料等</li>
<li>技术：Dreamweaver，ASP</li>
<li>发布：学校Windows Server 2003服务器，yoursunny.com域名</li>
</ul>
<img src="2006_yoursunny1.jpg" width="300" height="292" alt="你的阳光 截图"/>
<img src="2006_yoursunny2.jpg" width="300" height="243" alt="你的阳光 截图"/>
</div>

<div class="siteinfo">
<h3>2006 网上名片</h3>
<ul>
<li>内容：自我介绍、联系方式</li>
<li>技术：套用模板</li>
<li>发布：hubd.cn免费空间，m.yoursunny.com域名</li>
</ul>
<img src="2006_m.jpg" width="300" height="245" alt="网上名片 截图"/>
</div>

<div class="siteinfo">
<h3>2006 上海交通大学家园网</h3>
<ul>
<li>内容：生活园区信息</li>
<li>技术：Dreamweaver，ASP.NET</li>
<li>发布：学校Windows Server 2003服务器，学校域名</li>
</ul>
<img src="2006_ourhome.jpg" width="300" height="240" alt="上海交通大学家园网 截图"/>
</div>

<div class="siteinfo">
<h3>2006 第6届五星寝室</h3>
<ul>
<li>内容：五星寝室大赛</li>
<li>技术：Dreamweaver，ASP.NET、PHP</li>
<li>发布：学校Windows Server 2003服务器，学校域名</li>
</ul>
<img src="2006_5star.jpg" width="300" height="294" alt="第6届五星寝室 截图"/>
</div>

<div class="siteinfo">
<h3>2007 你的阳光</h3>
<ul>
<li>内容：个人网志、学习资料等</li>
<li>技术：Dreamweaver，PHP</li>
<li>发布：学校Windows Server 2003服务器，yoursunny.com域名</li>
</ul>
<img src="2007_yoursunny.jpg" width="300" height="250" alt="你的阳光 截图"/>
</div>

<div class="siteinfo">
<h3>2007 65536条网络技巧</h3>
<ul>
<li>内容：目标是写下65536条网络技巧，实际上只有1条</li>
<li>技术：Dreamweaver</li>
<li>发布：学校Windows Server 2003服务器+512j收费PHP空间，www.65536.cn域名</li>
</ul>
<img src="2007_65536.jpg" width="300" height="240" alt="65536条网络技巧 截图"/>
</div>

<div class="siteinfo">
<h3>2007 网上名片</h3>
<ul>
<li>内容：自我介绍、联系方式</li>
<li>技术：手写XHTML</li>
<li>发布：学校Windows Server 2003服务器，www.65536.cn域名子目录</li>
</ul>
<img src="2007_m.jpg" width="300" height="245" alt="网上名片 截图"/>
</div>

<div class="siteinfo">
<h3>2007 上海交通大学心擎网</h3>
<ul>
<li>内容：心理咨询中心信息</li>
<li>技术：Dreamweaver，ASP.NET</li>
<li>发布：学校Windows Server 2003服务器，学校域名</li>
</ul>
<img src="2007_xinqing.jpg" width="300" height="263" alt="上海交通大学心擎网 截图"/>
</div>

<div class="siteinfo">
<h3>2007 第7届五星寝室</h3>
<ul>
<li>内容：五星寝室大赛</li>
<li>技术：Dreamweaver，PHP、ASP.NET</li>
<li>发布：学校Windows Server 2003服务器，学校域名</li>
</ul>
<img src="2007_5star.jpg" width="300" height="234" alt="第7届五星寝室 截图"/>
</div>

<div class="siteinfo">
<h3>2008 你的阳光</h3>
<ul>
<li>内容：学习资料等</li>
<li>技术：Dreamweaver，PHP，Prototype.js</li>
<li>发布：学校Windows Server 2003服务器，yoursunny.com域名</li>
</ul>
<img src="2008_yoursunny.jpg" width="300" height="215" alt="你的阳光 截图"/>
</div>

<div class="siteinfo">
<h3>2008 技术频道</h3>
<ul>
<li>内容：技术文章和作品</li>
<li>技术：PHP，WordPress</li>
<li>发布：学校Windows Server 2003服务器，www.65536.cn域名</li>
</ul>
<img src="2008_65536.jpg" width="300" height="219" alt="技术频道 截图"/>
</div>

<div class="siteinfo">
<h3>2008 上海交通大学本科生GPA计算器</h3>
<ul>
<li>内容：GPA计算</li>
<li>技术：Prototype.js，TinyMCE</li>
<li>发布：『你的阳光』子栏目</li>
<li><a href="/t/2008/SJTU-GPA/">相关文章介绍</a></li>
</ul>
<img src="2008_GPA.jpg" width="300" height="216" alt="上海交通大学本科生GPA计算器 截图"/>
</div>

<div class="siteinfo">
<h3>2008 网上名片</h3>
<ul>
<li>内容：自我介绍、联系方式</li>
<li>技术：手写XHTML</li>
<li>发布：『你的阳光』子栏目</li>
</ul>
<img src="2008_m.jpg" width="300" height="176" alt="网上名片 截图"/>
</div>

<div class="siteinfo">
<h3>2009 你的阳光</h3>
<ul>
<li>内容：学习资料</li>
<li>技术：XHTML，PHP，jQuery</li>
<li>发布：volit.com收费linux空间，yoursunny.com域名</li>
</ul>
<img src="2009_yoursunny.jpg" width="300" height="161" alt="你的阳光 截图"/>
</div>

<div class="siteinfo">
<h3>2009 技术频道</h3>
<ul>
<li>内容：技术文章和作品</li>
<li>技术：PHP</li>
<li>发布：volit.com收费linux空间，www.65536.cn域名</li>
</ul>
<img src="2009_65536.jpg" width="300" height="156" alt="技术频道 截图"/>
</div>

<div class="siteinfo">
<h3>2009 阳光GPA计算器</h3>
<ul>
<li>内容：GPA计算</li>
<li>技术：jQuery，模块化JavaScript</li>
<li>发布：『你的阳光』子栏目</li>
</ul>
<img src="2009_GPA.jpg" width="300" height="139" alt="阳光GPA计算器 截图"/>
</div>

<div class="siteinfo">
<h3>2009 网上名片</h3>
<ul>
<li>内容：自我介绍、联系方式</li>
<li>技术：手写XHTML</li>
<li>发布：『你的阳光』子栏目</li>
</ul>
<img src="2009_m.jpg" width="300" height="154" alt="网上名片 截图"/>
</div>

<div class="siteinfo">
<h3>2010 你的阳光</h3>
<ul>
<li>内容：个人Web2.0聚合，学习资料，技术文章和作品</li>
<li>技术：XHTML，PHP，jQuery</li>
<li>发布：居无定所，yoursunny.com域名</li>
</ul>
<img src="2010_yoursunny.jpg" width="300" height="208" alt="你的阳光 截图"/>
</div>

<div class="siteinfo">
<h3>2010 阳光GPA计算器</h3>
<ul>
<li>内容：GPA计算</li>
<li>技术：jQuery，模块化JavaScript</li>
<li>发布：『你的阳光』子栏目</li>
</ul>
<img src="2010_GPA.jpg" width="300" height="182" alt="阳光GPA计算器 截图"/>
</div>

<div class="siteinfo">
<h3>2010 网上名片</h3>
<ul>
<li>内容：自我介绍、联系方式</li>
<li>技术：手写XHTML，PHP</li>
<li>发布：『你的阳光』子栏目</li>
</ul>
<img src="2010_m.jpg" width="300" height="177" alt="网上名片 截图"/>
</div>

<div class="siteinfo">
<h3>2010 注册Windows Live账号</h3>
<ul>
<li>内容：注册msn.cn账号</li>
<li>技术：ASP.NET，Windows Live Admin Center</li>
<li>发布：signup.msn.cn</li>
</ul>
<img src="2010_signup.jpg" width="300" height="222" alt="注册Windows Live账号 截图"/>
</div>

<div class="siteinfo">
<h3>2010 backuplet</h3>
<ul>
<li>内容：微博备份工具</li>
<li>技术：JavaScript，PHP</li>
<li>发布：『你的阳光』子栏目</li>
</ul>
<img src="2010_backuplet.jpg" width="300" height="194" alt="backuplet 截图"/>
</div>

<div class="siteinfo">
<h3>2011 必应出口通管理平台</h3>
<ul>
<li>内容：adCenter中文管理平台</li>
<li>技术：ASP.NET MVC，adCenter API</li>
<li>发布：platform.adcenter.msn.com.cn</li>
</ul>
<img src="2011_adCenter.jpg" width="300" height="132" alt="必应出口通管理平台 截图"/>
</div>

<div class="siteinfo">
<h3>2011 你的阳光</h3>
<ul>
<li>内容：个人Web2.0聚合，学习资料，技术文章和作品</li>
<li>技术：XHTML，PHP，jQuery</li>
<li>发布：linux收费空间、VPS，yoursunny.com域名</li>
</ul>
<img src="2011_yoursunny.jpg" width="300" height="208" alt="你的阳光 截图"/>
</div>

<div class="siteinfo">
<h3>2011 网上名片</h3>
<ul>
<li>内容：自我介绍、联系方式</li>
<li>技术：手写XHTML，PHP，二维码</li>
<li>发布：『你的阳光』子栏目</li>
</ul>
<img src="2011_m.jpg" width="300" height="177" alt="网上名片 截图"/>
</div>
