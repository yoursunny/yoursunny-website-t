---
title: 阳光男孩精神 sunny boy spirit
lang: zh
date: 2005-05-15
tags:
- commentary
---

## 乐观 optimistic

“阳光”的原意就是乐观。真正的阳光男孩，总是可以精神愉快，对事物的发展充满信心。他可以永不言败，因为，只要心跳还在，不管已沦落到什么地步，都有可能东山再起。
在阳光男孩的心里，不存在什么“悲观”。他可以伤心，可以哭，但那只是暂时的，因为，他不会绝望。只要有理想，就应该坚信自己的理想在某一天终会实现！

## 正直 upright

正直，就是没有偏私、心地坦白。世界上不可能存在绝对的公正，但阳光男孩总是尽力地使他周围的世界变得公平，至少他不会偏袒哪一个人而伤害另一些人。他尽力避免别人受伤害，同时也尽力保护自己。不要说他不勇于牺牲，他不怕死，只是他不死，从而来保护别人。
阳光男孩始终追求着真理。真理固然是相对的，而他有他自己的价值观。他会拥护他认定的真理，并为之奋斗。他也会帮助同样追求真理的人们。

## 善良 kind-hearted

真正的阳光男孩应该心地纯洁，没有恶意，他对每一个人都是这样的。“善”是一种美德，可是在历史上曾被歪曲了。阳光男孩的“善”，体现在他不会对任何人怀有敌意，不会主动攻击别人、伤害别人，也不会坐视别人受伤害，同时热心公益事业。

## 大度 magnanimous

阳光男孩的气量很大，既能够容纳各种不同意见。他可以判断这些意见的正确与否，并可能选用其中的一部分。虽然他可能很难被说服，但是如果给出足够的证明，他一定会接受的。
气量的另一层含义是容忍谦让的限度。阳光男孩自然可以比常人容忍更多，但绝不是无限的！所以他也不是个出气筒。

## 勤劳 diligent

又是一种传统美德，资本原始积累的重要途径。阳光男孩勤于学习，同样也勤于工作。不怕辛苦，但不能一味蛮干，否则在错误的方向上不管怎么辛苦都是徒劳，甚至是危害。
阳光男孩只做有利于社会的事情，那些害人害己的事情，哪怕给高薪，也不干。顺便说一句，阳光男孩不是神仙，所以只奉献不索取是不可能的，因此不可能不领取工资。
阳光男孩做事会选取便捷的方法。选对方法，做起事来四两拨千斤，事半功倍。不是他没有能力用传统方法，而是他利用自己的智谋省下资源，用于其它有意义的事。

## 勇敢 courageous

正如鲁迅所说：“真的猛士，敢于直面惨淡的人生，敢于正视淋漓的鲜血。”真正的阳光男孩，就必须不怕危险和困难，有敢作敢为毫不畏惧的气魄。凡是缩手缩脚，只会一事无成，所以，勇敢是成功的有力保证。
不过，不怕危险并不是不避危险。为了用更多的成就、减少不必要的牺牲，阳光男孩懂得要最大限度的规避风险。他不会自不量力，除非周围环境决定了他必须上去，他会思考换一种方式达到同样目的而减少可能的危险。当然，无法避免风险时，他一定会挺身而出的，这是万一遇到不测，也无怨无悔死而无憾了。
另外，勇敢不同于鲁莽。过于轻率、欠考虑的行动往往会增加危险，从而削弱自己，这是完全没有必要的，也是很不应该的。

---

2017-04-24注释：“阳光男孩”是本人曾经的昵称。『阳光男孩精神』始终指导着我的思想和行为。
