---
title: "NDN hackathon proposal: Clock setup for devices without hardware RTC"
lang: en
date: 2015-08-27
tags:
- NDN
---

This is one of the proposals I submitted to the [first Named Data Networking hackathon](https://ndncomm.github.io/).

## Motivation

Certain cheap devices, most notably the Raspberry Pi, comes without a hardware Real Time Clock (RTC), and relies on Network Time Protocol (NTP) to setup its clock during system boot.
NTP is an IP based protocol which would not work in a pure NDN network.
It's necessary to define an NDN based protocol for clock setup.

## Approach

protocol design and demo system

## Contributions to NDN

Explore trust model without relying on accurate clock.

## Functional Requirements

The protocol should allow a device without hardware RTC to obtain a timestamp from a time service over NDN network.
The signature from the time service must be verified.

Aspects to consider:

* The timestamp must be fresh, and accurate to the order of 100 milliseconds.
* The time service must serve the general public.
  It's unfeasible to manage one symmetric key per device.
* The key used by the time service must be replaceable, and every key directly used by the time service has a validity period of at most one year.
  Key signing keys can have longer validity period.
* Network transmission, signing, and signature verification consume non-zero time.

The protocol should be written as a document; having only a demo system but not protocol document is unacceptable.

The demo system consists of a time service and a client.
The time service should be running on a computer.
The client may be deployed on a Raspberry Pi or a computer, but it is not allowed to read the system clock.
It's okay to save state (such as last known timestamp) on external storage.
It's okay to require manual intervention if the device has been powered off for more than 30 days, but no manual intervention should be requested if the device has been powered off for less than 30 days (note: to demonstrate passing 30 days, fast forward the clock on time service).
It's assumed that time can never go backwards. It's okay to not consider leap seconds.

## Metric

* Accuracy (<1ms round trip delay): accuracy of timestamp, compared to NTP
* Accuracy (>100ms round trip delay): accuracy of timestamp, compared to NTP
* Security: whether the trust model can correctly determine whether the key used by the time service is valid or expired
