---
title: Faster Content Distribution with Content Addressable NDN Repository
lang: en
date: 2013-12-09
tags:
- NDN
---

The effectiveness of universal caching in Named Data Networking depends on data naming.
NDN routers cannot identify duplicate contents published under different Names.
This paper proposes an enhancement to NDN repository so that duplicate contents could be identified by their hash, in order to save bandwidth and shorten download completion time.
The repository indexes Data packets not only by Name, but also by hash of payload.
Client applications could retrieve chunks by hash from a nearby neighbor.
Therefore, the same payload does not have to traverse the Internet twice.
Our evaluation shows that total download time for two Linux Mint disc images is reduced by up to 38%.

[Download full report as PDF](https://github.com/yoursunny/carepo/blob/master/docs/report.pdf)  
[Browse code repository](https://github.com/yoursunny/carepo)
