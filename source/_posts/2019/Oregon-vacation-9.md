---
title: Oregon Vacation (9) Oregon City
lang: en
date: 2019-07-07
tags:
- Oregon
- geocaching
- travel
image: elevator.jpg
---

Driving around the highways south of Milwaukie, I repeatedly saw an [End of Oregon Trail](https://goo.gl/maps/5KNS8T2QB7CpzFW68) sign.
I vaguely remember having played a game called [The Oregon Trail](https://amzn.to/2XqV8pj), but I don't know exactly what "Oregon Trail" is, so I looked it up.
[Oregon Trail](https://en.wikipedia.org/wiki/Oregon_Trail) is a 2170-mile wagon route between Independence, MO and Oregon City, OR, traveled by some of the earlier pioneers that settled in the west.
Given its historic importance, after [completing my geocaching mission](/t/2019/Oregon-vacation-8/), I decided to spend part of my last two days to visit Oregon City.

## Elevator, Artwork, and Oddities

My evening tour of Oregon City was entirely guided by three geocaches:

* [Going Up](https://coord.info/GC7ACEE) brought me to Oregon City Municipal Elevator.
* [OC Downtown Artwork](https://coord.info/GC4PHWD) highlights some of the outdoor sculptures.
* [Oddities & Commodities](https://coord.info/GCMZYQ) showcases oddball Oregon City trivia.

![selfie with Oregon City Municipal Elevator](elevator.jpg)

I started with the [elevator](https://www.orcity.org/publicworks/municipal-elevator).
As I drove into Oregon City on an arch bridge, the elevator appeared right in front of me.
The geocache listing suggested me to park on top of the elevator.
To reach the top, I had to make several turns and drive up a steep hill.
Parking is free in the evening.

![above Oregon City Municipal Elevator](HighSt.jpg)

The elevator has an observation deck at the top, offering a view of the Oregon City Arch Bridge that I just drove on.
Inside the observation deck are some picture panels that display the "now and then" of Oregon City, but unfortunately they are too faded for a clear view.
I collected necessary information for the geocache, and proceeded to the actual elevator.

![Oregon City Municipal Elevator - floor 2](floor2.jpg)
![Oregon City Municipal Elevator - floor 1](floor1.jpg)

The actual elevator is less exciting than the building that contains it.
It does not have glass windows like Seattle's Space Needle, nor is there an altitude indicator like New York's Empire State Building.
It's just an elevator like in any office building.
The "official elevator operator" proudly claims that the elevator is [listed](https://npgallery.nps.gov/NRHP/AssetDetail/aeb5e4fb-d54d-4267-9bf2-5c7b40e21d82) on the national register of historic places.

Below the elevator lies Oregon City's central business district.
It is only two blocks wide between a cliff-side railroad and the Willamette River.
All of Oregon City's residential neighborhoods are located on top of the 27-meter cliff.
Having an elevator greatly improves the walkability of the city.

![mural at Don Pete's fresh Mexican food](DonPepes.jpg)

I walked around the area, saw numerous artworks and murals, as well as buildings dating back more than 150 years.
Interestingly, there are three [horse rings](https://goo.gl/maps/Sngi4jicqCKvir919) cemented into the sidewalk, where visitor could tie their horse so that it doesn't wander off.

![a horse ring in Oregon City downtown](horse-ring.jpg)

I rode the elevator back up at 20:58, half an hour before they close for the night.
I watched sunset at the observation deck, and then drove back to the motel.
There are more to see in Oregon City, and I'm not quite done yet.

## Industry and History of Oregon

Thursday, June 13 was the last day of my Oregon vacation.
My flight was scheduled at 20:55, so that I still have a full day.
However, I no longer have a hotel room to shower, and I need to move closer to the airport as the day progresses so that I won't miss my flight.

![Oregon City Arch Bridge](ArchBridge.jpg)

I had a late start at 09:13, after finishing off [remaining food in the fridge](/t/2019/Oregon-vacation-7/) and packing everything into the car.
I returned to Oregon City in the morning.

![Willamette Falls](WillametteFalls.jpg)

First, I viewed Willamette Falls.
In the 19th century, a power plant and several paper mills were built here, giving birth to Oregon's industry.

![Donation Land Claim marker](DonationLand.jpg)
![original municipal elevator](old-elevator.jpg)

Then, I proceeded to the [Museum of Oregon Territory](https://clackamashistory.org/museumoftheoregonterritory).
I selected this museum instead of [End of Oregon Trail interpretive center](https://historicoregoncity.org) because the former has better reviews on Google Maps.
Exhibits in this museum educated me about the history of Oregon Trail and Oregon Territory.
I'm fascinated to learn about [Donation Land Claim Act](https://en.wikipedia.org/wiki/Donation_Land_Claim_Act), which offered free land for agricultural use to anyone who came to settle in Oregon.
I saw a photo of the original elevator, which was replaced by the current elevator in 1955.
There's also a replica of Kaegi's Prescriptions, an antique pharmacy since 1890s that once had the cure for everything and sold everything from rattlesnack oil to rock candy.

<iframe width="600" height="400" allowfullscreen style="border-style:none;" src="/t/demo/pannellum.htm#panorama=/t/2019/Oregon-vacation-9/pharmacy.jpg&title=Kaegi's Presciptions&autoLoad=true&yaw=140&minPitch=-60&maxPitch=60"></iframe>

## Vacation Continues

11:41 was the time I left the museum.
I went to see the historic McLoughlin House, although I couldn't enter it.
It opens only on Fridays and Saturday, when I was [carried away by Oregon's oldest geocaches](/t/2019/Oregon-vacation-2/) and cancelled my reservation of a [Oregon City walking tour](https://www.meetup.com/uniquelyportlandtours/events/261792837/).
You can't have everything in life, right?

I rode the elevator once again to Oregon City downtown, looking for a nice place to have my last lunch.
As I'm browsing the menu, a cool kid [requested my help](https://twitter.com/yoursunny/status/1139248581855109135) to push his race car, which lacks a reverse mode, out of a hillside parking spot; I happily helped with my muscles, and declined his invitation of a beer.
Then, I had some local salmon at [Weinhard Grill](https://weinhardgrill.com/).

My Oregon vacation continues toward Portland International Airport in [part 10](/t/2019/Oregon-vacation-a/).
