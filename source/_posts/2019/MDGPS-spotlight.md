---
title: "Maryland Geocaching Society 2019 February Member Spotlight: yoursunny"
lang: en
date: 2019-02-17
tags:
- geocaching
- Maryland
discuss:
- https://www.mdgps.org/forum/viewtopic.php?f=50&t=9961
---

**Interview Date**: Feb 17, 2019.

**Caching Name**: `yoursunny`.

`yoursunny` should be all lowercase, even if it's the first word of a sentence, with no space in between.
`Yoursunny`, `yourSunny`, `Your Sunny`, `yoursonny`, etc makes me sad.
[GC830Q6](https://coord.info/GC830Q6) has additional explanation on case sensitivity.

**Real Name**: Junxiao Shi.

![yoursunny at GC7F93W](GC7F93W.jpg)

**1. How did you become involved in geocaching?**
It all started from Jenny.

Grocery stores offer discounts with a loyalty card, but I forgot to bring mine on Jul 12, 2013.
I [complained on Facebook](https://www.facebook.com/yoursunny/posts/10151700144103827), and my friends told me that I could give my phone number to the cashier, and they would be able to apply the discounts.
[Daniel](https://www.facebook.com/daniel.harms)'s answer mentioned that I could enter "area code + 867-5309" as the phone number, and there is usually a loyalty card associated with this phone number.
I did not understand what's special about this phone number, so I started online searching.

![Facebook post about forgotten grocery loyalty card](8675309.png)

The search brought me to [5208675309.com](http://web.archive.org/web/20090228025112/http://5208675309.com/), in which "520" is the area code of Tucson AZ, where I was living at that time.
I had no clue what this website is about, but it points me to [www.geocaching.com](https://www.geocaching.com/), so I curiously followed the link.
I read the description and watched the video about this treasure hunt game, and I thought it matches my interests:
First, I need more activities, because summer is boring when all my friends are out of town.
Second, my phone has a GPS, and I haven't been using it much.

![5208675309.com](5208675309.png)

I set out to [find my first geocache](https://coord.info/GLBH1FX6) on Jul 15, 2013.
It was [gopro25's BIG bucket cache](https://coord.info/GC30ZDD), which I carefully selected from the list recommended for beginners.
The cache is located in a neighborhood across the street from my student apartment.
I wrote down the GPS coordinates from the website, entered it into Google Maps app on my phone, and followed the blue dot as I walked toward the cache.
Without much difficulty, I uncovered a 5-gallon bucket half buried on the ground.
Following [the instructions I read on the website](https://www.geocaching.com/guide/), I signed the logbook, and traded a paddle ball toy with a shoe cleaning product.

Read my blog post [How I Started Geocaching](/t/2017/start-geocaching/) for more stories.

**2. How did you choose your caching name?**

I called myself `sunny boy` during high school, because it represents a positive attitude and I enjoyed tanning in the sunlight.
In 2006, when I wanted to register a `.com` domain name for my website, `yoursunny.com` was the first name I thought about that was still available.
Thus, many of my online IDs are `yoursunny`.

**3. How many caches have you found so far?**

I have 1125 finds as of interview date.
About half of these finds are since I moved to Maryland in Nov 2017.
16% of my finds are hidden by [the same CO wearing a red cap](https://www.mdgps.org/forum/viewtopic.php?f=50&t=9465).

**4. What brand/type of GPS do you use?**

What GPS? You mean smartphone?
I currently use a Moto x4 Android phone with a protective case.
I chose this model with geocaching in mind: it is water resistant so that I won't damage it in the rain, and I can wash it when it gets dirty.
My primary app is [c:geo](https://www.cgeo.org/) because it has the best offline features.
I still do not have an unlimited cellular data plan so being able to find caches offline is essential.
I also have [Geooh Live](http://www.geooh.com/) app for Wherigo only.

I also have a Windows Phone as backup, running [m:geo](https://maaloo7.wordpress.com/) app.

**5. What programs/software or hardware (PDA/laptop/phone) do you use to make caching easier?**

I have a Raspberry Pi 3B running Ubuntu Linux as my desktop.
I use this computer to write programs for solving puzzles.
For example, I solved [Latin GeoSquare](https://coord.info/GC782KZ) with linear programming.

```py
print("SUBJECT TO")

# one letter per cell
for row in ROWS:
  for col in COLS:
    print(" + ".join(["C%x%x%s" % (row, col, ltr) for ltr in LTRS]) + " = 1")

# each letter appears once on a row
for row in ROWS:
  for ltr in LTRS:
    print(" + ".join(["C%x%x%s" % (row, col, ltr) for col in COLS]) + " = 1")

# each letter appears once on a row
for col in COLS:
  for ltr in LTRS:
    print(" + ".join(["C%x%x%s" % (row, col, ltr) for row in ROWS]) + " = 1")

# each letter appears once on NW-SE diagonal
for ltr in LTRS:
  print(" + ".join(["C%x%x%s" % (row, col, ltr) for (row, col) in zip(ROWS, COLS)]) + " = 1")

# each letter appears once on NE-SW diagonal
for ltr in LTRS:
  print(" + ".join(["C%x%x%s" % (row, col, ltr) for (row, col) in zip(ROWS, reversed(COLS))]) + " = 1")
```

I use [Project-GC's Map Compare](https://project-gc.com/Tools/MapCompare) to plan trips with other geocachers.

**6. What type of cache do you prefer seeking – traditional, multi, puzzle, virtual?**

During my first year, I only sought Traditionals.
The "7 souvenirs of August" promotion in 2014 forced me to explore other cache types, but my preference was still Traditional.

Unlike most people that have a vehicle, I rely on public transit and walking.
I have been reluctant to work on puzzles because the final location could be too far from a bus stop.
Starting 2019, ["the dumbest geocacher out there"](https://coord.info/GLX5R533) made a deal with me: he'll drive me around forever if he can get puzzle solutions.
So far we have found seven puzzle caches during three trips.

**7. Which caches were the most challenging – physically/mentally?**

The most physically challenging cache I've found is [Sign of Niles Ohana (SOF Warrior)](https://coord.info/GC5860Y) in Tucson AZ.
The trail head looks like this on satellite map:

[![satellite map of GC5860Y trail head](GC5860Y.jpg)](https://www.google.com/maps/place/32%C2%B012'51.4%22N+110%C2%B058'53.3%22W/@32.2151246,-110.9811307,67a,35y,199.13h,53.84t/data=!3m1!1e3!4m5!3m4!1s0x0:0x0!8m2!3d32.2142833!4d-110.9814833)

The cache is in a drainage tunnel, located 262 meters from the entrance.
I went in alone, twice, and found it on the second try.
My blog post [Geocaching is All About Expanding My Comfort Zone](/t/2019/geocaching-comfort/) has the details about how I found this cache.

There's a baby version of it in Aspen Hill, MD: [MH Trail: You Mean I Have To Go In THERE?!?!](https://coord.info/GC5NFPE)
I crawled in without hesitation, since I was going to [Rock Creek Conservancy](https://www.rockcreekconservancy.org/)'s park and stream cleanup that day and would get dirty anyway.

![selfie at GC5NFPE](GC5NFPE.jpg)

The most mentally challenging puzzle I've solved and found is [It's Got Layers, Like an Ogre](https://coord.info/GC7HZ18) in Germantown MD.
It requires knowledge not only in modern technology, but also in classical ciphers.
I know my way around mathematics and computers, but I have trouble in those ciphers.
It took some teamwork to come up with the final solution, and I made this my 800th milestone.

**8. Do you have a favorite or favorites among the Maryland caches that you've found?**
(Feel free to list a favorite for each type of cache)

* Favorite placement style: [Suburban Jungle Continues](https://coord.info/GC7ZHDM) in Gaithersburg.
  It's on a tree taller than me that I'm not supposed to climb.
* Favorite creative container: [Paul's Scrap Parts #6](https://coord.info/GC6YE0Q) in North Bethesda.
  The container is very common, just not as a cache container.
* Favorite multi: [Monocacy Aqueduct](https://coord.info/GC6F7JD) in Dickerson.
  It's a tour that allows me to learn a bit of Civil War history.
* Favorite puzzle: [Benoit's Bender](https://coord.info/GC6XKJ9) in Columbia.
  It took me only 15 minutes, but it demonstrates the beauty of mathematics.
* Favorite non-puzzle mystery: [I Got The Geocaching Blues](https://coord.info/GC79Y51) in College Park.
  I got the rare "wireless beacon" attribute with this cache.

**Do you have a favorite in a nearby state?**

* Favorite in DC: [← U ↑ C → D ↓ C?](https://coord.info/GC7YYKJ) starting from Adams Morgan.
  It's my first Reverse Cache and my 900th milestone.
* Favorite in Virginia: [Ismo Dalgaard Laffybottom: Crow's Nest](https://coord.info/GC4TFBX) in Tysons.
  In true letterboxing style, I followed a hand-drawn map to find the cache.
  It was in August and I ended up with 33 mosquito bites.
* Favorite in Delaware: [Cypress Point](https://coord.info/GC3B8) in Laurel.
  It's in the same park as Delaware's oldest, but has a better view.

**9. What's the most unusual thing that you've ever found in a cache?**

There's a smartwatch in [Wootton Parkway @ Greenplace # RFC4648.xz](https://coord.info/GC7YC4G).
It's there because I put it there as FTF prize, but nobody took it so far.
I hope it gets claimed before water gets into the container.

**10. What are your current caching goals?**

My 2019 goal is: hide a cache every month.
It has been going well so far.

I don't set finding goals because they would be too stressful.
However, I notice that the lower part of my Jasmer grid doesn't have many blanks, and it would be nice to fill this part.

![my Jasmer grid since 2005](Jasmer.png)

**11. Is there a certain cache that you can't wait to do?**

I have a [wishlist](https://www.geocaching.com/bookmarks/view.aspx?guid=346ac807-45d5-40ab-a45d-53701a45bd92) that contains caches I want to visit someday.
One of them is [Intersection](https://coord.info/GC7TDB1), located at the intersection of Maryland, Virginia, and DC.
I want to go there because of its unique location.

**12. How many caches have you placed?**

11 in Montgomery County MD as of interview date.
More than half of my hides are non-traditionals, including two AR_ experiments.

Additionally, I had 7 hides in Tucson AZ that have been adopted away.

**12. What advice would you give someone that wants to place a cache?**

If you place a Mystery in Montgomery County, it will become a lonely cache.
The county has more than 1200 Traditionals owned by [the same CO wearing a red cap](https://www.mdgps.org/forum/viewtopic.php?f=50&t=9465).
Most people would seek those green icons and ignore your blue icon.
Thus, place your non-traditionals in an area with less cache saturation, so that you won't get frustrated of not receiving enough "found" emails.

**13. How often do you go caching?**

I had 88 caching days in 2018.
It's mostly a weekend thing, plus some weekday events.
I found exactly 365 caches in 2018.

**14. What advice would you give a beginning geocacher?**

You'll need a Premium Membership in Montgomery County.
Otherwise, most of the aforementioned 1200 green icons will be invisible to you.

**15. Have you completed CAM in the past?**

No, and I don't plan to participate in CAM because, like finding goals, it's stressful.

**16. Do you collect geocoins?**

Not really, although I often show off my [Yuma Mega 2017 geocoin](https://coord.info/TB847C1) at events.
I'm working on earning an [Eight Wards of Washington DC geocoin](https://dpr.dc.gov/page/eight-wards-washington-dc-geotrail).

**17. What type of gear do you carry with you on your caching trips?**

* two pens
* tweezers
* water bottle or water bladder
* one or two phones
* USB power bank and charging cable
* Bluetooth headset, so I can listen to [PodCacher](http://www.podcacher.com/) while walking
* SmarTrip card (bus pass)
* a change of clothes, if I'm driving a rental car; sometimes I jump mud puddles to cool off, and I need to keep the vehicle clean

New since 2019:

* Darn Tough socks, so that my feet stays warm even if I need to wade across a creek; I [don't like waterproof boots](https://coord.info/GLXY5JCR) so these socks are the next best thing
* replacement log sheets issued by [the CO wearing a red cap](https://www.mdgps.org/forum/viewtopic.php?f=50&t=9465) fr maintaining his and his granddaughter's caches; all other COs will continue to receive "needs maintenance" if logbook is full
* log roller
* magnetic pick up tool, although I haven't used it

**18. What is your most memorable caching experience?**

The most memorable so far has to be [Yuma Mega](/t/2018/YumaMega/), since this is my only multi-day trip primarily for geocaching.

![yoursunny met Signal the Frog at 2017 Yuma Mega](Signal.jpg)

During my return from that event, I encountered a freeway sign pointing to a parking area with "no service".
My instinct told me that there must be something interesting, so I exited the highway and parked the car.
I was right: this is the site of [Yuma Area Veterans B-12 Bomber Memorial](http://www.gendisasters.com/arizona/14133/yuma-az-near-bomber-crash-june-1944), in memory of a B-17 bomber crash during a training mission in 1944.
And yes, this parking area has a [geocache](https://coord.info/GCN6DR).

**19. What is your best caching story?**

In 2018, I noticed that my milestones since 400 have different icons: 400 is Multi, 500 is Virtual, 600 is Traditional, 700 is Event.
Since then, I have been attempting to collect different icons on my milestone page.
I chose a Mystery for 800 and a Wherigo for 900.
When 1000 was coming up, I decided to continue the pattern and find the rarest type: Webcam.

The only Webcam cache nearby is [Scott Circle Webcam](https://coord.info/GCN21R).
On Jan 08, 2019, I went for a walk in DC Ward 1 and Ward 2.
I not only found the Webcam as my 1000th milestone, but also collected eight icons in one day:

* 1x Webcam
* 2x Virtuals
* 6x Traditionals
* 1x Wherigo
* 1x EarthCache
* 1x Letterbox
* 1x Mystery
* 1x Multi

I pulled this off in 9 hours (including 2 hours in museums and restaurants), mostly walking and 45 minutes on a bikeshare.
I intend to log an EarthCache as a future milestone so that I can have eight icons in milestones.

![my milestones](milestones.png)

**20. What do you like about geocaching? What keeps you going?**

I started geocaching as a form of physical exercise, because it makes me go outside more often.
Geocaching also lets me discover new places.
I've found many tiny parks and museums and monuments because there is a geocache nearby.

**21. Besides geocaching, what other things do you like to do?**

I like to visit museums.
So far I've visited 10 museums in DC and Baltimore area.
I spent 6 days in Smithsonian Air & Space Museum (DC) alone.

Visiting museums enhances my knowledge, and gives me puzzle ideas such as [Firstfield @ Quince Orchard # Inka](https://coord.info/GC7ZXM8).

**22. What question did you expect us to ask but didn't. What is the answer?**

How many DNFs do you have?

I have 417 DNFs as of interview date.
I have so many DNFs because I often spend less than 10 minutes on a single cache because I may as well walk to the next cache in the same amount of time.
I ever [DNF'ed a cache](https://coord.info/GLXXCEMM) that I witnessed being placed.
I am proud of the existing DNFs, given that Geocaching HQ awarded April 2014 [Geocacher of the Month](https://youtu.be/sJRZnhq3v0Q) to `Dean F` for logging more than 50000 DNFs.

However, I'm working on reducing my DNF ratio.
I've been out caching with experienced geocachers, observing how they search for caches and when they declare a cache being missing.
I'm also trying to become more patient, especially for a non-traditional that I already invested time on.

**Bonus Questions: How does your family feel about your geocaching habits?**

My parents are unsupportive because they think:

* Looking for magnetic key holders in guardrails is a waste of time.
* Being out alone in the woods is dangerous.

Nevertheless, they are half a word away so they can't stop me.
They do enjoy seeing photos of colorful homes and interesting parks in DC and Maryland, and I would casually mention that I found them during geocaching.

When my father visited me in June 2018, I took a one-week vacation, driving him around in a rental car.
He is okay with me finding a few caches around the way, as long as the primary goal is sightseeing.
He even helped me taking pictures for [The Point at Harpers Ferry](https://coord.info/GC5RHJW), but he let me climb [Maryland Heights (part II)](https://coord.info/GCGZV1) alone and elected to wait in the town.

Their attitude turned slightly positive when I'm not alone.

![Georick402 and yoursunny at GC7FEKN](GC7FEKN.jpg)
