---
title: Mystery at the Museum - Plan a Trip
lang: en
date: 2019-07-11
tags:
- geocaching
image: filter-enabled.png
---

Geocaching.com's newest promotion, [Mystery at the Museum](https://www.geocaching.com/promos/mysteryatthemuseum), is online today.
To qualify for the first three souvenirs, you must find and log a number of geocaches, **in a certain order**.

Geocaching.com offers a filter function that shows which geocaches to find and log next.
However, it only allows me to select the clue tiers that I currently qualify for.
Since I haven't found any geocaches since the promotion started, I can only search for the *Detective* clue, but cannot search for any other clue types.

![CLUES filter disabled except for Detective](filter-disabled.png)

This is inconvenient for me, because I prefer to go geocaching offline.
I usually download a number of geocaches to my phone, go out to find them, and then log them at end of the day.
I want to be able to search for clue types beyond *Detective*, even if I don't currently qualify for them.
Then, I can go out, find enough geocaches to complete two or three souvenirs together, and then log them into the correct order.

I poked around the URI parameters in the address bar, and found two methods.
I thought I'd share them, in case anyone needs it.

## Search Map by Clue Type

### STEP 1

To get started, find and log 1 geocache with this clue:

* [Detective](https://www.geocaching.com/play/map?tr=1)

This should earn you "briefed on the case" souvenir.

### STEP 2

Then, find and log 2 geocaches with each of these clues:

* [Footprint](https://www.geocaching.com/play/map?tr=2)
* [Shadow](https://www.geocaching.com/play/map?tr=3)
* [Fingerprint](https://www.geocaching.com/play/map?tr=4)

You may use these search map links to identify which caches contain the necessary clues.
However, do not log these caches until you have logged the caches in step 1 and received "briefed on the case" souvenir.
Then, the 6 caches within this step may be logged in any order.
If done correctly, this should earn you "evidence collected" souvenir.

### STEP 3

Finally, find and log 3 geocaches with each of these clues:

* [Sapphire](https://www.geocaching.com/play/map?tr=5)
* [Ruby](https://www.geocaching.com/play/map?tr=6)
* [Diamond](https://www.geocaching.com/play/map?tr=7)
* [Emerald](https://www.geocaching.com/play/map?tr=8)
* [Topaz](https://www.geocaching.com/play/map?tr=9)

You may use these search map links to identify which caches contain the necessary clues.
However, do not log these caches until you have logged the caches in step 2 and received "evidence collected" souvenir.
Then, the 15 caches within this step may be logged in any order.
If done correctly, this should earn you "jewels recovered" souvenir.

## Unlock the Filters with JavaScript

If you want to perform a custom search, it's possible to unlock all grayed out filters with JavaScript.

1. Open the [search map](https://www.geocaching.com/play/map).
2. Open the browser's developer panel. In Firefox, the hotkey is F12.
3. Select "Console" tab.
4. Paste the following code snippet and press ENTER key to execute.

```js
for (let x of document.querySelectorAll('.is-disabled')) {
  x.classList.remove('is-disabled');
  x.classList.remove('is-active');
}
for (let x of document.querySelectorAll('input:disabled')) {
  x.disabled = false;
}
```

This would allow you to set filters as you wish.
To change the filters, just paste the code snippet again.

![CLUE filter enabled](filter-enabled.png)
