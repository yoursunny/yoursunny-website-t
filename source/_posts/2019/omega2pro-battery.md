---
title: Show Omega2 Battery Voltage in OpenWrt LuCI
lang: en
date: 2019-07-24
tags:
- OpenWrt
image: luci.png
---

Sometime ago, I bought an [Onion Omega2 Pro](https://onion.io/store/omega2-pro/) and [flashed it with OpenWrt 18.06](/t/2019/omega2pro-openwrt/).
I plan to take my Omega on the road, so I installed a Li-Po battery on the device, so that it does not need to depend on USB power input.
One big question is: what's the remaining power level?

## Install `power-dock2` to View Battery Voltage

The standalone Power Dock has a set of [battery indicator LEDs](https://docs.onion.io/omega2-docs/power-dock.html#battery-level-indicator-leds) that visually tells how much juice is left.
However, the Omega2 Pro that I have does not have battery indicators.
Instead, I need to use `power-dock2` command to read battery level.
Let me try it:

```shell
root@Omega2:~# power-dock2
-ash: power-dock2: not found
```

It does not work, because I [flashed my Omega with OpenWrt 18.06](/t/2019/omega2pro-openwrt/), and `power-dock2` command is not included in the standard OpenWrt distribution.
The command comes from OnionIoT's OpenWrt feed (package source).
I must add the OpenWrt feed before I can install the `power-dock2` package.

To add the OnionIoT feed to OpenWrt:

1.  Append the feed configuration to `/etc/opkg/distfeeds.conf`:

    ```bash
    echo src/gz omega2_onion http://repo.onion.io/omega2/packages/onion | tee -a /etc/opkg/distfeeds.conf
    ```

2.  Disable opkg signature check, because we don't yet have Onion's signing keys.

    ```bash
    sed -i '/check_signature/ s/^/#/' /etc/opkg.conf
    ```

3.  Refresh package feeds and install Onion signing keys.

    ```bash
    opkg update
    opkg install onion-repo-keys
    ```

4.  Enable opkg signature check, as now we have the signing keys.

    ```bash
    sed -i '/check_signature/ s/^#//' /etc/opkg.conf
    ```

5.  Refresh package feeds again, so that the package list is updated with the newly installed signing key.

    ```bash
    opkg update
    ```

Now we are ready to install packages from Onion's OpenWrt feed.
Unfortunately, the `power-dock2` package [did not properly declare its dependencies](https://github.com/OnionIoT/OpenWRT-Packages/blob/ab947f66214d687bb19277287327f098d5fc1090/power-dock/Makefile#L39-L45), so that a simple `opkg install power-dock2` would not work.
However, it's [easy enough](https://github.com/OnionIoT/power-dock/blob/01c7c611ca2ec61ea5b9e33312cdc58a60fd0dba/scripts/power-dock2.sh#L32) to figure out the correct dependencies.
The command of installing `power-dock2` and necessary dependencies is:

```bash
opkg install kmod-i2c-mt7628 i2c-tools power-dock2
```

After this, I can see my battery voltage via command line:

```shell
root@Omega2:~# power-dock2
Battery Voltage Level: 3.66 V
```

## Display Battery Level on LuCI

[LuCI](https://github.com/openwrt/luci/wiki) is OpenWrt's web user interface.
It is written in Lua programming language, designed to be extensible and easily maintainable, and structured as an Model-View-Controller (MVC) web framework.

I am still learning LuCI, but I have figured out the simplest way to display battery level on LuCI's homepage:
Create a file `/usr/lib/lua/luci/view/admin_status/index/powerdock2.htm`, and paste the following code:

```lua
<%-
  require "string"
  require "luci.sys"
  local powerdock2_output = luci.sys.exec("power-dock2")
  powerdock2_output = string.gsub(powerdock2_output, "Battery Voltage Level: ", "")
-%>

<div class="cbi-section">
  <h3><%:Omega2 - Power Dock%></h3>

  <div class="table" width="100%">
    <div class="tr"><div class="td left" width="33%"><%:Battery Voltage Level%></div><div class="td left" id="powerdock2_output"><%=powerdock2_output%></div></div>
  </div>
</div>
```

This code invokes `power-dock2` command, parses its output, and displays the result on the webpage.

Login to Omega's LuCI, and I can see the battery level at bottom of the page:

![LuCI: Omega2 - Power Dock, Battery Voltage Level 3.64V](luci.png)

It works because:

1. The "homepage" is actually ["status overview"](https://github.com/openwrt/luci/blob/openwrt-18.06/modules/luci-mod-admin-full/luasrc/controller/admin/status.lua#L9).
2. The page template is `/usr/lib/lua/luci/`[`view/admin_status/index.htm`](https://github.com/openwrt/luci/blob/openwrt-18.06/modules/luci-mod-admin-full/luasrc/view/admin_status/index.htm).
3. [At the end of this template](https://github.com/openwrt/luci/blob/openwrt-18.06/modules/luci-mod-admin-full/luasrc/view/admin_status/index.htm#L450-L460), the code *includes* every `.htm` file in `/usr/lib/lua/luci/view/admin_status/index/` directory.
4. Therefore, I can put a file into this directory to insert a new section at the end of LuCI homepage.

I have noticed two limitation in my current approach.
First, when I keep the "status overview" page open, system and network statistics are updated automatically, but the battery level information I added would not refresh.
Second, the code shows battery *voltage*, but it would be nice to know battery *percentage*.
I will improve on these aspects next time.
