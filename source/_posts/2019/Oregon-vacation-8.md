---
title: Oregon Vacation (8) GC16 in Molalla River
lang: en
date: 2019-07-06
tags:
- Oregon
- geocaching
- travel
image: GC16.jpg
---

My primary goal in Oregon is to find some of the world's oldest geocaches.
I've found [GC12, GC17](/t/2019/Oregon-vacation-2/), and the [Original Stash tribute](/t/2019/Oregon-vacation-3/) early on, but there's one more: GC16, hidden in June 2000.
Wednesday, June 12, after [a breakfast at Hitchin' Post](/t/2019/Oregon-vacation-7/), I started my quest into the [BLM lands along Molalla River](https://www.blm.gov/visit/molalla).

## GC16

Just like other historical caches in Oregon, GC16 does not have parking coordinates in its listing.
Once again, Google Maps tells me that [I can drive straight to it](https://goo.gl/maps/XB3GApjX4nySK8Us9).
I know it can't be so easy, so I did my planning and identified two potential trailhead / parking locations:

* Annie's Cabin trailhead, N 45°01.015 W 122°29.001, 3.0 km hike.
* Hardy Creek trailhead, N 45°02.382 W 122°29.348, 2.3 km hike.

I looked over the "Old Oregon Coast Cache Access" document that I received at [GEOregon event](/t/2019/Oregon-vacation-4/), and it suggests the first parking area.
I'd better listen.
It's a half-hour drive from Molalla on a wide, well-maintained road.
I saw several logging trucks passing by, which explain the necessity of good roads.

![Annie's Cabin trailhead](trailhead.jpg)

The hike was modest.
The first section is steep that I needed some [huff and puff](https://coord.info/GC3787A), and then the trail becomes easier.
I encountered two horse riders on the trail, followed by their dogs.

![Annie's Cabin](AnnieCabin.jpg)

At the [midpoint](https://coord.info/GCW9MB), a grass opening appeared, and I saw a log cabin and a fire ring in front of me.
Not knowing what it was <small>(I found the name of trailhead while writing this article)</small>, I walked closer to inspect.
I'm surprised to find that the door is unlocked, and I could go inside.
Inside the cabin, there's a sign explaining that this cabin was built by BLM volunteer Annie Miller, and visitors are welcome to use it, but should keep it clean.
There are even bottled water and first-aid supplies on the shelf for [people who need it](https://www.oregonlive.com/clackamascounty/2013/05/missing_silverton_hiker_found.html).

![inside Annie's Cabin](AnnieCabin-inside.jpg)

An hour of hiking from the trailhead, I reached [GC16](https://coord.info/GC16).
I'm so excited to see this stump, as it marks the completion of my goal of finding four year-2000 caches in Oregon.

![selfie with GC16](GC16.jpg)

The return hike took about 35 minutes.
I ate a protein bar, as I'm not ready to leave the area yet.
There's one more cache to find.

## Wading in Molalla River

The next target is [Oregon Ash](https://coord.info/GC1Z6N9).
This is by no means a historical cache, and has only five favorite points.
Why do I want to find it?
It's *lonely*: the last find was in August 2016, and it hasn't been found in almost 3 years.

![small island in Molalla River](GC1Z6N9.jpg)

Cache attributes tell us why it's lonely:

* may require wading: you may indeed need to get your feet wet
* may require swimming: you may get more than your feet wet

I'm all for [#KeepCalmAndDrenched](https://twitter.com/hashtag/KeepCalmAndGetDrenched) but had to pass [such a cache](https://coord.info/GC1JBPQ) in Colorado winter.
This time it's summer, so I'd go for it.

I have three pairs of shoes to choose from: "waterproof" hiking shoes that I [just bought in Premium Outlets](/t/2019/Oregon-vacation-4/), regular running shoes, and Crocs.
The hiking shoes are waterproof as long as water does not come in from the top, which would not be the case here.
The running shoes would definitely get soaked and would take a day to dry in the hot car, but I don't want to be wearing the hiking shoes all day.
So I chose the Crocs.

![Crocs in Molalla River](Crocs.jpg)
![selfie with car keys hanging from cap](car-keys.jpg)

I changed into shorts, brought a phone and the car keys with me, and left everything else in the car.
[My phone is waterproof](https://amzn.to/2XOQmWN), but the car keys are probably not.
I didn't have a tupperware container with me, so I improvised: I attached the key chain to my cap, hoping my head would stay above the water at all times.

Crocs turned out to be a suboptimal choice: the river is "paved" with mossy rocks.
I brought the ["classic" Crocs clog](https://amzn.to/2LzeJ4u) with a smooth bottom.
These clogs are great for a day at the sandy beach, but unsuitable for slippery surfaces.
I should have used the running shoes.

Once on the island, it didn't take too long to find the large and anchored cache.
23 minutes later, I'm back in the car.

## Three Clocks and a Mint Box in the Library

I drove out of Molalla River area at 11:10.
Lunch is in [Mt Angel](https://www.ci.mt-angel.or.us/), a small city of 2.95 km<sup>2</sup> area.
I chose this city because they have a [Virtual cache](https://coord.info/GCKCYX).

![three outdoor clocks in Mt Angel, Oregon](clocks.jpg)

I walked around the downtown area, saw a row of three clocks, and a [1949 truck](https://coord.info/GC7XEM0) in front of a 1912 warehouse.
I also found [an air-conditioned geocache](https://coord.info/GC6ACDY), a mint box between books in the city's library.

![1949 truck](GC7XEM0.jpg)
![library geocache](GC6ACDY.jpg)

## Push-ups in Clackamas River

A [heat advisory](https://www.oregonlive.com/weather/2019/06/summer-is-coming-blazing-hot-temperatures-in-store-for-tuesday-wednesday.html) appeared on my phone during my lunchtime.
Thus, I adjusted my plans and moved my afternoon Oregon City tour to the evening.

<iframe width="560" height="315" src="https://www.youtube.com/embed/MIwheRjazAY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

From Mt Angel, it could be a 45-minute drive back to Milwaukie, but I'm seeing a beach along the way at the confluence of the Clackamas and Willamette Rivers.
What's a better place to cool off on a hot summer afternoon than a beach?
Therefore, I stopped at [Dahl Beach](https://www.ci.gladstone.or.us/publicworks/page/dahl-beach), and did some push-ups in the Clackamas River.
This is also the fourth confluence I visited during my vacation.

## Vacation Continues

I returned to the motel and slept for two hours.
I needed the rest after a long day in Portland and a morning of hiking.

![High Rocks Park](GC6R4TZ.jpg)

I departed again in the evening at 18:38, and had teriyaki chicken for dinner.
I took a detour into [High Rocks Park](https://www.ci.gladstone.or.us/publicworks/page/high-rocks-park), wanting to find an overrated [D1 T5 geocache](https://coord.info/GC6R4TZ), but had to skip due to high muggle activity.
Then, my Oregon vacation continues onto a big elevator in [part 9](/t/2019/Oregon-vacation-9/).
