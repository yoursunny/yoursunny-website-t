---
title: Oregon Vacation (5) Eight Waterfalls, Two Beaches, and a Webcam
lang: en
date: 2019-06-28
tags:
- Oregon
- geocaching
- travel
image: BensonLake.jpg
---

I arrived in Oregon for a 7-day vacation, without a specific day-to-day trip plan.
I travel alone, have a rental car, and [live in a "central" location](/t/2019/Oregon-vacation-1/), so that I can have the flexibility to spontaneously decide where to go next.
I'm obsessed with geocaching and interested in museums, but I don't want to spend all my time on these.
I need to discover something new.

## Planning for Multnomah Falls

Every motel has a shelf of tourist information booklets.
When I was checking in on Jun 07 evening, I browsed that shelf, and grabbed a few maps.
I asked the front desk lady for recommendation, and she mentioned [Multnomah Falls](https://www.oregon.com/attractions/multnomah_falls).
Photos from the booklet and online search are gorgeous.
I have to see Multnomah Falls during this trip!

[Official website](https://www.fs.usda.gov/recarea/crgnsa/recarea/?recid=30026) tells me that parking is very difficult on weekends between 11:00 and 16:00.
Therefore, I planned the visit on Monday Jun 10, and I would depart early so that I'm parked well before 11:00.

The Q&A section of Google Places reveals a potential problem:

> Q: Are the trails open for hiking? Or are they still closed off from the fire?
>
> A: Closed at the bridge.

I do not know what "the fire" or "the bridge" refers to, but I just [bought hiking shoes](/t/2019/Oregon-vacation-4/) at Woodburn Premium Outlets, and a trail closure would make me frustrated.

I turned to geocaching.com for information: if a geocache on the trail has been found recently, it implies the trails are open.
I noticed a multi-cache, [Devilish Wahkeena Fairy](https://coord.info/GC2ND8R), has been [replaced on Jun 09](https://coord.info/GLYXA451).
It involves a 2-mile hike climbing 1200 feet to the final.
I decided to let this multi-cache be my tour guide, wherever it brings me to.

## Mist Falls

[OpenStreetMap data](https://osm.org/go/WIJZKrVg--) of the area shows dozens of waterfalls in the area; "Multnomah" is just one of them.
300 meters to the west of the multi-cache is [Remains: Mist Falls Lodge](https://coord.info/GCWG6X), which highlights a little-known trail leading to the second tallest waterfall of the area, Mist Falls.
I departed at Jun 10 07:45, and drove to the parking coords for Mist Falls at 08:41.

Finding the geocache [took too long](https://coord.info/GLYXP78P) due to poor coordinates.
I was able to catch a glance of something that resembles a waterfall, but it's deep behind the trees.
As the cache page points out:

> OK, if you're up to it, you can visit Mist Falls closer up.
> Continue following the small trail upstream.
> It becomes very steep - a loose rock and dirt scramble trail.
> Do not do this climb alone.
> If you decide to hike up to the falls, do so at your own risk.

[Loose rocks attacked me](/t/2017/hiking-Arizona/) in 2013 and put me into crutches for two weeks.
Attempting this climb would be unwise, especially when I'm alone and away from my home location.
Attempting this climb is also unprofitable, because there's no geocache on top.
Therefore, I carefully descended back to the car, and drove to Wahkeena parking lot to get started on the multi-cache.

## Wahkeena - Multnomah Loop

The [Devilish Wahkeena Fairy](https://coord.info/GC2ND8R) multi-cache requires me to collect information from a plaque near the parking lot and another plaque near Lemmons Viewpoint in order to determine the final.
It's a 2-mile hike to the final, but I have more sightseeing to do, so I packed a big sandwich, bought from Fred Meyer grocery store the night before, as well as 2 litres of water.

![selfie at Wahkeena Falls](Wahkeena.jpg)

Just a few minutes hiking on paved trails, I'm in front of Wahkeena Falls.
This came almost too easy.
Wahkeena Falls is high, but I'm too close to it that I cannot view the complete picture.

![map of GC2ND8R visible points](GC2ND8R-map.png)

Paved trails ended just after Wahkeena Falls, and here comes the real dirt-trail hiking experience.
Mountains in this area are steep: in a straight line, you could have 70-meter elevation gain within 100 meters, as the map shows.
Nobody likes a 30-degree uphill climb, so they built trail switchbacks.
Nevertheless, you are gaining altitude rapidly, and may have to pause and catch a breath in those switchbacks.

At Lemmons Viewpoint, I had a full view of the Columbia River, and determined the final coordinates of my multi-cache.
It looks that I'm going east toward Multnomah Falls, the namesake of this area.

![Fairy Falls](Fairy.jpg)

An hour into the trails, I'm at Fairy Falls.
An Instagrammer, who I met several times on this trail, was setting up his tripod.
I don't have a DSLR or a tripod during this vacation, but I figured out how to set a faster shutter speed on a smartphone, that allows me to capture each drop of water suspended in the air.
I figured out how to record slo-mo, too:

<iframe width="315" height="560" src="https://www.youtube.com/embed/49iSCUI2Sjc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

After Fairy Falls, there are rows and rows of trees:

<iframe width="600" height="400" allowfullscreen style="border-style:none;" src="/t/demo/pannellum.htm#panorama=/t/2019/Oregon-vacation-5/trees.jpg&autoLoad=true&yaw=150&minPitch=0&maxPitch=0"></iframe>

I found the final ammo can of the multi-cache, and also found evidence of [the fire](https://en.wikipedia.org/wiki/Eagle_Creek_Fire) that destroyed other geocaches in the area:

![burnt tree](burnt.jpg)

I encountered a "lost" hiker with a paper map in hand, asking me where he is.
I checked my geocaching trail maps, and circled a position on his paper map.
He asked "so, we are *approximately* here?"
My answer is "no, you are *exactly* here."

![Benson Lake viewed from Multnomah Falls Trail Switchback](BensonLake.jpg)

A hour since the Fairy Falls, I reached a trio of waterfalls: Ecola, Wiesendanger, and Dutchman Falls.
Then it's another set of steep switchbacks, where I got a view of the mighty Columbia River and Benson Lake.
This section of the trail is much more popular than Wahkeena Falls.
I can see a hundred people on the switchbacks, some without water, some wearing flip-flops - your mileage may vary.

![Multnomah Falls and Lower Multnomah Falls](Multnomah.jpg)

The tallest waterfalls, Multnomah Falls and Lower Multnomah Falls, are saved for the last.
It's a great place to end my 5-hour hike.

## Vancouver, WA

13:54 is the time I reached my car at Wahkeena Falls parking lot.
I don't have the full day planned out, but the closest place starred on my Google Maps is [Fort Vancouver national historic site](https://www.nps.gov/fova/).
One hour later, I found myself on the "wrong side of the state line": Vancouver is in Washington state, just north of Portland, Oregon.
That's fine, as long as I don't mistakenly drive to Vancouver, Canada, because I don't have a visa for that.

The spontaneous decision backfired: neither the visitor center nor the museum opens on Monday.
I can only view the [Historic Flight](https://coord.info/GCH4RJ) monument in front of the museum.
There's a multi-cache associated with this monument, and my [fat finger](https://coord.info/GLYXPAT7) put the final waypoint near the Columbia River.

![selfie at Surprise Beach in Vancouver, WA](SurpriseBeach.jpg)

I went down to the river, and saw a sandy beach extending into the river banks.
It's a hot day.
I haven't seen a beach in years.
I can't resist the urge, so I took off shirt and pants, and cooled off in the river.

The beach visit also earned me two extra smileys, but where's the multi-cache?
Soon enough, I realized my mistake of entering wrong coordinates, drove back to the museum parking lot, and find the cache.

## Webcam

I returned to Oregon shortly after 17:00.
I stopped briefly at [Mock Crest Tavern](https://mockcrest.com/), not for drinking alcohol, but to find [Portland's only Webcam Cache](https://coord.info/GCMNV1).

A [Webcam](https://www.geocaching.com/help/index.php?pg=kb.chapter&id=127&pgid=821) is a unique geocache type.
There's no physical container to find at the location, but a surveillance camera that streams on the Internet.
To "find" a Webcam Cache, one must stand in position, access the video stream on the web, and take a screenshot of a "selfie" as seen by the camera.
New Webcam geocaches are no longer being published, and only 259 Webcam Caches remain as of this writing, making it an incredible rare type.

![selfie via NoPo Blues webcam](GCMNV1.png)

This is my third [Webcam photo taken](https://coord.info/GLYXPBAJ) log since I started geocaching.

## Corner of North Portland

After the Webcam cache, I went to [Kelley Point Park](https://www.portlandoregon.gov/parks/finder/index.cfm?PropertyID=209&action=ViewPark).
I decided to visit this park because it features the [confluence of Willamette and Columbia rivers](https://coord.info/GC1HM4G), and I'm obsessed with "corners" on the map.
This is the second river confluence I visited during my Oregon vacation.

<iframe width="600" height="400" allowfullscreen style="border-style:none;" src="/t/demo/pannellum.htm#panorama=/t/2019/Oregon-vacation-5/KelleyPoint.jpg&autoLoad=true&hfov=60&minYaw=-120&maxYaw=120&minPitch=-30&maxPitch=30"></iframe>

I predict that Kelley Point Park would be a nice place to watch sunset.
However, it's been a long, hot day, and I don't have the energy to wait another two hours for sunset.
Therefore, I found a [popular neighborhood restaurant](https://screendoorrestaurant.com/), and had the only "upscale" dinner during this vacation.

![Crispy Fried Buttermilk-Battered Chicken at Screen Door restaurant](ScreenDoor.jpg)

## Vacation Continues

My Oregon vacation continues into Portland in [part 6](/t/2019/Oregon-vacation-6/).
