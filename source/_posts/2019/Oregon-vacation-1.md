---
title: Oregon Vacation (1) Planning and Departure
lang: en
date: 2019-06-19
tags:
- Oregon
- geocaching
- travel
image: MilwaukieInn.jpg
---

I had a 7-day vacation in northwestern Oregon in June 2019.
This is the biggest solo road trip I've ever had so far.

## Why Oregon?

Earlier this year, [Alaska Airlines messed up my flight](https://twitter.com/yoursunny/status/1103305558696841216), and then I received a $200 voucher.
The voucher entitles me to a free flight to west coast.

Alaska flies to five cities on the west coast:

* Seattle, Washington
* Portland, Oregon
* San Francisco, California
* Los Angeles, California
* San Diego, California

I've been to [Seattle](https://coord.info/TB7VEGQ), [San Francisco](/t/2021/BayArea/), [Los Angeles](/t/2020/LosAngeles/), and [San Diego](/t/2020/SanDiego/).
Portland is the only city in this list that I have never visited, so I choose this destination.
I set the vacation dates in June, because I hear there are less rain in this month.

In April, I booked my outbound flight using the Alaska voucher and my return flight with some AAdvantage miles.
During this process, I learned that booking a flight with voucher or miles differs from normal flight purchases:

* I can only book one-way tickets, not round-trip tickets.
* Only a few seats on each flight are available for award travel.
  Once these are sold out, I can only get inferior routes with long connection times.

My flight would arrive in Portland International Airport (PDX) in the evening of June 07, and depart PDX in the evening of June 13.
I have 6 full days at the destination.

## What to Do in Oregon?

My colleagues soon heard that I'll be vacationing in Portland, Oregon.
They tell me:

> There's nothing in Oregon.

Well, this statement is incomplete.
They should have said:

> There's nothing we know about in Oregon.

I know a little more than them: Oregon is [where geocaching started](https://www.geocaching.com/about/history.aspx).
My primary goal in Oregon is to find some of the world's oldest geocaches:

* [GC12](https://coord.info/GC12) from 2000-May
* [GC16](https://coord.info/GC16) from 2000-June
* [GC17](https://coord.info/GC17) from 2000-July
* [Original Stash Tribute Plaque](https://coord.info/GCGV0P)

Apart from geocaching, I started searching "Portland attractions" on Google Maps, and [identified several interesting places](/t/2019/Oregon-vacation-6/):

* International Rose Test Garden, where they planted 10000 kinds of roses
* Portland Japanese Garden, the most authentic Japanese garden outside Japan
* Pittock Mansion, Portland's local history museum
* Shanghai tunnel, I don't know what it is but I'm from Shanghai and the name catches my eye

Oregon has more to offer than "nothing", if you search for them.

## Where to Stay?

Many American films depict the lodging situation on a road trip to be spontaneous.
During the day, you keep driving.
When night falls, you look for a "motel" sign along the highway, park your car, and ask for a room.
This sounds fun, but it would cost a lot of money.
As an organized person, I'd like to have more predictability in lodging.

I decide to stay in the same place all week, instead of booking multiple motels and moving between them.
This allows me to spontaneously decide where to go based on weather condition and body energy level, and I do not have to commit to a day-to-day plan long before my trip.
I can also enjoy cheaper pricing for booking a longer stay.

![1-hour driving from Milwaukie](OAlley-Milwaukie.png)

I plotted locations of geocaches and attractions on a highway map, and decided a "central" location that I should stay at: Milwaukie, Oregon.
From Milwaukie, I can [drive to most destinations](https://www.smappen.com/app/map/t5e) in one hour or so.
I purchased an Expedia vacation package including 6 nights at the [Milwaukie Inn Motel](https://www.expedia.com/Portland-Hotels-Milwaukie-Inn-Portland-South.h1327163.Hotel-Information) and a rental car for $747.75.

## Departure Day

After booking the trip, I started counting days toward June 07, the departure date of my big vacation.
Although I did not build a [countdown clock](/t/2017/ESP8266-LCD-count-up-timer/), I was checking Oregon weather almost daily.
Early June, weather reports started appearing in "10 day" section of weather apps, and it [looked good](https://twitter.com/yoursunny/status/1136471475924459520).

June 07 finally arrived.
I packed my bags, and boarded the bus toward Baltimore (BWI) airport at noon.
I came to BWI airport four hours early for my flight, so that I have enough time to find [BWI/Rental Car Travel Bug Hotel](https://coord.info/GC85VK2) geocache.
Six hours later, I arrived at PDX airport and took the shuttle to Budget car rental.
Before going into the car rental counter, I walked across the street and found the nearest geocache, setting my "maximum distance in a day" record at 3784 km.

![maximum distance in a day](GC85VK2-GC72Q05.png)

The rental car is a Hyundai Elantra.
I noticed a 17% "tax" on the liability insurance I'm purchasing, and I'm told it's an airport surcharge.
I should have followed [this advice](https://twitter.com/parlyn/status/1117775195778048001) to avoid an airport rental car, but it's too late now.

## Vacation Continues

Traffic in Portland is "pretty bad" according to rental car counter, but I found it better than Washington DC.
I drove to the motel, got my room, and then ventured out to Popeyes for a late dinner.

![Milwaukie Inn](MilwaukieInn.jpg)

My Oregon vacation continues in [part 2](/t/2019/Oregon-vacation-2/).
