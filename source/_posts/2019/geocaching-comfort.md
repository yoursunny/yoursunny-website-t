---
title: Geocaching is All About Expanding My Comfort Zone
lang: en
date: 2019-01-10
tags:
- life
- geocaching
image: GC1ZK3Y.jpg
---

I [started geocaching](/t/2017/start-geocaching/) as a hobby in 2013.
Since then, I have found more than 1000 caches across 18 US states as well as in China.
During these years, I stepped out my comfort zone several times, and became a more experienced geocacher.

## Puzzles

Geocaches come with different types.
*Traditional* is the most common type: the webpage of a Traditional cache has its coordinates, and I can straightforwardly find the physical container at that location.
Another common type is a *Multi*: I need to visit a location, collect information such as reading text from a plague or counting the number of windows, and then find the physical container at a different location whose waypoint is computed from the answers from the first location.
I'm quite familiar with these types.

One geocache type I'm uncomfortable with is *Mystery* puzzle cache.
These caches are published with bogus published coordinates, but the webpage contains clues to find the real coordinates of the physical container.
The puzzle could be a piece of cipher text, a crossword puzzle, a strange picture, or something else.
Although I [know a thing or two about classic ciphers](/study/IS407/), I could not get a hold of them.

This all changed when Geocaching HQ [assigned me a mission](https://www.geocaching.com/blog/2014/07/your-new-mission-earn-the-7-souvenirs-of-august/) to find a **puzzle cache** in August 2014:

> This August, your mission is to explore more of the geocaching universe.
> Find specific geocache types and earn six unique souvenirs for your geocaching profile.
>
> Find a Mystery cache to earn the Puzzler souvenir.

The puzzle of choice is [Intro to Positional Astronomy](https://coord.info/GC1VY1Y).
It does not have weird code words or annoying Sudoku sheets, but contains a very scientific description of how astronomers measure the position of a star in the sky.
As soon as I see:

> In fact, there's no reason we couldn't measure Tucson's location in the same units

I knew this refers to the cache's location.

Using a [TI-83 calculator](/t/2005/gzjs/#TI), I calculated the coordinates, and headed to the location.
However, I didn't find it on the first try.
Suspecting that I've made a mistake, I emailed the cache owner, and got confirmation that my calculation was correct, as well as received a hint about the physical location.
I went there for another search, and successfully located the physical container.
Mission accomplished!

![The Puzzler souvenir by Geocaching HQ](ThePuzzler.jpg)

Although puzzle caches aren't my favorite, I no longer fear puzzle caches.
I still stink at cipher-based puzzles, but at least I can work on the mathematics kind.

## Down There?

I often [rode bicycles](/t/2017/on-two-wheels/) in Tucson.
Aviation Bikeway is one of the bike routes that I frequented.
I found most geocaches along this bike route, except one: [Urban Spooky Canyon](https://coord.info/GC1ZK3Y).

[![street view of GC1ZK3Y](GC1ZK3Y-streetview.jpg)](https://www.google.com/maps/@32.1939739,-110.9105017,3a,37.5y,232.61h,77.4t/data=!3m6!1e1!3m4!1snsx92FfiL0RYWQK2Hr_2gQ!2e0!7i13312!8i6656)

The cache is in a **concrete canyon** next to Palo Verde Rd.
The canyon about 5 meters deep.
It is fenced off, and has a 45° steep slope.
Cache listing webpage suggests "use your mountain bike", but all I have is a crappy road bike.
How can I tackle this cache?

I put it off.
But this unfound cache on my favorite bikeway bugged me every time I rode this route.
So I started investigating potential entry points that aren't fenced off.
After three investigation trips, I found an entrance!
I carried my bike down the slope, rode to the coordinates, and found the prize!
It's a [big smiley](https://coord.info/GLKAT6F2) on Halloween.

![selfie at GC1ZK3Y](GC1ZK3Y.jpg)

## In There?

The other bike route that I rode often is along Santa Cruz River, west of downtown Tucson.
It extends about 20 miles, and I have reached both north and south ends several times.
On this route, there's also a geocache that I kept putting off: [Sign of Niles Ohana (SOF Warrior)](https://coord.info/GC5860Y).
Look at the trail head and you'll know why:

[![satellite map of GC5860Y trailhead](GC5860Y-sat.jpg)](https://www.google.com/maps/place/32%C2%B012'51.4%22N+110%C2%B058'53.3%22W/@32.2151246,-110.9811307,67a,35y,199.13h,53.84t/data=!3m1!1e3!4m5!3m4!1s0x0:0x0!8m2!3d32.2142833!4d-110.9814833)

The cache listing webpage says:

> You'll definitely get a leg and bum burn accessing the site.
> Here's the deal...not for the faint of heart. Have fun. Also, you may not be alone...depending.
> Additional Hints: Headlamp

It is a challenging cache in a **drainage tunnel**, located 262 meters from the entrance.
I don't have a headlamp, and couldn't find a caching buddy.
I would have to walk in complete darkness, alone.
There could be vampires, zombies, unicorns, snakes, bears, and other predators inside the tunnel; they could catch me, peel off my skin, and eat me alive.
If a thunderstorm arrives suddenly, I could drown instantly.
Moreover, my cellphone would not pick up a signal inside the tunnel, making it impossible to call for help.

But with [my remaining days in Tucson counting](/t/2017/last-month-in-Tucson/), I increasingly wanted to find this challenging cache.
On 10 April 2017, I dared myself to climb into this hole, walking toward the cache.
Initially I can see the sunlight coming in from the entrance, and hear the noise from nearby roadways.
50 meters in, cellphone flashlight was the only light source, and my footstep was the only sound.
100 meters in, I heard an additional sound from my pounding heart.
200 meters in, I was thinking about turning back, but I refused to give up.
Finally, I found a side opening.
I crawled on my stomach, but found nothing.
I hastily ran out of the tunnel, and [logged a DNF](https://coord.info/GLQKX0WR).

Four months later, I received word that the cache was missing and had been replaced.
I wanted to try again before I leave Tucson.
I must try again before I leave Tucson.
On 28 August 2017, I climbed in the hole, walking toward the cache.
Sunlight fainting, traffic noise disappearing, heart pounding, body sweating.
I reached the side opening.
I crawled on my stomach, but found nothing.
I recalled that a previous finder mentioned "two possibilities, one with bricks, one without", I walked further in, and found another side opening.
I crawled on my stomach, and, bingo!
I'm drenched in sweat, but I [found it](https://coord.info/GLT0XDFK).

![selfie at GC5860Y](GC5860Y.jpg)

A year later, I ran into [another drainage tunnel cache](https://coord.info/GC5NFPE) in Aspen Hill, MD.
Its description says:

> Leave all your fancy clothes at home, because you will be getting dirty on this one!
> You are looking for a micro container hidden in an uncomfortable location.
No need to go too far in, and one should be able to find the cache using only natural light.

The previous finder says:

> OP is a terrible person. Tftc!

Despite the drainage tunnel is small, dark, and wet, I crawled in without hesitation.
My comfort zone has been expanded to include this type now.

![selfie at GC5NFPE](GC5NFPE.jpg)

## Up There?

Looking at my [Difficulty-Terrain matrix](https://project-gc.com/ProfileStats/yoursunny), I notice a lack of caches with Terrain rating of 4.5 or 5.0.
Searching [Project-GC maps](https://project-gc.com/Tools/MapMatrix), I figured that I can *easily* find a T5 cache if I fork over the money to [rent a kayak at Black Hill Regional Park](https://www.montgomeryparks.org/parks-and-trails/black-hill-regional-park/black-hill-boats-little-seneca-lake/), but I'll have to either **climb a tree** or do some significant hikes to get a T4.5.

I talked to others who have climbed trees.
They told me that the trees are normally quite big and tall so that they are strong enough to be climbed safely, and "climbing a tree isn't scary".
I am not confident I could do that.

Recently, in January 2019, I encountered a [tree-climbing cache that is rated T3](https://coord.info/GC3DC8T).
I thought it would be a good practice before attempting a T4.5 tree climb.

![tree of GC3DC8T](GC3DC8T-tree.jpg)

I requested the famous Georick402, who found this cache in 2014, to drive me there.
He helped me identifying the correct tree, and then I started climbing.
I moved slowly and steadily, kept my foot on a big branch, and wrapped my arms around another big branch for additional safety.
I didn't know how high I must climb, but I had the cache in sight just three meters up in the tree.
That's easier than I expected, but nevertheless it's my first time [climbing a tree like a squirrel](https://coord.info/GLXRT6P2).

![selfie at GC3DC8T](GC3DC8T.jpg)

[Georick's "AIR"mail](https://coord.info/GC681ZH) is ten times higher than this.
I hope I could be confident enough to climb that tree some day, when I'm not alone.
