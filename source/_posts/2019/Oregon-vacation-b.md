---
title: "Oregon Vacation: Looking Back and Looking Forward"
lang: en
date: 2019-07-09
tags:
- Oregon
- geocaching
- travel
---

My 7-day Oregon vacation is over.
It took even longer for me to blog about this experience:

1. [Planning and Departure](/t/2019/Oregon-vacation-1/) explains why I chose Oregon, how I planned the trip, and a personal record I set on the departure day.
2. [GC12 and GC17](/t/2019/Oregon-vacation-2/) follows my adventure to Oregon's oldest geocache.
3. [Birthplace of Geocaching](/t/2019/Oregon-vacation-3/) introduces a landmark that started one of my favorite sports.
4. [Hillsboro and Outlets](/t/2019/Oregon-vacation-4/) tells how I met a Twitter friend in real life, how I contributed to American economy, and a painful DNF story.
5. [Eight Waterfalls, Two Beaches, and a Webcam](/t/2019/Oregon-vacation-5/) takes me outdoors to the scenic Multnomah Falls recreation area.
6. [Portland](/t/2019/Oregon-vacation-6/) calms me down in the peaceful Portland Japanese Garden, and gets me running around in Portland State University.
7. [Breakfast and Laundry Solutions](/t/2019/Oregon-vacation-7/) discusses some logistic issues during my travels.
8. [GC16 in Molalla River](/t/2019/Oregon-vacation-8/) makes me huff and puff in seek of a 19-year-old plastic jar.
9. [Oregon City](/t/2019/Oregon-vacation-9/) lifts me up along the only vertical street in North America.
10. [Last Day](/t/2019/Oregon-vacation-a/) brings me from the peak of Portland to the other side of the state line, and then back to the normal life.

## Schedule

My vacation did not have a planned day-to-day schedule, but adopted a semi-flexible strategy.
This differs from my previous vacations such as the [Yuma Mega trip](/t/2018/YumaMega/), where the planning covered exactly where to exit the highway and take a rest.
Being flexible allowed me to spontaneously decide where to go next, based on weather, mood, and other factors.
It enabled me to add destinations that I did not know before departure, such as Multnomah Falls and Oregon City, as well as fulfill friend's request of an [IRL](https://www.urbandictionary.com/define.php?term=IRL) meet up.
This strategy did backfire once, when I went to a museum only to find it closed.

Eventually, my trip achieved an interleaved structure between urban and rural environments:

1. Jun 07: urban, flying around [airports](/t/2019/Oregon-vacation-1/).
2. Jun 08: rural, nabbing [GC12 and GC17](/t/2019/Oregon-vacation-2/) in a national forest, and worshipping the [Original Stash](/t/2019/Oregon-vacation-3/).
3. Jun 09: urban, spending money at [Premium Outlets](/t/2019/Oregon-vacation-4/).
4. Jun 10: rural, hiking around [waterfalls](/t/2019/Oregon-vacation-5/) and cooling off on the beach.
5. Jun 11: urban, taking a tram into [Portland downtown](/t/2019/Oregon-vacation-6/).
6. Jun 12: rural, geocaching in and around [Molalla River](/t/2019/Oregon-vacation-8/).
7. Jun 13: urban, staying cool in air-conditioned [Museum of Oregon Territory](/t/2019/Oregon-vacation-9/) and [Pearson Air Museum](/t/2019/Oregon-vacation-a/).

## Favorites and Wishlist

Among all the places I visited, the top 5 favorites are:

1. [GC17](/t/2019/Oregon-vacation-2/), a 2000-Jul geocache that requires a modest hike and offers a scenic view of the mountain, without the distraction of civilization.
2. [Multnomah Falls](/t/2019/Oregon-vacation-5/), a gorgeous view of two waterfalls and the Columbia River.
3. [Portland Japanese Garden](/t/2019/Oregon-vacation-6/), a beautiful garden where everything is green.
4. [Oregon City Municipal Elevator](/t/2019/Oregon-vacation-9/), the one-of-a-kind municipal elevator in the United States.
5. [The spiky blackberry bushes leading to GC25WP9](/t/2019/Oregon-vacation-4/), which made me bleed my own blood.

There are many more places that I learned about during the trip, wanted to visit, but wasn't able to:

1. [Western Antique Aeroplane & Automobile Museum](https://www.waaamuseum.org/), a collection of antique airplanes that can still fly.
2. [GCA5 Hembre Ridge](https://coord.info/GCA5), a 2000-Nov geocache.
   Passing this one means that I would have to make a trip to New York state to find a cache hidden in that month.
3. [Salem](https://en.wikipedia.org/wiki/Salem,_Oregon), the state capital of Oregon.
   I used to believe Portland is the state capital, until when I was writing these articles.
4. More river confluences.
   I visited four confluences but there must be more, and I'm obsessed with "corners".
5. [Pechuck Fire Lookout](https://coord.info/GC1E03), a location with great view and a lonely 2001-Sep geocache.
   Getting there would require hiking 8.3 km and an overnight stay at the Fire Lookout.
   I know the limits of my outdoor skills, and this is beyond what I'm trained for.

## Some Stats

### Geocaching related

* Finds: 53, including two D4 and two T4
* DNFs: 10
* PAFs: 2, one resulted in a [smiley](https://coord.info/GLYWWMX3) and the other resulted in a [very painful frown](https://coord.info/GLYXAH33)
* Jasmer boxes filled: 2000-May, 2000-Jun, 2000-Jul, 2000-Oct, 2001-Feb
* New record: maximum distance in a day - 3784 km
* Nanos encountered: 0
* Bison tubes encountered: 0

### Google Maps Timeline and Fitbit data

date   | drive       | steps | effective sleep
-------|-------------|-------|----------------
Jun 07 |  12 mi 0:48 |  6946 | 5:23
Jun 08 | 148 mi 4:34 | 21479 | 3:42
Jun 09 | 107 mi 3:55 | 16933 | 5:13
Jun 10 |  86 mi 3:22 | 26683 | 5:13
Jun 11 |   9 mi 0:46 | 30521 | 5:51
Jun 12 |  97 mi 4:22 | 19396 | 3:17, plus 0:57 nap
Jun 13 |  34 mi 3:22 | 18251 | 4:44
Jun 14 |             |  8342 | 1:02 in air and 2:31 in apartment

### Money

* Airfare: $19.90 and 25000 AAdvantage miles
* Hotel and car package: $747.75
* Rental car insurance: $125.49
* Gasoline: $48.26
* Transit (bus and Uber): $43.53
* Admissions (gardens, museums, and arcade games): $66.52
* SUBTOTAL of the above: $1051.45
* Food: I count food in calories, not in dollars
* Bragging rights: a lot!
* Memories: priceless!
