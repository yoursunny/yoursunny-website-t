---
title: Oregon Vacation (7) Breakfast and Laundry Solutions
lang: en
date: 2019-07-05
tags:
- Oregon
- travel
image: FredMeyer.jpg
---

Tuesday, June 11, I [visited Portland via TriMet light rail](/t/2019/Oregon-vacation-6/), and came back to Milwaukie Station at 20:54, just before sunset.
I walked to [Milwaukie Bay Park](https://www.milwaukieoregon.gov/parkssustainability/milwaukie-bay-riverfront-park), and watched sunset at Klein Point, the confluence of Willamette River and Johnson Creek.

![sunset at Klein Point in Milwaukie Bay Park](KleinPoint.jpg)

I retrieved my rental car from the Park & Ride lot.
Before driving back to motel, I need to take care of one more business: groceries.

## Breakfast from Grocery Stores

Since I [installed MyFitnessPal](/t/2017/heavy-iron-stuff/), I count my food in calories instead of dollars.
When I travel, I choose high quality restaurants where I can have a nutritious and enjoyable meal.
No matter where I go, I can usually find a good restaurant for lunch or dinner.

Breakfast is a different situation.
A traditional American breakfast served in a restaurant consists of two eggs, a stack of pancakes topped with sugary "maple" syrup, half a plate of greasy "home style" potatoes, and unlimited refills of watery coffee.
Such a meal costs over 1200 calories, and all these saturated fat makes my stomach turn.
I can't imagine having American diner breakfast every morning, six days in a row.

![my shopping basket at Fred Meyer](FredMeyer.jpg)

My "normal" breakfast is four slices of whole wheat bread and a tall glass of 2% milk.
It contains whole grains and proteins, and weighs 600-700 calories.
I have a 7-day vacation this time, longer than most other trips, so I decide to normalize my breakfast.
I went grocery shopping on the first night that I arrived in Oregon.
I bought a ½-gallon jug of milk, a loaf of bread, a small watermelon, and some protein bars.
Apparently my Fry's loyalty card number works in Fred Meyer, as they are both subsidiaries of Kroger Co.

The motel room has a small fridge where I kept the milk and watermelon.
Every morning, I pour two cups of milk, and drink them with four slices of bread.
Every night, I eat a quarter of the watermelon cut with a plastic knife from the airline before going to bed.
Having some normality in my life, even if I'm 3000 km from my apartment, reduces stress greatly.
Moreover, these foods are more nutritious than what's served in restaurants.

## Target Run

The milk, bread, and watermelon lasted four days.
By Tuesday when I came back from Portland, the fridge is empty.
Therefore, another grocery trip is in order.

I selected a familiar brand: Target.
There isn't a Target store in Milwaukie, but there is [one in Clackamas](https://www.target.com/sl/clackamas/346), 3.7 miles east of Milwaukie.
Driving there took about 15 minutes.
I bought two frozen dinners, one as dinner of that day (because I didn't have time for dinner in Portland), the other as Thursday's breakfast.
I also browsed the clothing clearance rack just as all my other Target runs, but didn't find anything worth my luggage space.

## Hitchin' Post

Although American diner breakfast isn't my favorite, I still want to have one from time to time.
On Wednesday, I'm [going for GC16](/t/2019/Oregon-vacation-8/), one of Oregon's original geocaches hidden in June 2000.
I departed the motel at 07:14 with an empty stomach, drove 44 minutes south to Molalla, and had my breakfast in [Hitchin' Post Cafe](https://www.tripadvisor.com/Restaurant_Review-g51974-d5085400-Reviews-Hitchin_Post_Cafe-Molalla_Clackamas_County_Oregon.html?m=19905).

![Gene's Hitchin' Post](HitchinPost.jpg)

Just as expected, the breakfast consists of an omelet made from two eggs and a lot of cheese, and three pancakes topped with sugary syrup.
I substituted the home fries with some mushrooms, and the coffee with a cup of hot chocolate.
I can hardly finish this big meal.

## Laundry in the Motel

Hiking and geocaching in the outdoors means I'm sweating non-stop.
Mr [Henry Pittock](/t/2019/Oregon-vacation-6/), who was an avid outdoorsman, employed servants to wash his clothes.
I have to do it myself.
Washing is easy enough: bathroom sink, water, and soap or shampoo.
But how to dry the clothes?

![Milwaukie Inn room](MilwaukieInn.jpg)

In most motels, every room has a clothes rack and some hangers.
Milwaukie Inn, a cheap 2-star hotel, does not have this basic equipment.
I have to improvise.

My initial solution was hanging wet clothes on a luggage stand, and running the air conditioner to remove moisture.
It works, but slowly.
When I got grilled by a hot rental car, I found a better solution: use the rear window of the car as a dryer.
This is more effective whenever the sun is out.

![clothes hanging under rear window of a car](dryer.jpg)

## Vacation Continues

After this logistics discussion, my Oregon vacation continues with GC16 in [part 8](/t/2019/Oregon-vacation-8/).
