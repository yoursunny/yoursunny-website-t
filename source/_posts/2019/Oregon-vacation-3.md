---
title: Oregon Vacation (3) Birthplace of Geocaching
lang: en
date: 2019-06-23
tags:
- Oregon
- geocaching
- travel
image: GCGV0P.jpg
---

Jun 08 morning, I [found GC12 and GC17](/t/2019/Oregon-vacation-2/), two oldest geocaches in Mt Hood National Forest, Oregon.
I returned to GC17 trailhead at 12:19.
It has been five hours from when I left the motel, I have chewed two protein bars, so it's time for lunch.
Google Maps says there's no restaurant in the middle of the forest, but there are some near my next destination.
I entered the address into HERE WeGo, ate a third protein bar, and started driving.

Here we go again, HERE WeGo commanded me to "turn sharp left toward SE Brian Ranch Rd", a narrow, steep, and unsafe road through the ranches.
I ignored this command, and continued on SE Wildcat Mountain Dr.
An hour later, I arrived at [View Point Restaurant & Lounge](https://www.facebook.com/viewpointrestaurant/), Estacada, OR.
I need a burger right now.

![view of Clackamas River from View Point Restaurant](ViewPoint.jpg)

## Original Stash Tribute Plaque

World's first geocache, then known as "GPS Stash", was [published](http://groups.google.com/group/sci.geo.satellite-nav/browse_thread/thread/99c847733cb3547a?hl=en&lr&ie=UTF-8&oe=UTF-8&rnum=25&pli=1) by Dave Ulmer on 03 May 2000:

> Well, I did it, created the first stash hunt stash and here are the coordinates:
>
> N 45 17.460
> W122 24.800

This location is easily accessible with a car, and does not need any hiking.
The original bucket no longer exists, but an [Original Stash Tribute Plaque](https://coord.info/GCGV0P) was created at this very spot.
The description suggests:

> don't forget to place your GPS unit on top of the plaque for a moment or two in order to receive extended battery life and super-accurate satellite reception!

Instead of a GPS unit that I do not own, I placed my head on top of the plaque for a moment or two in order to improve my puzzle solving skills.

![my head on Original Stash Tribute Plaque](GCGV0P.jpg)

Apart from the plaque, there is a 5-gallon bucket hanging from a tree as the cache container.
I signed the logbook, and traded trackables.

## Almost-DNF the Un-Original

The [Un-Original](https://coord.info/GC92), [published](https://web.archive.org/web/20010714121332/http://www.geocaching.com/seek/cache_details.asp?ID=146) on 22 Oct 2000, is the fourth oldest active geocache in Oregon.
It is "within 10 yards of the Original Stash", as the [528-feet rule](https://www.geocaching.com/help/index.php?pg=kb.chapter&id=22&pgid=199) was not established back then.

The container size is listed as a *Regular*.
Officially, a Regular is size of a shoe box.
Commonly, a Regular could be a lunch box or an ammo can.

I started searching.
I looked everywhere that a Regular would fit, but found nothing.
It's been half an hour, but I don't want to log a DNF (Did Not Find) on this important geocache.

My app says [Georick402](https://coord.info/PR441D5), my geocaching partner in Maryland, found this cache in 2017.
I sent him a text message, asking for a spoiler photo that he normally collects every time he found a cache.
Unfortunately, he's in Michigan and cannot access his photo library.
But I got an encouragement:

![You're there. Don't log a DNF. FIND that cache.](FIND-that-cache.png)

As I continued searching, Georick402 was reading online logs (that somehow isn't shown on my app).
He discovered an important fact: the container is now a film canister, not a Regular.
With this knowledge, I [found the cache instantly](https://coord.info/GLYWWMX3).

It seems that the container for this cache has gone through several iterations:

1. In 2003, it was an [ammo can](https://coord.info/GL1XHFC).
2. The ammo can lasted until [Apr 2011](https://coord.info/GL5C7GH7).
3. In May 2011, it became a [red thermos](https://coord.info/GL5Q1F95).
4. The red thermos was still seen in [Apr 2014](https://coord.info/GLDYB55T).
5. In Jul 2014, it became a [lunch box](https://coord.info/GLEW00X5).
6. The lunch box was in place until [Jan 2018](https://coord.info/GLTYKP3Z).
7. In Jun 2018, it's an [ammo can](https://coord.info/GLW35NWQ) again?
8. This ammo can was last seen in [Sep 2018](https://coord.info/GLX443JM).
9. In Oct 2018, the current [film canister](https://www.geocaching.com/seek/log.aspx?LUID=129891a8-10c7-40e0-8fe6-55eeec7296ea&IID=62ed6837-2f73-4e36-b51e-f9939b5fc8b2) emerged.

Facebook community [thinks it's a throwdown](https://www.facebook.com/groups/EyeSag/permalink/10151074597769982/), a poorly done one that does not even match the description.
I logged a [need maintenance](https://coord.info/GLYWWMX4) for the container size mismatch, but I got the smiley.

## Vacation Continues

My Oregon vacation continues with a GEO event in [part 4](/t/2019/Oregon-vacation-4/).
