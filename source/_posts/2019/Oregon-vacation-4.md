---
title: Oregon Vacation (4) Hillsboro and Outlets
lang: en
date: 2019-06-24
tags:
- Oregon
- geocaching
- travel
image: Ashlesh.jpg
---

Jun 08 afternoon, I [visited the birthplace of geocaching](/t/2019/Oregon-vacation-3/) near Estacada, Oregon.
My dinner is scheduled at [Copper River Restaurant & Bar](https://www.copperriverrestaurant.com/) in Hillsboro, where I can attend a geocaching event.

## GEO June 2019 Meet & Greet

Geocaching is generally a solo sport, except when I [run into other geocachers on the trail](/t/2019/Oregon-vacation-2/).
However, the "event cache" is an opportunity for geocachers to meet other participants of the sport.
When I was planning my Oregon trip, I kept searching the event listing every week, until a suitable event showed up: [*Geocachers Exploring Oregon* (GEO)](http://www.georegon.com/), Oregon's geocaching community, has a [meet & greet](https://coord.info/GC86WVP) on Saturday evening.
Therefore, I planned my dinner to be at this event.

After spending too much time on the Un-Original, I drove 79 minutes to the event venue.
Copper River is a large restaurant with more than 100 seats, and GEO is the largest group in the restaurant.
I arrived at 18:02, just as the "official program" was starting and a president-level figure picked up a loud speaker to make announcements.
I received a blue raffle ticket, and later won a geocoin.

I found a seat in the middle of a 30-person long table, and introduced myself as `yoursunny` to people around me.
[longtrails](https://coord.info/PR29V0D), who's seated to my left, remembers my name because I signed his [challenge cache](https://coord.info/GC7T5BA) this morning.
We chatted, and he offered me a document of driving directions to some of Oregon's historical caches.

After dinner, I attempted the [nearest geocache](https://coord.info/GC1B5V2) from the restaurant.
It is in an urban waterfall, but I miraculously did not get wet.

![a waterfall in which "In Honor of Wilderness Explorer" geocache is located](GC1B5V2.jpg)

I also visited [a sculpture](https://coord.info/GC7B8CF) in Orenco Woods Nature Park, and learned the origin of "Orenco": it means [Oregon Nursery Company](https://en.wikipedia.org/wiki/Oregon_Nursery_Company), producer of the *Orenco* apple.

![Orenco apple sculpture](GC7B8CF.jpg)

## Hillsboro Again

I returned to Milwaukie Inn motel at 20:50, and got a message from [Ashlesh Gawande](https://twitter.com/AshleshGawande), a research collaborator that I met a few times.
He told me that he's interning near Portland, and wanted to meet me.
It isn't the first time I receive this kind of requests, but this time I have a rental car and don't have fixed trip schedule, so I asked for his coordinates.
What he sent me isn't coordinates, but a street address.
It is in Hillsboro, 5 km from where I was having dinner 2 hours ago.
No!!!

I know there's another [geocaching event](https://coord.info/GC86T1J) on Sunday morning.
I checked its location: 13 km from Ashlesh's address.
Thus, I accepted the request, and scheduled the meeting for Sunday morning.

![Junxiao and Ashlesh at Commonwealth Lake Park in Portland](Ashlesh.jpg)

Ashlesh is not a geocacher, but I need to give him a taste of our great sport, don't I?
Therefore, I sent him the coordinates of a [geocache](https://coord.info/GC20KAB) 2 blocks from his address, and let him meet me at this spot.
Jun 09 08:50, I pulled into the parking lot, and he slowly walked to me while staring at his phone.
As he watched, I lifted up a fence post cap and had the geocache in hand.

We attended the event together.
Ashlesh stood on the side and listened to the crazy compulsion stories of us geocachers.
I shared my compulsion of hiding mystery caches, and received [an apple-shaped geocache container](https://coord.info/GC8E0X0) that I'll hide in Maryland soon.

We left the event early, to find a geocache in the same park.
I handed my phone to Ashlesh.
He followed the compass, and found a small lock-n-lock container behind a tree.
[That cache](https://coord.info/GC2E88) turns out to be hidden in Dec 2001, so it's an accidental oldie find for me.

## Fast Cars

Ashlesh declined my offer of going to lunch or a longer trip together, as he had another appointment at noon, so I dropped him off at 10:30.
After a cup of soup at [Zoup!](https://www.zoup.com/menu.html?location=ZOU00190), I headed to one of few non-geocaching locations I identified during my trip planning: [World of Speed motorsports museum](https://worldofspeed.org/wos-museum/).

I'm interested in transportation since childhood: I would watch trucks and buses along a major road, or count rail cars along a grade crossing.
I spent 8 days visiting Smithsonian's Air and Space Museum, and also went to several railroad museums.
This is the first time I notice a car museum, so it entered my trip plan.

Well, a motorsports museum is not the same thing as a car museum.
The exhibition is about fast cars and their drivers.
I'm unfamiliar with this topic, but I vaguely remember my high school classmates watching [Schumacher](https://en.wikipedia.org/wiki/Michael_Schumacher) on TV.
The museum does not have anything about Schumacher, unfortunately.
Schumacher is a German racing driver, who raced in Formula One originated in Europe, while the museum mainly focuses on American racing drivers and American races.

![some race cars in World of Speed motorsports museum](WorldOfSpeed.jpg)

I found many artifacts about NASCAR and IndyCar, two American-based races.
The first hall features two of America's greatest racing drivers, Mario Andretti and Andy Granatelli.
The second hall has dozens of drag race cars, and an exhibition about [Portland International Raceway](https://portlandraceway.com/).

I spent three hours in the museum.
From knowledgeable docents and online resources, I learned the [difference between NASCAR, IndyCar, and Formula One](https://montrealgrandprix.com/news/whats-difference-indy-formula-one-nascar/), as well as why a tire for drag racing [does not have treads](https://en.wikipedia.org/wiki/Racing_slick).

## Premium Outlets

Oregon [doesn't have a general sales tax](https://www.oregon.gov/DOR/pages/sales-tax.aspx).
Compared to Maryland's [6% sales tax](https://taxes.marylandtaxes.gov/Individual_Taxes/Individual_Tax_Types/Sales_and_Use_Tax/), everything purchased in Oregon would be 6% cheaper.
Therefore, I went shopping in [Woodburn Premium Outlets](https://www.premiumoutlets.com/outlet/woodburn), the largest tax-free shopping outlet in the Western United States.

Every time I mention the word "outlets" to my mother, it is triggering.
She would tell the story about how her tour group extended a 2-hour outlets shopping trip to over 4 hours, where people ignored tour guide's urge, skipped dinner, and missed the bus, just to get more time shopping.
For me, it's a simple and controlled trip: I know what I need to purchase, and I have limited luggage space to haul back to Maryland.

I need a pair of shoes.
My usual brand is Nike, but the durability of Nike running shoes cannot keep up with all the hikes needed for geocaching.
As I walked past a Merrell store, I entered to have a look on their hiking shoes.
I saw a pair of ANVIK waterproof hiking shoes priced at $69.99, just $20 more than a pair of Nike, I decided to give it a try, despite that I used to [have complaints against waterproof boots](https://coord.info/GLXY5JCR).

![Merrell hiking shoes and two pairs of Levi 541 jeans](Woodburn.jpg)

I also need some Levi jeans, which are priced at $89.98 for two pairs regardless of style.
My oversized quads can only fit into 541 athletic cut, and my size is 32x30.
The only degree of freedom is color selection, which is an easy choice.

In total, my shopping spree completed in 70 minutes, during which I contributed $160 to American economy.

## Shoe Test / Painful DNF

I just bought new hiking shoes and I'm eager to test them out.
The location of choice is Molalla River State Park, where I can see the confluence where Molalla River merges into Willamette River, and I might get a [lonely geocache](https://coord.info/GC25WP9).

There's only one road going into the west side of the park, and a car was following me along this narrow road.
As I parked at the end of the road, a father and his son came out of the other car.
I greeted them.
They said they are geocachers but they are going fishing today, and wished me good luck.

![Molalla & Willamette confluence](confluence.jpg)

Wearing my new hiking shoes, I walked along a trail in the middle of a chest-high grassy field, and reached the confluence.
I saw the father and son again, wading toward an island and setting up their fishing gear.
I also noticed that the trail toward the geocache has fallen into the river.

I started bushwhacking.
Initially, it's the same chest-high grassy field minus the trail, which isn't an issue.
Then, I encountered green briars and blackberry bushes.
While my new hiking shoes are sturdy and protect my feet well, the cheap Groupon jogger pants offer no protection to my legs.
I'm in pain from all the spikes.
It was 15 meters of hell.

![new hiking shoes are in action, but jogger pants are useless](GC25WP9.jpg)

When I finally reached the other side, I badly wanted to find this cache.
It is a lonely cache that was found about once a year.
It is a Difficulty 3, which officially means requiring a 30-minute search.
I know it would be difficult, but I must try my best.

I searched for one hour, but could not find anything.
Asking [Georick402](https://coord.info/PR441D5) would not help because he never visited this location.
I reached out to the last two finders who found this cache three months ago, hoping they would help me.
I waited 10 minutes, no reply.
I started walking away.
Before reaching the blackberry bushes, I received a reply.
I had enough water but no food, and I'm feeling hungry, but I didn't want to give up now after all the pain on the way in.
So I returned to the location.

I searched and searched, but still nothing.
I sent a picture of the scene to a previous finder, and got the word that the place looks very different from 3 months ago: a big tree has fallen into the Molalla River.
My conclusion is that, the cache is gone with the tree.

I made out through the blackberry bushes and green briars, painful but still alive.
I'm back in the car at 20:35, three hours after I parked.
Thanks Molalla River for [a DNF](https://coord.info/GLYXAH33), and the 60 holes on my arms and legs.

## Vacation Continues

My Oregon vacation continues in waterfalls in [part 5](/t/2019/Oregon-vacation-5/).
