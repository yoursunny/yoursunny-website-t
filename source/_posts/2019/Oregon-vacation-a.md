---
title: Oregon Vacation (10) Last Day
lang: en
date: 2019-07-08
tags:
- Oregon
- geocaching
- travel
image: PDX-sunset.jpg
---

By the time I finished my [Oregon City tour](/t/2019/Oregon-vacation-9/), I'm 22 miles from the Portland International Airport and I have 8 hours remaining before scheduled flight departure.
There are still places worth visiting.

## Portland's Highest Point

[Council Crest Park](https://www.portland.gov/parks/council-crest-park) is the highest point in Portland at 327-meter elevation.
It is also the location of [Portland's oldest geocache](https://coord.info/GC25C), hidden in February 2001.

![rusty statue and signal tower atop Council Crest](CouncilCrest.jpg)

The park was easy to locate.
The geocache, on the other hand, was difficult for me.
Its location is specified as a vector offset from the hilltop, at "185 meters in the direction of 285° from *magnetic north*".
I've [seen "magnetic north" once](https://github.com/MocoMakers/SolarTracking/issues/1) and looked up some scientific references, but I don't know a practical way to convert magnetic north to *true north* that is accepted in mobile apps.

Ignoring the difference between magnetic north and true north, I projected the waypoint using true north, and attempted to find the cache nearby.
However, my search was fruitless.
Pictures and log entries from previous seekers offered me some hints on the terrain near the cache.
I walked along the perimeter of a 185-meter-radius circle centered around the hilltop, and eventually found the cache after an hour.

Later, I [discussed this experience on Facebook](https://www.facebook.com/groups/183002578433642/permalink/2360457474021464/), and learned an online [magnetic declination calculator](https://geomag.nrcan.gc.ca/calc/mdcal-en.php).
[Its result](https://geomag.nrcan.gc.ca/calc/mdcal-r-en.php?date=2006-07-07&latitude=45.49885&latitude_direction=1&longitude=122.70787&longitude_direction=-1) would have brought me within 15 meters from the actual cache location, instead of over 40 meters away.

## Return to Vancouver

After Council Crest Park, I found myself on the "wrong side of the state line" again.
I returned to Vancouver, WA, to visit [Pearson Air Museum](https://www.nps.gov/fova/learn/historyculture/pearson.htm) that [wasn't open on Monday](/t/2019/Oregon-vacation-5/).
There are only two bridges connecting Portland and Vancouver, and traffic was bad that it took me 69 minutes to drive 9.1 miles to the museum.

![The Northwest Mobilizes for War display at Pearson Air Museum](spruce.jpg)

The museum is small and has only five airplanes, but from wall displays, I learned Pacific Northwest's contribution to World War I aviation: spruce production.
In 1917, the [Spruce Production Division](https://en.wikipedia.org/wiki/Spruce_Production_Division) was created to produce Sitka spruce lumber for building wartime aircraft.
More than 30000 soldiers and workers worked day and night, in the mill and in the woods, in support of American and Allies troops at the front line.
This effort was essential for Allies to win the war.

## PDX ✈ PHL ✈ DCA

The drive back to Portland was faster than the northbound trip.
I had Swedish meatball at IKEA cafe for dinner, and then refueled and returned my rental car.
For some [weird reason](https://www.popularmechanics.com/cars/a14539158/why-does-oregon-have-gas-station-attendants-in-the-first-place/), most gas stations in Oregon are "full service" and have an attendant to pump your gas, which in turn makes Oregon's gas price 20% higher than Maryland.
I drove 576 miles during this vacation, and spent $48 on fuel.

![Hollywood Theatre in PDX terminal](PDX-Hollywood.jpg)

I entered Portland International Airport at 18:40, well before my flight time.
I listened to musicians playing piano in Concourse E, and watched some short films in a [17-seat movie theatre](https://hollywoodtheatre.org/programs/hollywood-theatre-pdx/) in Concourse C.

![sunset in PDX](PDX-sunset.jpg)
![sunrise in PHL](PHL-sunrise.jpg)

My return flight was booked as a one-way trip, paid with American Airlines miles, [separate from the outbound flight](/t/2019/Oregon-vacation-1/).
It includes two legs, operated by Alaska Airlines and American Airlines, with a connection in Philadelphia (PHL).
Upon check-in at Portland (PDX), I'm informed that the first flight would be delayed by more than one hour, and I'm on the edge of missing the connection.
Alaska Airlines offered me the option of changing to a direct flight on the next day, but would not give me a hotel voucher because "the delay is weather related"; American Airlines, who sold me the ticket, told me to blame Alaska because it's them causing the delay.
I determined that my best choice was to board the PHL-bound flight, hoping for the best.
Alaska Airlines gate agent offered to "fix" my row-29 seat by upgrading me to the Premium class that includes in-flight beef jerky (that I accepted) and alcohol (that I declined), and more importantly a row-10 seat, so I can deplane fast.
I made my connection at PHL, caught [a glimpse of I-270 and Gaithersburg](https://goo.gl/maps/eKQ9YoBRhq4htgVbA) in the air, hailed an [Uber](/t/2017/hailing-taxis/) at DCA, and returned to my apartment at Friday 08:33.

![I-270 seen from air](I270.jpg)

## Vacation Ends

But there's more!
I look back and look forward to this trip in the [next article](/t/2019/Oregon-vacation-b/).
