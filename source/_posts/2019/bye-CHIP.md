---
title: Bye, C.H.I.P
lang: en
date: 2019-07-27
tags:
- CHIP
image: CHIPs.jpg
---

In 2015, I backed [CHIP - The World's First Nine Dollar Computer](https://www.kickstarter.com/projects/1598272670/chip-the-worlds-first-9-computer/description) on Kickstarter.
Since then,

* I used C.H.I.P as my desktop for a while.
* I learned to [compile the kernel](/t/2016/CHIP-wireless-UART/) or [a kernel module](/t/2018/one-kernel-module/).
* I [measured bike speed](/t/2017/PocketCHIP-GPS-bike-speed/) with a PocketCHIP.
* I [added a speaker](https://twitter.com/yoursunny/status/819754035830001664) and soldered pin headers to the PocketCHIP.
* I figured out how to [flash CHIP offline](/t/2018/CHIP-flash-offline/).

In 2018, [Next Thing Co went out of business](https://www.reddit.com/r/ChipCommunity/comments/86lu8h/what_happened_to_nexthing_wheres_my_order/dy1w0z1/).
Disappearing along with them is the software updates.
[Debian Buster](https://wiki.debian.org/DebianBuster) was released in July 2019, but CHIPs are stuck with now two-generation old [Debian Jessie](https://wiki.debian.org/DebianJessie), along with a 3-year-old Linux 4.4 kernel.
Without software updates, running C.H.I.P computers on the Internet is increasingly risky.

This week, I have decided to sell my collection of C.H.I.P single board computers.
They were [sold for $77](https://web.archive.org/web/20190728022225/https://www.ebay.com/itm/223597447407) (equals Kickstarter pricing) within two days of listing on eBay.

![my C.H.I.P computers](CHIPs.jpg)

Goodbye, C.H.I.P.
I enjoyed while they lasted, but [Linux 4.4 kernel is sexy no more](https://twitter.com/yoursunny/status/1154726955935293441), so I have to part with them.

## Upgrading CHIP to Debian Stretch

Before I sell the C.H.I.P computers, I flashed them with Debian Stretch, the "oldstable" version that is newer than the original Debian Jessie.
The procedure of upgrading is:

1.  Flash a C.H.I.P to the "server" image.

    Although I have published an [offline CHIP flashing guide](/t/2018/CHIP-flash-offline/), I found this step easier with [JF Possibilities' mirror of NTC's CHIP-tools images](http://chip.jfpossibilities.com/chip/).

2.  Using a USB serial cable, [setup WiFi](http://chip.jfpossibilities.com/docs/chip.html#wifi-connection).

3.  Update Debian Jessie packages with [JF Possibilities' mirror of CHIP ".deb" repositories.](http://chip.jfpossibilities.com/chip/debian/).

4.  Upgrade to [4.4.138 kernel](https://github.com/kaplan2539/CHIP-Debian-Kernel).

5.  Modify `/etc/apt/sources.list`.

    * Change every `jessie` to `stretch`.
    * Delete `jfpossibilities` and `kaplan2539` repositories, as they do not provide packages for Debian Stretch.

6.  Do another `sudo apt update; sudo apt full-upgrade`, and you'll have Debian Stretch in about an hour.

This method only works for the "server" image.
For PocketCHIP or "gui" image, you can (and should) upgrade Jessie packages and the kernel, but cannot upgrade to Debian Stretch as it would break the display drivers.

## What Replaces My CHIPs

Regular C.H.I.P computers are ARMv7 computers.
They are replaceable with Raspberry Pis.
Apart from my Pi 3B desktop, I have two Pi Zero Ws, one of which is running [a contour camera](/t/2018/contour-PiCamera/) 24x7.

PocketCHIP is both a game console and a handheld Linux terminal.
I have an [ESP32-based ORDOID-GO game console](https://amzn.to/2JVHkQ6) that I can [play Nintendo games](https://youtu.be/4Wjv03ZTPr8), and I am [starting to program it](https://twitter.com/yoursunny/status/1050187349957718016).
Handheld Linux terminal, on the other hand, is harder to come by.
I could replace some use cases with my new [Omega2 Pro](/t/2019/omega2pro-battery/), but I'll have to use a smartphone as its screen and keyboard.

Next Thing Co is dead, but the market for a PocketCHIP-style handheld Linux terminal is open.
Is anyone coming?
