---
title: Oregon Vacation (2) GC12 and GC17
lang: en
date: 2019-06-22
tags:
- Oregon
- geocaching
- travel
image: Boeing737.jpg
---

On Jun 07, I flew to Portland, Oregon and [started my 7-day vacation](/t/2019/Oregon-vacation-1/).
Oregon is where the sport of geocaching got started, and my primary goal in Oregon is to find some of the world's oldest geocaches.
Saturday Jun 08, the weather was less than perfect compared to next few days.
However, I couldn't control my excitement and decided to nab the important geocaches first.

## Planning for GC12

[GC12](https://coord.info/GC12), formerly known as [GPS Stash #8](https://web.archive.org/web/20000824172417/http://www.triax.com/yngwie/stash8trip.html), was hidden on 12 May 2000, nineteen years ago.
It is Oregon's oldest active geocache, and as I later learned, the second oldest active geocache in the world.

When I was doing my initial trip planning, Google Maps says I could [drive right to it](https://goo.gl/maps/HxdUjAxdm8mVtiMW7).
Upon closer inspection, oh well, I would have to drive over a "road block".

![Google Maps route for GC12](GC12-gmap.png)

After reading [the forums](https://forums.geocaching.com/GC/index.php?/topic/306717-gc12-gc16-gc17/), studying OpenStreetMap data, and zooming in the satellite images, I determined a potential trailhead and parking spot: N45°18.593 W122°04.513.

## Driving to GC12

Saturday 07:07, I entered the GC12 parking waypoint N45°18.593 W122°04.513 to HERE WeGo, my usual vehicle navigation app, and departed the motel.
I started driving on city roads, highways, then it came to "SE Wildcat Mountain Dr", which sounds familiar because I'm a [*Wildcat*](/t/2017/six-years-in-arizona/).
I can feel the elevation gain, the light drizzle due to being in mountainous area, and the cellular signal disappearing.

Suddenly, HERE WeGo instructed me to "turn right onto SE Brian Ranch Rd".
That road, hands down, is one of the narrowest and steepest road I've ever driven into.
I can hear rocks hitting the bottom of my vehicle.
I can almost touch the tree branches hanging from both sides of the one lane road.
I cannot confidently descend the 30° slope.
Am I going to slip off the road?
I feel unsafe down there.

![HERE WeGo route for GC12 via SE Brian Ranch Rd](GC12-here.png)

I want to turn around.
I cannot turn around, because the one lane road is too narrow to turn a vehicle.
Now I have to reverse a vehicle up a steep hill.
It's good to have a rear view camera, plus all three mirrors.

All right, I'm back on SE Wildcat Mountain Dr.
HERE WeGo found an "alternative route" via SE Wildcat Mountain Dr, which should have been the default route in the first place.
I made to the waypoint at 08:18, and parked nearby at N45°18.662' W122°04.564.

As I exited the car and pulled out my phone, I noticed a cache named [1217's Front Door](https://coord.info/GC15V8K) right next to me, described as "the last pull-out before the turn to GC12 and GC17".
If I saw this cache earlier, most of my planning work would be unnecessary.
Nevertheless, being able to find a parking spot using maps and satellite imagery is a useful skill to learn.

## GC12 with Australian Friends

As I signed the Front Door geocache and walked back up, I saw another car parked next to mine.
A family of three walked out of the car.
Are they geocachers?
It doesn't hurt to ask, and the answer was YES.
They are [Boeing737](https://coord.info/PRCQKHF) family from Australia.

![yoursunny with Boeing737 family](Boeing737.jpg)

So we teamed up, and started the 1 km hike toward GC12.
First half of the trail is paved, and I believe it used to be a road.
Second half is a flat grassy surface.

![paved trail to GC12](GC12-trail.jpg)

Somewhere along the trail, there's a handy [distance sign](https://coord.info/GC4KW0X) behind a stump:

![GC12 0.19 mi; GC17 1.99 mi; Original Stash 16.17 mi](GC4KW0X.jpg)

We took the group selfie, hiked the last 0.19 miles, and found the cache.
The bucket is white with a blue lid, which matches the [original description](https://web.archive.org/web/20000824172417/http://www.triax.com/yngwie/stash8trip.html), although it's not the [original container](https://coord.info/GLXTNP80) but a [replacement](https://coord.info/GLXTNP80).
The blue lid is [trackable](https://coord.info/TB6XPKG), but the bucket is disappointing: it is a bucket full of water, not a good container in modern terms.
However, everything is dry as they are enclosed in several high quality Ziploc bags.
We signed the log and traded trackables.

## GC17 in the Rain

[GC17](https://coord.info/GC17), another old geocache hidden on 21 Jul 2000, is nearby.
During my planning, I determined its trailhead and parking spot to be N45°19.076 W122°03.558, along the same road as where I parked for GC12.
My Australian friends, having limited time and not-so-good knees, regretfully decided to skip GC17.
I chewed a protein bar and continued on in the light rain.

![uphill trail to GC17](GC17.jpg)

The trail to GC17 is a 1.5 km uphill battle.
It is steep, muddy in certain sections, and slippery in the rain.
My Nike running shoes got wet from the rain and the mud, although the [Darn Tough socks](https://amzn.to/2FoLTj9) still kept my feet comfortable.
It took me 75 minutes round trip to get up the hill, find the cache, and come back down.

There's no cellular signal.
There's no other people walking or talking.
There's no vehicle or aircraft humming.
It's just me, the nature, and the geocache.

As I'm heading out, [another geocacher](https://coord.info/PR23PQF) came in to get her "daily workout".
I said "it's there, good luck", and got into my car.
I found [one last cache](https://coord.info/GC16A8Y) in the area, and started my return to civilization.

## Vacation Continues

My Oregon vacation continues with more historical geocaches in [part 3](/t/2019/Oregon-vacation-3/).
