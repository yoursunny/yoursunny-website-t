---
title: Oregon Vacation (6) Portland
lang: en
date: 2019-07-02
tags:
- Oregon
- geocaching
- travel
image: Pittock-exterior.jpg
---

Tuesday, June 11 is the fourth full day of my Oregon vacation.
Having accomplished most of my [geocaching goals](/t/2019/Oregon-vacation-2/), it's time to be a tourist.
Primary goal for the day is Portland, the largest city in Oregon.

## A Day Pass

I have a rental car during this vacation, but experience tells me that driving in a large city like Portland would not be an enjoyable experience.
[There's traffic, and parking is difficult](/t/2020/thought-provoker-3/).
Therefore, I opted for public transportation.

One of my strengths is being able to quickly figure out how transit works in any city.
I successfully used Boston subway, Denver commuter train, Las Vegas Deuce, Flagstaff Mountain Line, San Francisco BART, San Diego trolley, Honolulu TheBus, and many other transit systems during my travels.
Likewise, I familiarized myself with Portland's TriMet transit system through 15 minutes of online study, including its major routes and fare options.

[TriMet](https://trimet.org/) is, in fact, one of the easiest transit system I've ever used.
It offers both bus and light rail (tram) service.
Routes and real time tracking are available through Google Maps and [Transit app](https://transit.app/).
Every fare box can accept mobile payments such as Android Pay, in addition to cash and "Hop Fastpass" smart cards.
Day passes are offered, but you don't have to plan in advance: if you paid enough single trip fares in one day, your ticket is automatically upgraded to a day pass.

![Milwaukie station](Milwaukie.jpg)

Jun 11 morning, I departed Milwaukie Inn motel at 07:08, and drove to TriMet Milwaukie station.
A local church donated part of their parking lot as a Park & Ride lot, where commuters can park their vehicles for up to 24 hours while they enter the city via transit or carpool.
I parked my rental car at the church, and walked to the station.
While I'm still reading the instructions on the ticket vending machine, a tram pulled into the station.
I decided to purchase a day pass using a credit card, to avoid problems with Android Pay.
By the time I received my ticket, the tram has departed, and I had to wait 15 minutes for the next tram.

The tram has medium occupancy during the morning rush: 75% of the passengers, including myself, were able to find a seat.
It has traffic priority in most places, and rarely needs to wait after other vehicles or stop at a red light.
However, it moves fairly slowly that a 6-mile trip took more than 30 minutes.
Then it's a short bus trip on route 20, traveling west along Burnside Street, a major thoroughfare of Portland.
I arrived at northeast corner of Washington Park at 08:23.

## Two Gardens

Washington Park is home to two of Portland's famous attractions: International Rose Test Garden and Portland Japanese Garden.
I chose Washington Park as the first stop because the rose garden opens the earliest at 07:30.

Washington Park covers 1.66 km<sup>2</sup> of steep, wooded hillside.
From the bus stop on Burnside Street, I need to climb up 61 meters to reach the gardens.
A sign at the trail head warns that "this is a high challenge trail", but the steps are no biggie for me, as I just [hiked Multnomah Falls](/t/2019/Oregon-vacation-5/) the day prior.

[International Rose Test Garden](https://www.portland.gov/parks/washington-park-international-rose-test-garden) is famous for its 10000 rose bushes of 650 varieties from around the world.
I visited in June, exactly when peak bloom occurs.
That's a lot of flowers.
However, I do not find them more attractive than any other patch of flowers.

![International Rose Test Garden](rose0.jpg)
![International Rose Test Garden](rose1.jpg)
![International Rose Test Garden](rose2.jpg)
![International Rose Test Garden](rose3.jpg)

[Portland Japanese Garden](https://japanesegarden.org/) is a lot better.
It is said to be the most authentic Japanese garden outside Japan.
It consists of five gardens: flat garden, tea garden, strolling pond garden, natural garden, and sand and stone garden.
The most beautiful is the tea garden, where the scenery appears greenish from all the surrounding trees.
The most interesting is the sand and stone garden, where the landscape is created by raked gravel and stone.
The most peaceful is the natural garden, which is quiet and offers plenty of park benches where I can sit, relax, and think about the meaning of life.

![Portland Japanese Garden - flat garden](jp0.jpg)
![Portland Japanese Garden - pavilion gallery](jp1.jpg)
![Portland Japanese Garden - tea garden](jp2.jpg)
![Portland Japanese Garden - tea garden](jp3.jpg)
![Portland Japanese Garden - strolling pond garden](jp4.jpg)
![Portland Japanese Garden - strolling pond garden](jp5.jpg)
![Portland Japanese Garden - natural garden](jp6.jpg)
![Portland Japanese Garden - natural garden](jp7.jpg)
![Portland Japanese Garden - sand and stone garden](jp8.jpg)
![Portland Japanese Garden - Jordan Schnitzer Japanese Arts Learning Center](jp9.jpg)

## Twenty-Three Rooms

![Pittock Mansion - exterior](Pittock-exterior.jpg)

[Pittock Mansion](https://pittockmansion.org/) is a local history museum that tells Portland's story.
It was built in 1914 as a private home for Henry Pittock, publisher of *The Oregonian* newspaper, and his wife.
I'm generally interested in museums, so I included this unique museum as part of my trip.
The mansion is located on a hilltop north of Washington Park.
I [requested an Uber ride](/t/2017/hailing-taxis/), as walking from Portland Japanese Garden to Pittock Mansion would take almost an hour.

<iframe width="600" height="400" allowfullscreen style="border-style:none;" src="/t/demo/pannellum.htm#panorama=/t/2019/Oregon-vacation-6/Pittock-music.jpg&title=Pittock Mansion - music room&autoLoad=true&yaw=180&minPitch=0&maxPitch=0"></iframe>

Pittock Mansion has 23 rooms available for self-guided tour.
Furniture presented in these rooms are from the period, but only a subset has been used by the Pittock family.
Nevertheless, I can see the glory of the house.

![Pittock Mansion - kitchen exhaust hood](Pittock-kitchen.jpg)
![Pittock Mansion - fridge](Pittock-fridge.jpg)

It's said that Mr Pittock adopted "modern" technology in his home.
The house has kitchen exhaust hood, electric fridge, intercom system, dumbwaiter, and elevator.
My favorite is the shower, which has a massage feature built into the pipes.

<iframe width="600" height="400" allowfullscreen style="border-style:none;" src="/t/demo/pannellum.htm#panorama=/t/2019/Oregon-vacation-6/Pittock-bath.jpg&title=Pittock Mansion - master bathroom&autoLoad=true&yaw=255&minPitch=0&maxPitch=0"></iframe>

## Four Quarters

After visiting Pittock Mansion, I followed the trail down to Burnside Street, and returned to Portland downtown by bus.
It's 14:22, and I bought a burrito for lunch.
Then it's geocaching time!

[Top Score](https://coord.info/GC4042), a Virtual cache, has 260 favorite points.
It is a "one-of-a-kind experiment": the coordinates point to an arcade, and one must play a classic arcade game in order to claim the cache.

The arcade, [Ground Kontrol](https://groundkontrol.com/), has a large selection of games.
Each game costs as little as two quarters (50 cents).
While the geocache only requires one game, it's hard to choose only one, so I [played two games](https://coord.info/GLYXZ4XJ) before moving on to the next attraction.

## Fifteen Waypoints

The next attraction, as identified from travel guides, is [Portland State University](https://www.pdx.edu/).
A short bus ride along the [Portland Transit Mall](https://en.wikipedia.org/wiki/Portland_Transit_Mall) got me on campus.
But, what to see on campus?

Others would look for a [campus tour route](https://www.pdx.edu/undergraduate-admissions/visit-campus), but as a geocacher I would look for a multi-cache that brings me to various locations on campus.
In this case, it is [Viking Volkswalk](https://coord.info/GC7HTG6), listed as a Mystery.

![PSU - Simon Benson](SimonBenson.jpg)

This geocache has a whopping 15 waypoints, including several university buildings and outdoor artwork.
Can I finish them within two hours?
I decided to give it a try, and stop halfway if I run out of time.

I managed to finish 14 waypoints within an hour, although the tour has been quite a rush.
The last waypoint, however, must be found "scavenger hunt" style: a photo of a "mystery statue" is presented on the cache listing, and I need to locate the statue that I should have seen while going around campus to the other waypoints.
What?
I was too focused on the compass and maps in my phone, and did not pay attention to the surroundings.
Walking the whole campus again was not an option, as I did not have sufficient time or energy.
Then I noticed something on the photo itself: EXIF location tag!
It brought me to the mystery statue right away, and enabled me to find the final stage in time.

## The Spooky Underground

Why did I have a time constraint at PSU in the first place?
Well, I booked a guided tour in the evening, and I need to come back to Portland downtown by 18:15.
The guided tour is [Shanghai Tunnel / Portland Underground Tour](https://portlandtunnels.com/).
According to their website:

> The "Shanghai Tunnels Heritage Tour" delves into the early history of Portland and the development of shanghaiing and white slavery.
> You'll leave knowing more about the shanghai trade in the "City of Roses" than you ever considered wanting to know.

I'm attracted by the name alone, because I'm born in Shanghai, China.
When I checked in with the guide, I explained my motivation, to which he replied: the tour is interesting, but it may not have much relationship with Shanghai the city.
It turns out that the word "[shanghai](https://en.wiktionary.org/wiki/shanghai)" has a different meaning here: to force or trick (someone) into joining a ship which is lacking a full crew.

![Portland Tunnels - ladder in front of Hobo's Restaurant](Hobo.jpg)

The group gathered in the backyard of Hobo's Restaurant, where we received a brief orientation.
After that, the tour guide opened up a panel on the sidewalk, and invited us into the underground world.
In the tunnels, we saw several rooms that resemble jail cells to hold the abducted sailors, as well as a bunker bed in another room used for trading moonshine in prohibition era.

![Portland Tunnels - shanghaiing cell](shanghai-cell.jpg)
![Portland Tunnels - bunker bed](bunker.jpg)

My [Chinese diary](/t/2019/Oregon-vacation-d/) has a more detailed account of this underground tour.

## Vacation Continues

My Portland tour ended at 20:10, when I boarded a tram back to Milwaukie.
My Oregon vacation continues with a grocery shopping trip in [part 7](/t/2019/Oregon-vacation-7/).
