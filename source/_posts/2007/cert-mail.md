---
title: 发送数字证书加密和签名的电子邮件
lang: zh
date: 2007-10-09
tags:
- security
---

## 操作视频

[前往土豆网观看操作视频](https://video.tudou.com/v/XMTk1MjE5Mzg4MA==.html)

1. 申请证书
2. 导出私钥、导出公钥
3. 使用Foxmail发送签名、加密的邮件

## 数字证书申请地址

* <http://ca.foxmail.com.cn> 接收邮件获取下载码，进入后安装证书
* <http://www.ca365.com> “免费证书”，选择“电子邮件保护证书”，申请成功后即时安装证书
* <http://www.thawte.com> 选Personal Email可以申请成功，但是操作很麻烦；好处是它的根证书是系统自带的，权威性较高；但是证书的名字只能是thawte freemail member，想写自己名字得去找5个工作人员出示身份证

建议使用IE浏览器申请；thawte可以用Firefox，其他不行
还可以自己建立CA，不过我在Win2003中没有安装成功，说是网络路径找不到，不玩了

## 加密、签名邮件发送方法

1. 把收件人的证书导入系统（双击，按提示导入“其他人”存储区即可）
2. 申请一个支持SMTP的邮箱，比如新浪，或者用学校的信箱、自建服务器
3. 安装好Foxmail6，下载地址 <http://fox.foxmail.com.cn>
4. 添加上此信箱，确认能够发送和接收普通的邮件
5. 撰写邮件时，在工具菜单勾选“数字签名”和“加密”
6. 点击“发送”时，会让你选择自己的私钥以及收件人的证书，正确选择即可

(当然，你也可以选用其他客户端软件发送)

## 常用POP3/SMTP设置

* 交大邮箱 pop3.sjtu.edu.cn smtp.sjtu.edu.cn
* 新浪邮箱 pop.sina.com smtp.sina.com
* Gmail 默认不支持，即使设置里开启了，速度也很慢
* 163/126/sohu/sogou 新申请的账号不支持POP3/SMTP
* Yahoo/hotmail 不支持，也无法开启；[据livesino.net报道](http://livesino.net/archives/400.live)，hotmail几个月后将支持

本帖原发表于[饮水思源BBS站IS板](http://bbs.sjtu.edu.cn/bbscon,board,IS,file,M.1191656871.A.html)
