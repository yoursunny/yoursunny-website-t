---
title: EDA实验课程设计——电梯控制器
lang: zh
date: 2007-04-02
tags:
- FPGA
---

这是本人2007年4月的作品。根据上海交通大学闵行校区电信学院大楼内的电梯工作仿制。

* AHDL语言编写，使用带有FLEX®EPF10K20TC144-4芯片的JDEE-10K实验箱
* 4层楼的电梯控制
* 轿箱按钮、层站召唤按钮，按钮带有指示灯
* 用LED表示马达运转方向
* 点阵显示当前楼层
* 点阵开关门模拟动画
* 支持背景音乐，乐曲为“欢乐颂”

[下载源代码](https://github.com/yoursunny/code2014/tree/elevator)
[实验报告](https://github.com/yoursunny/code2014/blob/elevator/elevator.pdf)

---

模拟的输入输出部件
![输入输出部件](io.gif)

顶层GDF设计图
![GDF顶层设计](gdf.gif)

## 可重用的组件

* 7seg8digit.tdf 八个数码管定时扫描显示组件
* dot.tdf 两个8x8双色点阵定时扫描显示组件
* music.tdf “欢乐颂”乐曲演奏组件
