---
title: ImageResizer批量图片缩小工具
lang: zh
date: 2007-09-28
tags:
- .Net
---

把自己的电脑重装成Windows2003操作系统后，发现以前经常使用的一个图片缩小工具[Windows XP Image Resizer Powertoy](http://web.archive.org/web/20091229183033/http://www.microsoft.com/windowsxp/downloads/powertoys/xppowertoys.mspx)不能安装了。
我经常需要将大量图片发到网站上，每个图片都用ACDSee或者Photoshop手动缩小，太麻烦了。
于是花了半个小时用C#写了个简单的批量图片缩小工具，可以快速将一个目录中的所有jpg图片缩小成指定尺寸(当然放大也可以)。

程序需要.NET Framework 2.0或以上版本的支持。
原版编写于2007年9月28日，2011年12月29日修正内存泄漏bug。

[下载ImageResizer(含源码)](https://github.com/yoursunny/code2014/tree/ImageResizer)
