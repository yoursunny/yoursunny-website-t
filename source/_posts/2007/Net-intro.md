---
title: .Net介绍 & Visual Studio 2008 下载地址
lang: zh
date: 2007-12-11
tags:
- .Net
---

.Net Framework是微软主导设计的一个开发框架，它是COM的继承者。最新版本为3.5，而2.0和3.0版本已经通过自动更新安装在每一台 WinXP/2003机器上。微软的.Net Framework不是开源的，可以在WindowsNT和FreeBSD系统上安装；.Net Framework的一个开源实现是[mono-project](http://www.mono-project.com/)，可以在LINUX和Windows等系统上安装；这两种实现是几乎完全兼容的。

.Net是一个开放的框架，编程语言只需兼容CLS(Common Language Specification)，即可用于.Net。其编译程序只有“前端”，即翻译成一种中间语言，这种中间语言是MSIL；而“后端”就是由安装在每一台机器上的Framework在运行时完成的，称为JIT(Just In Time)编译。.Net语言有C#、VB.NET、C++.NET、JScript.NET、MSIL汇编等，超过20种；程序员可以选择任何一款自己喜欢的语言进行开发，而无需担忧开发结果的兼容性——因为编译后都变成了MSIL这种中间语言；其中C#是为.Net度身定做的，如果你是初学者建议学习C#，此外VB.NET也是一种常用的语言。

Visual Studio 2008是.Net Framework 3.5的一个集成开发环境，这个开发环境由微软发布；.Net是一个开放的框架，因此也存在其他的集成开发环境；你甚至可以只安装Framework，而不安装任何集成开发环境。官方介绍与下载页面 <http://msdn2.microsoft.com/en-us/vstudio/default.aspx>。VS2008有多个版本，其中免费的精简版本称为Express版本。【原来这里有一个本地下载链接，由于有人使用迅雷下载，盗链严重，现已删除；请珍爱网络，远离迅雷！】
