---
title: AlbumPlayer相册播放器
lang: zh
date: 2007-12-02
tags:
- JavaScript
---

一个JavaScript类库，用于动态显示相册，支持多种转场效果

[下载AlbumPlayer](https://github.com/yoursunny/code2014/tree/AlbumPlayer)

## AlbumPlayer效果演示

<div id="album"></div>

## AlbumPlayer调用代码

```html
<head>
<!--相册播放器要求使用Prototype和Script.aculo.us两个类库-->
<script type="text/javascript" src="prototype.js"></script>
<script type="text/javascript" src="scriptaculous.js?load=builder,effects"></script>
<!--导入相册播放器的JS文件-->
<script type="text/javascript" src="album_player.js"></script>
</head>
<body>
<!--这个div是相册的容器-->
<div id="album"></div>
<script type="text/javascript">//<[CDATA[
//创建对象，四个参数依次为：相册容器id，宽度，高度，背景色
var my_player=new AlbumPlayer('album',500,375,'#FFFFFF');
//添加照片，五个参数依次为：照片URI，宽度，高度，显示时间(毫秒)，转场效果
//大照片会缩小，小照片不会放大
my_player.add('photo1.jpg',640,480,3000,'fade');
my_player.add('photo2.jpg',640,480,1100,'dropout');
my_player.add('photo3.jpg',500,375,1600,'shrink');
my_player.add('photo4.jpg',480,640,500,'appear');
my_player.add('photo5.jpg',1024,768,2100,'blindup');
my_player.add('photo6.jpg',400,300,4000,'slidedown');
//启动相册播放，不要漏掉这行
my_player.start();
//]]></script>
</body></code>
```

当前版本号：
<span id="a_version"></span>

支持的转场效果：
<span id="a_effects"></span>

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/prototype/1.6.1.0/prototype.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/scriptaculous/1.8.1/scriptaculous.js?load=builder,effects"></script>
<script type="text/javascript" src="album_player.js"></script>
<script type="text/javascript">//<![CDATA[
$('a_version').update(AlbumPlayer.version);
$('a_effects').update(AlbumPlayer.effects.join(', '));

var my_player=new AlbumPlayer('album',500,375,'#FFFFFF');
my_player.add('photo1.jpg',640,480,3000,'fade');
my_player.add('photo2.jpg',640,480,1100,'dropout');
my_player.add('photo3.jpg',500,375,1600,'shrink');
my_player.add('photo4.jpg',480,640,500,'appear');
my_player.add('photo5.jpg',1024,768,2100,'blindup');
my_player.add('photo6.jpg',400,300,4000,'slidedown');
my_player.start();
//]]></script>
