---
title: 使用SelfSSL创建自签名的IIS证书
lang: zh
date: 2007-11-09
tags:
- Windows
- security
---

微软[IIS 6.0 Resource Kit](https://www.microsoft.com/en-us/download/details.aspx?id=5135)中的小工具**SelfSSL**可以创建自签名的IIS证书，用于测试目的。
所谓“自签名”，就是指该证书本身是一个根CA证书，同时又能作为服务器证书。
当然，这个根CA证书是默认不受信任的，所以只能用于测试，不能用于实际的公开站点。

步骤：

1. 下载SelfSSL.exe，然后在Administrator下打开

   ![Microsoft SelfSSL Version 1.0, Do you want to replace the SSL settings for site 1 (Y/N)?](selfssl.jpg)

   对问题回答y并按回车，就会为IIS的默认Web站点创建一个自签名的证书

2. 然后要在IIS里操作：右击Websites下的Default Web Site，选Properties，在Directory Security选项卡点击Server Certificate，然后按提示，指派一个现有证书

   ![IIS Certificate Wizard, Assign an existing certificate](wizard.jpg)

3. 选择刚才创建的证书

   ![IIS Certificate Wizard, Select a certificate](select.jpg)

4. 双击证书，可以查看证书内容，可以看到不受信任：

   ![Certificate Information](certificate.jpg)

5. 下一步选择HTTPS的端口，默认端口是443。

6. 尝试用IE访问一下：

   ![Security Alert](alert.jpg)

   警告根证书不受信任（用在公开站点，给用户感觉就是“网站不安全”），点Yes

7. 成功打开https协议的网站（注意状态栏的锁），我们成功了！

   ![website with padlock](site.jpg)

注意：因为SSL加密的内容包含域名，所以无法在相同IP地址、相同端口，用域名区分开设多个SSL站点；这一点在IIS的帮助文档里写得很清楚。
