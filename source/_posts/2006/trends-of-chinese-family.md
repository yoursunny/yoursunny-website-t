---
title: The Trends of Chinese Family
lang: en
date: 2006-09-29
tags:
- commentary
---

In USA in 2000, 97 million people are cohabitants, they live together without marriage. In China, I guess this figure was very small years ago, but is it going to rise in the near future? I did a survey these days to find this out.

I posted my survey on four groups of 5Q.com. 5Q is an online community for college students, so the result may reflect what college students think of cohabiting.
Here's my four threads:
[1](http://group.5q.com/topic/ShowTopic.do?topicno=200679426&commno=200045762)
[2](http://group.5q.com/topic/ShowTopic.do?topicno=200679424&commno=200013225)
[3](http://group.5q.com/topic/ShowTopic.do?topicno=200679418&commno=200003144)
[4](http://group.5q.com/topic/ShowTopic.do?topicno=200679422&commno=200000119)

I asked them whether they agree with cohabiting, and why should people cohabit. I provided eight common reasons for the second question:

1. People are in love, and what to spend more time together. But they are under the legal marriage age.
2. People want to check whether they are compatible with each other.
3. People are engaged, but not married.
4. People are saving money for their marry banquet.
5. People can stay together without pay two rents.
6. People don't want to marry, or are unable to marry because of the pressure from the society.
7. People know his/her lover now is not compatible for life-time partner, but they are in love at this time.
8. People may lose a considerable amount of money (eg. heritage) if they get married.

I got 29 valid replies in my threads, here's the counts:

CHOICE | count
-------|------
don't agree | 2
agree with 1 | 8
agree with 2 | 16
agree with 3 | 1
agree with 4 | 1
agree with 5 | 3
agree with 6 | 4
agree with 7 | 4
agree with 8 | 0
agree only, reason not provided or other reasons | 4

Most replies agree with choice 2 and 1.

**2. People want to check whether they are compatible with each other.**
So-called "trial marriage" is becoming common in China these years.
This reflects that people are trying to pursue his/her true love, instead of marry with the one well-match in social and economics status years earlier.

**1. People are in love, and what to spend more time together. But they are under the legal marriage age.**
I think this is a prescient result among college students.
College students are usually too young to marry, according to the law.
But having a girlfriend/boyfriend is thought "a required course" in one's college life. If their relationship develop to some extent, cohabiting is the only choice to be together all the time without going against the law.

There were also some good advices in the replies:

> It's no fault cohabiting, while the cohabitant is not right to do this.
> Cohabiting is not a good idea because people are together all the time, thus they would not have their own space.

My conclusion is that cohabiting may become common in China from now then.
I suggest the government to make law to restrict it.
