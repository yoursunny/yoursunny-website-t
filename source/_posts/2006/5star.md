---
title: 五星寝室大赛，弘扬寝室文化 ——论大学文化与和谐校园
lang: zh
date: 2006-12-24
tags:
- commentary
---

我国在紧抓社会主义物质文明和精神文明的基础上，进一步提出了建设和谐社会的理念。建设和谐校园，实质是大学文化建设，重点是提升教育思想、办学理念，提高办学水平和管理水平，核心是理想信念和道德规范的价值体系建设，关键是全面提高人的思想道德素质。在和谐校园建设中，要着力建设和谐文化，营造“家”的浓厚氛围，牢固树立和谐校园建设的共同理想与道德规范，促进学校的长远和谐。在建设大学和谐校园过程中实现以下几个方面的和谐非常关键：大学理念的和谐、发展战略的和谐、管理机制的和谐、人际关系的和谐、收入分配的和谐、校园文化的和谐、资源配置的和谐、校园内外的和谐。

五星寝室大赛是上海交通大学生活园区团工委的品牌活动，是面向全校学生举办的寝室风采展示活动，每年举办一届，至今已是第六个年头。大学生寝室需要的不仅仅是干净、整洁，更需要蕴涵一种精神、温馨、团结、健康、积极向上，有生活的气息，有文化的底蕴，有鲜活的元素，有灵动的色彩……五星寝室的评比，旨在宣扬和引导学生了解寝室文化建设和内涵修养的意义，培养营造寝室家园氛围、人文关怀的意识和能力。通过整个活动过程，同学们可以充分领悟和体会寝室建设的重要性和乐趣，热爱寝室，亲手为寝室套上美的光环，打造出自己的生活空间，建设一个在交大的温馨港湾。

五星寝室大赛的宗旨在于让寝室在干净整洁的基础上，能够包含一定的精神内涵：温馨、团结、健康、积极向上。这些精神内涵，体现的就是寝室文化。大学生40%~70%的时间是在寝室内度过，寝室文化理所当然是大学文化的重要组成部分。五星寝室大赛的评比规则中，卫生整洁占20分、文明和谐占30分、特色建设占50分，可见大赛的侧重面确实在寝室文化。

我是本次大赛合作网络媒体——上海交通大学家园网的站长，负责大赛网络平台的制作和维护。工作过程中，看到了通过复评的50个寝室的展示内容。我注意到，所有的寝室都是有一定亮点的，比如人文情怀、科技求索、时尚先锋、体育运动、公益服务。今年的参赛寝室已经超越了去年的“布置得花哨、漂亮”，而有了更多的文化内涵——寝室成员可能有共同的兴趣爱好，同时每个人又有自己鲜明的个性；同学间互相帮助、爱护，这种帮助既有学习上的、又有生活上的。当然，展示的时候，这种“文化”仍然要体现在陈设、布置上，于是，同学们贴出了参加志愿者服务的照片，挂起了文艺、体育明星的海报，摆设了喜爱的毛绒玩具，吊上了丁丁当当的风铃……

五星寝室大赛充分体现了建设大学文化与和谐校园的要求，欢迎大家都来捧场！

参考文献

1. “我寝我秀”第六届上海交通大学五星寝室大赛 <http://5star.iloveu4ever.cn/>
2. 大学和谐校园建设探析 光明日报 <http://news.xinhuanet.com/edu/2006-11/20/content_5351223.htm>
3. 建设和谐大学文化 山东农大报 <http://weekly.sdau.edu.cn/html2006/2006/jysp/2006_44_30_7872.htm>
