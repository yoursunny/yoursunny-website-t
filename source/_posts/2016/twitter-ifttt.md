---
title: Repost Twitter to Facebook through Losant and IFTTT
lang: en
date: 2016-07-09
tags:
- IFTTT
- Losant
- API
image: workflow.png
---

Twitter and Facebook are two primary social networks that I regularly use.
Despite many arguments against synchronizing contents across different accounts, I still think it's beneficial to repost my tweets onto my Facebook timeline.
There are already many solutions to achieve cross-posting between Twitter and Facebook, but they are not ideal, because I'm very picky on what I want:

* I want to cross-post from Twitter to Facebook, not the other way around.
* I want to tweet with native Twitter clients, not through a third party website or app.
* I don't want those tweets created by my Swarm check-ins to be re-posted onto Facebook, because Swarm app can directly post to Facebook.
* For plain text tweets, I want them as plain text Facebook status updates, without a link to Twitter which could only confuse my Facebook friends.
* If I tweet a photo, I want that photo to be uploaded to Facebook, instead of posting a link on Facebook.
* If I tweet a link to some webpage, I want Facebook to display a preview of the webpage.

**IFTTT** is one of my favorite online services.
It allows me to create a recipe for certain automated actions.
The name "IFTTT" stands for IF This Than That, where This could be "posting a new tweet", and That could be "posting a Facebook status".
However, IFTTT does not allow filtering: I couldn't specify conditions like "the tweet is not posted by Swarm", which is necessary to achieve my goals.
Therefore, I need a more advance solution to repost my Twitter feed to Facebook.

This year the world is greeted with **Losant**, an Internet of Things platform that allows makers to connect their sensor devices, collect data into the cloud, and take actions through workflow execution.
While neither Twitter nor Facebook has anything to do with IoT, Losant **workflow** has some great capabilities that makes it suitable for non-IoT usage:

* trigger execution from a timer or an HTTP request
* parse and stringify JSON
* send HTTP request to any server
* store execute state in a variable
* most importantly, execute raw JavaScript!

Therefore, I piece together a Losant workflow to retrieve my tweets from Losant, filter them however I wish, and post them onto Facebook.
The workflow looks like this:

![Losant workflow](workflow.png)

It appears complicated, so let me explain.

The first two columns on the left periodically invoke Twitter API to fetch my latest tweets:

1. Green "every 3 minutes" node schedules the workflow to be executed every 3 minutes.
   There's also a "manual trigger" button so that I can test the workflow faster during development.
2. Each time the workflow processes only one tweet.
   Although it's possible to do a loop over all new tweets, the workflow would be much more complicated.
   Since I don't tweet that much, processing one tweet is enough.
   Purple "get processedId" node reads a state variable to know the ID number of the last tweet that has been processed.
3. Blue "set API method" node constructs parameters to invoke Twitter API `statuses/user_timeline` method.
4. Purple "load crypto-js" and "load OAuth-1.0a" nodes download two JavaScript libraries for generating OAuth signatures.
   Blue "OAuth sign" node `eval`s the downloaded libraries and then signs the Twitter API request with OAuth.
5. Purple "call Twitter API" node actually sends the HTTP request to Twitter API server.
6. Blue "find next tweet" node looks through the response from Twitter API, and determines whether I have posted a new tweet.
   If not, blue "has new tweet?" conditional node would terminate the workflow.
7. Purple "set processedId" saves the ID number of the current tweet, so that the next workflow execution won't process it again.

The third column does some basic filtering:

1. Blue "is retry?" node checks whether the tweet is an @-reply, and stops execution if so.
2. Blue "is source 4sq" node uses a regular expression to check whether the "source app" of the tweet is Swarm app, and sets a flag.
   This flag is checked in blue "is 4sq?" conditional node, so that tweets created by Swarm won't appear on Facebook.
3. Blue "full RT text" node does some text manipulation so that retweets don't get cut off.

The big chunk on the right posts the tweet onto Facebook.
Although I could invoke Facebook APIs directly from Losant workflow, it would probably need more libraries and other magic, so I just delegation this part to IFTTT.
IFTTT can create three kinds of Facebook posts: status message, link post, and photo.
Therefore, in the workflow:

1. Blue "has picture?" and "has link?" nodes determine which kind of Facebook post we want IFTTT to create.
2. In case of a photo post, blue "choose photo size" node finds the largest photo resolution returned from Twitter API, so that the best picture can be uploaded to Facebook.
3. Blue "prepare text", "prepare link", and "prepare photo" nodes prepare the parameters to be sent to IFTTT.
4. Blue "encode IFTTT values" node encodes the parameters into JSON.
5. Yellow "call IFTTT maker" sends an HTTP request to [IFTTT Maker channel](https://ifttt.com/maker).

The final step is to create three IFTTT recipes to create each kind of Facebook posts:

* Maker event `twttr_text` creates a Facebook status message
* Maker event `twttr_link` creates a Facebook link post
* Maker event `twttr_photo` uploads a photo to Facebook

This workflow has been running for over two weeks now, and it works perfectly so far.
If you also want to re-post your Twitter to your Facebook timeline, so that your Facebook friends can see your tweets:

1. [grab the workflow](https://gist.github.com/yoursunny/69113aff0ab2e8215e0b8ebee0adcb04)
2. import to Losant, fill in your [Twitter API credentials](https://apps.twitter.com) and [IFTTT Maker channel key](https://ifttt.com/maker) in "workflow globals"
3. add three IFTTT recipes: [`twttr_text`](https://ifttt.com/recipes/439414-facebook-status-from-maker), [`twttr_link`](https://ifttt.com/recipes/439415-facebook-link-from-maker), [`twttr_photo`](https://ifttt.com/recipes/439416-facebook-photo-from-maker)
4. enjoy
