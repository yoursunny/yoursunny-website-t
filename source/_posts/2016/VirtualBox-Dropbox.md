---
title: Share Dropbox between VirtualBox Host and Guest
lang: en
date: 2016-01-03
tags:
- VirtualBox
- Linux
- Ubuntu
image: gvfs_media.png
---

My laptop comes with Windows, like most other laptops in the market.
But as a [computer science student](/m/cs/), I need to use Linux from time to time.
The laptop manufacturer advised me not to install Linux directly on this laptop.
Although this would not void my warranty, they would not provide technical support or supply device drivers if I install Linux.
Therefore, I turned into [VirtualBox](https://www.virtualbox.org), a hypervisor that allows me to run Linux in a virtual machine, alongside the Windows installation.

I'm also a heavy user of [Dropbox](https://bit.ly/dboxapp), a file hosting service that can synchronize my documents among all my device.
I have Dropbox clients installed everywhere, including the Windows of this laptop, and the Linux virtual machine.
When I edit a file, the Dropbox client uploads this file to the cloud, and then the Dropbox clients on all other devices download the file from the cloud.

One day, there's a congestion on my apartment's WiFi hotspot, and I notice that the Dropbox synchronization between Windows and the Linux virtual machine is having significant delay: every update travels a long way to the cloud, and then comes back.
I also realize that, in my setup, the entire Dropbox contents are duplicated twice: it has one copy in Windows, and another copy in Linux virtual machine.
Although having multiple copies is usually a good thing because you have more redundancy, having multiple copies on the same hard drive is not useful.
Can I eliminate the synchronization delay and the redundant copy?

## VirtualBox Shared Folder

VirtualBox has a nifty feature, [shared folders](https://www.virtualbox.org/manual/ch04.html#sharedfolders), which allows files of the host system to be accessed within a guest system.
In my setup, I could use this feature to access the Dropbox on Windows within the Linux virtual machine.

The setup is easy: open up *Settings* dialog of the Linux virtual machine, and add a machine folder named "Dropbox".

![VirtualBox: add shared folder](addSharedFolder.png)

Start or reboot the virtual machine, and a folder `/media/sf_Dropbox` shows up:

![/media/sf_Dropbox](media.png)

To access this folder, the Linux user must be in `vboxsf` group.
`sudo usermod -a -G vboxsf username` does just that.
(Changing groups takes effect after the user re-login.)

## Symbolic Link

Now my Windows Dropbox is accessible from the Linux virtual machine at `/media/sf_Dropbox` folder.
However, I'm more accustomed to `~/Dropbox`, the default folder of Linux Dropbox client.

To access Windows Dropbox at `~/Dropbox`, I need to stop and uninstall Linux Dropbox client, delete the old `$HOME/Dropbox` folder (along with all the duplicate files).
Then, one command does it all: `ln -s /media/sf_Dropbox ~/Dropbox`.
Bingo, the Dropbox is back at my home folder.

## gedit Save Error and Solution

Everything appears to work fine, until I start writing a document with my favorite text editor: gedit (and pluma, which is very similar).
As soon as I click the Save button, an error pops up: "Unexpected error: Error renaming temporary file: Text file busy".

![gedit save error](geditError.png)

It turns out that this is a bug in either [VirtualBox](https://www.virtualbox.org/ticket/2553) or [gio](https://bugzilla.gnome.org/show_bug.cgi?id=656225).
The gio library used by gedit saves a file by writing to a temporary file and renaming to the final name, without closing the file first.
This operation is valid on most Linux filesystems, but is not allowed on Windows.
Since my Dropbox is now on a Windows filesystem, saving a file to Dropbox this way would be rejected by Windows and cause an error.

I tried a few ways to fix this problem, and finally found a working solution: "loopback" SFTP.

1. Install OpenSSH Server: `sudo apt-get install openssh-server`.
2. Enable SSH public key authentication: `ssh-keygen` and confirm everything but don't set passphrase.
3. Try SSH connection to localhost: `ssh localhost`.
4. Install GVFS: `sudo apt-get install gvfs-bin gvfs-fuse`.
5. Mount localhost SFTP: `gvfs-mount -f sftp://localhost`.

After running these file commands, the filesystem of local Linux machine should be available at `/run/user/<uid>/gvfs/sftp:host=localhost`, where `<uid>` is my user id which can be determined with `id -u` command.
And of course, `/media/sf_Dropbox` would be available at `/run/user/<uid>/gvfs/sftp:host=localhost/media/sf_Dropbox`.

![/run/user/\<uid\>/gvfs/sftp:host=localhost/media](gvfs_media.png)

Re-create the symbolic link with `ln -s /run/user/$(id -u)/gvfs/sftp:host=localhost/media/sf_Dropbox ~/Dropbox` (after deleting the old one), and try gedit again.
This time, it works!

![gedit save success](geditSaved.png)

## Automate SFTP Mounting

Reboot the Linux virtual machine, and I find my Dropbox is inaccessible again!
Although the VirtualBox shared folder `/media/sf_Dropbox` is intact, the symbolic link does not work, because the GVFS mount isn't automatically mounted.

![~/Dropbox inaccessible](symlinkInaccessible.png)

Typing `gvfs-mount -f sftp://localhost` in a console solves the problem, but I want to automate this mounting process.
To automate SFTP mounting, I need to create an auto-start entry to be executed when I log into the desktop session.

1. Open `~/.config/autostart` folder.

2. Create a text file named `gvfs-mount.desktop`, and open it with gedit.

3. Paste the following contents.

    ```ini
    [Desktop Entry]
    Type=Application
    Exec=gvfs-mount sftp://localhost
    Name=mount Dropbox
    ```

The next time I reboot the Linux virtual machine, my Dropbox is ready to use at `~/Dropbox`.

![~/Dropbox OK](symlinkGood.png)
