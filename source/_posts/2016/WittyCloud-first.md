---
title: First Impression on ESP8266 Witty Cloud Board
lang: en
date: 2016-05-28
tags:
- ESP8266
image: ESP8266.jpg
---

I get addicted to ESP8266 ever since [Losant sent me a builder kit](https://t.co/J2njlnNWjJ) which includes an Adafruit Feather HUZZAH.
**Witty Cloud GitWits** is one of the cheapest ESP8266 device you can get on the market.
It has a microUSB port with USB-Serial interface, which means no soldering!
The board also contains an RGB LED and a button, which allows me to use it without a breadboard.
So, I decide to go for it!

I ordered the Witty Cloud board from [AliExpress](https://www.aliexpress.com/item/32643464555.html), a Chinese E-commerce site.
Ordering experience isn't so good.
For some reason, they cancelled my order due to "difficulty in verifying payment".
I had to upload a scanned copy of my passport and credit card statement, before they would ship my items.
After getting past those non-technical steps, the Witty Cloud development board is in my mailbox 2 weeks later.
There's no doubt [I'm excited](https://twitter.com/yoursunny/status/736284761027694593)!

## Appearance

The board is very small and lightweight.
Its dimension is 31mm x 30mm x 18mm.
Its weight is roughly the same as an Adafruit Feather HUZZAH.

Witty Cloud board has two PCBs connected via a pair of 8-pin headers.

![front of top PCB](ESP8266.jpg)

Top PCB has our main player, an ESP8266MOD WiFi microcontroller.
There is a photoresistor on the top-left corner, and an RGB LED on the top-right corner.

![back of top PCB](AMS1117.jpg)

Back side of top PCB has two rows of male headers for connection to the bottom PCB.
An AMS1117 voltage regulator sits in the center, which is responsible for supplying 3.3V power to the ESP8266.
There is a button on the top edge, and a micro USB port on the bottom edge.

![front of bottom PCB](CH340G.jpg)

Bottom PCB has two rows of female headers where the top PCB can be inserted into.
A CH340G USB-Serial chip sits in the center, so that my computer could talk to the ESP8266.
A little 12.000MHz crystal assists the CH340G to do its job.
There are two buttons on the top edge, and a micro USB port on the bottom edge.

![back of bottom PCB](bottom.jpg)

Back side of bottom PCB has nothing on it, because it's the very bottom of an assembled board.

![micro USB ports](USB.jpg)

When assembled, we can see two micro USB ports on one side, and three buttons on the other side.

![buttons](button.jpg)

It's said that this design allows the top PCB to be deployed alone, while the bottom PCB is connected only when the ESP8266 needs to be re-programmed.

## Plug it in!

As soon as I connect the board to my computer with a micro USB cable, the green LED turns on, and a tiny blue LED (next to the WiFi antenna) blinks once.

![device powered on](powered.jpg)

Windows 10 did a nice job and recognizes CH340G USB-Serial chip automatically.
No driver download is necessary.

![Windows 10 built-in CH340 driver](CH340.png)

## Simple Blink Program

Witty Cloud, being an ESP8266 chip, can be programmed with Arduino IDE.
Programming environment is same as Adafruit Feather HUZZAH, so I just followed [Losant IoT Developer Kit Environment Setup](https://web.archive.org/web/20190412184926/https://docs.losant.com/getting-started/losant-iot-dev-kits/environment-setup/) procedure (except that CP2102 driver is unnecessary because our board uses CH340G).
The `Streaming.h` library can be [downloaded as a ZIP](http://arduiniana.org/libraries/streaming/).

I have tried a simple program that blinks every GPIO pin.

```arduino
#include <Streaming.h>

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.println();

  Serial << "setup\n";
}

void blink(int pin) {
  Serial << "analogRead(A0)=" << analogRead(A0) << "\n";

  pinMode(pin, OUTPUT);
  Serial << "pinMode(" << pin << ",OUTPUT)\n";
  delay(1000);

  digitalWrite(pin, 0);
  Serial << "digitalWrite(" << pin << ",0)\n";
  delay(1000);

  digitalWrite(pin, 1);
  Serial << "digitalWrite(" << pin << ",1)\n";
  delay(1000);

  for (int pct = 10; pct <= 90; pct += 10) {
    analogWrite(pin, static_cast<int>(PWMRANGE * 0.01 * pct));
    Serial << "analogWrite(" << pin << "," << pct << "%)\n";
    delay(1000);
  }

  analogWrite(pin, 0);
}

void loop() {
  blink(0);
  blink(2);
  blink(4);
  blink(5);
  blink(12);
  blink(13);
  blink(14);
  blink(15);
  blink(16);
}
```

The available GPIO pin numbers are same as the [GPIO pins of Adafruit Feather HUZZAH](https://learn.adafruit.com/adafruit-feather-huzzah-esp8266/pinouts#gpio-pins):
\#0, \#2, \#4, \#5, \#12, \#13, \#14, \#15, \#16.
All these GPIO pins are accessible on the male headers of the top PCB.
Other pin numbers, such as \#1 or \#3, cannot be used.
When I attempt to "blink" them, the microcontroller would reset itself.

<iframe width="800" height="450" src="https://www.youtube.com/embed/GFZp6ajCdUU" frameborder="0" allowfullscreen></iframe>

I recorded a video showing the effect of this blink program.
It indicates that the pinout of this board is:

* GPIO\#2 controls the tiny blue LED next to the WiFi antenna.
  HIGH turns off the LED, and LOW turns on the LED.
* GPIO\#15 controls red of the RGB LED.
  HIGH turns on the color, and LOW turns off the color.
* GPIO\#12 controls green of the RGB LED.
  HIGH turns on the color, and LOW turns off the color.
* GPIO\#13 controls blue of the RGB LED.
  HIGH turns on the color, and LOW turns off the color.

Unlike [what other people have found](https://www.esp8266.com/viewtopic.php?p=37687), all four GPIO pins support PWM.

That's what I found during my first two hours with the Witty Cloud development board.
I can't wait to explore more.
