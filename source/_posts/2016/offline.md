---
title: How to Survive a Weekend Offline?
lang: en
date: 2016-04-29
tags:
- life
- Arizona
---

I'm a heavy smartphone user.
I read emails, access Twitter, check in on Swarm/Foursquare, watch YouTube, and look at weather forecast, multiple times a day.
I use smartphones so much that my primary phone, the Nexus 5, needs to be charged 2~3 times per day, and I am reluctant to stay in places without free WiFi.

But, everything is changing in the past weekend: I joined a camping trip to Grand Canyon National Park.
There is no electricity.
There is no cellular signal, because national parks do not want cell towers to ruin the beautiful landscape.
There is no WiFi, except in the cafeteria which we may or may not visit.
Without electricity, I cannot keep my smartphones charged.
Without cellular or WiFi, I cannot receive emails, access Twitter, check in on Swarm/Foursquare, watch YouTube, or use weather forecast apps.
How can I survive the weekend in the national park, without electricity or Internet access?

## Preparation

Internet access is unavailable, but it's not the only way to communication with the outside world.
On many smartphones, there's a forgotten app called the **FM radio**, which allows you to receive information without Internet access or cellular signal.
Therefore, I packed an old smartphone, the T-Mobile Comet, which has an FM radio tuner.
This would allow me to listen to the weather forecast, and maybe some news and music.

My camping trip is a short one: we depart on Friday and return on Sunday.
The lack of electricity can be solved by a few **USB PowerBank**s.
I brought two USB PowerBanks, which contain a total of 12800mAh of electricity, enough to charge the Nexus 5 five times, or the T-Mobile Comet ten times.

A camera is an essential during every scenic trip.
Instead of relying on a smartphone, I brought a DSLR camera.
I don't have a spare battery, and the camera cannot be charged from a USB PowerBank, but that's not a problem because the single battery has never run out even for a whole week trip.

## The Outbound Journey

It's a group trip, so I don't have access to the vehicle's power ports.
I kept using the Nexus 5 on the way out.
This phone has no FM radio, so it would be worthless once we travel out of Sprint's coverage area.
Emails, Twitter, Swarm, weather forecasts, etc, are all accessible.
I even found a geocache during the 30-minute dinner stop.

At Williams, AZ, the last town before entering the Grand Canyon National Park, I did a [last check-in](https://twitter.com/yoursunny/status/723716878087102464).
Sometime after that, there's no more cellular signal.

P.S.
Sadly, this was the last day I could enjoy the Nexus 5.
The power button on the phone was broken before the trip, and the repeated powering off and on (to conserve battery) further added to the stress, so that the phone won't power on the next day.
It's eventually sold to ecoATM for $23.

## First Night

The camping trip is a time to **embrace the nature**.
We arrived at Mather Campgrounds on Apr 22 night, just after a full moon this morning.
This night, I'm watching the moon, instead of YouTube.

It's a nice feeling to stay in a tent, hear the birds and the wind, and look at the moon light cast on the roof the the tent.
The moon light is more intensive than the light emitted from a smartphone screen; it has a yellowish color, so a [bluelight filter](https://play.google.com/store/apps/details?id=jp.ne.hardyinfinity.bluelightfilter.free&hl=en) is not necessary.

The wind sounds like music to me.
But when the wind picks up its speed and starts to shake the tent, it gets scary.
As a result, I waked up 3 times during the night.
I was able to go back to sleep quickly, because I'm not checking Twitter, which I would do if I have WiFi.

I eventually waked up at 5am and started to listen to the FM radio.
There is only one station: [KNAU](https://www.knau.org/), an NPR news station.
I listened to this station for one hour.
They talked about news, but the only sentence I could hear about weather is: "windy, 3 degrees above average".
By breakfast time, this became the only knowledge about weather among our group.

## The Day

We did [two hikes](/t/2017/hiking-Arizona/) on the Saturday, Apr 23: South Kaibab trail, and Shoshone Point.
Since I have a dedicated DSLR camera, the smartphone is only needed for **recording GPS tracks**.
[GPS Logger](https://play.google.com/store/apps/details?id=com.mendhak.gpslogger&hl=en) handles the tracking task nicely.
It still has a version for Android 2.2 so that I could install it on the Comet.
The battery usage of this GPSLogger app is incredibly small: 5 hours of tracking (at 150s interval) consumes less than 30% of a 1200mAh battery.
I made sure to take a few pictures on the smartphone, to go with my Swarm/Foursquare check-ins once I get out.

I ran into WiFi signal twice during the day: at the Grand Canyon Visitor Center, and the Grand Canyon Village General Store.
I know I'm hungry for an Internet signal, so I tried hard to connect to these WiFi signals.
However, neither would work.

## Second Night

The second night includes cooking a dinner at the camp, a campfire, before going to sleep.
Between the tasks like chopping broccolli, stacking the firewood, and washing the dishes, I have enough time to look at the stars in the sky, and **use the DSLR camera to take pictures** in the darkness.
I did a 30-second long exposure for a picture of the stars.
I wish I have read [Elements of Photography](http://smile.amazon.com/Arkenstone-Technologies-Pvt-Ltd-Photography/dp/B00D5XYY4M) before the trip, so I could take even better pictures.

We spent much time **talking to each other**, next to the campfire, face-to-face, not through Facebook.

## The Return Journey

AT&T signal (for the Comet) appears around Williams, AZ.
I spent one hour on the 2G network to do the Swarm/Foursquare check-ins for everything happened during the weekend.

As this point, I realize, I survived 36 hours in the Grand Canyon National Park, offline, without electricity or Internet access.
Next time, shall I try a 5-day trip and stay offline?

P.S. Photo albums from the trip can be found at [OneDrive](https://onedrive.live.com/redir?resid=1FA4AA816E7DC9F3!102031&authkey=!AK2IqirU5HKAb5k&ithint=folder%2cJPG), [Facebook](https://www.facebook.com/media/set/?set=a.10154140855323827.1073741971.721293826&type=1&l=90fbe1fa55), or [Renren](http://photo.renren.com/photo/200037590/album-1138311747/v7).
