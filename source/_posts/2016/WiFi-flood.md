---
title: "Do Evil with ESP8266: Slow Down the WiFi"
lang: en
date: 2016-06-05
tags:
- ESP8266
- WiFi
---

While hackers do good most of the time, we occasionally do evil and play a prank.
The ESP8266, unlike [JSON](https://wiki.debian.org/qa.debian.org/jsonevil), allows me to do evil.
Thus, I programmed the microcontroller for an evil purpose: slow down the WiFi.

## How it Works

802.11 WiFi typically operates in **infrastructure mode**, where a router acts as an **access point**, and other hosts (**station**s) connect to the router on a wireless frequency (a **channel**).

One property of the wireless channel is that, at any moment, only one party (station or access point) can be transmitting.
If multiple senders are transmitting at the same time, the wireless signal will be jammed, and the recipient is unlikely to receive the packet correctly.
In this case, the sender would have to transmit the packet again at a later time.

Packets can be transmitted at different speeds on the wireless channel.
With 802.11g standard, the maximum speed is 54Mbps, and the minimum is as slow as 1Mbps.
The sender (station or access point) dynamically chooses a speed for every packet depending on its perception of wireless channel quality.
Usually, we prefer to transmit at a higher speed, so that the wireless channel can be freed as soon as possible for other senders to use.
However, if the sender and recipient are far apart, high speed transmission is less likely to succeed because signals can be faded, and a slower speed is necessary to increase the chance of a successful transmission.

Packet from a station is always addressed toward the access point, even if its destination is another station.
The access point then relays the packet either via a wired interface, or wirelessly toward the recipient station.
The two transmissions (from station to access point, and from access point to recipient station) can choose speeds independently.

One class of network packets is **broadcast packet**, where a packet is addressed to a broadcast address, and is intended to be received by multiple recipients.
Broadcast packet from a station is also relayed through the access point: the access point receives it from the sender, and forwards the packet toward the recipients.
However, a major difference from regular unicast packets is: there are multiple recipients, and the access point is no longer able to choose a speed according to the channel quality between the access point and the single recipient.
In this case, WiFi standards mandate the access point to **transmit the broadcast packet at the lowest speed**, 1Mbps.
This means, if we transmit a lot of broadcast packets, the wireless channel will be busy for much longer duration than the same amount of unicast packets.

## The Evil Code

**WARNING**:
The code provided in this article is for education purpose.
I ran the evil ESP8266s only for several minutes to test its effect.
It's not recommended to leave this running for long periods of time, because this can disrupt normal communication which is a violation of FCC rules for 2.4G band.

```arduino
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

uint8_t pkt[1000];

void setup() {
  Serial.begin(115200);

  WiFi.mode(WIFI_AP);
  WiFi.softAP("00000000", "00000000", 6, 1);

  memset(pkt, 0, sizeof(pkt));
}

void loop() {
  WiFiUDP udp;
  udp.beginPacketMulticast(WiFi.softAPIP(), 1, IPAddress(255,255,255,255), 1);
  udp.write(pkt, sizeof(pkt));
  udp.endPacket();
}
```

This ESP8266 Arduino code establishes a WiFi access point on channel 6, and then repeatedly transmits broadcast packets as fast as possible.
Since each channel refers to a single wireless frequency, this would affect all WiFi networks on the same channel, even if the stations are connected to other access points.

## Performance Test

To test the effectiveness of this attack, I establish an access point on a laptop, and connect a tablet to the access point.
Then, the tablet downloads a 100MB file from the laptop over unencrypted HTTP.

The test is repeated 3 times, and the speed and duration of each download, as shown by `wget`, are reported below.

normal      | one evil ESP8266 | two evil ESP8266s
------------|------------------|------------------
2.29M/s 39s | 1.49M/s 67s      | 715K/s 142s
2.78M/s 36s | 1.59M/s 59s      | 959K/s 117s
3.09M/s 32s | 1.47M/s 61s      | 958K/s 95s

We can see that having some access points flooding broadcast packets has an adverse impact on network throughput.
Also, in the first two rounds of tests, the impact of adding a second evil ESP8266 is much greater than the first one.
One possible cause is the exponentially increased probability of having the channel jammed due to simultaneous transmissions on the same channel.

This concludes my evil experiment.
My ESP8266s are boiling hot because the CPU and RF are operating at full speed.
I'll stop the evil here, and re-flash them with something good.
