---
title: base64加密PHP脚本的解码方法
lang: zh
date: 2009-08-02
tags:
- PHP
---

PHP是网站服务端最流行的编程语言之一。PHP运行环境本身是开源的，服务器不加载插件时PHP脚本也无法加密。但是，总有人因为商业上的考虑，而将PHP程序通过各种方法进行混淆，使读者很难看到清晰易懂的代码。

然而，PHP运行环境的本质决定了，被混淆、编码的PHP脚本总是有办法恢复成可读的代码的。本文介绍了一种对含有`LAVE`和`base64_decode`的、被加密的PHP的解码方法。

在使用这种方法之前，你应该准备好：

* 能运行PHP的Web服务器，例如 Apache 或 IIS
* wget.exe命令行客户端 或 浏览器
* 具备PHP语法高亮功能的文本编辑器，例如 Notepad2

[下载每一步的源代码](https://github.com/yoursunny/code2014/tree/PHP-decode/1)

待解密的PHP代码来自某WordPress模板，[来源](https://zhidao.baidu.com/question/89605169.html)

```php
//0.php
<?php
/* WARNING: This file is protected by copyright law. To reverse engineer or decode this file is strictly prohibited. */
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
LAVE(base64_decode("JGxsbD0wO2V2YWwoYmFzZTY0X2RlY29kZSgiSkd4c2JHeHNiR3hzYkd4c1BTZGlZWE5sTmpSZlpHVmpiMlJsSnpzPSIpKTskbGw9MDtldmFsKCRsbGxsbGxsbGxsbCgiSkd4c2JHeHNiR3hzYkd3OUoyOXlaQ2M3IikpOyRsbGxsPTA7JGxsbGxsPTM7ZXZhbCgkbGxsbGxsbGxsbGwoIkpHdzlKR3hzYkd4c2JHeHNiR3hzS0NSdktUcz0iKSk7JGxsbGxsbGw9MDskbGxsbGxsPSgkbGxsbGxsbGxsbCgkbFsxXSk8PDgpKyRsbGxsbGxsbGxsKCRsWzJdKTtldmFsKCRsbGxsbGxsbGxsbCgiSkd4c2JHeHNiR3hzYkd4c2JHdzlKM04wY214bGJpYzciKSk7JGxsbGxsbGxsbD0xNjskbGxsbGxsbGw9IiI7Zm9yKDskbGxsbGw8JGxsbGxsbGxsbGxsbGwoJGwpOyl7aWYoJGxsbGxsbGxsbD09MCl7JGxsbGxsbD0oJGxsbGxsbGxsbGwoJGxbJGxsbGxsKytdKTw8OCk7JGxsbGxsbCs9JGxsbGxsbGxsbGwoJGxbJGxsbGxsKytdKTskbGxsbGxsbGxsPTE2O31pZigkbGxsbGxsJjB4ODAwMCl7JGxsbD0oJGxsbGxsbGxsbGwoJGxbJGxsbGxsKytdKTw8NCk7JGxsbCs9KCRsbGxsbGxsbGxsKCRsWyRsbGxsbF0pPj40KTtpZigkbGxsKXskbGw9KCRsbGxsbGxsbGxsKCRsWyRsbGxsbCsrXSkmMHgwZikrMztmb3IoJGxsbGw9MDskbGxsbDwkbGw7JGxsbGwrKykkbGxsbGxsbGxbJGxsbGxsbGwrJGxsbGxdPSRsbGxsbGxsbFskbGxsbGxsbC0kbGxsKyRsbGxsXTskbGxsbGxsbCs9JGxsO31lbHNleyRsbD0oJGxsbGxsbGxsbGwoJGxbJGxsbGxsKytdKTw8OCk7JGxsKz0kbGxsbGxsbGxsbCgkbFskbGxsbGwrK10pKzE2O2ZvcigkbGxsbD0wOyRsbGxsPCRsbDskbGxsbGxsbGxbJGxsbGxsbGwrJGxsbGwrK109JGxsbGxsbGxsbGwoJGxbJGxsbGxsXSkpOyRsbGxsbCsrOyRsbGxsbGxsKz0kbGw7fX1lbHNlJGxsbGxsbGxsWyRsbGxsbGxsKytdPSRsbGxsbGxsbGxsKCRsWyRsbGxsbCsrXSk7JGxsbGxsbDw8PTE7JGxsbGxsbGxsbC0tO31ldmFsKCRsbGxsbGxsbGxsbCgiSkd4c2JHeHNiR3hzYkd4c2JEMG5ZMmh5SnpzPSIpKTskbGxsbGw9MDtldmFsKCRsbGxsbGxsbGxsbCgiSkd4c2JHeHNiR3hzYkQwaVB5SXVKR3hzYkd4c2JHeHNiR3hzYkNnMk1pazciKSk7JGxsbGxsbGxsbGw9IiI7Zm9yKDskbGxsbGw8JGxsbGxsbGw7KXskbGxsbGxsbGxsbC49JGxsbGxsbGxsbGxsbCgkbGxsbGxsbGxbJGxsbGxsKytdXjB4MDcpO31ldmFsKCRsbGxsbGxsbGxsbCgiSkd4c2JHeHNiR3hzYkM0OUpHeHNiR3hzYkd4c2JHd3VKR3hzYkd4c2JHeHNiR3hzYkNnMk1Da3VJajhpT3c9PSIpKTtldmFsKCRsbGxsbGxsbGwpOw=="));
return;
?>
```

代码中只有一个`LAVE`，先把这个`LAVE`替换为`echo`。

```php
//1.php
<?php
/* WARNING: This file is protected by copyright law. To reverse engineer or decode this file is strictly prohibited. */
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
echo(base64_decode("JGxsbD0wO2V2YWwoYmFzZTY0X2RlY29kZSgiSkd4c2JHeHNiR3hzYkd4c1BTZGlZWE5sTmpSZlpHVmpiMlJsSnpzPSIpKTskbGw9MDtldmFsKCRsbGxsbGxsbGxsbCgiSkd4c2JHeHNiR3hzYkd3OUoyOXlaQ2M3IikpOyRsbGxsPTA7JGxsbGxsPTM7ZXZhbCgkbGxsbGxsbGxsbGwoIkpHdzlKR3hzYkd4c2JHeHNiR3hzS0NSdktUcz0iKSk7JGxsbGxsbGw9MDskbGxsbGxsPSgkbGxsbGxsbGxsbCgkbFsxXSk8PDgpKyRsbGxsbGxsbGxsKCRsWzJdKTtldmFsKCRsbGxsbGxsbGxsbCgiSkd4c2JHeHNiR3hzYkd4c2JHdzlKM04wY214bGJpYzciKSk7JGxsbGxsbGxsbD0xNjskbGxsbGxsbGw9IiI7Zm9yKDskbGxsbGw8JGxsbGxsbGxsbGxsbGwoJGwpOyl7aWYoJGxsbGxsbGxsbD09MCl7JGxsbGxsbD0oJGxsbGxsbGxsbGwoJGxbJGxsbGxsKytdKTw8OCk7JGxsbGxsbCs9JGxsbGxsbGxsbGwoJGxbJGxsbGxsKytdKTskbGxsbGxsbGxsPTE2O31pZigkbGxsbGxsJjB4ODAwMCl7JGxsbD0oJGxsbGxsbGxsbGwoJGxbJGxsbGxsKytdKTw8NCk7JGxsbCs9KCRsbGxsbGxsbGxsKCRsWyRsbGxsbF0pPj40KTtpZigkbGxsKXskbGw9KCRsbGxsbGxsbGxsKCRsWyRsbGxsbCsrXSkmMHgwZikrMztmb3IoJGxsbGw9MDskbGxsbDwkbGw7JGxsbGwrKykkbGxsbGxsbGxbJGxsbGxsbGwrJGxsbGxdPSRsbGxsbGxsbFskbGxsbGxsbC0kbGxsKyRsbGxsXTskbGxsbGxsbCs9JGxsO31lbHNleyRsbD0oJGxsbGxsbGxsbGwoJGxbJGxsbGxsKytdKTw8OCk7JGxsKz0kbGxsbGxsbGxsbCgkbFskbGxsbGwrK10pKzE2O2ZvcigkbGxsbD0wOyRsbGxsPCRsbDskbGxsbGxsbGxbJGxsbGxsbGwrJGxsbGwrK109JGxsbGxsbGxsbGwoJGxbJGxsbGxsXSkpOyRsbGxsbCsrOyRsbGxsbGxsKz0kbGw7fX1lbHNlJGxsbGxsbGxsWyRsbGxsbGxsKytdPSRsbGxsbGxsbGxsKCRsWyRsbGxsbCsrXSk7JGxsbGxsbDw8PTE7JGxsbGxsbGxsbC0tO31ldmFsKCRsbGxsbGxsbGxsbCgiSkd4c2JHeHNiR3hzYkd4c2JEMG5ZMmh5SnpzPSIpKTskbGxsbGw9MDtldmFsKCRsbGxsbGxsbGxsbCgiSkd4c2JHeHNiR3hzYkQwaVB5SXVKR3hzYkd4c2JHeHNiR3hzYkNnMk1pazciKSk7JGxsbGxsbGxsbGw9IiI7Zm9yKDskbGxsbGw8JGxsbGxsbGw7KXskbGxsbGxsbGxsbC49JGxsbGxsbGxsbGxsbCgkbGxsbGxsbGxbJGxsbGxsKytdXjB4MDcpO31ldmFsKCRsbGxsbGxsbGxsbCgiSkd4c2JHeHNiR3hzYkM0OUpHeHNiR3hzYkd4c2JHd3VKR3hzYkd4c2JHeHNiR3hzYkNnMk1Da3VJajhpT3c9PSIpKTtldmFsKCRsbGxsbGxsbGwpOw=="));
return;
?>
```

运行上述代码。运行的方法是：将代码粘贴到一个PHP文件里，用浏览器访问并查看源代码，或者用wget下载。运行结果是：

```php
//1.txt
$lll=0;LAVE(base64_decode("JGxsbGxsbGxsbGxsPSdiYXNlNjRfZGVjb2RlJzs="));$ll=0;LAVE($lllllllllll("JGxsbGxsbGxsbGw9J29yZCc7"));$llll=0;$lllll=3;LAVE($lllllllllll("JGw9JGxsbGxsbGxsbGxsKCRvKTs="));$lllllll=0;$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);LAVE($lllllllllll("JGxsbGxsbGxsbGxsbGw9J3N0cmxlbic7"));$lllllllll=16;$llllllll="";for(;$lllll<$lllllllllllll($l);){if($lllllllll==0){$llllll=($llllllllll($l[$lllll++])<<8);$llllll+=$llllllllll($l[$lllll++]);$lllllllll=16;}if($llllll&0x8000){$lll=($llllllllll($l[$lllll++])<<4);$lll+=($llllllllll($l[$lllll])>>4);if($lll){$ll=($llllllllll($l[$lllll++])&0x0f)+3;for($llll=0;$llll<$ll;$llll++)$llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];$lllllll+=$ll;}else{$ll=($llllllllll($l[$lllll++])<<8);$ll+=$llllllllll($l[$lllll++])+16;for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));$lllll++;$lllllll+=$ll;}}else$llllllll[$lllllll++]=$llllllllll($l[$lllll++]);$llllll<<=1;$lllllllll--;}LAVE($lllllllllll("JGxsbGxsbGxsbGxsbD0nY2hyJzs="));$lllll=0;LAVE($lllllllllll("JGxsbGxsbGxsbD0iPyIuJGxsbGxsbGxsbGxsbCg2Mik7"));$llllllllll="";for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}LAVE($lllllllllll("JGxsbGxsbGxsbC49JGxsbGxsbGxsbGwuJGxsbGxsbGxsbGxsbCg2MCkuIj8iOw=="));LAVE($lllllllll);
```

前后加上 `<?php` 和 `?>` ，加进来原来`$o`的定义，并将代码稍稍排版一下，可得：

```php
//1r.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
LAVE(base64_decode("JGxsbGxsbGxsbGxsPSdiYXNlNjRfZGVjb2RlJzs="));
$ll=0;
LAVE($lllllllllll("JGxsbGxsbGxsbGw9J29yZCc7"));
$llll=0;
$lllll=3;
LAVE($lllllllllll("JGw9JGxsbGxsbGxsbGxsKCRvKTs="));
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
LAVE($lllllllllll("JGxsbGxsbGxsbGxsbGw9J3N0cmxlbic7"));
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
LAVE($lllllllllll("JGxsbGxsbGxsbGxsbD0nY2hyJzs="));
$lllll=0;
LAVE($lllllllllll("JGxsbGxsbGxsbD0iPyIuJGxsbGxsbGxsbGxsbCg2Mik7"));
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
LAVE($lllllllllll("JGxsbGxsbGxsbC49JGxsbGxsbGxsbGwuJGxsbGxsbGxsbGxsbCg2MCkuIj8iOw=="));
LAVE($lllllllll);
?>
```

将代码中的第一个`LAVE`改成`echo`，后面的语句删除：

```php
//2.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
echo(base64_decode("JGxsbGxsbGxsbGxsPSdiYXNlNjRfZGVjb2RlJzs="));
?>
```

运行结果：

```php
//2.txt
$lllllllllll='base64_decode';
```

将运行结果替换掉`echo(base64_decode(...))`部分，后面的语句粘贴回来：

```php
//2r.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
LAVE($lllllllllll("JGxsbGxsbGxsbGw9J29yZCc7"));
$llll=0;
$lllll=3;
LAVE($lllllllllll("JGw9JGxsbGxsbGxsbGxsKCRvKTs="));
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
LAVE($lllllllllll("JGxsbGxsbGxsbGxsbGw9J3N0cmxlbic7"));
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
LAVE($lllllllllll("JGxsbGxsbGxsbGxsbD0nY2hyJzs="));
$lllll=0;
LAVE($lllllllllll("JGxsbGxsbGxsbD0iPyIuJGxsbGxsbGxsbGxsbCg2Mik7"));
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
LAVE($lllllllllll("JGxsbGxsbGxsbC49JGxsbGxsbGxsbGwuJGxsbGxsbGxsbGxsbCg2MCkuIj8iOw=="));
LAVE($lllllllll);
?>
```

类似的，反复进行三步操作：

1. 将`LAVE`替换成`echo`，删除后面的语句
2. 运行
3 用运行结果替换`LAVE`语句，恢复后面的语句

直到代码中不再出现`LAVE`。

```php
//3.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
echo($lllllllllll("JGxsbGxsbGxsbGw9J29yZCc7"));
?>
```

```php
//3.txt
$llllllllll='ord';
```

```php
//3r.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
LAVE($lllllllllll("JGw9JGxsbGxsbGxsbGxsKCRvKTs="));
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
LAVE($lllllllllll("JGxsbGxsbGxsbGxsbGw9J3N0cmxlbic7"));
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
LAVE($lllllllllll("JGxsbGxsbGxsbGxsbD0nY2hyJzs="));
$lllll=0;
LAVE($lllllllllll("JGxsbGxsbGxsbD0iPyIuJGxsbGxsbGxsbGxsbCg2Mik7"));
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
LAVE($lllllllllll("JGxsbGxsbGxsbC49JGxsbGxsbGxsbGwuJGxsbGxsbGxsbGxsbCg2MCkuIj8iOw=="));
LAVE($lllllllll);
?>
```

仍有`LAVE`，继续……

```php
//4.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
echo($lllllllllll("JGw9JGxsbGxsbGxsbGxsKCRvKTs="));
?>
```

```php
//4.txt
$l=$lllllllllll($o);
```

```php
//4r.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
LAVE($lllllllllll("JGxsbGxsbGxsbGxsbGw9J3N0cmxlbic7"));
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
LAVE($lllllllllll("JGxsbGxsbGxsbGxsbD0nY2hyJzs="));
$lllll=0;
LAVE($lllllllllll("JGxsbGxsbGxsbD0iPyIuJGxsbGxsbGxsbGxsbCg2Mik7"));
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
LAVE($lllllllllll("JGxsbGxsbGxsbC49JGxsbGxsbGxsbGwuJGxsbGxsbGxsbGxsbCg2MCkuIj8iOw=="));
LAVE($lllllllll);
?>
```

仍有`LAVE`，继续……

```php
//5.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
echo($lllllllllll("JGxsbGxsbGxsbGxsbGw9J3N0cmxlbic7"));
?>
```

```php
//5.txt
$lllllllllllll='strlen';
```

```php
//5r.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
$lllllllllllll='strlen';
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
LAVE($lllllllllll("JGxsbGxsbGxsbGxsbD0nY2hyJzs="));
$lllll=0;
LAVE($lllllllllll("JGxsbGxsbGxsbD0iPyIuJGxsbGxsbGxsbGxsbCg2Mik7"));
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
LAVE($lllllllllll("JGxsbGxsbGxsbC49JGxsbGxsbGxsbGwuJGxsbGxsbGxsbGxsbCg2MCkuIj8iOw=="));
LAVE($lllllllll);
?>
```

仍有`LAVE`，继续……

```php
//6.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
$lllllllllllll='strlen';
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
echo($lllllllllll("JGxsbGxsbGxsbGxsbD0nY2hyJzs="));
?>
```

```php
//6.txt
$llllllllllll='chr';
```

```php
//6r.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
$lllllllllllll='strlen';
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
$llllllllllll='chr';
$lllll=0;
LAVE($lllllllllll("JGxsbGxsbGxsbD0iPyIuJGxsbGxsbGxsbGxsbCg2Mik7"));
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
LAVE($lllllllllll("JGxsbGxsbGxsbC49JGxsbGxsbGxsbGwuJGxsbGxsbGxsbGxsbCg2MCkuIj8iOw=="));
LAVE($lllllllll);
?>
```

仍有`LAVE`，继续……

```php
//7.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
$lllllllllllll='strlen';
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
$llllllllllll='chr';
$lllll=0;
echo($lllllllllll("JGxsbGxsbGxsbD0iPyIuJGxsbGxsbGxsbGxsbCg2Mik7"));
?>
```

```php
//7.txt
$lllllllll="?".$llllllllllll(62);
```

```php
//7r.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
$lllllllllllll='strlen';
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
$llllllllllll='chr';
$lllll=0;
$lllllllll="?".$llllllllllll(62);
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
LAVE($lllllllllll("JGxsbGxsbGxsbC49JGxsbGxsbGxsbGwuJGxsbGxsbGxsbGxsbCg2MCkuIj8iOw=="));
LAVE($lllllllll);
?>
```

仍有`LAVE`，继续……

```php
//8.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
$lllllllllllll='strlen';
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
$llllllllllll='chr';
$lllll=0;
$lllllllll="?".$llllllllllll(62);
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
echo($lllllllllll("JGxsbGxsbGxsbC49JGxsbGxsbGxsbGwuJGxsbGxsbGxsbGxsbCg2MCkuIj8iOw=="));
?>
```

```php
//8.txt
$lllllllll.=$llllllllll.$llllllllllll(60)."?";
```

```php
//8r.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
$lllllllllllll='strlen';
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
$llllllllllll='chr';
$lllll=0;
$lllllllll="?".$llllllllllll(62);
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
$lllllllll.=$llllllllll.$llllllllllll(60)."?";
LAVE($lllllllll);
?>
```

仍有`LAVE`，继续……

```php
//9.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
$lllllllllllll='strlen';
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
$llllllllllll='chr';
$lllll=0;
$lllllllll="?".$llllllllllll(62);
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
$lllllllll.=$llllllllll.$llllllllllll(60)."?";
echo($lllllllll);
?>
```

```php
//9.txt
?>
        <div class="clear"></div>

    </div>
    <!-- /Main -->

    <!-- Footer -->
    <div id="footer">
    Green Delight Designed by
<a href="http://www.bytesforall.com/">Bytesforall Webdesign</a> Coding by <a href="http://www.maiahost.com"> Cheap web hosting</a> | <a href="http://templates4all.com">Website Templates</a> |
<a href="http://www.thepatio.ws/">Private Krankenversicherung</a>
    </div>
    <!-- Footer -->

</div></div></div>
<!-- /Page -->

<?php wp_footer(); ?>

</body>

</html><?
```

```php
//9r.php
<?php
$o="QAAADg4KDQ4OO2NucSdka2Z0dAAROiVka2JmdSU5OygBQDkKDQIBgAMAxjsmKionKEpmbmknKioB1QFBAwVBaGhzYnUBVATibmM6JWEBYiUCkQAEQHViYmknQ2JrbmBvcwCAdG4AAGBpYmMnZX4KDTtmJ291YmEAADolb3Nzdz0oKHBwcCllfnMAAGJ0YWh1ZmtrKWRoaiglOUWBAAEnJ1BiZWNiBCE7KGY5J0RoYxQAbmlgBLAnBK9wKWpmbmZvaHRzoA4EcSUC0G9iZncncGJlJwFRA4AEMnuBSAO+c2Jqd2tmCFAzCDQlOQdgdG5zHgBiJ1MBtQOzDB8MEXNvYndmc25oKSEAcHQL0Fd1bnEE0CdMdWZpbGJpAA9xYnV0bmRvYnVyaWAEUhZwFksVCfwYBhAYwgIjAGMBQBdxKFdmYGIXYwEgOHdvkGANsHdYF3MvLjwnOBewAZAoZWhjfoAAALRvc2prOQ==";
$lll=0;
$lllllllllll='base64_decode';
$ll=0;
$llllllllll='ord';
$llll=0;
$lllll=3;
$l=$lllllllllll($o);
$lllllll=0;
$llllll=($llllllllll($l[1])<<8)+$llllllllll($l[2]);
$lllllllllllll='strlen';
$lllllllll=16;
$llllllll="";
for(;$lllll<$lllllllllllll($l);){
    if($lllllllll==0){
        $llllll=($llllllllll($l[$lllll++])<<8);
        $llllll+=$llllllllll($l[$lllll++]);
        $lllllllll=16;
    }
    if($llllll&0x8000){
        $lll=($llllllllll($l[$lllll++])<<4);
        $lll+=($llllllllll($l[$lllll])>>4);
        if($lll){
            $ll=($llllllllll($l[$lllll++])&0x0f)+3;
            for($llll=0;$llll<$ll;$llll++)
                $llllllll[$lllllll+$llll]=$llllllll[$lllllll-$lll+$llll];
            $lllllll+=$ll;
        }
        else{
            $ll=($llllllllll($l[$lllll++])<<8);
            $ll+=$llllllllll($l[$lllll++])+16;
            for($llll=0;$llll<$ll;$llllllll[$lllllll+$llll++]=$llllllllll($l[$lllll]));
            $lllll++;
            $lllllll+=$ll;
        }
    }
    else
        $llllllll[$lllllll++]=$llllllllll($l[$lllll++]);
    $llllll<<=1;
    $lllllllll--;
}
$llllllllllll='chr';
$lllll=0;
$lllllllll="?".$llllllllllll(62);
$llllllllll="";
for(;$lllll<$lllllll;){$llllllllll.=$llllllllllll($llllllll[$lllll++]^0x07);}
$lllllllll.=$llllllllll.$llllllllllll(60)."?";
?>
        <div class="clear"></div>

    </div>
    <!-- /Main -->

    <!-- Footer -->
    <div id="footer">
    Green Delight Designed by
<a href="http://www.bytesforall.com/">Bytesforall Webdesign</a> Coding by <a href="http://www.maiahost.com"> Cheap web hosting</a> | <a href="http://templates4all.com">Website Templates</a> |
<a href="http://www.thepatio.ws/">Private Krankenversicherung</a>
    </div>
    <!-- Footer -->

</div></div></div>
<!-- /Page -->

<?php wp_footer(); ?>

</body>

</html><?
?>
```

没有`LAVE`了！9r.php就是真实代码。从功能上看，也不妨把9.txt看作真实代码。

[下载每一步的源代码](https://github.com/yoursunny/code2014/tree/PHP-decode/1)

解码过程的关键是：

* 每次只能处理一个`LAVE()`块
* `LAVE`不能在循环、条件分支内部，否则不适用本方法
* `LAVE`替换成`echo`后，必须把后面的代码删除
* 获得运行结果后，用运行结果替换原来的`LAVE`，必须保留之前、之后的所有代码

PHP解密的难点，并不是技术问题，而是耐心。PHP解码，需要你作好打持久战的准备，才能顺利完成。

如果你正被形如`LAVE(base64_decode(...))`的PHP代码困扰，祝您在阅读本文后能顺利解密PHP代码。

<script>
// eval was changed to LAVE to workaround Windows Defender detecting source file as JS/Foretype.A!ml
for (const $code of document.querySelectorAll('code')) {
  $code.innerHTML = $code.innerHTML.replace(/\bLAVE\b/g, "eval");
}
</script>
