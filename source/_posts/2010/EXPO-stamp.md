---
title: 用XSLT欣赏所有世博印章
lang: zh
date: 2010-07-13
tags:
- XSLT
- Fiddler
---

中国2010年上海世博会吸引了众人的关注，五彩缤纷的“世博印章”更是令人趋之若鹜。作为2010年世博会的两大创新项目之一的[网上世博会](http://www.expo.cn)的“我的护照”栏目，可以看到大部分场馆的世博印章图片。那么，能否一次性看到所有的世博印章呢？

用[Fiddler](http://www.fiddler2.com)观察网络流量可以得知：每个片区有一个xml配置文件包含了片区内各场馆的名称和代码。例如，A片区的xml配置文件是：

```xml
<!-- http://www.expo.cn/configs/tencent/azone.xml -->
<root>
<err>0</err>
<p>
    <pid>11100001</pid>
    <dis>A片区</dis>
    <cnname>中国国家馆</cnname>
    <code>z0001</code>
    <type>体验馆</type>
    <name>China Pavilion</name>
</p>
<p>
    <pid>11200011</pid>
    <dis>A片区</dis>
    <cnname>哈萨克斯坦馆</cnname>
    <code>c1050</code>
    <type>浏览馆</type>
    <name>Kazakhstan Pavilion</name>
</p>
<p>
    <pid>11200013</pid>
    <dis>A片区</dis>
    <cnname>沙特馆</cnname>
    <code>c1086</code>
    <type>体验馆</type>
    <name>Saudi Arabia Pavilion</name>
</p>
<p>
    <pid>11200025</pid>
    <dis>A片区</dis>
    <cnname>印度馆</cnname>
    <code>c1018</code>
    <type>浏览馆</type>
    <name>India Pavilion</name>
</p>
<!-- 以下省略 -->
</root>
```
而世博印章的图片URI，可以根据pid字段构造。例如，中国国家馆的印章URI是：`http://www.expo.cn/images/HZ/11100001.png`。

程序处理xml有很多种方法，其中一种好用的方法是XSLT。XSLT是一种声明性的、基于XML的语言，用于将一个或多个xml文档转换成另一些xml文档。XSLT经常被用于将xml数据转换成可以作为网页显示的html或xhtml文档。

因此，我们要做的是：编写一个xslt文件，将网上世博会某一片区的xml配置文件，转换成能显示出该片区所有场馆“世博印章”的html文档。

```xml
<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns="http://www.w3.org/1999/xhtml">

<xsl:output method="html" indent="yes" encoding="UTF-8"/>

<xsl:template match="/root">
<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<title>世博印章</title>
</head>
<body>
<h1>世博印章</h1>
<table>
<thead><tr><td>pid</td><td>中文名称</td><td>英文名称</td><td>世博印章</td></tr></thead>
<tbody>
<xsl:apply-templates select="p"/>
</tbody>
</table>
</body>
</html>
</xsl:template>

<xsl:template match="p">
<tr>
<td><xsl:value-of select="pid"/></td>
<td><a><xsl:attribute name="href">http://www.expo.cn/pavilion/<xsl:value-of
 select="pid"/>.html?l=cn</xsl:attribute><xsl:value-of select="cnname"/></a></td>
<td><a><xsl:attribute name="href">http://www.expo.cn/pavilion/<xsl:value-of
 select="pid"/>.html?l=en</xsl:attribute><xsl:value-of select="name"/></a></td>
<td><img><xsl:attribute name="src">http://www.expo.cn/images/HZ/<xsl:value-of
 select="pid"/>.png</xsl:attribute><xsl:attribute name="alt"><xsl:value-of
 select="cnname"/> 世博印章</xsl:attribute></img></td>
</tr>
</xsl:template>

</xsl:stylesheet>
```

## 欣赏各场馆所有世博印章的方法

1. 下载片区的xml配置文件： [A片区](http://www.expo.cn/configs/tencent/azone.xml) [B片区](http://www.expo.cn/configs/tencent/bzone.xml) [C片区](http://www.expo.cn/configs/tencent/czone.xml) [D片区](http://www.expo.cn/configs/tencent/dzone.xml) [E片区](http://www.expo.cn/configs/tencent/ezone.xml)
2. 将上述xslt文件保存为expo.xsl，请用utf-8编码保存。
3. 用记事本编辑xml配置文件，在开头加上一行：`<?xml-stylesheet href="expo.xsl" type="text/xsl"?>`
4. 用浏览器打开xml配置文件，即可看到转换后的html文档，欣赏该片区所有场馆的世博印章；有些世博印章无法显示（或显示为红叉），说明该场馆没有印章。

声明：本文只作技术讨论，不提供任何形式的世博印章打包下载。你用上述方法看到的世博印章，其著作权属于2010年上海世博会的组织者和参展方，因此不允许随意传播。
