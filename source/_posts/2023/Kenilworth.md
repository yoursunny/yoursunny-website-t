---
title: 华盛顿周边一日游 2023-07-01 Kenilworth 水生植物园、华盛顿界碑、加拿大日烟花
lang: zh
date: 2023-12-20
tags:
- travel
- Maryland
discuss:
- https://shuiyuan.sjtu.edu.cn/t/topic/229126/11?u=yoursunny
---

![lotus](100607.jpg)

华盛顿有两条主要河流，西边的叫波托马克河（Potomac River），东边的叫阿纳卡斯蒂亚河（Anacostia River），两者相交呈“Y”形。
Potomac 之西南的土地，本来属于华盛顿，但是在 1847 年归还给了弗吉尼亚州，变成了亚历山德里亚（Alexandria）市。
Anacostia 之东南的土地，至今还是属于华盛顿，居民以黑人为主，经济比较落后、治安也比较差劲。

东南区的 Anacostia 河畔有一条 3.5 英里长的步道，连通周围的绿地被称为 [Anacostia 公园](https://www.nps.gov/anac/)，由国家公园局管辖。
步道的最东端是 [Kenilworth 水生植物园](https://www.nps.gov/keaq/)，是华盛顿最大的潮汐沼泽，居住着数百种动植物。
那里最著名的是每年夏天盛开的荷花与睡莲。

![lotus](103621.jpg)

根据公园网站的介绍，荷花、睡莲盛开的时节是每年 6 月下旬至 7 月下旬。
网站还说，观荷必须要赶早，因为荷花会在午后的高温中合上自己的花瓣。
7 月 1 日星期六，估摸着荷花已经开了，我赶上清晨 7 点的头班 78 路公共汽车，经过 2 个小时的公交、地铁、走路，上午 10 点到达 Kenilworth 水生植物园。

![lotus](100652.jpg)

植物园里有十几片篮球场那么大的池塘，池塘里生长着荷花（lotus）或睡莲（waterlily），池塘间有泥土堆成的堤供游客行走。
我看到了刚刚萌出的荷花、含苞待放的荷花、已经盛开的荷花。
荷花之间夹杂着一些绿色的莲蓬，让我不禁想起小时候奶奶买莲蓬给我吃的场景。

![lotus pod](100714.jpg)

睡莲则以绿色的叶子为主，上面有一些星星点点的白色花朵。

![waterlily](095721.jpg)

公园内还有一条木栈道，通向大片的湿地，是观鸟人的天堂。
可是我是近视眼，只要不是老鹰，我基本上看不见。

![southeast 8 boundary stone](SE8.jpg)

11 点离开水生园，我步行前往华盛顿东南 8 号界碑。
1791~1792 年，Andrew Ellicott 少校带领测绘员在华盛顿总统选定的首都位置的四周安置了 40 块界碑、每英里一块；这些界碑可以说是美国建国后最早的纪念碑。
200 多年之后，40 块界碑尚存 36 块，我已经见过大约一半。
这一块界碑位于茂密的小树林里，需要披荆斩棘才能见到。

![Anacostia - Where We Play](WhereWePlay.jpg)

界碑瞻仰完毕，我乘坐公共汽车前往同样位于华盛顿东南的 Anacostia 社区博物馆，并在途中吃了午餐。
虽然名叫“社区博物馆”，这家博物馆可是史密森学会（Smithsonian）下属的国家级博物馆。
那天，博物馆正在展出“华盛顿的妇女与环境公平”，有一些相关图片视频。
还有互动展项，比如用毛毡表达“你对环境公平有什么愿想”，我也制作了一幅。

![Anacostia - no trash in waterway](NoTrashInWaterway.jpg)

下午 14 点刚过，我就上地铁打道回府，因为我要恢复体力、晚上还有节目。
晚上 21 点天黑之后，是盖瑟斯堡市一年一度的国庆烟花。
我们知道，美国的国庆节是 7 月 4 日。
也许是为了防止与附近其他城市 7 月 4 日的烟花表演“撞车”，盖瑟斯堡市政府宣布，今年的烟花表演提前到 7 月 1 日举行，并在核心场地举行大型派对。
我们也知道，7 月 1 日是“加拿大日”，所以美国盖瑟斯堡市政府的烟花，是庆祝了加拿大。
懒惰的我没有前往核心场地，而只走到一个能看见烟花的街角，然后站在电线杆下，在大树、楼房的空隙中观看了 20 分钟的烟花。

花费：公交地铁 6 元，门票免费，午餐约 15 元。
