---
title: May I Borrow your IPv4?
lang: en
date: 2023-09-09
tags:
- hosting
- IPv6
discuss:
- https://lowendspirit.com/discussion/7226/may-i-borrow-your-ipv4/p1
---

With the rise of IPv4 costs, IPv6-only servers are becoming more popular in low end hosting world.
In one occasion, I acquired an IPv6-only server, but wanted to have IPv4 on it.
Somewhere else, I have a dual-stack IPv4+IPv6 server, idling away.
Can I "borrow" the IPv4 of that dual-stack server, to use on the IPv6-only server?
I tried several methods, and found a way to make this work.

## Situation / Assumption / Requirement

In this case, both servers have KVM virtualization and are running Debian 12 operating system.
Server A is a dual-stack server, with IPv4 address `192.0.2.158` and IPv6 address `2001:db8:aefc::2`.
Server B is an IPv6-only server, with IPv6 address `2001:db8:eec0::2`.
At server A, both IPv4 and IPv6 service are delivered on the same network interface.

My goal is to somehow "move" the IPv4 address from server A to server B.
In particular, I want all IPv4 traffic to the `192.0.2.158` destination address to reach server B, and allow server B to send any IPv4 traffic with `192.0.2.158` source address.
This shall include all TCP and UDP ports, as well as other IPv4 traffic such as ICMP ping.
A "port forwarding" solution would be insufficient, as it cannot deliver non-TCP/UDP traffic.

## Tunnel + Ethernet Bridge (does not work)

The first idea came to my mind is creating an Ethernet bridge:

```bash
# server A
ip link add br4 type bridge
ip addr flush dev eth0
ip link set eth0 master br4
ip addr add 2001:db8:aefc::2/64 dev br4
ip route add default via 2001:db8:aefc::1
ip link add tnl4 type ip6gre remote 2001:db8:eec0::2 local 2001:db8:aefc::2 hoplimit 64
ip link set tnl4 up master br4

# server B
ip link add tnl4 type ip6gre remote 2001:db8:aefc::2 local 2001:db8:eec0::2 hoplimit 64
ip link set tnl4 up
ip addr add 192.0.2.158/24 dev tnl4
ip route add default via 192.0.2.1
```

These commands would achieve the following:

1. Establish an ip6gre tunnel between server A and server B.
2. Add the `192.0.2.158` IPv4 address on server B's tunnel interface.
3. Create an Ethernet bridge on server A that includes both the `eth0` "real" network interface and the tunnel interface.

Theoretically, incoming ARP requests for the `192.0.2.158` IPv4 address would travel over the ip6gre tunnel to server B, server B would respond to those ARP requests, and then IPv4 traffic could flow normally.
However, this method does not work, because:

* Most hosting providers have MAC address filtering in place.
  The hypervisor does not allow server A to send packets with a source MAC address that differs from the assigned MAC address of its `eth0` "real" network interface.
* When server B responds to the ARP requests for the `192.0.2.158` IPv4 address, the source MAC address would be the MAC address of server B's ip6gre tunnel interface.
  These packets would be dropped by the hypervisor of server A's hosting provider.

I tried changing the MAC address of server B's ip6gre tunnel interface to be same as server A's `eth0`, but this does not work either.
Effectively, there are two network interfaces with duplicate MAC address in the `br4` bridge, confusing the Linux bridge driver.

## Tunnel + IPv4 Routing (it works)

The second idea is configuring IPv4 routing, but without any address in server A.
According to [ip-route(8) manpage](https://man7.org/linux/man-pages/man8/ip-route.8.html), the next hop of a route could specify a device name without an address:

```abnf
NH := [ encap ENCAP ] [ via [ FAMILY ] ADDRESS ] [ dev STRING ] [ weight NUMBER ] NHFLAGS
```

Therefore, I could make a tunnel and route the IPv4 address over the tunnel interface:

```bash
# server A
echo 1 >/proc/sys/net/ipv4/ip_forward
ip addr del 192.0.2.158/24 dev eth0
ip link add tnl4 type ip6gre remote 2001:db8:eec0::2 local 2001:db8:aefc::2 hoplimit 64
ip link set tnl4 up
ip route add default via 192.0.2.1 dev eth0 onlink
ip route add 192.0.2.158/32 dev tnl4
echo 1 >/proc/sys/net/ipv4/conf/all/proxy_arp

# server B
ip link add tnl4 type ip6gre remote 2001:db8:aefc::2 local 2001:db8:eec0::2 hoplimit 64
ip link set tnl4 up
ip addr add 192.0.2.158/32 dev tnl4
ip route add default dev tnl4
```

These commands would achieve the following:

1. Establish an ip6gre tunnel between server A and server B.
2. Delete the `192.0.2.158` IPv4 address from server A, and add it on server B's tunnel interface.
3. Make server A forward IPv4 traffic with destination address `192.0.2.158` over the tunnel interface, and route all other IPv4 traffic over the `eth0` "real" network interface.
4. Make server A respond to ARP requests for the `192.0.2.158` IPv4 address.
5. Make server B send all IPv4 traffic over the tunnel interface.

This method worked.
Traceroute and tcpdump suggest that the traffic is indeed being passed to server B.

## Traceroute Reports

I tested this procedure between [Crunchbits](https://crunchbits.com/) "server A" in Spokane WA and [Limitless Hosting](https://limitlesshost.net/) "server B" in Chicago IL, with 50ms RTT in between over IPv6.

Before tunnel setup, from outside to our IPv4 address:

```shell
sunny@clientC:~$ mtr -wnz -c4 192.0.2.158
Start: 2023-09-09T22:19:32+0000
HOST: vps4                   Loss%   Snt   Last   Avg  Best  Wrst StDev
  1. AS???    10.0.0.73       0.0%     4    9.3  13.0   9.3  18.7   4.0
  2. AS201106 172.83.154.5    0.0%     4    1.3   2.2   1.0   4.5   1.6
  3. AS???    ???            100.0     4    0.0   0.0   0.0   0.0   0.0
  4. AS400304 23.147.152.15   0.0%     4   24.3  55.7  24.3 130.8  50.2
  5. AS400304 192.0.2.158     0.0%     4   13.7  13.7  13.6  13.7   0.1
```

After tunnel setup, from outside to our IPv4 address:

```shell
sunny@clientC:~$ mtr -wnz -c4 192.0.2.158
Start: 2023-09-09T22:22:38+0000
HOST: vps4                   Loss%   Snt   Last   Avg  Best  Wrst StDev
  1. AS???    10.0.0.73       0.0%     4    4.8  12.8   2.5  36.4  15.9
  2. AS201106 172.83.154.5    0.0%     4    0.3   0.3   0.2   0.3   0.0
  3. AS???    ???            100.0     4    0.0   0.0   0.0   0.0   0.0
  4. AS400304 23.147.152.15   0.0%     4   19.5  31.7  17.4  67.7  24.1
  5. AS???    ???            100.0     4    0.0   0.0   0.0   0.0   0.0
  6. AS400304 192.0.2.158     0.0%     4   63.0  63.1  63.0  63.2   0.1
```

Notice that the RTT of the last hop increased by 50ms, which corresponds to the IPv6 RTT between server A and server B.
There is also a mysterious "hop 5" that we cannot see the IPv4 address, because server A no longer possesses a globally reachable IPv4 address.

Before tunnel setup, from server A to an outside IPv4 destination:

```shell
sunny@serverA:~$ mtr -wnz -c4 1.1.1.1
Start: 2023-09-09T22:25:33+0000
HOST: vps6                   Loss%   Snt   Last   Avg  Best  Wrst StDev
  1. AS400304 104.36.84.1     0.0%     4   18.5  49.1  13.2 151.5  68.3
  2. AS400304 23.147.152.14   0.0%     4    0.4   0.7   0.4   1.3   0.4
  3. AS???    206.81.81.10    0.0%     4   12.0  12.5  12.0  12.9   0.4
  4. AS13335  172.71.144.3    0.0%     4   12.2  13.2  11.8  16.0   1.9
  5. AS13335  1.1.1.1         0.0%     4   11.7  11.7  11.7  11.8   0.1
```

After tunnel setup, from server B to an outside IPv4 destination:

```shell
sunny@serverB:~$ mtr -wnz -c4 1.1.1.1
Start: 2023-09-09T18:23:43-0400
HOST: box4                   Loss%   Snt   Last   Avg  Best  Wrst StDev
  1. AS???    192.0.0.8       0.0%     4   49.8  51.1  49.6  55.1   2.7
  2. AS400304 104.36.84.1     0.0%     4   72.7  67.9  59.6  73.7   6.6
  3. AS400304 23.147.152.14   0.0%     4   49.9  50.1  49.9  50.6   0.3
  4. AS???    206.81.81.10    0.0%     4   61.6  63.4  61.6  68.3   3.3
  5. AS13335  172.71.144.3    0.0%     4   61.5  61.9  61.5  62.4   0.4
  6. AS13335  1.1.1.1         0.0%     4   61.3  61.2  61.1  61.4   0.1
```

Likewise, we can see the RTT of the last hop increased by 50ms, which corresponds to the IPv6 RTT between server A and server B.
There is an additional "hop 1", which has an IPv4 address that is technically globally routable but does not belong to us.

## Speedtest Reports

I also tested TCP and UDP speeds over the same environment.

Before tunnel setup, from server A to a private iperf3 server over IPv4:

* TCP upload: 290 Mbps
* TCP download: 362 Mbps
* UDP upload: 783 Mbps
* UDP download: 686 Mbps

After tunnel setup, from server B to a private iperf3 server over IPv4:

* TCP upload: 46.6 Mbps
* TCP download: 30.9 Mbps
* UDP upload: 407 Mbps
* UDP download: 304 Mbps

From server B to server A, over IPv6:

* TCP upload: 375 Mbps
* TCP download: 263 Mbps
* UDP upload: 317 Mbps
* UDP download: 590 Mbps

We can see that there's considerable speed reduction in TCP traffic.
There are three possible reasons:

* Both plain IPv4 flow and tunneled IPv4 flow are competing for bandwidth at server A.
* Server A must re-fragment the TCP segments, because the tunnel interface has a smaller MTU than the `eth0` "real" interface.
* TCP offload features including Large Receive Offload (LRO) and TCP Segmentation Offload (TSO) provided by the virtio network interface driver are ineffective on the tunnel interface of both servers, which means the kernel and `iperf3` application must handle more packets.

## Conclusion

This article explores the method of moving an IPv4 address from a dual-stack KVM server to an IPv6-only KVM server.
A successful way to achieve this goal is establishing an ip6gre tunnel and configuring IPv4 routing.
It then compares the traceroute and iperf3 results before and after tunnel setup.
