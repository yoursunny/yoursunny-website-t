---
title: 华盛顿周边一日游 2023-10-15 阿灵顿现代美术馆
lang: zh
date: 2023-12-17
tags:
- travel
- Virginia
discuss:
- https://shuiyuan.sjtu.edu.cn/t/topic/229126/4?u=yoursunny
image: RecliningLiberty.jpg
---

这是[纽约归来](/p/2023/NewYork-vacation/)后的第一次周边游。
我决定去位于弗吉尼亚州阿灵顿县的阿灵顿现代美术馆 [Museum of Contemporary Art Arlington](https://mocaarlington.org/)。
中午 12 点出发，搭乘公交、地铁 2 小时到达 Virginia Square 站，出站先喝一杯星巴克，然后过两条马路就可以找到这家美术馆。

我选择这个景点的原因是，那里有她：

![Reclining Liberty by Zaq Landsberg](RecliningLiberty.jpg)

咦，纽约的自由女神像怎么跑到阿灵顿来了，而且躺下了？
原来，这是 Zaq Landsberg 的作品 [Reclining Liberty](https://mocaarlington.org/exhibits/2023/zaq-landsberg-reclining-liberty/)。
艺术家将亚洲的“卧佛”与美国的标志自由女神像相结合，创作了这尊雕塑。
这尊雕塑曾在纽约曼哈顿、新泽西州前往自由女神像的轮渡码头等处展出，而现在来到了阿灵顿县。

![Tomato by Elliot Doughtie](Tomato.jpg)

美术馆里还有多件让人难以理解的展品，比如一组金属管道组成的[番茄](https://mocaarlington.org/exhibits/2023/elliot-doughtie-tomato/)、一张带有脚丫与[珍珠](https://mocaarlington.org/exhibits/2023/marissa-long-blister-pearl/)的桌子。

![Blister Pearl by Marissa Long](BlisterPearl)

美术馆参观完毕，我步行 1 小时寻了几个地谜藏宝，就到了傍晚 17 点可以吃晚餐了。
这天天气比较冷，所以我选择了[泰国餐馆](https://www.sawatdeeva.com/)的热菜。
可是因为菜比较油腻，所以我一吃就肚子不舒服。
看着盘子休息了 20 分钟，身体在空调的作用下渐渐热了起来，才吃完了饭。

![Pad-Pew-Waan at Sawatdee Thai Restaurant](SawatdeeThai.jpg)

花费：地铁公交 5 元，门票免费，咖啡 200⭐，晚餐 20 元。
