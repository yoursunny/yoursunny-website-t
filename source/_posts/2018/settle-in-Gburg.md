---
title: Settling in Gaithersburg
lang: en
date: 2018-02-14
tags:
- life
- Maryland
alias: 2018/OrchardPlace-1/
image: SpringRidge.jpg
---

I moved to Gaithersburg Maryland to start a research job at [National Institute of Standards and Technology (NIST)](https://www.nist.gov/people/junxiao-shi).
As I exited Washington Union Station on Oct 26, 2017 after enjoying [an epic 4-day Amtrak train journey](/t/2017/cross-country-move/), I felt lost immediately: this is no longer Tucson, the toasty city that I know and love, but a completely different place.
Nevertheless, Cortana's voice guided me along Interstate 270, and my rental car arrived at Extended Stay America Gaithersburg South, where I was greeted by James and received a spacious and comfortable room.

## Apartment Search

Before I [first came to Tucson](/t/2017/first-days-in-Tucson/), I signed up for a student housing apartment online.
Unlike [six years ago](/t/2017/six-years-in-arizona/), I did not select an apartment before traveling to Maryland.
Gaithersburg is not a college town, and I am no longer a student, so that I couldn't get a fairly straightforward "student housing" option.
Instead, I am facing a much larger and complex apartment rental market.
I want to see the apartments myself before committing to sign a lease.

Oct 26 is a Thursday, so there was only one weekday remaining before the weekend when most leasing offices would be closed.
I browsed online apartment listings, and selected four apartment complexes to visit on Friday.
I still wanted to [avoid owning a vehicle](/t/2017/behind-the-wheel/), so location was the most important factor in my consideration.
I also preferred a furnished apartment, to avoid the hassle of buying and selling furniture.

![Spring Ridge apartments model room](SpringRidge.jpg)

NIST is Gaithersburg's largest employer, and there are many apartment complexes built around NIST's campus.
There are a row of apartment buildings along N Summit Ave, and I can walk cross the street to visit the other leasing office without moving the car.
However, a *furnished* apartment is much harder to get by.
Most apartment complexes do not offer furniture at all.
The handful of apartments that have "furnishings available" would charge a monthly furniture rental fee starting at $300, on top of their $1200+ monthly rent.
Although I understand I would not be able to get a room at Arizona's $535 price tag, spending $1500 on housing is "a little too much".

**Diamond Square Apartments** stand out from all others: they offer furnished rooms at no additional cost, and the monthly rent is less than $900.
Jasmine the manager showed me the room: it is smaller than those nicer apartments, has limited kitchen appliances, but includes a basic set of furniture.
Returning to the leasing office, she then told me that the available rooms were part of an affordable housing program, and my income is too high to qualify for this program.
The monthly rent for me would be $986, and I would have to wait about two months before a room at this price tier would become available.
As the Chinese proverb goes, "远水解不了近渴" (*distant water cannot quench present thirst*), I cannot count on Diamond Square Apartments as I needed an apartment immediately.

## IKEA the Meatball Store

The nice lady at Summit Crest apartments suggested me to look into cheap furniture from IKEA: "you can pay $300 once and own all the furniture".
Cortana brought me to IKEA that night.

It was my first time at an IKEA store, so I did not know how it works.
I made my way into the main hall, where about 20 pieces of furniture are hung up between huge shelves of boxes.
I was confused: why does IKEA the world's most famous furniture store have so few selections?
After inquiring a team member, I learned that all their selections are upstairs in the "showroom", and I can write down the product numbers and then grab the boxes from the shelves.

![IKEA main hall](IKEA.jpg)

I headed upstairs and browsed the furniture showroom.
There are thousands of choices.
I estimated that my minimal set would cost approximately $600:

* twin bed: $150
* mattress: $100
* dresser: $150
* standing desk: $150
* bar stool: $50

![IKEA meatballs](meatball.jpg)

As dinner time neared, I ate their famous Swedish meatballs that was surprisingly cheap.
[IKEA is a meatball store](https://www.iheart.com/podcast/105-part-time-genius-28198159/episode/why-is-ikea-obsessed-with-building-30229457/) after all.
I did not buy any furniture that day, but I became less concerned on unfurnished apartments after this visit.

## Room Rentals

The invitation letter from NIST includes a link to [GuestResearcher.org](http://guestresearcher.org/forum/adverts.php), an online forum for foreign guest researchers.
The classifieds section of this forum lists advertisements of housing, furniture, and cars.
Those posts come from local homeowners that have spare rooms, or fellow guest researchers that want roommates to share a large house.
Although I am [wary about having roommates](/t/2017/roommates-or-not/), I decided to give them a try.
I sent a few emails on Friday night, and made appointments to visit two houses on Saturday.

My first stop was [Ms Groves's house](https://web.archive.org/web/20180214010940/http://guestresearcher.org/forum/ads_item.php?id=5424), located in the DuFief neighborhood, "just 10 minutes from NIST".
Ms Groves is a map artist retired from *National Geographic*.
She has a lot of stories to tell, and we chatted pleasantly for one hour.
However, a major drawback of this house is that it is located deep inside a large neighborhood.
According to Google Maps, it would be a 20-minute walk to get out of the neighborhood and reach the bus stop.
I couldn't imagine how to survive this walk on a snowy day.

After lunch, I proceeded to my second stop, [Mike's condo](https://web.archive.org/web/20180214012716/http://guestresearcher.org/forum/ads_item.php?id=5328).
Its location is better, but the room had no furniture other than an airbed.
Although I could write blogs on the floor in a plank position, I wouldn't pile all my clothes on the carpet.
I also do not want to buy non-edible meatball from IKEA, because they are going to cause more trouble down the road.

Failing to find an ideal room, I returned to the hotel, feeling concerned: if I could not find a place to move, I would have to pay $60 per day to stay at the hotel, and going to office would be time-consuming because there isn't a direct bus route.
I checked the forum again, and found a new post titled "[10 mins walk to NIST](https://web.archive.org/web/20180214010301/http://guestresearcher.org/forum/ads_item.php?id=5402)".
I contacted Monica, author of that post, right away and made an appointment at Sunday 17:00.

## Six Homeless Hours

Check-out time for the hotel is 11:00, six hours before my appointment with Monica.
I packed everything into the car, dropped off my room key, and said goodbye to James.
At this moment, I officially became homeless.

I stayed in the hotel parking lot for an hour, leeching on the hotel WiFi to figure out where I could go to kill the afternoon.
I also walked to the gas station nextdoor, and approached a police officer to ask questions about Maryland traffic laws.
Finally, I drove to Rockville and spent the remaining hours visiting [Stonestreet Museum of 19th Century Medicine](http://montgomeryhistory.org/stonestreet-museum-of-19th-century-medicine/).

## Move into Orchard Place

Shortly before 17:00, I came to Monica's address, located in **Orchard Place** neighborhood.
Mr Argoti let me in, and showed me the bedroom.
It is a small room, but it has a private bathroom, includes complete furniture, and even contains a small fridge and microwave.
Since I enjoy cooking, I paid attention to the shared kitchen, and confirmed how often I am permitted to use the kitchen.
I was happy with the room and the common areas.

![my bedroom in Mr Argoti's house](OrchardPlace.jpg)

Signing the contract was not so straightforward.
Mr Argoti and his family are Colombians, and their native language is Spanish.
Although they know some English, they were unable to understand my complex questions about contract terms.
Their daughter Monica, who posted the advertisement, helped with translation via video calling.
It took a while to address my concerns and make necessary amendments.

Then, the lease was signed, I wrote a check for payment, and Mr Argoti provided me a key to the house.
I moved in right away.
That night, I slept well because I was relieved to be settled in the new city, after four days on the train and three days searching for housing.

[To be continued](/t/2018/OrchardPlace/).
