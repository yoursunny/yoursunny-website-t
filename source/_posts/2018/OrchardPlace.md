---
title: Orchard Place
lang: en
date: 2018-02-24
tags:
- life
- Maryland
alias: 2018/OrchardPlace-2/
image: bedroom.jpg
---

I [moved into Mr Argoti's house](/t/2018/settle-in-Gburg/) on the afternoon of Oct 29, 2017, and became a resident of Orchard Place neighborhood.

## The Tiny Bedroom

My assigned bedroom is incredibly small.
There are five pieces of furniture: a BED that later turns out to be a sofa, a four-drawer DRESSER, a big glass DESK, a nice office CHAIR, and a small wooden end TABLE.
My room also includes a small FRIDGE and a MICROWave.
The following diagram shows their placement.

```text
+--------------------------------------E------------------+-------------------------+
| [HEAT]            +-----------------------------------+ |   =====   |             |
| +-------+         |                                   | |  | TOI |  |   SHOWER    |
| |       |         |                                   | |  | LET |  |             |
| |DRESSER|         |                BED                | |   \---/    \____________|
| |       |         |                                   | |                  |      M
| |       |         |                                   | |S                 | SINK I
E +-------+         +-----------------------------------+ | TC               |      R
|                                                         +D            /-+------E--+
W                                                                  /DOOR            |
I                                 |CHAIR|                      DOOR         CLOSET  |
N                                 \-----/                                           |
| +--------+ +-----+    +----------------------+                      DOOR+---------+
| | FRIDGE | |     |    |         DESK         |                          |
| | MICROW | |TABLE|    |                      |             DOOR         E
| +--------+ +-----+    +----------------------+                 \DOOR-\  |
+------------------------------------------------------S--D             \-+

<--- SOUTH            E=electric outlet  S=light switch
```

The existing furniture took up more than half of the floor space, so that everything I have must fit into the other half.
When I [moved from Arizona](/t/2017/cross-country-move/), I had four roller bags, two small bags, and a large box of cookware.
Half of my clothes already filled the dresser and the space on top of it, so the other half had to stay in the luggage.
The desk obviously belonged to my computers and other gadgets.
I managed to squeeze the cookware box and a smaller roller bag into the tiny CLOSET.
One of my roller bags could fit under the bed by opening it to two halves.
The last two roller bags had to stay in the open in front of the WINdow.
After settling all my belongings, I only have an open "hallway" between the desk and the bed, just enough for doing a few push-ups.

![bedroom, looking south](bedroom.jpg)

The bathroom is even smaller.
Equipment is pretty standard: a TOILET, a SINK with MIRror, a glass SHOWER enclosure, and a Trash Can.
It does not have a towel rack, and does not have a clothesline.
I had to put towels on top of the glass shower enclosure, and hang wet clothes on the sliding shower door and move them into the closet when they are dry.

## Cooking

Being able to cook is a major difference between a short term hotel accommodation and a long term living arrangement.
The next day after moving in, I went on a shopping trip to gather everything I needed for cooking.
I bought a [rice cooker](https://amzn.to/2BNCq45), some brown rice, a box of ground turkey, a bag of frozen green beans, and a bottle of hot sauce (aka Tucsonan's favorite spice).

![kitchen shelf](kitchen-shelf.jpg)

As I brought my pots and pans downstairs, Mrs Argoti immediately allocated four shelves of kitchen storage space to me.
I was glad that I wouldn't have to keep these cookware in my tiny bedroom.

![stream pot on the stove](stove.jpg)

In the next months, I cooked three times per week.
Each cooking session prepared enough food for two or three meals.
My food smelled so nice that Tyson the family dog followed me everywhere in the house, but I would not give him any of my expensive proteins.

![Tyson in the kitchen](Tyson.jpg)

## Walk, Walk, Walk

Monica advertised this house as "[10 mins walk to NIST](https://web.archive.org/web/20180214010301/http://guestresearcher.org/forum/ads_item.php?id=5402)".
It is an [alternate fact](https://en.wikipedia.org/wiki/Alternative_facts).
The first time, it took me 30 minutes to reach NIST's main gate, and another 20 minutes to get into my office.
I started trying different routes to find the shortest path.

My "forwarding strategy" located a 15-minutes route after two weeks, and I've been following this route ever since.
This route involves crossing a grassy lawn, trespassing through a church parking lot, and walking next to speeding cars on a section of roadway that does not have a sidewalk.
The grassy lawn made my feet muddy every time it rained; the trespassing was illegal but god is forgiving; the walk-in-motor-lane thing is the worst that I could be killed (my office mate estimated 1% probability).

![walking dangerously](walk.jpg)

## Moving Out

Orchard Place is a short term solution for me, as I have known since the first day.
The room is too small, the walk is too dangerous, and the house is too noisy on weekends.
As soon as a unit in Diamond Square Apartments opened up, I set my moving date.
On Jan 28, 2018, I said goodbye to Mr Argoti, and started living in a big apartment all to myself.

Looking back, I am grateful that Mr Argoti gave me a place to stay when I needed it the most.
It was a fun experience to rent a room in an owner-occupied house.
Though chatting, I got to know their family, their dogs, their profession, their home country.
I also met three other housemates, all of which are NIST guest researchers on 3-month programs.

If I ever move to another city, I would be more open-minded and consider renting a room in an owner-occupied house again.
