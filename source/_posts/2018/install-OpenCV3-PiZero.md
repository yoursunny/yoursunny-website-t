---
title: Install OpenCV 3.2.0 on Raspberry Pi Zero W in 15 Minutes
lang: en
date: 2018-03-12
edited: 2021-07-17
tags:
- RaspberryPi
- OpenCV
---

**OpenCV**, or [Open Source Computer Vision Library](https://opencv.org/), is an open source computer vision and machine learning software.
It works on Raspberry Pi computers, and can process photos captured by the Raspberry Pi Camera Module.

OpenCV has two supported versions: 2.4.x and 3.x.
New features are being added to 3.x branch, while 2.4.x only [receives bug fixes and efficiency fixes](http://answers.opencv.org/question/92583/why-there-are-two-versions-of-opencv-3x-and-24xx/).
It is recommended that new developments should use OpenCV 3.x, to take advantage of new features.
However, the official operating system for the Raspberry Pi, Raspbian Stretch, [comes with OpenCV 2.4.9](https://packages.debian.org/stretch/python-opencv).
While I am not yet familiar with OpenCV algorithms, one thing notably missing from OpenCV 2.4.9 is a Python 3 binding.

I wanted to have OpenCV 3 running in Raspbian Stretch on a Raspberry Pi Zero W.
Unable to find existing packages for Pi Zero and Stretch, I had no choice but to compile my own OpenCV 3.
I decided to do it the proper way: [build a backported Debian package](https://wiki.debian.org/DontBreakDebian#Backported_packages).
This method is superior to installing from the source code, because the packages can easily be deployed to other Raspberry Pi Zero W computers.
I followed the [Simple Backport Creation](https://wiki.debian.org/SimpleBackportCreation) instruction, and [spent a week building the packages](/t/2018/build-OpenCV3/).
Now I'm sharing my compiled packages, so that you can use them if you dare.

## What You Need

* [Raspberry Pi Zero W](https://amzn.to/2DjtzEz) running Raspbian Stretch
* [Raspberry Pi Camera Module](https://amzn.to/2tx14Ug) (optional)

The packages are built for ARMv6 architecture used in Raspberry Pi Zero W, and depends on Raspbian Stretch libraries.
They are not compatible with Raspberry Pi 2/3 that has a different CPU architecture.
They will not work on Raspbian Jessie/Buster or Ubuntu systems.

## How to Install OpenCV 3.2 to Raspberry Pi Zero W with Raspbian Stretch

**UPDATE 2020-07-25**:
This repository is only for Raspbian "stretch".
If you have Raspberry Pi OS "buster", do not add my repository.
You can start from step 2 to install OpenCV 3 from default APT sources.

**UPDATE 2021-07-17**:
Bintray has ended, taking away my APT repository.
If you have Raspberry Pi OS "buster" or newer, you can start from step 2 to install OpenCV 3 from default APT sources.

1. ~~Enable my [Pi Zero repository](https://dl.bintray.com/yoursunny/PiZero/):~~

   <details>
   <summary>You don't need this step on Raspberry Pi OS "buster" or newer.</summary>
   <pre><code>
   echo 'deb [trusted=yes] http://dl.bintray.com/yoursunny/PiZero stretch-backports main' |\
   sudo tee /etc/apt/sources.list.d/bintray-yoursunny-PiZero.list
   </code></pre>
   </details>

2. Update package lists:

   ```shell
   sudo apt update
   ```

3. Install OpenCV package:

   ```shell
   sudo apt install python3-opencv
   ```

   Depending on the speed of your Internet connection, this may take between 15 and 30 minutes.

4. Verify the installation:

   ```shell
   python3 -c 'import cv2; print(cv2.__version__)'
   ```

   It'll take around 30 seconds, and then you should see:

   ```text
   3.2.0
   ```

## How were the Packages Built

Short version: They were [built as a private backport](https://wiki.debian.org/SimpleBackportCreation) from [Debian Buster's opencv package](https://packages.debian.org/source/buster/opencv).

Long version: [read about my quest](/t/2018/build-OpenCV3/).
