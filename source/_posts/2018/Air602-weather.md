---
title: Air602 Weather Indicator
lang: en
date: 2018-12-28
tags:
- Air602
image: schematics.png
---

Air602 is a 2.4GHz Wi-Fi module based on [Winner Micro W600](http://w600.chip.haus/) system-on-chip.
Being a new release, it has limited software support.
It took me more than a day to [make it blink](/t/2018/Air602-blink/).

Naturally, the next step is to connect a sensor.
I decide to reproduce the [ESP8266 weather indicator](/t/2018/HTU21D-TM1637/) but with Air602 instead.

## Air602 Board Pinout

The [Air602 development board](https://www.seeedstudio.com/Air602-WiFi-Development-Board-p-3140.html), labelled `EVB_W602_A11`, has six pins:

Air602 pin | W600 pin
-----------|---------
IO         | PB08
CTS        | PB09
RTS        | PB10
RX1        | PB11
TX1        | PB12
GND        | GND

![Air602 development board, obverse and reverse](board.jpg)

Despite that the serial port labeling, W600 SDK supports pin multiplexing.
For example, PB11 and PB12 can be used for I2C, as [one of five valid choices](https://github.com/w600/sdk/blob/f4815e7376620407e843756559ca297d289357c7/include/driver/wm_gpio_afsel.h#L333-L357).
Otherwise, every pin is usable as GPIO, as I was able to [blink a row of LEDs](https://twitter.com/yoursunny/status/1076619241455931392).

### Where's the Power?

Notably, **Air602 has no POWER pin**.
This is a major obstacle in connecting external devices to the Air602.

TM1637 needs 5V power, same voltage as USB.
To obtain 5V power, I looked up [USB connector pinout](http://www.hobbytronics.co.uk/usb-connector-pinout), and soldered a male pin header to the 5V line of the USB connector.
A drawback to this approach is that the added solder ball shortens the USB plug, so that the plug would be loose on some USB ports.

![Air602 with pin headers as well as a pin on the USB connector](headers.jpg)

HTU21D is a 3.3V sensor.
[Adafruit HTU21D sensor breakout](https://amzn.to/2SmDAcc) is 5V-compatible, but I cheaped out and bought a [generic HTU21D](https://amzn.to/2ODC9UD) that does not have a voltage regulator, so I have to come up with a way to supply 3.3V.
[Air602 hardware design manual](https://github.com/SeeedDocument/Air602_WiFi_Module/raw/master/res/Air602%20Hardware%20Design%20Manual.pdf) says that pin 4 of the 12-pin square board is 3.3V power.
However, hand-soldering a pin header or a wire onto this position would be difficult.
I chose an easier way: using a GPIO.
HTU21D's max current consumption is 500μA when measuring, which is much lower than W600's max drive of 24mA.
Therefore, I can safely supply power to HTU21D from a GPIO pin.

## Hardware Assembly

Bill of materials:

* Seeed Studio's [Air602 development board](https://www.seeedstudio.com/Air602-WiFi-Development-Board-p-3140.html)
* [HTU21D sensor breakout](https://amzn.to/2ODC9UD)
* [RobotDyn 4-digit 7-segment LED display](https://amzn.to/2IRla1Y), based on TM1637 controller
* pin headers, resistors (if necessary), breadboard, jumper wires

Wiring:

![schematics of Air602 weather indicator](schematics.png)

## Device Drivers

W600, being a new chip, does not have an Arduino core.
This means, all my favorite Arduino libraries aren't going to work on Air602.
I have to translate everything to fit [W600's C SDK](https://github.com/w600/sdk).

**TM1637 driver** was translated uneventfully.
The [TM1637 Arduino library](https://github.com/avishorp/TM1637/) controls the LED display by bit-banging on two wires, and I did the same.
One challenge is that I can't find an equivalent of `delayMicroseconds` in the C SDK, but I made a for loop and it seems to be working:

```c
  for (int i = 0; i < 0x03FF; ++i) {
    *((volatile uint32_t*)0xE0000000);
  }
```

**HTU21D driver** is more complicated.
The [HTU21D-F Arduino library](https://github.com/adafruit/Adafruit_HTU21DF_Library/) uses Arduino's I2C library that works at a higher level than W600's [`wm_i2c.h`](https://github.com/w600/sdk/blob/f4815e7376620407e843756559ca297d289357c7/include/driver/wm_i2c.h) APIs.
I had to read [I2C tutorial](http://www.circuitbasics.com/basics-of-the-i2c-communication-protocol/) to understand things.

My initial translation fails at READREG step.
In Adafruit's library, [`Adafruit_HTU21DF::begin()` function](https://github.com/adafruit/Adafruit_HTU21DF_Library/blob/94c0af1c32b5257b3c93c0e0cacff6b016184a2d/Adafruit_HTU21DF.cpp#L39-L50) has this code:

```cpp
Wire.beginTransmission(HTU21DF_I2CADDR);
Wire.write(HTU21DF_READREG);
Wire.endTransmission();
Wire.requestFrom(HTU21DF_I2CADDR, 1);
return (Wire.read() == 0x2); // after reset should be 0x2
```

Translated to W600 C SDK, it is:

```c
tls_i2c_write_byte((HTU21DF_I2CADDR << 1), true); // write address 0x80
tls_i2c_wait_ack();
tls_i2c_write_byte(HTU21DF_READREG, false); // command 0xE7
tls_i2c_wait_ack();
tls_i2c_stop();

// tls_os_time_delay(1);
tls_i2c_write_byte((HTU21DF_I2CADDR << 1) | 0x01, true); // read address 0x81
tls_i2c_wait_ack();
uint8_t reg = tls_i2c_read_byte(false, false);
tls_i2c_stop();
```

However, the `reg` value I got is either `0x00` or `0xFF`, not the expected `0x02`.
Even if I ignore this error and proceed to read the temperature and humidity, the outputs are all wrong.
Clearly there are some signal problems that can only be sorted out with [a logic analyzer](https://amzn.to/2RoFqvO) that I do not have, and the [makerspace](http://rockvillesciencecenter.org/programs/studio-i-makerspace/) isn't open until next year.
So I built a [logic analyzer](https://github.com/aster94/logic-analyzer) to watch the signals:

![I2C signal seen by logic analyzer, bad signal](i2c-bad.png)

Tracing the signal lines, I notice that the I2C read address `0x81` did not appear on the SDA line.
I don't know the root cause, but inserting a 1-tick delay before that line solved the problem.

![I2C signal seen by logic analyzer, good signal](i2c-good.png)

As soon as I disconnect the logic analyzer, the numbers are off again.
I tested a few things, and found that adding a pull-up resistor on the SDA line solves the problem.
I can't choose resistor value because the only one I have is 330Ω, since I cheaped out again and bought an industry supply 100-pack instead of a [resistor kit](https://amzn.to/2RgHlmk).

## Multi-Tasking

I'm surprised to learn that they squeezed an operating system into this tiny W600 chip.
It's not Linux, but [FreeRTOS](https://www.freertos.org/), a real time operating system for microcontrollers.
One of its features is to support multiple tasks.
Therefore, I can perform measurement on HTU21D in one task and manage TM1637 display in another task.
Each task can have its own loop, and I don't have to combine them into a single `loop()` function like Arduino does.

![Air602 weather indicator with HTU21D and TM1637](indicator.gif)

**Source code** of this project is in [GitHub yoursunny/w600 repository weather branch](https://github.com/yoursunny/w600/tree/weather) ([commit 6c03f27](https://github.com/yoursunny/w600/tree/6c03f270baff34fbabe73762c143f83d211d2a19)).
