---
title: Listen to Podcasts
lang: en
date: 2018-08-11
tags:
- life
---

Apartment life involves a lot of chores: cooking, cleaning, folding laundry, etc.
Many American housewives watch TV while doing chores, but I do not have a TV.
The next closest thing is the radio, but I find it boring to listen to the same news over and over.
In Dec 2016, as I scroll through my [iPad Mini](https://amzn.to/2P28HIW), I discovered the best activity to accompany chores: podcasts!

A *podcast* is a series of periodically published digital audio files that a user can download and listen to.
iPad Mini comes with a [Podcasts](https://www.apple.com/itunes/podcasts/discover/) app that allows me to search for, subscribe to, download, and listen to podcasts.

My initial subscriptions include:

*   [Radiolab](https://www.wnycstudios.org/shows/radiolab/), in-depth news stories.
    It's similar to a magazine, but in an audio format.

    My favorite episode is [The Ceremony](https://www.wnycstudios.org/story/ceremony), a report about how [Zcash](https://z.cash) generated their cryptocurrency parameters.

*   [Stranglers](http://stranglers.fm/), true crime stories about the Boston stranglers.
    They kept my eyes wide open at nights.

Listening to podcasts improves the quality of my housekeeping, because I do not rush through the chore so that I can switch to more "enjoyable" activities sooner.
Instead, I can take my time cleaning the bedroom, while enjoying great audio content.
I do have to keep it quiet, so that I can hear what's coming out of the small speaker on the iPad Mini.

I purchased a pair of [Shackle Bluetooth headphones](https://amzn.to/2Ox3Gah) in May 2017.
This technology upgrade allowed me to enjoy podcasts around the apartment without carrying the iPad Mini.
In particular, I can wear the headset into the kitchen, and the audio remains audible in the noise of water tap, microwave, and exhaust fan.
I still have this headset to this day.
Despite some broken buttons, it has better sound than all other headsets I've purchased afterwards.

The other technology change is switching to [Player FM](https://player.fm/) app, an Android app installed on my smartphone instead of tablet.
This allows me to listen to podcasts anywhere, and not limited to the time of doing chores in my apartment.
Since then, I've been listening when walking, [geocaching](/t/2017/start-geocaching/), taking the bus, or [pumping iron in the gym](/t/2017/heavy-iron-stuff/).
**Player FM** has great offline features: it can automatically download unplayed episodes from my subscription when the phone is being charged at night, ready for me to listen the next day.

My current [subscription list](https://player.fm/yoursunny/fm) includes:

*   [TechStuff](https://www.stuffmedia.com/techstuff), how technology works.

    Jonathan Strickland tells in-depth stories about [tech company history](https://www.stuffmedia.com/techstuff/the-intel-story-part-one.htm), [how technology works](https://www.stuffmedia.com/techstuff/how-loran-worked.htm), and [tech policy updates](https://www.stuffmedia.com/techstuff/what-exactly-is-gdpr.htm).

*   [Planet Money](https://www.npr.org/sections/money/127413729/planet-money/), how economy works.

    They tackle seemingly small economy issues such as [cheap shipping from Chinese sellers](https://www.npr.org/sections/money/2018/08/01/634737852/episode-857-the-postal-illuminati), [sand on the beach](https://www.npr.org/sections/money/2018/07/13/628894815/episode-853-peak-sand), or [a pack of black peppers](https://www.npr.org/sections/money/2018/05/16/611732499/episode-696-class-action), and reveal the big impact behind them.

*   [How I Built This](https://www.npr.org/podcasts/510313/how-i-built-this), how business works.

    Guy Raz invites the founder of a successful startup company to talk about how the company was the launched.
    This allows me to learn some business, in case I want to launch a company someday.

*   [The Inquiry](https://www.bbc.co.uk/programmes/p029399x), how the world works.

    In British English, they explain one pressing question in the world politics.

*   [Sidedoor](https://www.si.edu/sidedoor) and [AirSpace](https://airandspace.si.edu/editorial-series/airspace-podcast), how museum exhibits work.

    I have been visiting Smithsonian museums in Washington DC, and this podcast allows me to learn more about the stories behind the exhibits, or to draw me into new exhibits.

*   [AvTalk](https://www.flightradar24.com/blog/category/avtalk-podcast/), how airlines work.

    Ian Petchenik and Jason Rabinowitz talk about the airline industry.
    I am interested in airplanes, so I subscribe to them.

*   [PodCacher](https://www.podcacher.com), how geocaching works.

    Sonny and Sandy talk about the latest news about geocaching, my favorite hobby.
    Once they [mentioned](https://www.podcacher.com/show-612-0-avatar-geocaching-adventure/) that they [found](https://coord.info/GLTHRH3M) a geocache named [Air](https://coord.info/GC2B383), which I immediately recalled that it's [my first time finding a trackable](https://coord.info/GLCQNH0Q).

*   [Ted Talk Daily](https://www.ted.com/about/programs-initiatives/ted-talks/ted-talks-audio), how motivation works.

    Sometimes I need a short audio to fill the gap.

Podcasts keep my updated with the latest news, allow me to learn more knowledge, all without creating stress on my eyes.
They are a great way for entertainment and continued learning.
I'm not stopping anytime soon.
