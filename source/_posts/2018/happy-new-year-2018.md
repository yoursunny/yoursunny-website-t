---
title: Happy New Year 2018 on ESP32 OLED
lang: en
date: 2018-01-01
tags:
- ESP32
image: OLED.jpg
---

I've been playing with ESP8266 for one and a half years now.
Recently, I'm upgrading to its bigger brother, the ESP32.
I purchased two development boards, one of them is the [Heltec WiFi\_Kit\_32](https://amzn.to/2CaCk3X) that comes with a 0.96 inch OLED display.

When 2018 arrives, what's a better way to say Happy New Year than on the ESP32?
Therefore, I crawled out of the bed at 4AM, turned on the computer, and started Arduino IDE.
Half an hour later, I [tweeted this picture](https://twitter.com/yoursunny/status/947760741339336704):

!["Happy New Year 2018" on Heltec WiFi\_Kit\_32 OLED display](OLED.jpg)

The code is simple yet effective.
It was modified from [u8g2 library](https://github.com/olikraus/u8g2/) full\_buffer/HelloWorld example, with the constructor line found on [Robot Zero One](https://robotzero.one/heltec-wifi-kit-32/).

```
#include <U8g2lib.h>

U8G2_SSD1306_128X64_NONAME_F_SW_I2C u8g2(U8G2_R0, 15, 4, 16);

void setup() {
  u8g2.begin();
}

void loop() {
  u8g2.clearBuffer();
  u8g2.setFont(u8g_font_helvB10);
  u8g2.drawStr(0, 20, "Happy New Year");
  u8g2.setFont(u8g_font_helvB24);
  u8g2.drawStr(0, 60, "2018");
  u8g2.sendBuffer();
  delay(1000);
}
```

I can't wait to explore more about ESP32.
