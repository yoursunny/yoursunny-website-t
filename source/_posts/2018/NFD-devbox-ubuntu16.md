---
title: Ubuntu 16.04 NFD Development Machine
lang: en
date: 2018-05-12
tags:
- NDN
- VirtualBox
- Ubuntu
---

I shared [how I setup my NFD development machine](/t/2017/NFD-devbox/) in 2017.
Back then, NFD's minimum system requirement is Ubuntu 14.04 so my virtual machine is 14.04 as well.
In May 2018, ndn-cxx [started requiring Ubuntu 16.04](https://github.com/named-data/ndn-cxx/commit/844b09331bfca9cd06a96775e39bee8225cb2220), so it's time for a rebuild.

## Vagrantfile for NFD Development in Ubuntu 16.04

Here's my new **Vagrantfile**:

```ruby
$vmname = "devbox"
$sshhostport = 2222

$deps = <<SCRIPT
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get dist-upgrade -yq
apt-get install -yq git build-essential gdb valgrind libssl-dev libsqlite3-dev libboost-all-dev pkg-config libpcap-dev doxygen graphviz python-sphinx python-pip
pip install sphinxcontrib-doxylink sphinxcontrib-googleanalytics
SCRIPT

Vagrant.configure(2) do |config|
  config.vm.box = "bento/ubuntu-16.04"
  config.vm.network :forwarded_port, guest: 22, host: $sshhostport, id: "ssh"
  config.vm.provider "virtualbox" do |vb|
    vb.name = $vmname
    vb.memory = 6144
    vb.cpus = 8
  end
  config.vm.provision "deps", type: "shell", inline: $deps
  config.vm.provision "hostname", type: "shell", inline: "echo " + $vmname + " > /etc/hostname; hostname " + $vmname
  config.vm.provision "sshpvtkey", type: "file", source: "~/.ssh/id_rsa", destination: ".ssh/id_rsa"
  config.vm.provision "sshpubkey", type: "file", source: "~/.ssh/id_rsa.pub", destination: ".ssh/id_rsa.pub"
  config.vm.provision "sshauth", type: "shell", inline: "cd .ssh; cat id_rsa.pub >> authorized_keys"
  config.vm.provision "gitconfig", type: "file", source: "~/.gitconfig", destination: ".gitconfig"
end
```
<!--</script>-->

## Differences from 2017

The base image is Ubuntu 16.04.
Rumor goes that the official Ubuntu Cloud Image has numerous weirdnesses, so I choose [bento/ubuntu-16.04](https://app.vagrantup.com/bento/boxes/ubuntu-16.04) as the base.

CPU and memory allocations are increased.
GCC 5 is slower than GCC 4, so I have to compensate.

Package installation script gains a `export DEBIAN_FRONTEND=noninteractive` line.
This stops `apt-get` from asking questions while it's building the machine.

`libcrypto++-dev` dependency has been [killed with fire](https://redmine.named-data.net/issues/3946).

## Ending Words

[Tricks and scripts in the previous post](/t/2017/NFD-devbox/) still work in the new box!
Also, Nick wrote a [NFD Contributor Guide](https://github.com/named-data/NFD/blob/master/CONTRIBUTING.md) that is worth a read.
