---
title: Loud Pumpkin with Circuit Playground
lang: en
date: 2018-10-27
tags:
- Arduino
---

It's October.
Pumpkins are filling the grocery stores.
This means one thing: Halloween is coming!

I'm told that I'm supposed to wear a costume on Halloween.
However, I'm too broke to buy an outfit for just one day, so I'm going to wear a colorful circuit board instead.
This year, I'm going to become a pumpkin.
Well, sorta.

<iframe width="560" height="315" src="https://www.youtube.com/embed/L903Uq71Z28" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

The board is an [Adafruit Circuit Playground Express](https://www.adafruit.com/product/3333).
It packs 10 NeoPixel LEDs that can display any color, four sensors, and a few buttons.
My code uses the microphone sensor to measure how loud is the environment.
Based on measured sound pressure, it then lights up a number of NeoPixels to #ff7619 the "pumpkin color".

```arduino
#include <Adafruit_CircuitPlayground.h>

void setup() {
  CircuitPlayground.begin();
}

void loop() {
  float sp = CircuitPlayground.mic.soundPressureLevel(100);
  int peak = map(sp, 56, 90, 0, 9);

  CircuitPlayground.strip.clear();
  for (int i = 0; i <= peak; ++i) {
    CircuitPlayground.strip.setPixelColor(i, 0xff7619);
  }
  CircuitPlayground.strip.show();
}
```
