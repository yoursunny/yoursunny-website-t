---
title: PO Box 3943
lang: en
date: 2018-03-10
tags:
- life
- Maryland
image: boxes.jpg
---

PO Box, or post-office box, is a [uniquely addressable lockable box](https://en.wikipedia.org/w/index.php?title=Post-office_box&oldid=828105158) located on the premises of a post office station.
There is a wall of PO Boxes in the University of Arizona's Student Union Memorial Center, where it costs $25 to rent one for a semester.
I found them interesting, although I didn't use one because I could receive mail in my apartment, and [the Tucson heat](/t/2017/heat-and-monsoon/) did a good job to keep the grounds dry so I didn't have to worry about packages.

When I first came to Maryland, I [rented a room in Orchard Place neighborhood](/t/2018/OrchardPlace/).
It rains often, and the house does not have a porch.
Seeing the post office right outside the neighborhood, I thought it would be the perfect chance to try a PO Box.

## Opening My PO Box

Rental fee for a small PO Box at Diamond Farms post office is $54 for six months, or $30 for three month.
I would live at Orchard Place for two or three months, so the 3-month option seems more reasonable.
However, it is cheaper to choose the 6-month option, because the post office offers a ½ refund if I close the PO Box within three months, so the final price is only $27.

PO Boxes at this location include "premium service": every PO Box comes with a street address, and can accept deliveries from non-postal carriers.
This is an important factor for me, because most items I buy online are not shipped via USPS, and having a street address allows me to receive them at the PO Box.

On Oct 31, I "reserved" a PO Box on [USPS.com](https://poboxes.usps.com/), and completed the payment online.
I was instructed to print a "PS Form 1093" and bring it to the post office, but I don't have a printer and I assumed that the post office would have electronically received my reservation.
I came to the post office with required identification documents, but I was informed that they could not proceed unless I have a printed PS Form 1093.
However, I could pay them directly, and cancel the online reservation with a full refund.
I agreed with this plan.

![a wall of PO Boxes, highlighting PO Box 3943](boxes.jpg)

10 minutes and another $54 payment later, I received my "other address", along with two keys to open it.

> PO Box 3943
> Gaithersburg, MD 20885
> (note: this is not my current address)

## Spreading My New Address

I submitted a Change-of-Address form, redirecting mail from my previous [Sahara Apartments](/t/2017/last-month-in-Tucson/) address to my PO Box.
The clerk commented that it would take "several days" for letters to travel from Arizona to Maryland.

I put up my PO Box address [on my website](https://bitbucket.org/yoursunny/yoursunny-website/commits/91b366b4170b2d20570b440396da5156c1ec4eb3).
I have been open in [publicly displaying my contact information](/m/), but one thing I would keep private is my apartment room number.
PO Box is located in a post office, and I have no concern in making it public.

I also updated my address with various agencies, such as:

* Arizona Motor Vehicle Division, because my [Arizona driver's license](/t/2017/behind-the-wheel/) says I have to report an address change within ten days.
* University of Arizona, in case they want to send me a tax form.
* Banks and other credit card issuers.
* [Amazon](https://amzn.to/2Hd5BgA), Walmart, Target, and other online retailers.
* [NatureBox](https://bitly.com/snackinvite), the subscription service for awesome snacks.

During the next week, I visited the post office daily to open my box.
My box was empty every time, and I anxiously kept waiting for the first letter.
The first letter arrived on Nov 06, from your truly United State Postal Service.
It contains instructions to activate *Informed Delivery* service on my new address.

![a letter from USPS](first-letter.jpg)

Then, letters came from most banks and credit card issuers, alerting me that my address has been changed.
These could have been emails or text messages instead.

## 2-day Boxes from Amazon

I joined [Amazon Prime](https://www.amazon.com/tryprimefree?ref_=assoc_tag_ph_1427739975520&_encoding=UTF8&camp=1789&creative=9325&linkCode=pf4&tag=yoursunny-20&linkId=0b0d5ba911eb40f292d6c75540b7bda5) on Nov 26.
Membership fee is $49 every year, same price as six [movies at the cinema](/t/2017/moviegoer/), while I could watch many more via Amazon Prime Videos with no additional cost.

Then I started buying stuff on Amazon, such as:

* [Xtreme Comforts slim pillow](https://amzn.to/2oVYtys), the best pillow for stomach sleepers.
* [LEPOWER LED light](https://amzn.to/2GdZPvA), [as seen in my bedroom](/t/2018/OrchardPlace/).
* [Brita Longlast water filters](https://amzn.to/2HpiHaD), to make my water crispy clear.
* [Gloves](https://amzn.to/2GbyytK), because Maryland is cold.
* [Heltec WiFi\_Kit\_32](https://amzn.to/2CaCk3X), so that I can [say happy new year](/t/2018/happy-new-year-2018/).

Amazon fulfillment service ships merchandise using a variety of carriers, including USPS, UPS, FedEx, and some unnamed regional carriers whose tracking numbers start with "AMZN".
For any item not shipped via USPS, Amazon would reject my PO Box address and ask for a "physical address".
This is when the "premium service" of my PO Box becomes useful:

> 23 Firstfield Rd Unit 3943
> Gaithersburg, MD 20885
> (note: this is not my current address)

Smaller things, such as the gloves, came in padded envelopes that fit in my PO Box.
Larger items were in boxes much larger than the 4x6 PO Box.
When they arrived, I would receive a "pointer" that looks like:

![a parcel locker key in my PO Box](parcel-locker-key.jpg)

I can dereference this pointer by using this key to open one of ten parcel lockers in the post office.
My package will be securely inside.
So far, no [NullReferenceException](https://msdn.microsoft.com/en-us/library/system.nullreferenceexception.aspx) has occurred.

![two parcels on a table](parcels.jpg)

Occasionally, I would receive a [302 Found](https://tools.ietf.org/html/rfc2616#section-10.3.3) notice instead.

![PS Form 3907, Post Office Box -- Mail Pickup Notice](PS3907.jpg)

I need to present this notice to the counter clerk, who can help retrieving my oversized packages.
In case I see this yellow notice after business hours, I would have to hold my excitement until the next day.

## The B.S. from RXBAR

My shopping spree didn't end at Amazon.
When Twitter presented me an opportunity to try a dozen RXBAR protein bars for $19.98, I took the offer.

I entered my PO Box address and clicked the order button.
There was an unknown error at the first time, but the system accepted my order when I clicked the button for second time.
Order confirmation email came instantly, which indicates the order will be shipped via "UPS Ground".
I promptly replied asking them to use the street address of my PO Box, but they assured me that they'll ship via USPS instead.
A week later, USPS Informed Delivery showed me a scanned post card:

![post card from UPS](UPS-postcard.jpg)

Despite my email request, RXBAR shipped my samples via UPS to my PO Box address!
Now the package was sitting at a UPS store, and I'm not going to spend two hours on the bus to pick up from that store.
Based on my knowledge about UPS, the sender has the power to redirect deliveries if they shipped via a UPS account, which is likely the case with any business.
So I emailed RXBAR again asking them to correct the address, and the protein bars are in the parcel locker five days later.

The bars themselves contain "No B.S." as advertised, but their fulfillment service has 6 grams (weight of a typical post card) of B.S per package.
But hey, [protein is protein no matter the taste](https://twitter.com/yoursunny/status/949420290509025282).

## More B.S. from Barclaycard

[Uber](https://www.uber.com/invite/junxiaos1ui), the [taxi app](/t/2017/hailing-taxis/), offered me a credit card.
The highlight of Uber Visa Card is an unparalleled 4% cashback on restaurants, a category that I spend lots of money monthly, so I applied for this credit card right away.
The application form would not accept my PO Box, so I entered Mr Argoti's house address.

My application remained "pending" for three weeks, so I called up Barclaycard the issuer.
They asked me to photocopy my driver's license and "utility bill or bank statement", and send these to their "customer security department" in a letter.
My bank statements either display my PO Box address, or my previous addresses in Arizona, but they said it's fine.

A month later, my Uber Visa Card application was still "pending", so I called again.
As I expected, they were unable to approve the credit card because the bank statement had a different address than the credit card application.
I had to change the bill address of one of my bank accounts to the physical address, wait for the next statement, and send Barclaycard another letter.

Eventually, this credit card application took three months, six phone calls, and two stamps from me.
Total B.S. amount is estimated to be 120 grams (including "address change notices" from banks).
The lesson is that, applying for a new credit card is probably not a good idea when you have just moved and when you are using a PO Box.

## The End

I received keys to my new apartment on Jan 22, 2018, and filled a Change-of-Address form on the same day.
Upon seeing the form, the "PO Box lady" at the post office informed me that my box would close "automatically" that day as a side-effect of Change-of-Address form.

I claimed a refund the day after.
The clerk was very confused on whether he should pay me or I should pay him, because I shipped a package to an eBay buyer and bought a few stamps during the same visit.
[Americans stink at math](https://qz.com/638845/americans-are-spectacularly-bad-at-answering-even-the-most-basic-math-questions/), even if the computer was assisting.

This marked the end of my less-than-3-month test run with a PO Box.
It's fun.
