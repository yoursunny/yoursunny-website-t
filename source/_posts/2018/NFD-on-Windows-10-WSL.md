---
title: NFD on Windows 10 WSL
lang: en
date: 2018-03-03
tags:
- NDN
- Windows
- Ubuntu
---

The [NDN Forwarding Daemon (NFD)](https://docs.named-data.net/NFD/current/) connects every Ubuntu and Mac OS machine to the Named Data Networking (NDN) testbed network.
While it's awesome to [get your NFD connected](/t/2014/nfd-connect/) from a Linux server or a Macbook, [82.56%](http://gs.statcounter.com/os-market-share/desktop/worldwide/#monthly-201712-201712-bar) of the desktop users running Windows are out of luck.
[Compiling NFD for Windows](https://github.com/yoursunny/NFD-Windows) is possible, but the amount of patches needed is astonishing.

Then the good news came: Microsoft announced [Windows Subsystem for Linux (WSL)](https://learn.microsoft.com/en-us/windows/wsl/about), which lets developers run Linux environments directly on Windows, unmodified, without the overhead of a virtual machine.
Ubuntu is the first Linux distribution supported by WSL.
This means, we can now run NFD natively on Windows!

## How to Install NFD on WSL

This section outlines how to install NDN Forwarding Daemon (NFD) on Windows Subsystem for Linux (WSL).
As of this writing, I have Windows 10 version 1709 (Fall Creators Update), and the latest NFD release is version 0.6.1.

The steps to install NFD on Windows are:

1. Install [Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/install), and download [Ubuntu](https://apps.microsoft.com/store/detail/ubuntu-on-windows/9NBLGGH4MSV6).

2. Within Ubuntu environment, add [ppa:named-data/ppa](https://launchpad.net/~named-data/+archive/ubuntu/ppa), and do a `sudo apt update`.

3. Install NFD and NDN essential tools:

    ```bash
    sudo apt install libndn-cxx-dev nfd ndn-tools
    ```

4. Compile and install [infoedit](https://github.com/NDN-Routing/infoedit), a program for editing NFD configuration files through a script:

    ```bash
    git clone https://github.com/NDN-Routing/infoedit.git
    cd infoedit
    make
    sudo make install
    cd
    ```

5. Edit NFD configuration file.

    ```bash
    # lower Content Store capacity
    sudo infoedit -f /etc/ndn/nfd.conf -s tables.cs_max_packets -v 2048

    # disable WebSockets
    sudo infoedit -f /etc/ndn/nfd.conf -d face_system.websocket
    ```

6. Unlike a real Ubuntu system, there is no *systemd* in WSL environment.
   Therefore, `nfd-start` and `nfd-stop` scripts must be replaced.

    `sudoedit /usr/bin/nfd-start`, delete everything, and replace with the following:

    ```bash
    #!/bin/sh
    sudo pkill -x nfd
    sudo HOME=/var/lib/ndn/nfd sh -c '
        /usr/bin/nfd --config /etc/ndn/nfd.conf 2>/var/log/ndn/nfd.log &
        sleep 2;
        if [ -f /etc/ndn/nfd-init.sh ]; then . /etc/ndn/nfd-init.sh; fi'
    ```

    `sudoedit /usr/bin/nfd-stop`, delete everything, and replace with the following:

    ```bash
    #!/bin/sh
    sudo pkill -x nfd
    ```

7. Setup connection to an NDN testbed router.

    `sudoedit /etc/ndn/nfd-init.sh`, and paste the following:

    ```bash
    ROUTER=udp4://192.0.2.1
    nfdc face create $ROUTER permanent
    nfdc route add / $ROUTER cost 200
    nfdc route add /localhop/nfd $ROUTER cost 200
    ```

    You'll need to change `192.0.2.1` with the IP address or hostname of the nearest NDN router.

8. Insert a Windows firewall rule to allow incoming NDN packets over UDP.

    Open PowerShell with administrator privilege, and execute the following command:

    ```powershell
    New-NetFirewallRule -DisplayName "NDN-UDP" -Direction Inbound -LocalPort 6363 -Protocol UDP -Action Allow
    ```

9. Create a certificate for your user account:

    ```bash
    ndnsec key-gen /$(whoami)
    ```

    Without this, you'll encounter errors in local prefix registrations.

10. Start NFD service:

    ```bash
    nfd-start
    ```

## What Works and What Doesn't

The classical "local `ndnping` and `ndnpingserver`" verification works.
`ndnping` to a testbed router works over both UDP and TCP unicast.

Ethernet faces do not work, because it relies on libpcap, and [WSL does not have AF\_PACKET](https://wpdev.uservoice.com/forums/266908/suggestions/17817685) so even `tcpdump` isn't working.
UDP multicast [does not work over WiFi](https://redmine.named-data.net/issues/4534), because WSL presents WiFi interfaces as "link/ieee802.11", and NFD only recognizes "link/ether".
The only chance for multicast is UDP multicast on a wired interface, but my laptop does not have that.
Thus, I can't type `ndn-autoconfig` and let it find my in-house router.
