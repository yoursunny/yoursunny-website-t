---
title: Air602 Hands On and Blink with C SDK
lang: en
date: 2018-12-15
tags:
- Air602
image: Air602.jpg
---

I hear there's a new WiFi microcontroller that is cheaper than ESP8266's "unbeatable" low price tag.
It is the **Air602**, a 2.4GHz Wi-Fi module based on [Winner Micro W600](http://w600.chip.haus/) system-on-chip.

## Buying an Air602

Currently, Seeed Studio is the only seller for this module.
It comes as a [bare module](https://www.seeedstudio.com/Air602-WiFi-Module-p-3139.html) for $1.90, or a [development board](https://www.seeedstudio.com/Air602-WiFi-Development-Board-p-3140.html) for $2.90.
The bare module lacks an antenna and requires surface mount soldering that I'm not comfortable with, so I purchased the development board version that comes with antenna, power regulator, and USB-UART chip.

![photo of Air602 development board](Air602.jpg)

My order was shipped from Hong Kong, and it took about two months for the order to arrive United States.

## Quick Start with AT Firmware

The original firmware burned into the W600 is known as **AT instructions firmware**.
When I plug in the Air602 development board onto a USB port on my computer, it is recognized as a CH340G USB-UART adapter.
I can open Arduino IDE's Serial Monitor and start typing [AT commands](https://github.com/SeeedDocument/Air602_WiFi_Module/raw/master/res/Air602%20AT%20Command%20User%20Manual.pdf) (note: "line ending" should be set to "Newline").

![screenshot of Air602 WiFi association by AT commands](AT.gif)

To connect Air602 to WiFi, I just need five commands:

1. `AT+WPRT=0`: change to WiFi STA mode.
2. `AT+SSID=`*ssid*: set SSID.
3. `AT+KEY=1,0,`*password*: set WPA2 password.
4. `AT+WJOIN`: start WiFi association.
5. `AT+NIP=0`: run DHCP client to obtain IP address. Note: [Seeed Studio's tutorial](http://wiki.seeedstudio.com/Air602_WiFi_Development_Board/#sta-joins-the-ap-process) suggests typing this command before WJOIN, but it does not work in my test.

Afterwards, I can type `AT+LKSTT` to find out the IP address of this Air602 module, and then ping it from my computer.

## Compiling the C SDK

AT firmware allows the Air602 to act as a WiFi transceiver controlled by another microcontroller, such as the Arduino Uno that does not have any communication by itself.
However, I'm more interested in running the Air602 standalone, with both application and communication on the W600 chip.
To fulfill this goal, I'll need to use the **C SDK**.

Seeed Studio offers a version of C SDK [AirM2M\_W600\_SDK\_20180824.zip](https://github.com/SeeedDocument/Air602_WiFi_Module/blob/master/res/AirM2M_W600_SDK_20180824.zip).
The [tutorial](http://wiki.seeedstudio.com/Air602_Firmware_Programming_Manual/#sdk-download) suggests building the SDK with **Keil MDK5**.
1.2GB of downloading later, I have Keil and its "legacy support" installed, and opened the project.
However, the compilation process does not work!
While the C code seems to be building, it ends with a linker error:

```text
linking...
.\objs\WM_W600.axf: error: L6050U: The code size of this image (295760 bytes) exceeds the maximum allowed for this version of the linker.
Finished: 0 information, 0 warning, 0 error and 1 fatal error messages.
```

It turns out that the Keil MDK5 software I have downloaded is an evaluation version.
It [limits binary code to 32KB](http://www.keil.com/demo/limits.asp).
MDK-Essential version does not have this restriction, but it costs $1535 per year, which is far beyond what I can afford for a tiny project.

Then I found an ["unofficial" W600 SDK from ThingsTurn](https://github.com/w600/sdk).
It supports both Keil compilation (doesn't work due to the same license problem), and GCC compilation.
The **GCC compiler is [gcc-arm-none-eabi](https://packages.ubuntu.com/xenial/gcc-arm-none-eabi)** and it can be easily installed on Ubuntu via APT.
Then, I can build the SDK by typing `make`.
It creates four files under `bin/w600` directory.
`w600.map` contains a list of function names, which I assume is for debugging purpose.
The other three files, `w600.bin`, `w600_gz.img`, and `w600.fls` are binary files.
What are these files, and how to upload them to the Air602 development board?

## Uploading Air602 Firmware

Like most other chips, the W600 chip needs to enter the **bootloader** in order to upload new firmware.
According to [W600 firmware generation guide](http://docs.thingsturn.com/_downloads/W600_%E5%9B%BA%E4%BB%B6%E7%94%9F%E6%88%90%E8%AF%B4%E6%98%8E.pdf), the chip actually has two bootloaders: SECBOOT and ROM.

bootloader | stored in | upgradable | accepts format
-----------|-----------|------------|---------------
SECBOOT    | flash     | via `.fls` | `.img` only
ROM        | hardened  | no         | `.fls` only

**SECBOOT** is a software bootloader that **only accepts `.img` firmware format**.
The binary code of this bootloader is stored in the flash memory.
The `.img` file contains the user application only.

According to [W600 firmware upgrade guide](http://docs.thingsturn.com/_downloads/W600_%E5%9B%BA%E4%BB%B6%E5%8D%87%E7%BA%A7%E6%8C%87%E5%AF%BC.pdf), the steps for entering SECBOOT bootloader are:

1. Connect W600's UART0 or UART1 to a computer, and open serial console.
2. Press and hold ESC key on the computer, and press RESET button on the W600.
3. Release ESC key when "secboot running" appears.

Both [Luat LuaTools](https://v2.fangcloud.com/share/f348cba5a0e3899fd97c57b9f2) and [wm\_tools](https://github.com/w600/wm_tools) can simulate this "press and hold ESC key" process.
However, they only worked *occasionally*.
I found better luck with [download.py](https://github.com/w600/sdk/blob/f4815e7376620407e843756559ca297d289357c7/tools/py_scripts/download.py) on a real Linux computer (i.e. not in Windows Subsystem for Linux).

**ROM** is a hardened bootloader that **only accepts `.fls` firmware format**.
The binary code of this bootloader is burned into the CPU, to ensure that the W600 chip cannot be bricked.
The `.fls` file contains an image of the entire flash memory, including SECBOOT, parameters, and user application.

Entering ROM bootloader is a lot more difficult: it requires grounding pin 13 when powering on the W600 chip.
However, pin 13 is located on the tiny 5mm x 5mm QFN32 package only; it does not have a header pin on the development board or a "stamp hole" on the inner Air602 board.
To ground pin 13, I had to align a jumper wire to the correct trace, and at the same time plug the board into a USB port.
I only had to do it once to recover after [uploading a wrong AT firmware](https://forum.seeedstudio.com/viewtopic.php?p=28790#p28790).

## Blink!

![video of Air602 blinking an external LED](blink.gif)

`make` on the [ThingsTurn SDK](https://github.com/w600/sdk) builds `app/main.c` file, which by default only prints a `"user task"` message.
To blink an LED on the "IO" pin, I need to put the following code in `app/main.c`:

```c
#include "wm_include.h"

void UserMain()
{
  bool isOn = 0;
  tls_gpio_cfg(WM_IO_PB_08, WM_GPIO_DIR_OUTPUT, WM_GPIO_ATTR_FLOATING);

  while (true) {
    tls_gpio_write(WM_IO_PB_08, isOn);
    tls_os_time_delay(HZ);
    isOn = !isOn;
  }
}
```

This code is adapted from [example/blink](https://github.com/w600/sdk/blob/f4815e7376620407e843756559ca297d289357c7/example/blink/user/main.c), with pin number changed to PB08, matching Air602 development board's IO pin.

Afterwards, I can `make`, find a way to upload the firmware, and the board blinks!

In my next post, I [made a weather indicator](/t/2018/Air602-weather/) on Air602.
