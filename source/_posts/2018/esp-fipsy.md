---
title: Program Fipsy FPGA Breakout Board from ESP32
lang: en
date: 2018-07-22
tags:
- ESP32
- FPGA
- Fipsy
image: ESP32-Fipsy.jpg
---

MocoMakers is creating [Fipsy FPGA Breakout Board](https://www.mocomakers.com/fipsy-fpga/), a tiny circuit board offering a piece of Field-programmable gate array (FPGA).
I worked with FPGA [years ago](/t/2007/elevator/) in [class projects](/study/IS209/), but didn't have access to a device after that.
I [backed the project](https://www.kickstarter.com/projects/1013562009/fipsy-the-fpga-breakout-board-for-beginners/), and received two Fipsy boards on Jul 20.

Fipsy is a very simple board: there is no power regulator or USB port.
The official method to program the Fipsy is [through the SPI port on a Raspberry Pi](https://www.mocomakers.com/basic-setup-of-the-fipsy-fpga/).
It is easy to setup, and is a good use case for my [Raspberry Pi Zero W](https://amzn.to/2DjtzEz), but there is one problem:
It is good practice to power off the circuit when modifying hardware wiring.
However, powering off a Raspberry Pi cleanly requires sending a `shutdown` command and waiting for a few seconds.
If I just pull the power cord, I would risk corrupting the filesystem.

ESP32 microcontroller has SPI ports, and can be powered off and restarted very quickly.
Can I program Fipsy from an ESP32?

![Fipsy connected to Heltec WiFi\_Kit\_32](ESP32-Fipsy.jpg)

Hardware side is easy.
ESP32 has two available SPI ports, [HSPI and VSPI](http://esp-idf.readthedocs.io/en/latest/api-reference/peripherals/spi_master.html), and I connected Fipsy to the [Heltec WiFi\_Kit\_32](https://amzn.to/2CaCk3X)'s HSPI port.
All that remains is deciphering the spaghetti code of [the official programmer](https://github.com/MocoMakers/Fipsy-FPGA-edu/blob/5c92287a49163b5af05e66ec773df81054acaf3f/Programmer/RaspberryPi/fipsyloader.c).
After a day of hard work, I got it working:

<iframe width="560" height="315" src="https://www.youtube.com/embed/SLeXRU_YHLQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Code link: [**esp-fipsy** library](https://github.com/yoursunny/esp-fipsy).

There are a few differences from the official programmers:

* My code is clean. I'm obsessed with this.
* I only write the fuse table to the MachXO2-256. By not writing Feature Row and FEABITS, there's less chance of mistakenly disabling slave SPI port and bricking the device.
* JEDEC parsing and FPGA programming are separate.
* Fuse checksum is verified.

References:

* [MachXO2 Programming and Configuration Usage Guide](http://www.latticesemi.com/~/media/LatticeSemi/Documents/ApplicationNotes/MO/MachXO2ProgrammingandConfigurationUsageGuide.pdf)
* [Using User Flash Memory and Hardened Control Functions in MachXO2 Devices Reference Guide](http://www.latticesemi.com/-/media/LatticeSemi/Documents/ApplicationNotes/UZ/UsingUserFlashMemoryandHardenedControlFunctionsinMachXO2DevicesReferenceGuide.pdf)
* [fipsyloader for Raspberry Pi](https://github.com/MocoMakers/Fipsy-FPGA-edu/blob/5c92287a49163b5af05e66ec773df81054acaf3f/Programmer/RaspberryPi/fipsyloader.c)
* [Arduino Fipsy Programmer](https://github.com/MocoMakers/Arduino-Fipsy-Programmer/tree/0690019ef01a7e3a06ca808339d916f7181d9b7d)
* [libFipsy8266](https://github.com/WereCatf/libFipsy8266/tree/1b4ade2ab7a02c9a1ed5eaa6351ca3bb6ed8ea10)
