---
title: Yuma Mega
lang: en
date: 2018-04-12
tags:
- Arizona
- geocaching
- travel
image: Signal.jpg
---

Since my spontaneous visit of Pima Air & Space Museum [on my 2015 birthday](/t/2017/Tucson-museums/), I started a tradition of having a little road trip for every birthday.
I rode a bike to Sweetwater Wetlands Park to see some birds with [Tucson Audubon Society](http://tucsonaudubon.org/) on my 2016 birthday.
When my 2017 birthday came close, I planned something big: I wanted to attend the Yuma Mega, the biggest geocaching event in the Southwest region.

## Finding the Event

I [started geocaching](/t/2017/start-geocaching/) as a hobby in 2013.
Geocaching for me is mostly an individual sport: I [rode bikes](/t/2017/on-two-wheels/) all over Tucson metro area, lift up lamp post covers and poke my hand into guardrails to find mint containers hidden within.
Event Caches, on the other hand, are special geocaches that allow geocachers to gather and socialize.
I browse Geocaching.com's event listing from time to time, and attend those events regularly.
Normally, 15~30 people would show up in a [local restaurant](/t/2017/Tucson-restaurants/) or city park.
People would tell their stories, and plan out-of-state trips to search for large number of geocaches.

Yuma Mega is not just any event, but a "Mega-Event Cache".
Geocaching HQ awards Mega status to events attracting more than 500 geocachers.
I heard about Yuma Mega in 2015, but the date was adjacent to a [conference trip](http://www.caida.org/workshops/ndn/1502/) so I wasn't able to arrange it.
[2017's Yuma Mega event](https://coord.info/GC6CKW2) falls on Sunday Feb 12, which happens to be my birthday.
2017 is also my last year living in Arizona.
It was "now or never", so I have to attend Yuma Mega!

I made up my mind on Nov 24, 2016, and booked a rental car and a motel room for the trip.
Both reservations were cancelable in case there's a [paper deadline](/t/2017/academic-papers/) on that weekend, but thankfully there wasn't one, so I'm greenlighted for the trip.

## Tiny Town and Rough Road

Yuma is located on the west edge of Arizona.
Driving distance from Tucson to Yuma is 241 miles along Interstate 10 and Interstate 8, which takes about three hours.
However, I am [by no means an experienced driver](/t/2017/behind-the-wheel/), so I planned a rest stop every hour.

I carefully selected my rest stops according to the following guidelines:

* Driving time between stops are about 60 minutes, not counting traffic.
* Stops are on the same side where I exit and re-enter the highway.
* There are geocaches near the stops.
* Outbound and return trips should use different stops.

![Feb 11 timeline as recorded by Google Maps](20170211.png)

My first stop is Eloy, AZ.
Near the junction between two major highways, I-10 and I-8, this town has many truck stops.
I took a few pictures, used the restroom, grabbed a geocache, and went on my way within 10 minutes.

The second stop is Gila Bend, AZ.
After parking my car in front of Love's Travel Stop (what a lovely name!), I read that there is a virtual geocache in the town's visitor center, so I walked over, only to find it closed.
The walk took 20 minutes round-trip; I should have driven the car.
I did find another letterbox-hybrid geocache in Gila Bend.
The total time spent in this "rest stop" was over one hour, and I need my lunch soon.

![Love's Travel Stop, Gila Bend AZ](Love.jpg)

The third stop, Tacna, AZ, is my lunch stop.
Tacna is a tiny town on the north side of I-8, with a population of 602.
I ate a lamb shank at [Basque Etchea](https://www.yelp.com/biz/basque-etchea-tacna), one of the only two restaurants in Tacna.
Geocaches of Tacna are mostly located on the south side of I-8.
As I drove over, I saw another car parked right next to where the geocache is.
Are they muggles?
No, they are geocachers, and they are also going to Yuma.

![another geocacher at GC1JMTY](GC1JMTY.jpg)

After another hour of driving, I reached my first destination: [Castle Dome Mining Museum](http://castledomemuseum.org/).
Castle Dome was home to over 300 silver mines, and its population peaked at over 3000 people in 1880.
However, when silver prices dropped in 1979, the last legitimate miners left, and Castle Dome became a ghost town.

![Castle Dome City](CastleDome.jpg)

The museum opened in 1998.
Curators collected everything from the mining era, and housed them in 50 restored buildings sitting on top of one of the old mines.
Unfortunately, they lack the funds to further restore the artifacts, so that the artifacts do not look pretty.
I saw piles of rusty metals resembling vehicle parts, mining tools, and heavy machinery.
One interesting item is a pair of Levi's jeans from 1890:

![1890 Levi's jeans](Levi.jpg)

The only road in and out of Castle Dome is 7 miles of unpaved dirt road.
The road goes across Yuma Proving Ground, a US Army installation.
There was no inspection station, but warning signs indicate that you cannot stop or take pictures while on military grounds.
The road is rough: I can hear tiny stones continuously hitting the vehicle's under carriage.
I was scared driving on this road: if my Buick Impala breaks down, I would not be able to call for help because there's no cellular signal, and I have seen plenty of TV shows about how Army soldiers could come after me.
Fortunately, I made out alive and the car was intact.

![Castle Dome Mine Road](RoughRoad.jpg)

## Motel 6

With the rough road behind, I reached downtown Yuma just before sunset.
I had some tacos at Mariscos Mar Azul for dinner, then I'm ready for [one more geocache](https://coord.info/GC60VCQ).

My overnight stay was booked at Motel 6 Yuma East via Expedia.
There was [a lot of trouble](https://goo.gl/maps/p42H1zJnZGt) during check-in, which wasted me almost one hour.

The next morning, my birthday started with a rainbow:

![rainbow seen at Motel 6 Yuma East](rainbow.jpg)

I drove across the state border to California, to see the [bend of Colorado River](https://coord.info/GC5HHV8).

![Colorado River bend at Yuma](GC5HHV8.jpg)

After that, I'm going back to Arizona for the big event: Yuma Mega Event.

## The Mega Event

Douglas J Nicholls, Yuma's mayor, opened the Yuma Mega Event at 09:00.
This event, after all, is beneficial to Yuma's economy, and tourism industry in particular.

Activities spread across West Wetlands Park.
There was a mega-sized log board:

![Yuma Mega log board](GC6CKW2-log.jpg)

There were vendor booths selling geocache containers:

![geocache containers sold at a vendor booth](geocache-containers.jpg)

There was a display of cool gadget caches:

![a gadget cache made from LEGO bricks](gadget-cache.jpg)

There were presentations about alternative geocaching software, and activities to test your GPS accuracy or CITO (Cache-In Trash-Out) skills.

[Signal the Frog](https://www.geocaching.com/signal/), the official Geocaching mascot, also made a appearance:

![me with Signal the Frog](Signal.jpg)

At the event's massive travel bug exchange booth, I discovered the biggest trackable ever: a frying pan, aptly named ["I hate nanos"](https://coord.info/TB6EERH).

![I hate nanos trackable](TB6EERH.jpg)

I also purchased a [Yuma Mega Event geocoin](https://coord.info/TB847C1) as a souvenir.
If you meet me in person at a geocaching event, chances are I have it with me, and you can ask to see it.

## Wading to California

I stayed at West Wetlands Park for two hours, then I wandered off to discover other tourist attractions of Yuma.
I visited Colorado River State Historic Park and Yuma Territorial Prison State Historic Park.

![guard tower of Yuma Prison](YumaPrison.jpg)

I also tried a new way to cross a state line: wading!
I was standing in California, while my camera was in Arizona.
I'm open to [#KeepCalmAndGetDrenched](https://twitter.com/hashtag/KeepCalmAndGetDrenched); just let me take out all electronics.

![wading in Colorado River at Gateway Park](wading.jpg)

## Twilight Caching

I had "Yuma's best burger" at Lutes Casino, purchased some fuel, and started the return trip on Interstate 8.
Not long after leaving Yuma, I encountered a sign pointing to a parking area with "no service".
My instinct tells me that there must be something interesting, so I exited the highway and parked the car.
I was right: this is the site of Yuma Area Veterans B-17 Bomber Memorial, in memory of a B-17 bomber crash during a training mission in 1944.
There is also a [geocache](https://coord.info/GCN6DR) placed at this area.

![Feb 12 timeline as recorded by Google Maps](20170212.png)

My planned stop is Dateland Travel Center in Dateland, AZ.
I had a "Love's Travel Stop" on the outbound trip, so it's only fitting if I match that lovely name with a "date" on the return trip.
But no, this "date" has nothing to do with "love", but refers to date palm, a kind of fruit.
Date palm trees lined the background of this town.
I purchased a bag of dried dates in the gas station shop.

![date palm trees in Dateland](Dateland.jpg)

Next, I proceeded to Sonoran Desert National Monument.
It was almost dark when I exited the highway into Freeman, AZ.
I did not see anyone else or any other car.
It's just me, my Buick Impala, and the desert.
I parked the car at the end of pavement, and hiked toward a [geocache](https://coord.info/GC22M8B) on top of a knoll.
20 minutes later, I found the geocache, enjoyed the view, and watched sunset.

![Sonoran Desert National Monument](SonoranDesert.jpg)

The sun disappeared as I hiked down the hill.
Although there was a full moon, I could not see my parking spot.
This was another scary moment of this trip, but I made out alive: I determined the north direction using the traffic noise from I-8, and walked east in the general direction of my car; as I moved near the car, I pressed the car key's button to cause the car to honk, and walked toward the sound.

My dinner stop is Cracker Barrel Old Country Store in Casa Grande, AZ.
The meal was ordinary, but the store has nice country themed decor.
My favorite thing is this sign: "I like to play in the DIRT!"
That describes me completely.

!["I like to play in the DIRT!" sign in Cracker Barrel](PlayInTheDirt.jpg)

## Conclusion

This is the story of my adventure on my 2017 birthday.
It was an unforgettable experience.
There were a few scary moments, but I'm still living to tell the tale.
