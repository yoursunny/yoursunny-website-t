---
title: "Moving Dot: How Many Displays Can You Fit on an ESP8266?"
lang: en
date: 2018-05-19
tags:
- ESP8266
image: demo.jpg
---

In yoursunny.com's toy vault, there is an assortment of LED displays.
I'm wondering, how many LED displays can I fit on an ESP8266?
So I built this "moving dot" demonstration, with two LED displays and a buzzer.

![moving dot demo](demo.jpg)

The LED matrix serves as the game board.
A dot appears on the matrix.
In each time step, the dot randomly moves by one pixel or stays in the same position.
The 4-digit displays current time step number.
Whenever the dot reach any of the four corners, the buzzer plays a piano note selected between C3 and B5.

## Hardware

**Bill of materials**

* [NodeMCU v1.0](https://amzn.to/2IzDTLO)
* [RobotDyn 8x8 LED matrix](https://amzn.to/2LgRzgG)
* [RobotDyn 4-digit 7-segment LED display](https://amzn.to/2IRla1Y)
* [passive buzzer](https://amzn.to/2Lg7rzN)
* [full-size 830-point breadboard and jumper wires](https://amzn.to/2KGwyuv)

**Wiring**

peripheral|peripheral pin|NodeMCU pin|ESP8266 GPIO
----------|--------------|-----------|------------
8x8 matrix|SDI           |D7         |13
8x8 matrix|SCL           |D5         |14
8x8 matrix|CS            |D8         |15
8x8 matrix|5V            |3V3        |
8x8 matrix|GND           |GND        |
buzzer    |S             |D6         |12
buzzer    |GND           |GND        |
buzzer    |VCC           |3V3        |
4-digit   |CLK           |D2         |4
4-digit   |DIO           |D1         |5
4-digit   |GND           |GND        |
4-digit   |5V            |3V3        |

## Arduino Code

```arduino
// https://github.com/bartoszbielawski/LEDMatrixDriver/
// commit bf0637ba17624035b2657d2743e2e878aae9923f
#include <LEDMatrixDriver.hpp>

// https://github.com/bremme/arduino-tm1637/
// commit 4f4196a3b2e56540de7782e1ea138b25361ae71f
#include <SevenSegmentTM1637.h>

// http://www.sengpielaudio.com/calculator-notenames.htm
const int MUSIC_NOTES[] = {
  130, 146, 164, 174, 195, 220, 246,
  261, 293, 329, 349, 391, 440, 493,
  523, 587, 659, 698, 783, 880, 987,
};

LEDMatrixDriver matrix(1, 15);
SevenSegmentTM1637 seven(4, 5);

int step = 0;
int x = 4, y = 4;

void setup() {
  matrix.setEnabled(true);
  matrix.setIntensity(2);
  seven.begin();
}

void loop() {
  matrix.setPixel(x, y, false);
  x += random(-1, 2);
  y += random(-1, 2);
  x = max(min(x, 7), 0);
  y = max(min(y, 7), 0);
  matrix.setPixel(x, y, true);
  matrix.display();

  ++step;
  seven.clear();
  seven.printf("%4d", step);

  if ((x == 0 || x == 7) && (y == 0 || y == 7)) {
    int freq = MUSIC_NOTES[random(21)];
    tone(12, freq, 100);
  }

  delay(100);
}
```

## Video Demo

<iframe width="560" height="315" src="https://www.youtube.com/embed/y2l6WsupNts" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
