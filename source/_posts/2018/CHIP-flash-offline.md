---
title: How to Flash C.H.I.P Offline
lang: en
date: 2018-02-25
tags:
- CHIP
---

Next Thing Co (NTC), the company that made the $9 C.H.I.P computer, is [not doing well](https://web.archive.org/web/20180918185648/https://bbs.nextthing.co/t/is-c-h-i-p-dead/20091): orders are not shipping on time, and support is not answering emails.
While the little CHIPs would still work even if NTC no longer exists, same cannot be said for firmware flashing: the recommended method for flashing firmware is through Chrome App developed by NTC, which in turn downloads from NTC's file servers.
If Next Thing Co evaporates and their file servers go offline, the Chrome App would not be able to download the firmware, and therefore I cannot re-flash my CHIPs if I ever mess up their operating system.

To keep my C.H.I.Ps working for as long as possible, I have to [plan for the inevitable](https://web.archive.org/web/20180928121407/https://bbs.nextthing.co/t/planning-for-the-inevitable/19958) and find out how to flash a CHIP without relying on the Internet, or at least, without relying on the `opensource.nextthing.co` file server.
NTC has published some scripts, known as [CHIP-tools](https://github.com/Project-chip-crumbs/CHIP-tools), to work with CHIP computers.
The `chip-update-firmware.sh` script is a command line tool to flash the device.
While the content of this script is complicated, to make it work offline, I just need to figure out what it needs from NTC file servers, and download these files in advance.

A brief read and tests indicate that `chip-update-firmware.sh` has the following workflow:

1. Verify all necessary programs are available.
2. Select a *flavor* according to command line arguments. It is one of `server`, `gui`, `pocketchip`, and `buildroot`.
3. Download a `latest` file that contains the latest firmware version number.
4. Download SPL, sunxi, and U-Boot images.
5. Connect to CHIP in FEL mode, and determine whether the CHIP has a Hynix or Toshiba NAND chip.
6. Download CHIP operating system image that suitable for the NAND chip.
7. Connect to CHIP in Fastboot mode, and send the operating system image to CHIP.

Files for each flavor is hosted in a different directory on the file server.
For `server` flavor, the files are:

```text
# lastest version
http://opensource.nextthing.co/chip/images/stable/server/latest

# SPL, sunxi, U-Boot
http://opensource.nextthing.co/chip/images/stable/server/149/spl-40000-1000-100.bin
http://opensource.nextthing.co/chip/images/stable/server/149/spl-400000-4000-500.bin
http://opensource.nextthing.co/chip/images/stable/server/149/spl-400000-4000-680.bin
http://opensource.nextthing.co/chip/images/stable/server/149/sunxi-spl.bin
http://opensource.nextthing.co/chip/images/stable/server/149/u-boot-dtb.bin
http://opensource.nextthing.co/chip/images/stable/server/149/uboot-40000.bin
http://opensource.nextthing.co/chip/images/stable/server/149/uboot-400000.bin

# CHIP operating system for Hynix NAND
http://opensource.nextthing.co/chip/images/stable/server/149/chip-400000-4000-680.ubi.sparse

# CHIP operating system for Toshiba NAND
http://opensource.nextthing.co/chip/images/stable/server/149/chip-400000-4000-500.ubi.sparse
```

I successfully flashed all four flavors onto a Hynix CHIP, using a Raspberry Pi 3 running Ubuntu Mate 16.04 as the host computer.
My steps were:

1. Clone [CHIP-tools](https://github.com/Project-chip-crumbs/CHIP-tools) repository to `$HOME/CHIP/CHIP-tools`.
2. Install Ubuntu packages: `sudo apt install android-tools-fastboot sunxi-tools u-boot-tools`.
3. Download SPL, sunxi, U-Boot, and CHIP operating systems images into `$HOME/CHIP/stable-server-b149`. Internet connection is no longer needed after this step.
4. Install a LiPo battery on the CHIP, because Raspberry Pi 3's USB port cannot supply enough current for CHIP to operate smoothly.
5. Ensure CHIP is powered off and not connected to the host computer. Put a jumper wire between FEL and GND pins.
6. Execute `sudo whoami` so that the next command won't ask for my password again.
7. In `$HOME/CHIP/CHIP-tools` directory, execute `FEL='sudo sunxi-fel' FASTBOOT='sudo fastboot' SNIB=false ./chip-update-firmware.sh -L ../stable-server-b149`.
8. When `waiting for fel......` prompt appears, connect CHIP to the host computer with a microUSB cable.
9. When `FLASH VERIFICATION COMPLETE.` message appears, flashing is successful.

NTC file server `opensource.nextthing.co` is still online today, so I can download from them in step 3.
~~As it is going to disappear, I have copied the files to [Bintray](https://dl.bintray.com/yoursunny/CHIP/) repository to keep them available.
Note that CHIP operating system images in my Bintray mirror are compressed by 7-Zip, and they must be manually decompressed before running `chip-flash-firmware.sh` script.~~

July 2019 update: Next Thing Co is no more.
I reflashed my C.H.I.P one last time using [JF Possibilities' mirror of NTC's CHIP-tools images](http://chip.jfpossibilities.com/chip/) that is somewhat easier than the method above.
Then, I [upgraded them to Debian Stretch](/t/2019/bye-CHIP/) and sold them on eBay for original Kickstarter pricing.
Bye, C.H.I.P.

July 2021 update: Bintray has ended.
You can get the files from [C.H.I.P. Flash Collection](https://archive.org/details/C.h.i.p.FlashCollection) on the Internet Archive.
