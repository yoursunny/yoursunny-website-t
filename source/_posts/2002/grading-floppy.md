---
title: 批考卷
lang: zh
date: 2002-10-20
tags:
- life
- China
---

星期二（10月15日）我班和年级里其他几个班参加了第一次信息科技测验，内容是Windows资源管理器基本操作和Access2000数据库简单操作。
在一天内，这么多人考试，考试软盘全由老师批改显然是不可能的，于是，作为班级信息课代表的我被老师“看上”了，老师让我和另一位同学负责批考盘。

中午12:30，我们准时来到１号机房。
20分钟的简单培训后，我们就开始工作了。
15分钟内，我批改了2张盘（当然不是我们班的）。

以后，不管中午、自修课，只要一有时间，我就拉上她去机房批改考盘。
这其实是一项并不容易的工作。
资源管理器操作是有专用的自动批改程序的，只要插上盘，运行一下就可以了；可是，Access题目只能手工批改，在多批了几张盘后，机械、重复的“E1.mdb→FruitA→设计→……→查看‘超市名称’排序”的工序真让人乏味。
在去了三次后，她终于忍不住了，再也不想去了；而我去了五次，基本完成了任务。
