---
title: Standard of Cool Man
lang: en
date: 2002-11-03
tags:
- commentary
---

Nowadays, more and more young men have learned the word "cool man". However, what is a "cool man"?

Sometimes, "cool man" means "male". In a forum of Net Friends website ([www.pcdiy.com.cn](https://web.archive.org/web/20021121220225/http://www.pcdiy.com.cn:80/)), in the sex field, "cool man" can be found in place of "male", and "pretty girl" instead of "female".
It's clear that "cool man" means "male" here.

My dictionary didn't give a definition of "cool man", but the definition of "cool" is "handsome".

"Handsome" is a target of many young men as well as a standard of the boyfriends of young women.
And so that "cool man" became the pronoun of "male".

I have wished myself a cool man of a long time, but I have to say I'm only "half cool man" now, because I haven't reached my standard of "cool man".
A cool man in my mind is like this:

1. Brave.
   A cool man must be brave.
   He won't be afraid of difficulty.
   A cool man must be tear-less and always strong.
   He is ready to save others as a hero and dares to admit mistakes.
2. Intellectual.
   A real cool man always keeps calm, and he won't be annoyed by the sentiment.
   A cool man must be able to control himself, he doesn't hurt others easily.
   When a cool man is hurt or even injured, he won't say a word.
3. Capable.
   A cool man must be splendid in all fields, and he must be a complex person of talent.
   A cool man has enough energy and he can do something what others cannot.

[简体中文版本](/t/2002/sg-standard/)
