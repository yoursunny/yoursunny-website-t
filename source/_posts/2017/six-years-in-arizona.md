---
title: Six Years in Arizona
lang: en
date: 2017-08-04
tags:
- life
- Arizona
---

Six years ago, [on Aug 04, 2011, I flew from Shanghai, China to Tucson, Arizona](/t/2017/first-days-in-Tucson/), and began my life as a PhD student in The University of Arizona.
Six years later, I have defended my dissertation, and am on my way to graduation.

During these years,

* I [published three conference papers](/t/2017/academic-papers/) as the first author, and attended one conference to present my paper.
* I took 88 units of classes, and maintained 4.0 GPA.
* I received three scholarships including the Galileo Circle Scholar for the finest science students.
* I joined [International Student Association](/t/2017/ISA/) since its beginning, met an incrediable group of people, and served as an officer.
* I started exercising consistently in the form of [swimming](/t/2017/floating-in-the-pool/) and [weight lifting](/t/2017/heavy-iron-stuff/).
* I picked up [Geocaching](/t/2017/start-geocaching/) as a hobby.
* I watched over 150 [movies](/t/2017/moviegoer/).
* I enjoyed [hiking](/t/2017/hiking-Arizona/) and other outdoor adventures including [camping](/t/2016/offline/) and canoeing.
* I visited [every corner of Tucson](/t/2017/Tucson-museums/) that is reachable by [bicycle](/t/2017/on-two-wheels/) or [city bus](/t/2017/SunTran/), [participated in various local festivals](/t/2017/Tucson-festivals/), and [tried many local restaurants](/t/2017/Tucson-restaurants/).
* I travelled to California, Colorado, District of Columbia, Hawaii, Massachusetts, Nevada, New York, Pennsylvania, Tennessee, Texas, Utah, and Washington.
* I obtained an [Arizona driver's license](/t/2017/behind-the-wheel/) and drove rental cars in Arizona, California, Massachusetts, and Washington.
* I am proud to be an Arizona Wildcat, and proud to be a Tucsonan.

In this month, I am going to write a series of articles about my life in Arizona, to record the good memories of the past six years.

[简体中文版本](/t/2017/six-years-abroad/)
