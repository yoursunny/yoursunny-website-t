---
title: Heavy Iron Stuff
lang: en
date: 2017-08-22
tags:
- life
- Arizona
image: legday-every-week.jpg
---

I started going to the gym regularly during my [fourth through sixth years at University of Arizona](/t/2017/six-years-in-arizona/).
Weight lifting helped me to lose weight of up to 30lb, and replaced some of my body fat with juicy muscles.

## How It All Started

I [have been swimming](/t/2017/floating-in-the-pool/) since [the second day](/t/2017/first-days-in-Tucson/) I arrived in Tucson, but my body looked the same, because my roommate was cooking large bowls of white rice and fatty meats for me.
Since I [moved to University Arms](/t/2017/roommates-or-not/) and started living alone, I decided to change things up.
I started running on treadmill and using the elliptical trainer on and off in the [Campus Rec](http://rec.arizona.edu/).
I was thinking to eat less and cut back on ice cream.

I ate only one apple at the breakfast of Oct 14, 2014, and felt hungry two hours later.
I wondered, am I eating too little?
To find the answer, I decided to calculate how much calories I was eating on that day.
I searched online for a "calorie calculator", and found [MyFitnessPal](http://www.myfitnesspal.com/).
It is not only a "calculator", but also an app to keep track of how much I ate.
I made my first entry:

![MyFitnessPal food diary on Oct 14, 2014](20141014.gif)

MyFitnessPal gave me some idea of how much food I should eat every day in accordance to the amount of exercises I was doing.
Finding it useful, I continued logging my food consumption and exercises into MyFitnessPal.

## Lose Weight

MyFitnessPal calculates a daily calorie goal based on my gender, weight, height, normal life style, and fitness goal.
When I log a food item I ate, it reduces the remaining calories of the day.
When I log an exercise I performed, it increases the remaining calories and therefore allows me to eat more.

I weighed 173lb on the day I installed MyFitnessPal.
I wasn't satisfied with my weight, so I entered "lose weight" as my fitness goal, with the eventual target set to an arbitrary 140lb.
I chose a modest "lose ½lb every week" plan, and my ration was calculated as 1910 calories per day.

My life started to change immediately.
I went to the gym more often.
I began reading nutrition labels.
I became conscious on the amount of cooking oil I use.
I ordered [lighter fare at restaurants](/t/2017/Tucson-restaurants/).

I continued to eat ice cream and other sweets, but I practiced portion control and divided my ice cream into the number of servings suggested on the nutrition label:

![ice cream portion control](icecream-portions.jpg)

By the end of 2015, I lost 30lb of weight.

![MyFitnessPal weight report](weight-chart.gif)

I never got down to the original target of 140lb.
When I reached 145lb, I decided that it was too stressful to eat so little food, so I changed the setting in MyFitnessPal to "maintain weight", and never changed back.

## Pick Up the Iron

I started my gym life on cardio machines, including treadmill, Stair Master, and various types of ellipticals.
All these machines are located on the second floor of Campus Rec weight room.
On every visit, I would spend 30~45 minutes on one of these machines.

I [felt](https://twitter.com/yoursunny/status/503681099286212608) something was off:

> I'm not sure if I'm in the wrong section of the gym. Most people around me are girls. Where are the boys?

The [answer](https://www.facebook.com/yoursunny/posts/10154817441713827?comment_id=10154818787858827) was:

> All of the men are lifting

So I started strength training.
I turned to the two rows of strength circuit machines on the second floor, including: lat pulldown, mid row, chest press, shoulder press, triceps push down, bicep curl, leg extension, leg curl, leg press.
After each cardio session, I would try one or two of these machines.
A few weeks later, after some reading, I swapped the order to strength then cardio, because doing strength first could lead to more weight loss.
Nevertheless, cardio remained my focus at that time, and my strength exercises were limited to these machines.

I tried free weight lifting for the first time on Jul 15, 2015.
It was a "20lb" bench press.
The bench press stations are located downstairs, and usually occupied by big boys with huge muscles.
In summer, many of them are gone, so I could take my time without being stared upon.
I watched [Buff Dude's bench press tutorial](https://youtu.be/gRVjAtPip0Y), loaded a pair of 10lb plates on a barbell, and tried the bench press for the first time.
I felt it heavier than setting "20lb" on the "bench press" machine upstairs, but I didn't figure out the reason until two weeks later: the barbell is heavy!

![learn squat with a rod](learn-squat.jpg)

I learned to squat on Aug 27, 2015, and tried dead lifts for the first time on Sep 08, 2016.
I also learned many other free weight exercises from YouTube videos, including: bicep curls, triceps extension, T bar row, bent over row, overhead press, farmer's walk.

## Proper Form!

I recorded [a video of my lifting](https://youtu.be/qIU_4UHdaEs) in a hotel gym during a conference trip, and I was immediately [frowned upon](https://www.facebook.com/yoursunny/posts/10154034280913827):

> 😡 That is the way that you work out?
>
> lol. Your form on those curls is terrible. Looks like you should be using lighter weights.
>
> Your shoulders probably hurt from your elbows flaring out.

This was my first lesson on the importance of **proper form**.
I watched more tutorial videos, and my form was improving at the end of this conference trip.

![bicep curls: bad form vs good form](bicep-curl.jpg)

Bicep curls isn't the only exercise in which I had problems with the form.
Although I watched videos before doing an exercise, I have been too eager to add more plates, and as a result, the range of motion was reduced and the exercise became less effective.
Here I would like to thank [Chris](https://www.facebook.com/cmsanch) and [Spyros](https://www.facebook.com/spyros.mastorakis) for pointing out my mistakes when spotted in my videos.

## Phalanges Day

In summer 2016, I started a quest of going to the gym for 40 consecutive days.
I would train a different body part on each day, and do two or three exercises targeting this body part.
I had a chest day, a bicep day, a triceps day, a back day, a quad day, a hamstring day, a calf day.
I found I was missing a shoulder day, so I went for a few overhead presses, upright rows, and lying low pulley lateral raises.

Then I [received a suggestion](https://www.facebook.com/yoursunny/posts/10154492288003827?comment_id=10154492356623827):

![Daniel Harms: Don't neglect phalanges day.](phalanges-comment.gif)

I could not find any information on what a "phalanges day" is, but the [definition of "phalanges"](https://en.wikipedia.org/wiki/Phalanges) is clear:

> The phalanges are the bones that make up the fingers of the hand and the toes of the foot.

So I selected a few exercises related to fingers and toes, and had my first #PhalangesDay on Sep 09, 2016.
My phalanges day exercises include: wrist curl, finger extension, finger cable pull (invented by me), toe press.

[![finger cable pull on phalanges day](finger-cable-pull.gif)](https://youtu.be/gdbp2ktrVZw)

These exercises are slightly useful.
Each of my fingers was able to pull a cable at 10lb resistence when I started, and now my little fingers can pull 20lb while my eight other fingers can pull up to 30lb cable.

To this day, I still do not know whether "phalanges day" is a real thing, or if [Daniel](https://www.facebook.com/daniel.harms) pranked me.

## Leg Day

Unlike many people, my favorite day at the gym is #LegDay.
I would do squats, dead lifts, calf raises, and more.
I even bought [a shiny new outfit](https://bit.ly/fl2invite) for leg day.

[![squat on leg day](squat.jpg)](https://youtu.be/UQRe-EjqkqY)

I never skipped a leg day since I started them.
However, my liking for leg day wasn't well perceived.
I once wrote "#LegDay every week" on a poster board.
After several days, someone changed it to "#LegDay every month" and then "#LegDay every year".

![#LegDay every week post note](legday-every-week.jpg)

Leg day eventually made onto the big screen when I was awarded my PhD degree at College of Science commencement.
Nobody could change my message this time.

![big screen at commencement, courtesy of Mingjun Zhou](commencement.jpg)

## DOMS

Every leg day, or any other heavy lifts, comes with Delayed Onset Muscle Soreness (DOMS).
During my attempt on a 40-day streak in summer 2016, I had one or two sore body parts almost every day.
On the 30th day, DOMS finally got me to the point that I no longer had a body part that could be trained.

![massage day on a foam roller](foamroller.jpg)

I still went to the gym on that day.
Instead of a lifting day, I had a massage day.
Following a [YouTube playlist](https://www.youtube.com/playlist?list=PLLALQuK1NDrgHBKONa2t6hJVWHrVawyfn), I used a foam roller to massage all over my body.
It was immensely helpful that I could resume training the next day.
I eventually extended the streak to 60 consecutive days before I had to stop.

## Where I Am Now

![my upper body on Aug 22, 2017](muscle.jpg)

It has been more than 1000 days since I started.
I used to wear M-size shirts, and now I can fit in S-size shirts.
I lifted heavy iron weights, and gained considerable muscles.
My body definitely looks different from three years ago.
