---
title: On Two Wheels
lang: en
date: 2017-08-19
tags:
- life
- Arizona
image: Jacinto.jpg
---

Tucson is walkable when staying around UA campus and downtown area.
To go farther, I often [ride a bus with six wheels](/t/2017/SunTran/), [hail a taxi with four wheels](/t/2017/hailing-taxis/), or [drive a rental car behind one wheel](/t/2017/behind-the-wheel/), all of which costs money.
More frequently, I would ride a bike on two wheels.
I enjoy riding a bike because it is free: I can go places without buying a ticket, and I have the freedom to go anywhere without worrying about missing the bus or being unable to find parking.

![Rillito River Park, π day, 2015](pi.jpg)

## First Ride on Cat Wheels

[Cat Wheels bike sharing program](https://parking.arizona.edu/bicycle/cat-wheels/), offered by Parking & Transportation Services, loans bikes to students for free.
I can pick up a bike at any parking garage, ride for a whole day, and return by 16:00 the next day.
Since garages are only open on weekdays, a bike borrowed on Friday won't be due until Monday, making this a plusgood deal.

![my first Cat Wheels rental bike](CatWheels.jpg)

I borrowed a Cat Wheels bike for the first time on Feb 02, 2012.
I rode it back to [NorthPointe](/t/2017/roommates-or-not/) via Mountain Avenue.
The next morning, I visited four parks in a row (Rillito River Park, Rio Vista Park, La Madera Park, and Himmel Park), experienced [Sun Tran](/t/2017/SunTran/)'s bike rack, and returned the bike.
The ride was a little over an hour, and I was already feeling tired.

![Rillito River Park, Nov 02, 2013](Rillito.jpg)

## Safety Class

The next week, on Feb 07, 2012, I found myself taking a bicycle safety class.
A bike ambassador explained the safety rules of cycling on Tucson streets.

In Arizona, you can ride a bike almost anywhere except highways.
There are bike lanes, but technically, they are "road shoulders"; bikes can travel on shoulders, cars cannot.
Where bike lanes are unavailable, or when you are preparing to turn left, you can legally ride on the car lanes just like a car.

The number one safety rule is to **let cars see you**.
Lights are required at night.
Reflective clothing and helmet are recommended.
Cyclists should use hand signals to indicate left/right turns.

These rules are very different from China.
In China, almost all roads have dedicated bike lanes, and bikes are only allowed in bike lanes.
Lights are not required; nobody wear a helmet.
I collided with another bike in 2001, because she was riding the wrong way, and neither of us could see each other in the darkness.

Apart from learning the safety rules, I received a bike route map, and a map of "The Loop", which would later become my favorite bike routes.

## Geocaching on Bikes

I didn't ride much in 2012 and 2013.
NorthPointe is more than 4 miles from UA campus.
I did not feel comfortable riding such a long distance every day I wish to use a bike.
Moreover, NorthPointe provides a shuttle to UA campus, groceries and [a movie theater](/t/2017/moviegoer/) are within walking distance, so I had no need to go anywhere else by bike.
This regular life continued until I [started geocaching](/t/2017/start-geocaching/).

![Julian Wash Archaeological Park, Nov 01, 2014](Julian.jpg)

Geocaches are placed all around Tucson.
They are waiting for me to go outdoors and explore the city.
After exhausting most geocaches near NorthPointe, the Tucson Mall, and University of Arizona campus, I started to look further.
Geocaching by bus is possible but inefficient, because buses run on fixed schedule, and I would be spending more time waiting for the bus than having fun with the geocaches.
Geocaching by bike is a better option, because I can ride the bike at any time without being limited to the bus schedule.

![Aviation Bikeway bridge over Euclid Ave, Dec 14, 2014](Euclid.jpg)

Starting 2014, especially after I have moved to University Arms that is closer to UA campus, I used to borrow Cat Wheels almost every weekend to go geocaching.
I covered all major bike routes in Tucson, including Mountain Avenue, Third Street, and more than half of [The Loop](https://www.pima.gov/162/The-Chuck-Huckelberry-Loop).
I even descended into a dry river and rode on the riverbed for a few hundred meters to find a geocache down there.

![Urban Spooky Canyon geocache, Oct 31, 2015](UrbanSpookyCanyon.jpg)

## My Own Mountain Bike

I wasn't able to secure a Cat Wheels bike during Thanksgiving 2015, because they wouldn't loan the bike to anyone for the "long weekend".
[Teng](https://www.facebook.com/philo.Teng.Liang) left his bike in the office since he bought a car, so I asked to borrow his bike and was approved.
Having a [mountain bike](https://amzn.to/3WMphzG), I was eyeing the unfound geocache on top of A Mountain, so I started to ride the bike up A Mountain, and things went horribly wrong.
As soon as I entered the mountain, a part of the bike broke off, the chain was caught into the spokes, and the bike would not move!
Having no bus pass with me, I had to walk two hours to get the bike back to my office.
It was Nov 26, 2015, the day of Thanksgiving.
My Thanksgiving was officially ruined, and I would be on the hook of repairs.

I searched online to determine the name of the broken bike part.
I saw the word "Shimano" on the piece connecting to the broken part, so I started my search from this word, and found it is called a "shifter".
Then, I looked through diagrams, and determined that the part that broke off is the [rear derailleur](https://amzn.to/3N7Zu1Q).

The next day, I called a few bike shops to ask for quotes for replacing a derailleur, and the price is around $15 for a rear derailleur, plus another $15 labor charge for installing it.
Then I found a cheaper place: [BICAS](https://bicas.org/).
They said I could have the derailleur replaced in 30 minutes for about $10, but I have to do it myself, and they will teach me.

![first repair at BICAS](BICAS.jpg)

On Saturday, two days after the accident, I walked the bike to BICAS.
It is located in a basement, and the whole place smells like machine oil.
I was told to search for a suitable derailleur in a row of giant bins full of used derailleurs.
The requirement is that the attachment point must look exactly same as my broken derailleur, so that it could fit the bike.
I picked out several derailleurs similar to the old one, and the front desk girl helped me choosing the best piece.
Then, volunteer David instructed me to remove my broken derailleur, install the new one, replace the cable, and repair the bent chain when it was caught into the spokes.
David told me that my accident was most likely caused by imprecise adjustment when the bike was assembled in the big box shop (Sports Authority, in this case), which was exposed when I moved the shifter when entering the mountain.
He taught me how to adjust the "high limit" and "low limit" screws so that the chain would stay on the cassette regardless of which gear the shifter is on.
The whole repair took 2 hours, and cost $12 including parts and shop time.

I eventually decided to buy the bike from Teng for $50.
Having my own bike would give me more freedom on when to ride, because I was tired of visiting multiple garages to hunt down an available Cat Wheels bike every Friday.
Moreover, Cat Wheels were expected to close again on the upcoming Christmas.

The bike did not come with a lock.
I brought a [cable lock](https://amzn.to/3C6Zskm) from Shanghai in 2011.
The cable lock is labeled "extra strong for motorcycle", but it could be cut in a few minutes and is too weak for thieves in Tucson.
[UA Police Department (UAPD) recommends](https://web.archive.org/web/20150919100929/http://uapd.arizona.edu/article/welcome-back-safety-tips) a [U-lock](https://amzn.to/3IPkx6L), which is stronger than a chain lock.
Buying a U-lock would cost $15 or more, and I was unwilling to pay for that, so I kept the bike inside in my apartment or in the office, where a bike lock isn't necessary.

![Pima County bike safety class](class.jpg)

I heard that I could receive a U-lock and a helmet for free by attending a "Confident and Capable Cyclist" [bike safety class](https://web.archive.org/web/20171010014822/http://webcms.pima.gov/cms/One.aspx?portalId=169&pageId=54575), so I signed up.
This class is a two parter.
The first part was a classroom session in the evening of Dec 15, 2015, where we were taught the names of major bike parts, and Arizona traffic laws regarding bicycles.
Although I have attended a bike safety class in 2012, this class was more thorough.
The second part was a ride practice in the morning of Dec 19, 2015.
We were taught traffic skills such as how to dodge a rock, and then we went for a short group ride on Third Street.
At the end of the class, I received a coupon for two safety items, which I redeemed two days later.
They are both quality stuff, worth $39.50 on the labels.

![freebies from Pima County bike safety class](freebies.jpg)

I'm incredibly happy to have my own bike.
It enabled me to go geocaching and run errands without relying on Cat Wheels.

![riding near Jacinto Park, Jan 01, 2016](Jacinto.jpg)

The downside of owning a bike is, I was forced to become a regular customer of BICAS.
Over the course of 1.5 years, I replaced the front derailleur which broke off on the way back from the bike safety class, patched and later replaced both tubes and a rim tape, changed a [pedal](https://amzn.to/42kDXHq), replaced the rim of front wheel because I hit the curb, and changed part of the handlebar.
I visited BICAS 8 times, and spent $69.50 for those repairs.

My bike repair bills would have been higher without the good people at the [Campus Bicycle Station](https://parking.arizona.edu/bicycle/bicycle-station/).
Experienced bicycling enthusiasts operate a bike station on campus during the semester, where students and the public can get help with basic repairs including adjusting brakes and fixing a flat.
Service is free and tipping is unnecessary, but they do not sell any bike parts.
I went there so often for flat tires and adjustments, that they could recognize me (although they said that they "recognize the bike").

I estimated that my bike needed repairs after an average of three rides.
I often ride long distance or on rough terrain, because this is what it takes to find geocaches.
The desert is unforgiving: high temperature melts the tire, cactus needles punch holes in the tube, my powerful feet wear down pedals on long rides, and stop signs add pressure on the brakes.

Shortly before I graduate, on May 02, 2017, I sold the bike to [Indrayudh](https://www.facebook.com/indrayudh.roy), who I met when hiking with [ISA](/t/2017/ISA/).
I charged $68, a little more than my buy-in price, given that the bike is in a better mechanical condition as everything has been "professionally" adjusted, and I added two locks into the sale.

TRIVIA: All four owners of this bike, [Ethan](https://www.facebook.com/yi.huang.560) (from whom I [borrowed a car for road test](/t/2017/behind-the-wheel/)), Teng, myself, and Indrayudh, are in computer science department.

## Back to Cat Wheels

I continued borrowing Cat Wheels, including when I had my own bike.
They are of lower quality of my mountain bike, but I'm not responsible for repairs as long as I could bring it back to the garage and the damage was not intentional.

![Cat Wheels rack at 6th Street Garage, Jan 06, 2015](6th.jpg)

The worst thing of Cat Wheels is improper seat height: the saddle is sometimes too high, and sometimes too low.
I hate high saddles because I cannot touch my foot on the ground when waiting at a red light, so I used to opt for a bike with low saddle.
Until one day, I discovered Cat Wheels are also available at [Outdoor Rec](https://rec.arizona.edu/outdoor-challenge).
Unlike garage cashiers, student workers at Outdoor Rec are outdoor enthusiasts, and many of them know bikes.
When I complained about seat height, they taught me how to adjust the seat so it fits my legs.
It was actually simple: loosen the quick release, raise or lower the saddle, and lock the quick release.

Outdoor Rec soon became my go-to place for borrowing Cat Wheels bikes.
It is a less known place, which means they are more likely to have an available bike.
Since I went there frequently, I was named "number one customer of free bikes", and received personalized service.
Some girls at Outdoor Rec started to recognize me, and they would bring me a Cat Wheels rental form as soon as I approach the desk.
Moreover, Geoffrey went out of his way to provide even better service: he would walk out to the bike rack and inspect the mechanical condition; I wrote a praise letter to his boss.

![Arizona Department of Game and Fish, Nov 25, 2016](GameFish.jpg)

I found many more geocaches on Cat Wheels borrowed from Outdoor Rec.

## Pair Rides

I usually ride alone, so that I have the freedom of determining routes, pace, and I can stop for geocaching at any time and keep searching for a geocache for as long as I want.
Occasionally, I would participate in a group ride.
Not counting the group ride in Confident and Capable Cyclist bike safety class, I had four "group" rides, or more accurately "pair rides" because each of these rides had only two participants.

I organized three of these rides, with [Yumin](https://www.facebook.com/yumin.xia) on Sep 23, 2015, with [Jasmine](https://www.facebook.com/jasmine.cheng.56) on Nov 14, 2015, and with [Ren](https://www.facebook.com/rencaballero) on Aug 11, 2017.
There is no doubt that I either took them geocaching, or had them help with my geocaching.

![Santa Cruz River Park, with Yumin, Sep 23, 2015](Yumin.jpg)

I followed [Russ](https://www2.cs.arizona.edu/~russelll/) on Jul 20, 2017.
He said that I was the only one ever show up in his "10 little rides" series.
That ride was just over one hour, considerably less than what I would ride myself.
It gave me a good chance to hear some stories about undergraduate computer science education, because Russ is a lecturer teaching undergraduate classes.

## Conclusion

Tucson is a [bike-friendly city](https://web.archive.org/web/20220521115120/http://wildcat.arizona.edu/article/2016/09/n-az-top-50-for-bike-rankings) offering plenty of bike routes including more than 100 miles in [The Loop](https://www.pima.gov/162/The-Chuck-Huckelberry-Loop) and good bike shops such as BICAS.
Moreover, a biannual event, [Cyclovia Tucson](/t/2017/Tucson-festivals/), would close several miles of the city streets to motor vehicles for a few hours, where people can ride, walk, and play in the worry-free streets.
I enjoy riding on two wheels in Tucson.
