---
title: My Story with Sun Tran
lang: en
date: 2017-08-11
tags:
- life
- Arizona
image: bus3000.jpg
---

Tucson is a "small" town.
Compared to my hometown Shanghai, Tucson is 10 times smaller in land area, and has 3% of Shanghai's metro population.
Nevertheless, Tucson is still the second largest city in Arizona.
You can't reasonably walk to everything, so transportation is a necessity.

America is a country on wheels, but Tucson has a nice public transportation system, [Sun Tran](http://www.suntran.com/).
During my [six years at University of Arizona](/t/2017/six-years-in-arizona/), I never bought a car, but used Sun Tran bus system extensively.

![Sun Tran bus 3000](bus3000.jpg)

## City Bus in Shanghai vs Tucson

American city bus system do not have a good reputation: vehicles are old, schedules are infrequent, and operating hours are limited.
Nevertheless, the first transportation service I experienced in Tucson is the city bus, when I [visited UA campus for the first time](/t/2017/first-days-in-Tucson/).

Shanghai's and Tucson's city bus systems are quite different.
In Shanghai, if you want to ride the bus, you just wait at a bus stop, and a bus would show up in 20 minutes or less.
Times for the first bus in the morning and the last one at night are posted at the bus stop.
Beyond that, there isn't a bus schedule, so you don't know exactly when the bus will arrive.

In Tucson, interval between two buses could be as much as one hour, but [the bus has a schedule](http://www.suntran.com/routes.php).
You can find out when the bus would arrive online, and wait at the bus stop five minutes before a bus would arrive.

## Transfer, Day Pass, and U-PASS

During my [first days in Tucson](/t/2017/first-days-in-Tucson/), I took many bus trips between Grant Inn and University of Arizona, as well as travelling to grocery stores and shopping malls.
Since I had insufficent cash, I bought a "full fare stored value" pass from 7-Eleven with my credit card for pay for the trips.
I learned two important concepts: "transfer" and "day pass".

A **transfer** allows you to pay only one $1.50 fee and ride three buses within two hours.
It is intended for you to go to a place that is unreachable by a single bus route, but cannot be used to make a return trip.
A bus driver told me that while I cannot ride the same route in opposite direction (which makes a "return trip"), I can ride the same route twice in the same direction.
This turned out to be very useful as I can make a stopover at Fry's which sits between UA campus and Grant Inn.

![Sun Tran paper transfer](transfer.jpg)

A **day pass** costs $3.50 and offers unlimited rides on the same day.
If I need to take 3 or more bus rides, I would save money if I buy a day pass.
However, I must plan ahead and buy the day pass when I get on the first bus.

After [moving to NorthPointe](/t/2017/roommates-or-not/) and finding the shuttle unreliable, I bought myself a U-PASS, a discount bus pass that gives me unlimited rides on city buses throughout the semester.
It not only allowed me to get to school without relying on the NorthPointe shuttle, but also gave me freedom to visit elsewhere in the city.
I bought two more U-PASSes on my second and third semester, until [I lost one on Oct 28, 2012](/t/2017/hiking-Arizona/), at which point I decided not to buy another.
The NorthPointe shuttle also improved significantly by that time, so I could live with commuting on the shuttle and paying occasional bus rides with cash.

![Sun Tran U-PASS and full fare stored value pass](u-pass.jpg)

## I Made an App for Sun Tran

Sun Tran publishes bus schedules as a book, revised twice a year, and on the website.
In theory, buses run according to the schedule.
In practice, traffic and weather can cause delays, and buses are late.
Sun Tran's website has a "real time tracking" section, which gives more accurate predictions on when the bus would arrive at each stop, based on the current vehicle position.

Accessing the website wasn't easy for me.
First, the website was designed for computers, and wasn't easy to use on a mobile phone.
Second, my [first phone](/t/2017/first-days-in-Tucson/) comes with only 30MB cellular data per month, which is barely enough.

To solve this problem, I made a [Sun Tran SMS schedule](http://web.archive.org/web/20121011082231/http://yoursunny.com/p/suntran-sms/) app.
I can send a text message containing a bus route number and bus stop name to a cloud-based service number, which invokes a script to scrape Sun Tran's website, and sends back a reply containing live bus arrival/departure times.

Eventually, I bought new phones with new service plans where cellular data isn't as limited as before, and I no longer need the SMS-based app.
Sun Tran bus system also showed up in Google Maps, finally.
I still built another app related to Sun Tran: during Hack Arizona 2017, I made an Alexa Skill to query live bus departure times.
Unfortunately, the app did not win a prize, and I did not continue working on it after the hackathon.

<iframe width="560" height="315" src="https://www.youtube.com/embed/F5gyqJEpTtk" frameborder="0" allowfullscreen></iframe>

## Large Group!

When I became an officer at [International Student Association (ISA)](/t/2017/ISA/), I organized several trips utilizing Sun Tran.
The most popular trip, a walking tour of Downtown Presidio Trail on Aug 14, 2012, attracted 29 participants.
We packed the bus.

![ISA downtown presidio trail tour, courtesy of Gaurav Singh](presidio-tour.jpg)

I think this is a good way to introduce students to Tucson's public transportation system.

## SunGO Woes

Sun Tran introduced [SunGO card](https://www.suntran.com/sungo/) in 2013.
I ordered mine by mail while I'm still in China, so that it would be ready as soon as I'm back in Tucson.
However, SunGO has been a bumpy ride for me.

![my first SunGO card](sungo.jpg)

SunGO is a smartcard-based fare payment system.
I can add money to my SunGO card at a ticket vending machine (TVM), at a Fry's store, or through Sun Tran's website.
Then, I just need to tap the SunGO card on the fare box located on every bus.
A transfer is automatically loaded onto the SunGO card, and it's valid for 2 hours.
The "no return trip" restriction was also lifted, so a transfer is effectively a 2-hour pass.

While the above sounds great, in practice, adding money to a SunGO card has been difficult.
There are exactly three TVMs across the entire city, one in each transit center.
There are more Fry's Food Stores, but none is within walking distance from the university.
Even if I went to Fry's for groceries, recharging SunGO would be time consuming because the only machine is located at the customer service desk which usually has a long line.
The online option usually works, but I have to wait about 2 hours before the money is available, and would be denied boarding if I try to get on a bus earlier than that.

Another problem is, it is difficult to tell exactly how much money is left on a SunGO card.
Neither TVM nor Fry's could tell me accurately.
The only way is to call Sun Tran customer service phone line, or visit Sun Tran's office in downtown.

As a comparison, Shanghai Public Transportation Card (SPTC) has recharge machines in every subway station and almost every convenience store.
Recharging is instant, and you can check balance at any recharge machine.
However, you must have an NFC device to add money to a SPTC through online systems.
Nevertheless, I like SPTC's system more.

## Sun Link

Tucsonans welcomed [Sun Link Modern Streetcar](http://www.sunlinkstreetcar.com/) in 2014.
Every UA student received a free 30-day bus and streetcar pass so that we can experience Sun Link.

![free 30-day pass on a special SunGO card](30-day.jpg)

I took advantage of the 30-day pass, and rode both Sun Link streetcar and Sun Tran buses.
However, once this free pass expired, I don't see myself on Sun Link very often.

Sun Link serves a short route crossing five districts including University of Arizona campus, Fourth Avenue, and Downtown Tucson.
From UA campus, I could walk to Fourth Avenue in 10 minutes, or borrow a bike to reach downtown.
There isn't a reason to pay a full fare just to ride Sun Link, although I would ride Sun Link if I already have a transfer or day pass to go elsewhere on Sun Tran.

The only exception is when Sun Link organizes a [concert](https://www.tucsonweekly.com/TheRange/archives/2016/07/21/saturday-sun-link-shindig-free-rides-concert-and-activities) on the streetcar.
They did it a few times, inviting a local band to play on the streetcar, and the vehicle was packed every time.

![Jul 23, 2016 concert on Sun Link](concert.jpg)

Honestly, the sound quality of a concert on Sun Link is not good, but this is so unique that I experienced it three times.

## Conclusion

I ♥ Sun Tran and Sun Link!
Despite all the woes, having a public transportation system has made my life easier, and saved me tons of money by not owning a car.

![load a bike on Sun Tran bus bike rack, Aug 23, 2014](bike.jpg)

P.S. Sun Tran is not my only transportation.
I also [rode bikes](/t/2017/on-two-wheels/), [called taxis](/t/2017/hailing-taxis/), and [drove rental cars](/t/2017/behind-the-wheel/), if I need to go somewhere out of Sun Tran service boundary or after Sun Tran hours.
