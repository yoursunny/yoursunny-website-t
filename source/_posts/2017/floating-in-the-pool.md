---
title: Floating in the Pool
lang: en
date: 2017-08-09
tags:
- life
- Arizona
image: recpool.jpg
---

As it's [said](https://twitter.com/RTfocus/status/314758527753273344), if a foreign student does not have a girlfriend, he would start physical exercises sooner or later, because he has nothing else to do.

> RT @Dtiberium 顺便有个定律：来了美国的中国留学生如果没有女朋友，迟早会开始健身。道理很简单：他们实在没什么别的事情可干。

This is very true of me.

## Before I Came to Arizona

When I was an undergraduate student in Shanghai Jiao Tong University (SJTU), although the university has a gym, it only has a few treadmills and ellipticals, and students must pay an entrance fee upon each usage.
I never went to this gym.
There was no swimming pool at SJTU, and a security guard once warned me not to swim in the lake, when I stepped one foot into the lake trying to feel the water temperature.

The nearby East China Normal University (ECNU) has a better gym which includes a heated swimming pool that is open year-round.
My father bought me some tickets for ¥20 each, and I used to go swimming there once a week.
This routine continued during my two years working in a company in Shanghai: on Saturdays, I drove to ECNU for swimming.
My father sometimes went with me, and he taught me some swimming skills.

Within two days after I landed in Tucson, I started swimming again, in the small pool at [Grant Inn](/t/2017/first-days-in-Tucson/).

![Grant Inn pool](grant-inn.jpg)

## Campus Rec Pool

During the orientations, I heard that there is an Olympic-size swimming pool at University of Arizona's [Campus Rec](http://rec.arizona.edu).
I became interested immediately.
However, reading through Campus Rec's website, it was unclear whether I could take a shower before and after my swimming, and where to place my belongings while I'm swimming.
I sent an email to ask the aquatics office, and I was told that there are showers and day-use lockers, and I just need to bring a [combination lock](https://amzn.to/2HL21v3).

![my combination lock](lock.jpg)

I spent an hour in Walmart searching for a combination lock, and finally found it.
I imagined a combination lock to have three or four single-digit dials, but this lock looks nothing like that.
A Walmart associate taught me how to open this lock.

I had my first swim at Campus Rec's Olympic-size pool on Sep 29, 2011, and never stopped since then.
Frequency varied between once a week and four times a week.
NorthPointe's shuttle stop is at Sixth Street Garage, so it was convenient to go for a swim after getting off the shuttle in the morning, or before my return trip in the afternoon.
My skin was tanned while swimming, which led to my following statement:

> The darker your skin is, the more attractive you are.

My routine is 6 laps (300 feet total), doing backstroke southbound and breaststroke northbound.
The goal is to complete this routine within 8 minutes.
I can finish in time about half of the time.
There is also one lap of warming up at the beginning, and one lap of relaxation at the end which could take up to 5 minutes.

![Campus Rec pool](recpool.jpg)

In the summer, Campus Rec would sometimes setup the pool in a "long course" arrangement, where each lap is 100 meters.
I still swam 6 laps, all as breaststroke, and it usually takes 20 minutes.

## Float Elsewhere

My research often involved long simulations taking hours to complete.
Whenever those experiments were running, I would go floating in the apartment pool.

[NorthPointe Apartments](/t/2017/roommates-or-not/) has a big pool.
University Arms has a little pool.
Those floating times are less "swimming" and more "playing with water".

![underwater selfie in University Arms pool](selfie.jpg)

When I was on conference trips, I wouldn't forget my swim suits, and jump in hotel pools whenever I have a chance.

![Westin Bellevue pool](westin.jpg)

## Conclusion

Swimming was my primary form of exercise during my first [three years at University of Arizona](/t/2017/six-years-in-arizona/).
I added [weight lifting](/t/2017/heavy-iron-stuff/) on my fourth year but never stopped swimming.
I enjoyed floating in the pool as an exercise and as a relaxation.
