---
title: My First Days in Tucson
lang: en
date: 2017-08-06
tags:
- life
- Arizona
image: pool.jpg
---

Six years ago, on Apr 12, 2011, I was admitted as a PhD student in the Computer Science PhD program at The University of Arizona.
After a round of paperwork and an interview for the US visa, I flew from Shanghai, China to Tucson, Arizona on Aug 04, 2011.
This was when it all began.

## The Flight and Arrival

My uncle bought me an one-way flight ticket.
I was booked on United flight 588 departing PVG at 20:10, transferring to United flight 6458 arriving TUS at 21:10.
The time difference between Shanghai and Tucson is 15 hours, so this seemingly 1-hour flight route is actually 16 hours.

My dad drove me to the airport, with mom and grandpa also in the car.
After saying goodbye to my family, an officer stamped my passport indicating that I had left China territory.
I had two checked bags full of clothing and cookware, a duffle bag with more clothes, and a briefcase containing an old laptop.
I also had thirteen $100 bills, a few $20 and $10 bills, and several quarter-dollar coins; this was all the cash I had.

![say goodbye to grandpa](goodbye-family.jpg)

About 12 hours later, I found myself in front of a CBP officer, where I "petition to enter the United States".
The CBP officer asked me what school I would be attending and the starting date of the semester, instructed me to scan my ten fingerprints, and then stamped my I-94 form with "D/S" indicating that I am authorized to remain in the US as long as I am a student.
At this point, I am officially in the United States, with little risk of getting sent back.

A few more hours later, I was in the welcome lounge of Tucson International Airport.
I spent two of my very limited quarter-dollar coins to call the contact of [The Association of Chinese Students and Scholars (ACSS)](http://acss.clubs.arizona.edu/) who agreed to send a volunteer to pick me up at the airport, but the call did not go through and the phone ate my coins.
Nevertheless, I met the volunteer after a few minutes.
He introduced himself as Jonathan from [Tucson Chinese Baptist Church](http://tcbcf.org/), and he drove me and another student to a motel.

Jonathan's pickup truck arrived at **Grant Inn** just before midnight.
The room price was $50 per night including taxes, and it was shared between two students so I only needed to pay half of that; I paid my share with a credit card from China, and didn't have to spend cash.
After settling into the room, I turned on my laptop, my only Internet-connected device, and made my [very first check-in on Foursquare](https://twitter.com/yoursunny/status/99379691298893824).

## First Visit of UA Campus

The next morning, I spent an hour on the laptop researching the [city bus system](/t/2017/SunTran/), and found that there was a bus stop right in front of the motel.
Each bus ride would cost $1.50, "exact change only".
The innkeeper at Grant Inn agreed to exchange my $10 bills with $1 bills and quarter-dollar coins, so that I could ride the bus.

I took a route 20 bus, and reached the University of Arizona campus for the first time.
The most important task for this visit is to open a bank account, so that I won't lose my $1300 of cash.
The admission packet that the university sent me contains information about Wells Fargo Bank, which is located right on campus.
I found the bank without much difficulty.
A banker checked my passport, opened my accounts, gave me a packet of seven checks, and helped me deposit my cash into an ATM.
She also explained to me the difference between a checking account and a savings account, how to write a check, and the importance of balancing the checkbook.

## Housing

I started looking for housing shortly after I accepted the offer from University of Arizona.
I [made a spreadsheet](/t/2017/roommates-or-not/) about available apartments, including lease term length, monthly rent price, included utilities, distance to campus, and nearby grocery stores.
Eventually I chose [NorthPointe Apartments](http://northpointeua.com/).
It is far from campus, but they provide a shuttle on weekdays, and there is a direct city bus route if I need to go to school during the weekend.
Rent price is cheap, and there are two grocery stores and a Walmart within walking distance.

I signed the lease via email one day before flying to Tucson.
The lease says that I couldn't move in until Aug 18, which means I had to stay in Grant Inn for two weeks, which significantly increased my costs.

On Aug 05, I took a bus to NorthPointe after opening the bank account, and paid the "security deposit" and my first month's rent with a check.
The amount was $1097.50.
This was the time I realize that my $1300 cash was barely enough, as only $202.50 was left after paying the deposit and rent.

I wasn't able to tour the room on that day, but the girl pointed to the pool behind the leasing office, and told me that "you can jump in, it's super cool".
I didn't because I was wearing jeans and didn't have any clothes to change into.

P.S. Five months later, in the freezing January, I jumped in the pool wearing jeans and [had a picture taken](https://www.facebook.com/NorthPointeUA/photos/a.10150537327398643.394563.189462203642/10150537327483643/?type=3&theater):

![jump in the pool wearing jeans, courtesy of NorthPointe Apartments](pool.jpg)

## Life at Grant Inn

I recharged my Google Voice account, so I could call my family in Shanghai.
They were happy to hear from me.

ACSS volunteers were bringing in more and more Chinese students, we were all chatting with each other.
President of ACSS helped arrange room sharing, ensuring every room had two or more students, to reduce costs for everyone.
On some nights, I cuddled with another boy on the same bed, so that each of us only had to pay $15 per night, while the guy having a whole bed paid $20.
I would happily take their cash, swipe my Chinese credit card to pay the hotel charge, and deposit the cash into my bank account.

Grant Inn has a [small swimming pool](/t/2017/floating-in-the-pool/).
I went swimming almost every day.
Poolside was also a popular place for us Chinese students to socialize and talk about our new life.
We became good friends, and I had a [road trip](/t/2020/SanDiego/) with two of them a few months later.

The motel does not have an on-site restaurant, but it is adjacent to a Circle K gas station which sells food.
Every evening, I would go to Circle K and grab a sandwich or a salad, which comes with a free soda pop that is more than 1 liter.
I discovered **sales tax** at Circle K, but couldn't understand why sandwiches are taxed while babanas are not.
Taxes do exist in China, but everything is priced inclusive of taxes, so I'm not used to being taxed on top of the labeled price.

I discovered there is a Fry's Food Store on the bus route between UA campus and Grant Inn.
I suddenly had more dinner choices than Circle K's sandwiches and salads.
I bought some fish, and cooked them in the microwave, without any spices.
I bought frozen packaged dinner, which were also heated in the microwave.
My favorite was all kinds of potato salad, sold in half pound tubs.

The bed in Grant Inn differs from beds in China.
In China, we sleep under a comforter, which has a double-sided comforter cover outside and a cotton or wool comforter inside.
An American hotel bed has two sheets below a blanket, and I'm supposed to sleep in between two sheets.
Roommates and I often mess up the sheets.

![I made the bed](bed.jpg)

One day I watched the maid servicing my room.
She taught me the proper way to sleep on the bed, and showed me how to make the bed.
This bed-making skill was soon put into use when my roommate refused maid service one day because he lost an important document.
After he located his document, I serviced the room, and my roommate thought my work was quite close to what the maid did.

## Contract and Orientations

My admission to the Computer Science PhD program comes with a research assistant position.
On Aug 08, 2011, I went to computer science department, and signed my paperwork.
That day was also the first time I met Professor Zhang, who would become my advisor and dissertation committee chair.
He gave me a few scientific papers to read.
Things are about to get real!

The **International Student Orientation** was on Aug 11-12.
We were asked to stand up to represent the country students came from, and I noticed there were many Chinese and Indian students; I was also surprised when "Taiwan" was named as a country, in contrast to "a province of China" as I was taught in Chinese schools.
We were reminded the importance of not losing the passport and other documents.
We were made aware about various resources available on campus such as the library and the campus health.
We were given safety tips by a police officer, who has a firearm on his belt; police officers in China do not carry firearms.
I asked one question during the orientation "is it safe to drink tap water?" the nurse from campus health gave me a positive answer.
This orientation is also where I learned about the [International Student Association](/t/2017/ISA/), then named "Meet The World" club.

The **Graduate Student Orientation** was on Aug 17.
Every student was given a free T-shirt and a free lunch.
Several sponsors attended this orientation, including [Sun Tran](http://www.suntran.com) the city bus agency, and [ArizonaShuttle](http://www.arizonashuttle.com/) which provides transportation to Phoenix.

Computer Science department also had an orientation on Aug 19.
We were told how to get an account to use department computers, how to use the printer, etc.

## Shopping

I needed a backpack, because it was a hassle to carry a briefcase for the laptop and a camera bag for the camera at the same time.
Although I had many backpacks back in Shanghai, I didn't bring any of them because my flight ticket said I'm only entitled to one carry-on (the duffle bag) and one "personal item" which could be a "laptop bag" (the briefcase) but backpack is off-limits.
The brand that came to my mind was **Walmart**, the biggest retailer of the world.

Google Maps says there is a Walmart "near Tucson Mall", which can be reached if I ride route 6 bus all the way to the north end; but I did not look carefully where it is.
On Aug 10, I set out to find Walmart.
I reached Tucson Mall without any problem, and had spaghetti with meatballs for lunch.
Then, I started looking for Walmart.
I saw a truck printed "WAL★MART" across a bridge, so I walked there.
I found the store named "Sam's Club", but I walked in anyway.
An associate told me that this store is not Walmart, although they are related to Walmart.
She also gave me directions to find the real Walmart.

After 15 minutes of walking, I found myself in a "Walmart".
It was a big supermarket, selling everything from clothes to home appliances.
I selected a black backpack, and also bought a 1.25 liter bottle of diet Pepsi for $2, thinking it's cheap because the same bottle would sell for ¥6 in China.

Another purchase I made is a **cellphone**.
Without a cellphone, the only way I can communicate is to carry around the bulky laptop, and open up Google Voice whenever I have a chance.
I researched online to learn about the major cellphone service providers in the US.
I visited cellphone shops in Tucson Mall to ask for prices, including CenturyLink which doesn't even sell cellphone (and they referred me to Verizon).
I talked with fellow Chinese students at the Grant Inn, some of which were inviting me to join their "family plan".

Eventually, I decided to join T-Mobile's $30 pre-paid plan, rather than a post-paid plan, so that I could have better control over my spendings.
I saw an LG brand Android smartphone on T-Mobile's website, but I wanted a phone "now" rather than waiting a week for delivery.
On Aug 12, I took a bus to T-Mobile's store in Tucson Mall.
Unfortunately, the LG phone I wanted was unavailable at this store.
I was recommended the T-Mobile Comet, which also has Android operating system.
To pursuade me into this purchase, the sales representative pulled out his personal phone, which is a Comet!
Thus, I agreed to buy the T-Mobile Comet, believing that "all Android phones are the same because they are after all Android" (which isn't true; similarly, not all computers are equal even if they all have Windows).
A few minutes after swiping the credit card, I got my phone number (that I couldn't select): 520 269 8772.

The Comet has a camera, so I can take picture anywhere without carrying a bulky DSLR camera.
However, I cannot freely post the pictures, because the $30 pre-paid plan comes with only 30MB of data, which would run out rapidly after posting just a few pictures.
On the other hand, tweeting without pictures is practically unlimited, because the $30 plan comes with 1500 minutes of talk or text messages.

After I returned to Grant Inn, I installed Foursquare so I can check in without using the laptop.
I also installed [Talking Santa](https://play.google.com/store/apps/details?id=com.outfit7.talkingsantafree&hl=en), and let my Santa talk with my roommate's Talking Tom.

## Moving to NorthPointe

![NorthPointe living room](northpointe.jpg)

I moved from Grant Inn to NorthPointe Student Apartments [by taxi](/t/2017/hailing-taxis/) on Aug 18.
This concludes my first days in Tucson.
My stories at NorthPointe [continue in the next article](/t/2017/roommates-or-not/).
