---
title: Chinese Gym vs American Gym
lang: en
date: 2017-10-24
tags:
- life
- China
image: deadlift.jpg
---

After graduating from University of Arizona, I went back to China for six weeks to apply for a new U.S. visa.
Since [weightlifting](/t/2017/heavy-iron-stuff/) has become a part of me, I continued my workouts in a Chinese gym.
The experience there is significantly different from American gyms.

## Annual Membership, Please

Most gyms in China only sell annual memberships.
Gym managers are secretly wishing that you would give up your ambitious exercise plan after several weeks, so that they could profit off you without devoting resources.
My stay in China was six weeks, so an annual membership would be overkill and prohibitively expensive.
I had to find a gym that offers single tickets.
My gym of choice was "Fitness Club" (菲特妮斯健身会所) located in the basement of a grocery store.
They agreed to offer me single tickets at ¥50 per visit.

In United States, most gyms are happy to sell you a day pass.
Some gyms even offer a few free tickets so that a potential customer can experience the facility; this is also perfect for short-term visitors.

In terms of price, ¥50 per visit is about as much as $8 charged by University of Arizona Campus Recreation Center.
However, personal income in China is much lower than the United States, which means a Chinese gym costs more than an American gym for local people.

## It's Hot in There

I bought my tickets and entered the gym.
As soon as I stepped under the barbell, I began sweating profusely.
The gym, located in a basement, is hot and humid, and the air conditioners are barely useful.

![dead lift at Fitness](deadlift.jpg)

I asked a gym attendant about the temperature.
The response was: "the gym is supposed to be hot, so that you can sweat and burn more calories".
In other words, "this is not a bug, but a feature".
Well, a hotter environment may be good for cardio, but overheating definitely affects weightlifting performance.
I had to pause longer between sets and between exercises, in order to get through a lifting session.

In contrast, all American gyms have strong air conditioning.
Both cardio and weightlifting sections are cool and comfortable.
The only thing that feels hot is the particular muscle I am training.

## Boiling Water Only

Tap water in the United States is portable.
Water fountains are widely available in public places including gyms.

![water fountain at UA Campus Rec](fountain.jpg)

Tap water in China is not portable.
Many families and offices have installed water filters so that one can enjoy cool drinking water at any time.
However, the gym only supplies boiling water that takes forever to cool to room temperature.
Without sufficient water, I cannot cool off my overheating body fast enough to continue my exercises.

## Nobody is Wiping or Reracking

Two notices are commonly seen in American gyms: wipe down equipment after use; rerack weights.
In practice, most members are good enough to follow these rules.

In Chinese gym, paper towels and spray bottles do not exist, so that wiping the equipment is impossible even if one wants to.
The gym attendant told me that their staff would wipe down the equipment "daily".
In other words, the exercise bench I'm using contains the sweat from a whole day.
Ewww.

Nobody is reracking weights, partially because rack space is insufficient to hold all plates.
Weight plates are scattered on the squat rack, on the bench press, on the Smith machine, etc.
Dumbbells are dropped all over the floor.
If you need a specific weight, you'll need some good luck to find one all across the gym.

## The End

I am disappointed in the sad situation of Chinese gyms.
I visited the "Fitness Club" five times.
At minimum, I kept my muscles from shrinking.
This is all I ask for.
