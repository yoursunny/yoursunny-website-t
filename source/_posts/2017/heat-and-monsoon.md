---
title: Heat and Monsoon
lang: en
date: 2017-08-23
tags:
- life
- Arizona
image: flooding.jpg
---

Tucson has four seasons: spring, summer, monsoon, and winter.
Located in the desert, the climate is very different from my hometown Shanghai.
I have been accustomed to Tucson's climate after [living here for six years](/t/2017/six-years-in-arizona/).

## Summer Heat

The summer is extra long.
[@TheTucsonHeat](https://twitter.com/TheTucsonHeat) arrives in April or earlier, and would not leave until Halloween at end of October.
Thus, seven of the twelve months belong to the summer.

Heat is the number one weather related killer in Arizona.
House cooling is not optional.
Heat warning signs are everywhere:

!["the heat is on" sign in Joel D Valdez Main Library](heat-is-on.jpg)

Hiking on hot days is forbidden.
In just one weekend of 2016, [the heat killed six hikers in Arizona](http://www.azcentral.com/story/news/local/arizona-breaking/2016/06/21/missing-german-hiker-found-dead-tucson-weekend-heat-toll-rises-6/86215376/).

Other outdoor activities are also not recommended after 10 AM.
As I tweeted:

> [Get shopping done](https://twitter.com/yoursunny/status/744152638929907713) before @TheTucsonHeat comes out for his angriest weekend.

> [Get errands done](https://twitter.com/yoursunny/status/744536437626904576) before @TheTucsonHeat melts my bike tire.

> [Get bike rides done](https://twitter.com/yoursunny/status/871799819194552320) before @TheTucsonHeat turns the park into hell.

An exception to the rule is activities in the water, like [floating in the swimming pool](/t/2017/floating-in-the-pool/):

![me at University Arms pool](pool.jpg)

Other than that, all I could do during the heat-dominated summer is to stay inside.
I once programmed an ESP8266 to illuminate a red LED when the heat outside reaches "triple digits" (100°F, or 37.8°C).
The result?
The LED was lit every day, and it's sometimes still over 100°F outside at my bedtime.

![red LED indicates triple digits](led.jpg)

## Monsoon Rains

The monsoon is exactly 3½ months long: it begins on Jun 15 and ends on Sep 30, and intervenes with the summer.
During these months, [@ElTucsonMonsoon](https://twitter.com/ElTucsonMonsoon) provides a balance of power and makes sure the heat does not go out of hands.

Although monsoon takes a secondary role, it gives me endless joy.
I can [#KeepCalmAndGetDrenched](https://twitter.com/hashtag/KeepCalmAndGetDrenched).
Yup, that's a wet push-up:

[![22 push-ups in the rain](pushup.jpg)](https://youtu.be/gQMOgzaZXSw)

I can watch the beautiful clouds which blocks off the heat:

![clouds on Aug 30, 2015](monsoon-clouds.jpg)

There is even a [festival](/t/2017/Tucson-festivals/) to celebrate monsoon: [Return of the Mermaids](https://www.facebook.com/TucsonMermaid/).
In 2016, it was accompanied by lightning:

![lightning on Aug 13, 2016](lightning.jpg)

Monsoon also gives Tucsonans lots of trouble.
When it rains, streets turn into rivers:

![flooding on Mabel / 3rd Ave, Jul 01, 2016](flooding.jpg)

Residential trashcans are running like boats:

![trashcan washed away, Dunbar Spring Neighborhood, Aug 10, 2017](trashcan.jpg)

In 2016, @ElTucsonMonsoon not only ruined my phone, but also entered our computer lab to "research burrito specials", and our lab staff was unhappy about that:

![water leak in computer lab, Aug 10, 2016](computer-lab.jpg)

Monsoon is also the number two weather related killer in Arizona.
The blame, however, is on the stupid motorists who decide to drive through running water.
I [never owned a car](/t/2017/behind-the-wheel/) but I would remember to [Turn Around Don't Drown®](http://www.nws.noaa.gov/os/water/tadd/) when I drive.

## Honorable Mentions

Apart from summer and monsoon, Tucson also has spring and winter, and probably autumn.

[Spring](https://twitter.com/TucsonSpring) is pleasant.
It is suitable for hiking, [bike riding](/t/2017/on-two-wheels/), [geocaching](/t/2017/start-geocaching/), and all other outdoor activities.
You can see the wildflowers.

![wildflowers along Sabino Canyon Phoneline trail, Mar 17, 2017](wildflower.jpg)

Autumn technically exists, but I'd rather count it as part of summer because [@TheTucsonHeat overstays his welcome](https://twitter.com/TucsonWinter/status/798693418637504513) every single year.
When autumn approaches, the leaves on this tree in front of the student union would turn red, to remind everyone that the winter they have been missing is coming.

![tree turning red, Dec 06, 2013](red-tree.jpg)

[Winter](https://twitter.com/TucsonWinter) is the most expected season of the year.
When it is burning hot in the summer, everyone is talking about what they would do when winter comes.
It is not as cold as the northern states, so [a pair of gloves](https://amzn.to/2g36e3Y) is probably all you need for winter clothing.

![glove for winter](glove.jpg)

Clouds are better in winter:

![clouds on Jan 09, 2015](winter-clouds.jpg)

However, if I have to pick my favorite season, it would be the summer, because it is the longest season so I could be happy for 7 months every year.
