---
title: Share LaTeX Document on Netlify
lang: en
date: 2017-08-18
tags:
- LaTeX
---

My dissertation is finally finished.
Before finishing my dissertation, I was eager to share it with my classmates so that they could read and give me suggestions.
Normally, I would give them read-only access on the Git or Subversion repository containing my dissertation, so that they can check out the LaTeX source files, and then compile the dissertation.
This procedure creates a hurdle for them, because LaTeX is normally not installed on mobile devices they might use.

For most people, the solution would be sending PDF attachments.
However, when I add more and more text and pictures into my dissertation, the PDF becomes increaseingly large, and I don't want to send large files and use up the quota of my and my classmates' mailboxes.
There is a better solution.

[Netlify](https://www.netlify.com/) is a free hosting service for static web sites.
It provides a Node.js client app, which can deploy a prepared website from the command line.
It can also pick up commits into a git repository, and compile the website from source code.

While my dissertation is not a website, hosting it on Netlify is totally feasible.
Although Netlify's [continuous deployment](https://www.netlify.com/docs/continuous-deployment/) does not support LaTeX, I can still compile the PDF locally, and upload it to Netlify.
To do so, I added a **Makefile** rule:

```makefile
REVISION = `git rev-parse HEAD`

dissertation.pdf:
  # normal build steps

upload: dissertation.pdf
  rm -f www/*.pdf
  cp dissertation.pdf www/$(REVISION).pdf
  echo '<!doctype html><title>paper</title><h1>Named Data Networking in Local Area Networks dissertation</h1><a href="'$(REVISION)'.pdf">revision '$(REVISION)'</a>' > www/index.html
  netlify deploy -t ndn-lan-thesis -p www
```

The Makefile rule copies an up-to-date PDF file into `www/` folder, generates a homepage with my dissertation title, and deploys the `www/` folder as `https://ndn-lan-thesis.netlify.com` website.
The PDF file is named after the git commit, so that my classmates could tell me which draft version they are reading when they send me a suggestion.

I also appended the following to **.gitignore**, so that dynamically generated files would not be committed into the git repository:

```gitignore
.netlify
www/index.html
www/*.pdf
```

With this simple technique, I can easily share a draft of my dissertation or any other PDF document compiled from LaTeX.
Typing `make upload` instantly uploads the PDF to a website on Netlify, so that my classmates could read the latest draft of my PDF document.
