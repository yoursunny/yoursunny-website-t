---
title: Behind the Wheel
lang: en
date: 2017-08-12
tags:
- life
- Arizona
alias: 2017/behind-the-wheels/
image: sundial.jpg
---

[Sun Tran](/t/2017/SunTran/) bus system was my primary form of transportation during my [six years at University of Arizona](/t/2017/six-years-in-arizona/), but it wasn't my only transportation.
Sun Tran services city of Tucson, city of South Tucson, and select areas out of city limits.
When I needed to go beyond the service boundary, or to haul cargo, it makes sense to get my own set of wheels.

I decided not to buy a car during graduate school early on.
When I attended my first "grad tea", a social event among computer science grad students, I asked an American student the cost of owning a car, and the answer was $200 per month including "everything".
A simple calculation indicated that owning a car would not be a good financial decision for me.
Owning a car would allow me to rent an apartment far from the campus, but it is unlikely to find an apartment that is cheaper than $439/mo NorthPointe.

![Zipcar Nissan Altima Brenton](brenton.jpg)

## Arizona Driver's License

Fast forward to spring 2014, when my life at NorthPointe [went downhill](/t/2017/roommates-or-not/) due to a noisy roommate.
I signed a lease at my new apartment, and **moving** was put on the agenda.
I [moved from Grant Inn](/t/2017/first-days-in-Tucson/) to NorthPointe on a taxi, at which time all I had was two suitcases and two bags.
Three years later, I had accumulated many more things than that, and the [taxi driver](/t/2017/hailing-taxis/) would be charging by minute while I'm loading my stuff into his trunk.
Thus, I need a driver's license!

To get a driver's license in Arizona, it is only required to take two exams.
Unlike in China, attending a driving school is optional.
However, I still decided to attend a driving school, because I believed it would make me more confident while driving.

I contacted [Stop and Go Driving School](http://tucsonstopandgodrivingschool.com/).
Given I already received formal driving training in China back in 2009, they suggested me to sign up for the "silver package" which includes a classroom instruction about rules of the road, and two 3-hour in-car lessons with an instructor.
I paid $329 for this package.

![Stop and Go Driving School, permit classroom](stop-and-go.jpg)

The classroom session was on Mar 29, 2014.
Ms Beckert explained the meaning of each road sign, what to do at a stop sign, when it is legal to make a left turn on red light, etc.
She also showed us scary videos about traffic accidents caused by drink-and-drive and road rage.
With her instruction, I gained a good understanding about the traffic law and principles of defensive driving.

I took my "written" exam at Arizona Motor Vehicle Division Tucson East on Apr 04, 2014.
"Written" is in quotes, because the actual exam is on a computer screen.
All questions are multiple choices, and I passed the exam at 96% accuracy.
I received my "instruction permit", which would allow me to practice driving when a fully licensed adult driver (not necessarily a professional instructor) is next to me.
The photo on the permit was taken on the same day, and I was wearing a dark dress shirt from my father, which was used on almost all my ID photos.

![Arizona instruction permit](permit.jpg)

Mr Herrick picked me up for my first in-car lesson on Apr 09.
Unlike in a Chinese driving school where three or four students take turns driving a vehicle, Stop and Go's in-car lesson is one-on-one: I drive continuously throughout the 3-hour lesson.
Mr Herrick let me do a few left turns, right turns, and U-turns in NorthPointe's parking lot, and then we went out of the neighborhood, where he taught me how to deal with stop signs, which (almost) do not exist in China.
I was unable to understand his instruction about parallel parking, something many American drivers have trouble at, but I performed it perfectly according to what my Chinese driving instructor taught me.
Perpendicular parking, a supposedly easier trick, was my trouble.
I practiced it several times, but my success ratio was still less than 50%.

The second in-car lesson was on Apr 16, also with Mr Herrick.
He took me onto I-10 the highway, where I could drive as fast as 65MPH.
In contrast, the curriculum of a Chinese driving school would never involve "unsafe" high speed driving, and I was always told by my Chinese instructor to drive as slow as I need during the exams as long as I don't hit anything.

![Certificate of Completion from Stop and Go Driving School](certificate.png)

Arizona used to allow the driving school to administer the exams, but [since 2012](https://www.azfamily.com/mvd-no-longer-offers-waiver-to-road-test/article_08966337-03c8-593b-b0df-6296aa90113c.html), everyone must take the exams at a Motor Vehicle Division office.
Stop and Go would not lend their vehicle for me to take the exams.
My friend [Ethan Huang](https://www.facebook.com/yi.huang.560) was kind enough to let me borrow his car for the road test.

I drove his Mazda to Tucson North MVD on May 01, 2014, and attempted my first road test.
An officer told me to move the car toward the "parallel parking" test area, where she would be waiting.
However, I misunderstood her instruction, and started performing parallel parking before she arrived.
She was unhappy that I did not "follow the command", but allowed me to continue after I backed out of the test area and started over.
I successfully passed the parallel parking test, and she climbed into the car and instructed me to drive around the block.
I was very cautious about stop signs, red lights, pedestrians, oncoming vehicles, and other hazards.
However, as I pulled into the MVD lot, I was told that I failed the exam, because my right turns were so slow that I was blocking the flow of traffic.
Evidently, this is a bad habit from Chinese driving school: you can drive as slow as you want in a Chinese road test, but you cannot drive as slow as a pedestrian in Arizona!

I had to re-take the exam on the following day.
I paid attention not to drive too slowly, and successfully passed the test.
My license was printed in 10 minutes, and I am officially an Arizona driver!

## Night Driving Practice

All in-car lessons at Stop and Go are offered during daytime, but I feel I would benefit from some nighttime driving.
Although there are other driving schools offering night practice, I did not want to pay another $178 for a 2-hour lesson.
My friends are already laughing at me for paying $329 to attend a driving school, which they all skipped.

I went back to Shanghai right after I received my Arizona driver's license.
Therefore, I turned to my father for night driving training.
Once a week, my father would let me drive to a nearby industry park or, occasionally, busy city streets, where he taught me how to drive safely at night.

![night driving in Shanghai](night.jpg)

The one thing my father could not teach me is how to turn on the lights in every vehicle.
It is critical to turn on vehicle lights, but different vehicles have different light switches.

## First Rental Car

When I'm back to Tucson at end of July, moving is imminent.
I booked a rental car from [Enterprise](https://www.enterprise.com/en/car-rental/locations/us/az/speedway-501m.html), based on my previous positive experience during [a road trip in 2011 Christmas](/t/2020/SanDiego/).
Enterprise is famously "the company that picks you up".
I just need to call them, and someone from the local office would come to me, drive me to their office, and get me on the road.

![Enterprise Hyundai rental car](hyundai.jpg)

On Jul 29, 2014, I moved [from NorthPointe to University Arms](/t/2017/roommates-or-not/) and began my new life of living alone with this car.
Additionally, I experienced fast food drive-through for the first time, and drove up A Mountain.
I waited on A Mountain until sunset, after which I would have to drive downhill in the darkness.
I remembered to turn on the lights, and I walked around the vehicle to confirm all the lights were actually turned on before pulling out of the parking lot.
Then I realized I didn't take the mountain driving lesson, and my father didn't teach me because there are no mountains in Shanghai.
Nevertheless, I managed to get the car down without running into anything, and parallel-parked next to my new apartment.

The next morning, I fuelled the vehicle at Safeway, practiced perpendicular parking in Target's parking lot, and returned the car.
I drove 49 miles.
Including car rental price, insurance, and fuel, this 24-hour drive cost me about $70.

## Zipcar

Since I got my Arizona driver's license, I decided that I should drive at least once every three months.
The reason being, if I don't drive often, I would forget how to drive and would have to attend driving school again, which is a significant cost compared to rental cars.
On the other hand, Enterprise rentals are priced by the day, and I don't really need to spend a whole day to "practice" driving.

Just as I was weighing my options, [Zipcar](http://www.zipcar.com/) arrived at University of Arizona.
In Nov 2014, after walking around UA campus and seeing the fleet, I applied for Zipcar membership.
First year's $25 of membership fee was waived, and I received a "booklet" of coupons during the first few months which gave me more than 30 hours of free driving time.

After some planning, my first Ziptrip was on Dec 30, 2014.
I chose a Ford Focus, because it appeared in The Amazing Race, my favorite TV show.
I recruited three friends to join me on a day trip to [Titan Missile Museum and Biosphere 2](/t/2017/Tucson-museums/).
I even got one of them interested in obtaining an Arizona driver's license, and I drove her to Green Valley MVD to pick up a manual.
On the way back, the driver in another car yelled at me saying that my lights weren't turned to the correct brightness setting, and I quickly corrected the situation; I knew this would happen someday.

![group photo in front of Titan Missile Museum sign](titan.jpg)

After this 24-hour trip, I took a few short Ziptrips to buy groceries, [eat at good restaurants](/t/2017/Tucson-restaurants/), or just to enjoy the (fast but within legal limits) speed driving on a highway.
The last category, highway joyrides, do not cost money beyond the car rental itself, because all highways around Tucson are free, and they are aptly called "freeway".
This is unlike many highways in Shanghai which charge a "passing fee" (toll) based on distance.
[Jonathan](/t/2017/first-days-in-Tucson/) once said that the highways are funded by taxes, and "you are benefiting from the highway even if you don't drive because things you buy come from there".

![Zipcar Hyundai Elantra Sedan Sundial](sundial.jpg)

I kept my Zipcar membership for two years, and drove nine times.
Advantages of Zipcar include:

* It is convenient to rent a Zipcar compared to an Enterprise.
  Vehicles are located on UA campus.
  I can reserve a vehicle through the website and unlock it with a smartcard, and don't have to sign a contract every time.
* There is a variety of vehicles to choose from.
  I've experienced Ford Focus, Volkswagen Jetta, Toyota Prius, Honda Fit, Ford Escape, Nissan Altima, and Hyundai Elantra during my Ziptrips.
* Every car has a nickname and a personality.
  Shall I call the car a "she" or a "he"?
* Fuel is included in the price.

The main drawback of Zipcar is their late fee policy: if I return a vehicle a minute late, I would be charged $50 because I would "leave the next Zipster hanging".
This created constant stress during my Ziptrips, especially those short ones.
Fortunately, I never incurred the late fee, although there was one trip that almost went badly.

## Zipcar ONE>WAY in Boston

That one trip was the only Ziptrip I took away from Tucson.
I went to Boston, MA for a conference on May 31, 2015.
Headquarters of Zipcar is located in Boston, and the city is littered with Zipcars in every corner.
They also offer a unique program not found elsewhere: ONE>WAY.
A ONE>WAY rental allows a Zipster to pick up a car at one location, and return it to another location.
It appeals to me because I thought I could spend less than [taking a taxi](/t/2017/hailing-taxis/), and I could eat at a Chinatown restaurant, which I couldn't stop at if I were on a taxi.
A Boston cabdriver warned me not to, but I still decided to take a chance.

![tweets about Boston Zipcar](boston.png)

My flight landed at Boston Logan Airport (BOS) just after 17:30, and I booked a ONE>WAY from the airport to a spot near my hotel.
The reservation was for two hours: it takes 30 minutes to drive straight to hotel, Chinatown is along the way, and I would have one hour for the dinner.
The trip went wrong as soon as I arrived at the airport's rental car garage: I saw a row of Zipcars, but the one I reserved was nowhere to be found!
I called Zipcar customer service, but they had no idea where the ONE>WAY cars are, and the only way they could help was to make the car honk.
When I eventually found the car, it was already 18:20, 20 minutes into my reservation.
They agreed to adjust my reservation to start at 18:30 and end at 20:30.

![Zipcar ONE>WAY Honda Fit Twyla](twyla.jpg)

The cabdriver was right: Boston's roads and traffic are indeed "pain in the ass".
Chinatown is straight ahead from the airport, but the bridge charges a toll, and my [HERE Drive+](https://www.microsoft.com/en-us/store/p/here-drive/9wzdncrfhwjp) was configured to avoid tolls, so I ended up driving the long way through Chelsea and Charlestown.
Parking garage in Chinatown charges $8 per hour, and parking spots are so tiny that I almost hit another car.
The roads are as complex as those in Shanghai, I kept missing highway exits, and eventually I had to let my travel companion [Teng](https://www.facebook.com/philo.Teng.Liang) watch the GPS screen and help with navigation.
I returned the car at 20:27, only 3 minutes before the late fee would start.

The receipt came three hours later, and it wasn't cheap at all.
I was charged $6 "consolidation facility charge fee" for starting the trip at the airport garage, and a $10 "once per year" tax although I would drive in Boston only once in that year.

![Zipcar ONE>WAY receipt](receipt.png)

## Back to Traditional Car Rental

I cancelled my Zipcar membership after two years.
To fulfill "drive once every three months", I had to rent Enterprise and the likes again.

Fortunately, 2016-2017 was job interview season.
If the company offers a choice between a rental car and reimbursement of taxi fees, I would choose a rental car every time.
I drove an Avis in Seattle, a Hertz in San Jose, and a Sixt also in San Jose.
Apart from attending the job interview, it was some combination of seeing museums, hiking, eating at fine restaurants, and just enjoying the highways.
The only thing scared me was a "severe tire damage" warning sign at airport's car return entrance; I got off the car to inspect the device, and understood that I would be fine if I drive forward without stopping or backing off.
During two of these trips, I also realized that I need a "fog driving" lesson, but no driving school is offering that because they can't control the weather.

![foggy morning in Sunnyvale, CA](fog.jpg)

I took two road trips on Enterprise in 2017.
I drove to Yuma, AZ on my birthday to attend a [Geocaching mega event](/t/2018/YumaMega/).
That trip also involved some driving along a rough road across U.S. Army Yuma Proving Ground, during which I could hear small rocks bumping onto the vehicle.
The car came back "dirty", but the manager at Enterprise did not find any damage.
The other trip was with my mom when she came visiting me, going to Sedona, AZ.
Mom was approval of my driving skills.

![Enterprise rental car on a rough road](yuma.jpg)

## Conclusion

I feel "free" when I drive on the open road.
Once I get past the stress in dealing with traffic and vehicle lights, it is all happiness.
Attend a driving school, get a driver's license, turn on the lights, drive safely and defensively, and enjoy!
