---
title: Hailing Taxis
lang: en
date: 2017-08-13
tags:
- life
- Arizona
image: lincoln.jpg
---

When I was little, taxis were my favorite form of transportation.
I could have my own seat in the taxi, and the taxi would go directly to the destination.
I would not need to walk to the bus stop, wait for the bus, get squeezed in the bus, and walk to the destination after getting off the bus.
In early 1990s, taxi fare of a 5KM trip was about 30 times the bus fare.
However, I didn't understand the value of money at that time, so I always wanted to travel by taxi.

I started attending a boarding school since the seventh grade.
Mom would give me ¥65 every week, and I was allowed to spend the money however I wish, but this money was all I have for meals, transportation, and anything else except books and school supplies.
While I could spend ¥5 to take a taxi from the bus terminal to the school, I usually chose to walk 20 minutes and save the money for snacks.
I got used to bus rides and long walks, and forgot about taxis, when money became a constraint.
In fact, I got so used to public transportation that, even if I was on a business trip when I later worked for a company, I would prefer to take a bus instead of a taxi, and I sometimes had to explain to finance why I could not produce a receipt for a bus ride.

When I came to Tucson in 2011, having limited cash and being accustomed to public transportation, my primary form of transportation is of course [the city bus](/t/2017/SunTran/).
However, the operating hours of city buses are limited.
When I [stayed in Grant Inn during my first days](/t/2017/first-days-in-Tucson/), the last route 20 bus from UA campus back to Grant Inn was departing at 18:20.
If I wanted to stay at school later than that, I would have to find my alternate transportation.

## Clint's Taxi

When I was dropped off at Grant Inn, the volunteer gave each student a business card of "Clint's Taxi", and told us to call Clint when we need to go to school.
Since buses are cheaper, I took the city bus most of the time.
On Aug 14, 2011, I attended a "Campus Race" scavenger hunt activity, organized by an organization called Meet The World, which later became [International Student Association (ISA)](/t/2017/ISA/).
The activity lasted until 20:00, at which point there were no more buses available.
This was when I called Clint for the first time.

Clint showed up with his van.
He does not speak Chinese, but his accent is "compatible with Chinese students" so his had the volunteer pass business cards to us.
Knowing I should tip the cab driver, I asked "what is the price including tax and tip".
Clint told me that the price including tax would be $10, while the tip is optional and should be paid only if I received good service.
I had no idea how much I should tip, so he suggested "at least 10%", and thus I paid $1 as tip.

Clint served me a second time when I moved from Grant Inn to [NorthPointe](/t/2017/roommates-or-not/), sharing a taxi with another student.
Since he helped loading and unloading the bags, I paid 20% tip on my portion of taxi fare.

For the next two years, Clint's taxi, or "Transportation And eXcellence Integrated LLC" as he named his company, was my go-to choice when I needed a ride outside Sun Tran's service hours.
Clint would show up at 23:00 in the evening or 06:00 in the early morning, as long as I requested the trip in advance.
He usually drove a white van, but he once drove me in a luxury 2004 Cadillac.
Every time it's Clint himself driving.
He admitted that his company had multiple cars but only himself as the driver, because he couldn't find other drivers that he's satisfied with.

I stopped calling Clint when he nearly caused me to miss a flight.
It was Feb 01, 2014.
I was flying to Honolulu, HI on that day to attend International Conference on Computing, Networking and Communication (ICNC), where I published my first [research paper](/t/2017/academic-papers/) as the first author.
Having a 07:15 flight, I scheduled the taxi at 06:00.
As I [tweeted](https://twitter.com/yoursunny/status/428916110147072001):

> My life is in the hands of a family business.

On the morning of departure, I went downstairs at 05:55, and Clint didn't show up.
I called him at 06:02, but he did not answer.
After 06:10, I decided not to wait for Clint and risk my $700 flight ticket, and called a Yellow Cab, which dropped me off at the airport at 06:53 and I narrowly made the flight.

Clint later explained that his car had a mechanical failure on the highway.
He was willing to make up by giving me a free ride.
Willing to give him another chance, I reserved his taxi for my next flight.
However, due to some misunderstanding, he abruptly cancelled that reservation.
Since then, I stopped calling Clint's taxi.

## The Cool Cab

My next flight was on May 18, 2014 at 08:55.
Although I could ride Sun Tran bus to the airport, I would have to depart around 05:00 in order to reach the airport by 07:30, and that would not be a good way to start a trip that is already more than 24 hours.
After Clint cancelled my free ride, I emailed every company I found in "Tucson airport taxi" search results.
I received four quotes: $32 from AAA Airport Cab, $34 from Tucson Resort Transportation, $35 from Jeannie's Taxi, and $35 from The Cool Cab Company.
Unlike other quotes, Kevin Mannion, owner of The Cool Cab Company, wrote the following in his email reply:

> I'll offer you an exact flat rate of $35.00.
> With your reservation you will receive a confirmation call 30 minutes prior to your pick up from the actual driver assigned to your fare.
> We never miss with our assigned system.
> Beware of others who will only give you an estimate (It's always going to be more).
> This price is exact.
> You may book via email,or by calling.
> Hope to hear from you.

Sold!
Nathan Wilson the driver called me at exactly 06:35 and he was waiting downstairs at my 07:05 departure time.
He deserved a 20% tip, and I emailed Kevin the boss about Nathan's good service.

I took three more trips with The Cool Cab Company in 2015.
The first two went well.
The owner even explained to me why the price of an airport pickup would be higher than an airport drop-off: the airport would charge $5.44 for each pickup.
The last one went wrong: my colleague Teng paid for that trip, and he claimed that he was overcharged.
However, nobody could prove exactly how much was paid because it was a cash transaction.
I did not book with The Cool Cab Company again after this incident, although I did not "blacklist" them.

## Orange Cab

Shortly after I moved into [University Arms](/t/2017/roommates-or-not/), I discovered there is a taxi company around the corner: Orange Cab.
Apart from many orange vans, there is a Lincoln town car parked in front of the house.
I spoke with the man sitting on the porch, who identified himself as the owner of Orange Cab, and quoted $25 for a trip to the airport in that Lincoln.
I had a business trip to UCLA three days later, and he said I could knock on his door when I want to depart.

![Orange Cab's Lincoln](lincoln.jpg)

I knocked on his door on the afternoon of Sep 03, 2014.
He wasn't in the house, but he came back in an orange van while I was trying to explain the situation to his wife.
He asked me to allow 10 minutes while he was getting the Lincoln out of the backyard, and then we were on the way to the airport.
During the drive, he said he was from Pakistan, and gave me some insights about taxicab business.

I reserved Orange Cab again on Feb 04, 2015.
I called their reservation phone number a day before, and the dispatcher mentioned "we have an office near there" when he accepted my reservation.
On the morning of departure, I found an orange van outside my window more than 15 minutes before departure time.
Since I was travelling with my colleague [Teng](https://www.facebook.com/philo.Teng.Liang) who might not be ready yet, I waited inside until the scheduled time.
The driver was not the owner, and he said that the boss had a policy that the driver should be at the pickup location 15 minutes in advance.
I mentioned my previous ride with the owner himself and the price I was charged, but I was told that this time the price would be more because "the owner could lower the prices but we cannot".
This is understandable, and it's not my concern given this was a business trip.

## Taxis in Other Cities

Taxi fares are reimbursable during business trips, but I usually prefer public transportation.
I've ridden Los Angeles Metro bus, San Diego trolley, Bay Area Rapid Transportation (BART), Boston subway, Honolulu "The Bus", etc.
I only used taxis when buses were too inconvenient.

I had mixed feelings for taxis in those "major" cities.
In Los Angeles, one can request Yellow Cabs through the [TaxiMagic](https://my.gocurb.com/invite/26y87t) app.
The first trip was awesome: a taxi showed up in less than a minute.
The second trip went wrong: I requested a ride from UCLA Macgowan Hall by placing a pin on the map, but "the app created an invalid address" and thus the driver could not find me; I almost missed a flight because of that.
The third trip didn't even happen, because TaxiMagic (which has been renamed to [Curb](https://my.gocurb.com/invite/26y87t)) won't allow me to enter "airport" as a destination, and I had to track down a driver on the street, who gave me his business card and asked me to call his cellphone when I'm ready to go.
However, despite all the dispatching problems, Los Angeles drivers provided good service.
My colleague once left his iPhone on a taxi, and the driver immediately returned to airport when we called him.

Las Vegas Strip is served by a bus called "The Deuce", but sometimes I was too tired to ride the bus, and opted for taxi three times.
Two of these rides went well.
There was one driver who demanded a $3 tip over a $10 fare, and I wasn't happy about that; I eventually complied, and I won the $3 back in the slot machine two minutes later.

## Lyft and Uber

Lyft launched in Tucson in Apr 2014, following Uber's launch in Oct 2013.
Advertisements were all over the place, and people were handing out new rider coupons on UA campus.
I was skeptical on those services, so I did not come onboard.

My first [Lyft](https://www.lyft.com/i/JUNXIAO404540) ride was on Mar 15, 2016.
[Kayla](https://www.facebook.com/kayla.stack) brought me out for [a hike](/t/2017/hiking-Arizona/), but as she drove toward the mountains, the car broke down, and we were stranded.
She called a tow truck for her car, and sent me back in a Lyft.
I downloaded the app using the WiFi of a nearby restaurant, entered her invitation code, and requested a ride.
A car showed up in 10 minutes.
The car was clean, and the driver was very polite.

I saved my first [Uber](https://www.uber.com/invite/junxiaos1ui) ride until Mar 07, 2017, when my mom came to visit me in Tucson.
It was also a pleasant experience.
When I sent my mom off to the airport, I helped her create a separate Uber account, which entitled her for an almost free ride, and gave me a discounted ride back to my apartment.

Since I had [Lyft](https://www.lyft.com/i/JUNXIAO404540) and [Uber](https://www.uber.com/invite/junxiaos1ui), I never looked back to traditional taxis.
For a traditional taxi, I have to reserve with a phone call or track down a driver in the street, or send an email in the case of The Cool Cab Company; I have to pay cash or incur a "credit card surcharge"; I would have no idea whether the taxi is coming or there is a mechanical failure.
None of these problems exists in Lyft and Uber.

[Lyft](https://www.lyft.com/i/JUNXIAO404540) and [Uber](https://www.uber.com/invite/junxiaos1ui) are cheaper.
My professor once paid $70 to hire a limo from La Jolla, CA to San Diego (SAN) airport; a year later, I requested a Lyft for the same trip, and the professor was impressed that this trip was less than $20.
For shorter rides, they are 3 times of the bus fare (remember the 30x fare in Shanghai?), and I sometimes request a ride so I can get to the destination faster without waiting for a bus [in the heat](/t/2017/heat-and-monsoon/).

[Lyft](https://www.lyft.com/i/JUNXIAO404540) and [Uber](https://www.uber.com/invite/junxiaos1ui) have nicer vehicles.
Although I cannot request a specific vehicle, I have been picked up in luxury cars like BMW and Cadillac, and all kinds of SUVs.
Sometimes I was also offered candies and bottled waters to enhance the experience, although I never accepted them.

I hailed a traditional taxi once, and it was a disaster.
I walked up to the taxi line at Tucson airport on Jun 16, 2017.
A Yellow Cab driver confirmed that I could pay with credit card without surcharge, so I climbed into the taxi.
During the trip, he was talking on his cellphone without a headset, and was constantly "suggesting" me to withdraw cash at an ATM instead of paying with a credit card so that he could earn a little more.
When we reached the destination, I informed him that there would be no tip because talking on the phone without a hands-free device is illegal in Tucson.
The only advantage of a traditional taxi at the airport is to save the 4-minute waiting for a Lyft or Uber to come out of their parking lot, but I would not trade this wait time with an unpleasant ride in a rude driver's car again.

[Lyft](https://www.lyft.com/i/JUNXIAO404540) and [Uber](https://www.uber.com/invite/junxiaos1ui) are the way to go.
P.S. Click on the above links for a $15 coupon.
