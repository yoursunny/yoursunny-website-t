---
title: Tucson's Museums
lang: en
date: 2017-09-17
tags:
- life
- Arizona
image: MiniTimeMachine-Lagniappe.jpg
---

Visiting museums is a good way to learn about the culture of a region.
During my [six years living in Tucson](/t/2017/six-years-in-arizona/), I found and visited many museums in this city, and here are some of my favorites.

## 5. Arizona State Museum

[Arizona State Museum](https://statemuseum.arizona.edu/) is located on University of Arizona campus.
I knew about this museum since the beginning when I participated in [ISA's very first campus race event](/t/2017/ISA/), but I never paid a visit because I thought it is literally steps away from my office so I can visit "some time".
In August 2017, when I'm days away from graduation, I finally visited this museum.
My visit spanned two afternoons due to the rich content in this museum.

![Paths of Life exhibit in Arizona State Museum](ASM-Oodham.jpg)

The main exhibit at Arizona State Museum, *Paths of Life*, presents the origins, histories, and contemporary lifeways of ten Native American cultures in Southwest America.
While I have interacted with members of Tohono O'odham and Yaqui tribes through their presentations at [Tucson Meet Yourself](/t/2017/Tucson-festivals/), I gained better understanding at this *Paths of Life* exhibit about their culture, religious beliefs, and struggles.
I also learned about eight other indigenous tribes in the Southwest region but further from Tucson.

## 4. Pima Air & Space Museum

I had a passion about aviation since I took my first flight in 2005.
On my 2015 birthday, I spontaneously decided to visit [Pima Air & Space Museum](http://www.pimaair.org/).
The museum sits on the east side of Tucson, just outside the service boundary of [Sun Tran](/t/2017/SunTran/), so I [borrowed a bike](/t/2017/on-two-wheels/).
80 minutes and 10 miles later, I arrived at the museum, sweaty and thirsty.

![selfie with Bumble Bee in Pima Air & Space Museum](PimaAir-BumleBee.jpg)

Pima Air & Space Museum offers two walking tours of the hangars and a tram tour of the outdoor grounds.
I joined all three guided tours, and received valuable information on the most notable aircrafts.
I learned the origins of commercial aviation, "air mail".
I noticed older aircrafts with non-retractable landing gears.
I poked my head into a WWII bomber that has a gun in the tail.
My favorite aircraft in the museum is Robert Starr's Bumble Bee, the world's smallest plane, which isn't much larger than my bed.

![aircraft storage area in AMARG](AMARG.jpg)

The museum also operates a bus tour into 309th Aerospace Maintenance and Regeneration Group (AMARG) in Davis-Monthan Air Force Base.
I joined this tour on Mar 16, 2017, with my mom.
I was shocked to see the vast number of aircrafts being stored by the US Air Force.
These war machines ensure nobody dares to invade America.
On the other end, they are also where my tax dollars went and probably "wasted".

## 3. Titan Missile Museum

[Titan Missile Museum](https://titanmissilemuseum.org/) is one of the kind: it is the only remaining missile silo from the Cold War.
The museum is located south of Tucson near Green Valley, unreachable by bus, and too far for a bike ride.
Thus, I selected Titan Missile Museum as part of [my first Ziptrip](/t/2017/behind-the-wheel/).

![control room in Titan Missile Museum](Titan.jpg)

The museum is small and the guided tour is short, but it is informative.
I learned the history of Cold War, significance of the Titan Program, and how mutual assured destruction prevented a nuclear war.
I also heard stories about the work life and operational routines in the silo, and watched a demonstration of a missile launch toward a still classified "target number two".
The silo is built with hard cold metal, but a heartwarming part is that, in case of a missile launch, the soldiers on duty would have enough food and air to survive for several weeks and they would not have to die for the country.

## 2. Mini Time Machine Museum of Miniatures

I spotted [The Mini Time Machine museum of miniatures](https://theminitimemachine.org/) on Google Maps in 2013, but I didn't have a chance to visit this museum until Jun 06, 2017.

![a miniature living room of Lagniappe exhibit in Mini Time Machine museum of miniatures](MiniTimeMachine-Lagniappe.jpg)

The word to describe this museum is **cute**.
Artists created little houses with little rooms, complete with little humans, little pets, little tables and chairs, little plates and silverware, little lamps, little clocks, etc.
There are even a set of little books with the complete works of William Shakespeare written in tiny letters.
While the miniature dollhouses have no practice use, it is amazing to see them being built.

## 1. Gadsden-Pacific Division Toy Train Operating Museum

My top choice goes to [Gadsden-Pacific Division Toy Train Operating Museum](http://www.gpdtoytrainmuseum.com/).
They only open twice per month.
Admission is free.

![GPD Toy Train Operating Museum](ToyTrain.jpg)

The museum features several model railroad systems with toy trains running on them.
Hobbyists built all exhibits, separated by "gauge" aka the width between two parallel tracks of the railway.
They combine cute little things like Mini Time Machine and heavy metal machines like Pima Air & Space Museum.

I talked to volunteers operating the exhibits, and learned how these toy trains were powered and controlled.
The museum also led me to read Wikipedia articles about railroad systems, and made me interested in [this mode of transportation](/t/2017/cross-country-move/).

The museum also offers a 7½-inch gauge cargo train that guests could sit on for a ride.
Mom and I rode this train on Mar 12, 2017.
An "engineer" controlled the battery-powered train in the front, and a "conductor" sat in the rear to watch the passengers.
The railroad, built in the museum's parking lot, contains a realistic spring switch, and a "railroad crossing" signal to stop real-life cars from entering while the train is passing.
It was an amusing ride.

## Conclusion

These are my experiences visiting some of the museums in Tucson.
The biggest challenge of visiting museums is finding them.
I hope my article could introduce you to a Tucson museum that you do not know before, and you would enjoy visiting some of them.
