---
title: Roommates or Not?
lang: en
date: 2017-08-08
tags:
- life
- Arizona
image: dunk.jpg
---

Food, water, shelter, and warmth are the basic needs of human beings.
Food is available in any restaurant and grocery store.
Tap water is safe to drink, as I asked [at the orientation](/t/2017/first-days-in-Tucson/).
Warmth is not an urgent problem when I arrive because I have enough clothes in my luggage.
The biggest problem is: housing!

There are two main housing options for University of Arizona students: on campus housing and off campus housing.
If I live on campus, I can walk to class and enjoy all the good vibes, but the room is small and costs a ton of money.
Thus, during my [six years in Arizona](/t/2017/six-years-in-arizona), I lived exclusively in off-campus housing.
This article describes the two apartments I lived in, shares my experiences of living with or without roommates, and includes reviews of NorthPointe Student Apartments and University Arms Apartments from a real tenant.

## How I Evaluate Off-Campus Housing

When I started surveying off-campus housing options, I set a few requirements:

* I must have my own bedroom.
  When I was an undergraduate in Shanghai Jiao Tong University, I shared an on-campus dormitory with three other boys.
  We lived in the same bedroom, went to class together, and were good friends.
  The dorm manager enforced strict rules such as "lights off" time.
  However, American off-campus housing has no such strict rules, so sharing a bedroom is going to cause roommate conflicts in the long term, and I'm not really into [cohabiting](/t/2006/trends-of-chinese-family/).
* I want to have my own bathroom.
  I saw too many TV episodes about how American kids make a mess in the bathroom, or spend too long time in the shower in the morning when I would need to go to class.
* I prefer to have individual leases.
  Some property owners would rent out a big house to multiple students on one lease.
  If any tenant quits, the remaining tenants still must pay the full rent.
  I would not want to get into such trouble.
* I can get to school without a car.
  Owning a car is too much hassle for me.

I searched for many off-campus housing options on Google, Craigslist, and [UA's own off-campus housing office](https://offcampus.arizona.edu/).
I made a spreadsheet about what I found:

![my UA Housing spreadsheet](spreadsheet.gif)

Then I started calling or emailing some of my preferred options, and crossing out any apartments that were already fully booked.
Eventually, I signed lease for a room in 3-bedroom 3-bathroom unit at [NorthPointe Apartments](http://northpointeua.com/).
I can have my own bedroom and bathroom, an individual lease.
At a distance of 4.4 miles, it is too far to walk to school, but NorthPointe provides a shuttle that goes to UA campus every hour.

## The Food Sharing Program

I moved into NorthPointe apartment 621, bedroom A on Aug 18, 2011.
My two roommates, Youxi and Jason, arrived within the next few days.
We are all from China.

The three of us soon setup a "food sharing" program.
We took turns to cook meals, and each was charged food costs according to number of meals consumed.
I acted as the accountant, and designed a complicated spreadsheet to calculate how much each person needed to pay.

I wouldn't say this program has no cracks.
There were constant complaints on the dinner table, about two problems: flavor and money.

First, China is a big country and the different regions we came from have different cuisines.
I hated when Youxi put too much salt in the vegetables or when Jason's steak came out bloody.
Likewise, Youxi disliked my undercooked vegetables or flavorless chicken.
To solve this problem, I would plate my portion first, and then add extra minutes or some more salt to the pot, and put Youxi's portion in another plate.

Second, each person consumes a different amount of food, leading to concerns about fairness in food cost charges.
This problem is actually easy to solve.
Since I'm well paid by the university, I wouldn't mind charging them less to make them feel good.

With the above solutions in place, the food sharing program ran well between Youxi and myself for all three years when we lived together.
Jason moved out after one semester, replaced by John who lived for one year.
John brought us delicious Northeastern Chinese cuisine.
Then a Korean, Sam, lived for a few months, who did not participate in the "Chinese food" program since he isn't Chinese.
The last roommate, Yuan, participated for a while but eventually quit because his meal timing was very different.

## Life at NorthPointe

With more than 20 three-story buildings, NorthPointe is a big community.
There are all kinds of social events, most of which revolve around free food.
For example:

* breakfast at shuttle stop on Monday
* Taco Tuesday
* Wing Wednesday
* Thirsty Thursday
* Free Food Friday

People at the leasing office are awesome, especially [Ashley](https://www.facebook.com/profile.php?id=100001343211882) and [Ej](https://www.facebook.com/ej.matchin).
They would go an extra mile to make you happy.

![me on NorthPointe hammock](hammock.jpg)

Amenities are plenty.
My favorites are the gas grills and the hammock.
There are also [a pool](/t/2017/floating-in-the-pool/), a little gym, a basketball court, and a grassy area.
I can even borrow movies, get coffee, and print documents in the leasing office.

Maintenance is just fine.
There is an online ticket system where I can report any damage, but I have no way to know when they would come to fix the problem.
However, if I wrote the report accurately, problems can get fixed within a week or so.
Otherwise, Ashley would be happy to bump my ticket.

![Maslow's hierarchy of needs](maslow.png)

Internet access comes before food, water, shelter, and warmth in this revised Maslow's hierarchy of needs.
NorthPointe provides both wired Internet access and WiFi.
The Internet was terrible during the first semester such that I had to arrange all my TV watching at 4AM.
After a new fiber connection was added, it's peace.
[Pavlov Media](https://www.pavlovmedia.com/), the company that provides the Internet service, threw a party to make up for the network congestion, during which I climbed into a dunk tank.

![me in a dunk tank, courtesy of NorthPointe Apartments](dunk.jpg)

Shuttle was the major headache.
NorthPointe does provide a shuttle, but shuttle service is "first come first serve".
The van had 28 seats, which was far from enough during the start of semester.
I had to go to school a few hours early in order not to miss a class, while my roommates who slept late had to choose between missing a class or [taking an expensive taxi](/t/2017/hailing-taxis/).
Things eventually got better when the 28-seat van was replaced with a 60-seat "Big Blue Bus", but at that time, I had purchased a [city bus pass](/t/2017/SunTran/) so that I could get around without relying on NorthPointe shuttle.

## Moving Out

My first two years at NorthPointe had "world peace".
Things went downhill on the third year, when Yuan moved in.
His meal timing was "very different".
By that, I mean he would cook his meals at midnight.
Worse, he would play loud music while cooking, making it impossible for me to sleep.
Dirty plates were piling up in the kitchen, and the fridge was full of uncovered plates of food.

I had the leasing office organize a roommate meeting to resolve these issues.
Yuan reluctantly agreed to change some of his behaviors, so things became slightly better.
However, given Youxi would graduate at the end of that year, it's uncertain who would take his place, I decided to move out and live alone.

I moved from NorthPointe to University Arms on Jul 29, 2014.
By this time, I have obtained my Arizona driver's license, so I [rented a car](/t/2017/behind-the-wheel/) and drove two round trips.

![fully packed car during moving](moving.jpg)

## Living Alone

[University Arms](http://www.ashtongoodman.com/) offers 1-bedroom apartments at a rate of $535 per month.
It costs more than NorthPointe, but I have the whole bedroom, bathroom, living room, and kitchen for myself.
It feels good not having to share the living space with a noisy or messy roommate.

![a fridge all for myself](fridge.jpg)

I lived in University Arms for three years.
Every day is quiet and peaceful, but plain.
The only amenity is [a little swimming pool](/t/2017/floating-in-the-pool/).
The property has only two buildings and about 100 residents, so there isn't any community event.
Everyone lives his or her life behind the shut doors.

Mr Del Mack and his wife manage the property.
Del wouldn't bother you as long as you are paying rent on time, and he never exercised his right to "inspect" the room during the whole time.
There isn't a ticket system for maintenance, but Del remembered what I complained, and would have them fixed in a few days and sometimes in a few hours.

The nice part of living at University Arms is its prime location.
There is no shuttle, and no need for it.
I can walk to the university's [gym](/t/2017/heavy-iron-stuff/) in 5 minutes, to computer science department or university library in 15 minutes, or to downtown Tucson in 30 minutes.
This means, everything happening on campus or in downtown is now at my fingertip.
I wouldn't have to reject an event because it's too late in the evening that I no longer have a shuttle to go back to my apartment.
I can even call a free [Safe Ride](http://saferide.asua.arizona.edu/) car when it is too dark to walk.

The ugly part is the [Clearwave](http://www.getclearwave.com/) WiFi.
Due to the way they setup the network, WiFi signals could be affected by weather.
Whenever the wind speed was high, the wireless repeaters on the roof would come out of alignment, causing a network latency of more than 1000 milliseconds.
This means, when I type a key on a remote terminal, I would not see the response until two seconds later.
I phoned Clearwave dozens of times until they stopped answering my calls.
Eventually, I had to pay extra money and install a separate CenturyLink DSL Internet to get through my final year when a stable Internet connection became increasingly important.

## Conclusion

If I can afford, living alone is definitely a better choice than dealing with roommates.
I'm glad to have Youxi and John as my roommates for a period of time, where everything is peaceful.
But when someone incompatible appears, I would be stuck in the hell with a 12-month lease and have little power to change anything.
