---
title: Hiking in Arizona
lang: en
date: 2017-08-31
tags:
- life
- Arizona
alias: 2017/hiking-in-arizona/
image: BearCanyon.jpg
---

I was a couch potato before I came to the United States six years ago.
There aren't many outdoor adventures in my hometown Shanghai, because Shanghai is located on a flat peninsula, and the only place that resembles a mountain is the 97-meter [Sheshan Hill](https://en.wikipedia.org/wiki/Sheshan_Hill).
In contrast, Tucson is surrounded by five mountain ranges, and it is a hiking paradise.

## My First Hike

Among the grad students in computer science department, there is an organization called the Graduate Student Council (GSC).
On the same week as [orientations](/t/2017/first-days-in-Tucson/), I attended the first GSC meeting, and learned about various activities organized by GSC.
One of them is a "hiking club".
I joined their mailing list, although I did not know what I was signing up for.

The first hike was on Oct 01, 2011 going to Butterfly Trail.
I took an [early morning bus](/t/2017/SunTran/) to school, and brought "plenty of water for yourself, sturdy hiking shoes, lunch, warm cloth" as instructed by the trip leader.
We departed shortly after 09:15, and it was a long ride in [Jeremy Wright](https://www.facebook.com/12621879)'s car.

![Butterfly Trail, 01OCT2011](Butterfly-forest.jpg)

As soon as I stepped out of the car, I was greeted by a row of tall trees.
Following the group, I walked for several hours under those trees.
The trail is unpaved and we walked directly on the dry mud.
There are even dead trees blocking our way, which we had to climb over or crawl under.
This is very different from most tourist attractions in China, where all walking paths are paved with cement or rock steps, and any blockage would be cleared immediately.

![Butterfly Trail blocked by dead tree, 01OCT2011](Butterfly-blocked.jpg)

This hike was an exercise of my endurance.
Going in was easy, coming out was tougher because the trail is uphill.
I found it somewhat easier to run in front of everyone else, and then stop and take a break while waiting for others to catch up.

After the hike, we stopped for a frozen yogurt at [Kona Beri](https://www.yelp.com/biz/kona-beri-frozen-yogurt-tucson) before heading back to campus.
I felt the hike was "a little more than a piece of cake", and I wasn't particularly tired afterwards.

## Grand Canyon

In spring break 2012, I planned a trip with [my roommate John](/t/2017/roommates-or-not/) to the world famous Grand Canyon.
Since neither of us had a driver's license, we joined [a guided tour](http://web.archive.org/web/20130131005646/http://explorearizonatours.com/grand-canyon-explorer-hike-.agt-01.tour) in which the tour guide could pick us up from Flagstaff.

We spent a whole day on Greyhound buses to get to Flagstaff, including a 5-hour layover in Phoenix during which we visited downtown Phoenix:

![Civic Space Park in downtown Phoenix, 11MAR2012](CivicSpacePark.jpg)

After an overnight stay in a Flagstaff motel and a 2-hour ride in a van, we went on a "mild" hike in Grand Canyon's South Kaibab trail.
The trail starts with a steep section partially covered with ice, where we were given crampons to put on our shoes to prevent slipping.
I saw a hiker walking this trail on bare feet; when I exclaimed "bare feet?" he responded "of course!".

![Grand Canyon, 12MAR2012](GrandCanyon.jpg)

After several zigzags of the trail, we saw the canyon.
It looks narrower than I expected, and the colors aren't as vivid as I saw on pictures.
Nevertheless, the rock formation is wonderful.

During the hike, our expert guide pointed out geographical features in Grand Canyon, and a fossil stuck in the rock:

![fossil along South Kaibab Trail, 12MAR2012](SouthKaibab-fossil.jpg)

The most amazing thing of our expert guide, however, is his huge backpack.
He carried the lunches of all nine people, plus six extra bottles of water in case some of us need them.
I didn't dare to ask how heavy that backpack was.

![tour guide on South Kaibab Trail, 12MAR2012](SouthKaibab-guide.jpg)

I returned to Grand Canyon in April 2016 on [a 3-day camping trip](/t/2016/offline/).
One of the two hikes were on the same South Kaibab Trail, but the landscape is a bit different due to warmer weather.
I felt the trail slighter shorter, as I grew stronger.

![me at Cedar Ridge, 23APR2016](CedarRidge.jpg)

## The Search of a Binary Cactus

The second hike with computer science GSC hiking club was a year later, on Oct 28, 2012.
[Prof Christian Collberg](http://collberg.cs.arizona.edu/) wanted to go hiking to find the binary cactus in Saguaro National Park East, and take some more pictures to be used on the department's new website.

![binary cactus, 28OCT2012](BinaryCactus.jpg)

We found the binary cactus with the help of a GPS, and saw plenty of other cactus as well.
Prof Collberg also bought frozen yogurt for all of us, as a reward of completing the mission.
The sad part is that I lost my CatCard and [bus pass](/t/2017/SunTran/) during this trip, which made my life "complicated" for the next few weeks.

![Loma Verde Trail, 28OCT2012](LomaVerde.jpg)

## When Rock Attacked Me

I was introduced to Sabino Canyon, the hiking area closest to the city of Tucson, by [Association of Chinese Student and Scholars (ACSS)](http://acss.clubs.arizona.edu/).
The hike was on Nov 25, 2012.
We hiked a part of Phoneline Trail, but it was boring because there is neither a breathtaking view like in Grand Canyon, nor a variety of saguaros like in Saguaro National Park.

On Apr 06, 2013, [Laura](https://www.facebook.com/laura.jael) invited me to another hike in Sabino Canyon.
She told me that there are multiple trails in that area, so that I could have a better experience than the Phoneline Trail hike.
She picked me up in the morning, and I found myself surrounded by pretty girls.

!["Junxiao's bitches", courtesy of Kayla Stack](bitches.jpg)

The hike was through Bear Canyon Trail, in which we must wade across a small stream several times.
Our destination was Seven Falls, a series of natural pools connected by seven waterfalls.
All of us swam in the pools.

![swim in Seven Falls, 06APR2013](SevenFalls-swim.jpg)

After having a good time, the unexpected happened on the way out.
My brand new white T-shirt was wet in the pools.
I decided not to put it back on right away, so that the shiny shirt would not be dyed green by the backpack straps.
Thus, I held it in my hands so that the sun could dry it.
Half an hour later, the shirt was mostly dry, so I wanted to put it back on.
Just as I pulled the shirt over my head, I felt a sharp pain in my right foot.
The pain subdued after a minute, so I walked faster in an attempt to catch up with my girls.
They slowed down to wait for me, and I was able to reach them, but the pain came back.
Laura realized something was wrong.
Another hiker passing by looked at my foot, and indicated that I might have twisted my ankle.
He lent me his hiking sticks to get out of the trail, and then I took a shuttle to reach the parking lot according to his advice.

After Laura dropped me off at my apartment, I searched online about "twisted ankle", and found that it would become a serious issue if not treated by a doctor.
I called [Campus Health](https://www.health.arizona.edu/) right away, and a nurse told me to put my injured foot in iced water, and "not walk on this foot at all".
Therefore, I had to jump my way to the bed, the kitchen, or the bathroom with my left foot for the next two days.

I had a morning class on Monday.
Not confident on the 100-meter single-legged jump to the apartment shuttle stop, I asked [Ethan](https://www.facebook.com/yi.huang.560) for a favor to drive me to school.
It was my first time taking the elevator in Gould-Simpson building, as I could not climb the stairs as I usually do.
After class, I took a 30-minute Cat Tran ride to Campus Health stop in place of a walk that usually takes less than 5 minutes, and crawled to the hospital after getting off the shuttle.
I was immediately put on a wheelchair, in which I waited for more than an hour to see an urgent care doctor.
The doctor wrapped my ankle, and loaned me a pair of crutches.
I must walk with those crutches for at least a week, and I should not put any weight on the injured foot.

Life was miserable on crutches.
To attend a class in Harvill building, I had to take a Cat Tran from 6th Street Garage, where the apartment shuttle drops off, to Science-Engineering Library; from there, it would be a 30-minute "short" walk to Harvill, and my armpits were painful.
To buy food on campus, I had to have student union worker pack my food into a plastic bag and hang it on my crutches, because I could not balance a food tray with both of my arms on the crutches.
Grocery stores were off-limits because I estimated it would take me an hour to walk there, so I had to rely on [Youxi my roommate](/t/2017/roommates-or-not/) to do the shopping.
He placed a chair in the kitchen for me to sit on while cooking.
A few days later, when he went out for a trip, I had to live off Domino's Pizza.

Looking back to this incident, I learned a lesson: always be mindful on what you are stepping on.
The direct reason that I twisted my ankle was stepping on a loose rock that moved when I shifted my center of gravity.
The root cause was not paying attention to the trail: my vision was obstructed when I was putting on that shirt, so I could not see where I placed my foot on altogether.

## ISA Hikes

Since the ankle injury, I was very concerned on going on more hikes, because I don't want to walk on crutches ever again.
After resting for almost a whole year while seeing the pictures from my friends, I started hiking again on Sep 14, 2014, because the beauty of nature is calling.
I chose Seven Falls, the same trail where I injured my ankle, as my first destination.
I'd stand up where I fall.
Having learned the hard lesson, I never run on the trail where rocks are present, and I stop walking whenever I could not look where I am going.

![stream crossing on Bear Canyon Trail, 25OCT2015](BearCanyon.jpg)

That hike was organized by the [International Student Association (ISA)](/t/2017/ISA/).
Since then, I joined many ISA hikes.
Five of these hikes were going to different trails in Sabino Canyon area, including two hikes to Seven Falls, two hikes to Blackett's Ridge, and one hike on the boring Phoneline Trail.

![Blackett's Ridge Trail, 24JAN2015](BlackettRidge.jpg)

Three other hikes went to Catalina State Park.
[Kayla](https://www.facebook.com/kayla.stack) called for an "unofficial" ISA hike on Jan 31, 2016.
Four people joined but two of them cancelled, and it was just Kayla and me.
We hiked Sutherland Trail according to a route I planned.
I also tried to find some [geocaches](/t/2017/start-geocaching/) along the way.

![Sutherland Trail, 31JAN2016](Sutherland.jpg)

The "official" ISA hikes were Baby Jesus Trail and Romero Pools.
I secretly enjoyed watching people getting wet in the river, and I [took a dip in the pools](https://youtu.be/G0nrGMEkRSI) myself.

![Baby Jesus Trail, 22JAN2017](BabyJesus.jpg)

## Hiking in Moonlight

[Outdoor Rec](http://rec.arizona.edu/program/outdoor-rec) is a university department that gets members of the university community involved with the natural world through adventurous, human-powered activities.
Although most of my interactions with them were [borrowing Cat Wheels bike](/t/2017/on-two-wheels/), I joined several hiking trips that they were offering.

A hiking trip with Outdoor Rec (OR, formerly Outdoor Adventures or OA) is not as cheap as an ISA hike.
Participants of an ISA hike bring their own food, and pay the driver a few dollars for fuel cost and parking fees.
An OR hike costs $30-$50.
The price includes food, park permits, vehicle cost, equipment rental, and trip leader wages.
Given the higher price, I would choose an OR trip if I could not get the same experience elsewhere.

My first hike with OR was a "moonlight hike" on Oct 16, 2013, going to Catalina State Park.
I joined this trip because I've never hiked at night.
The trip was advertised as going to Wasson Peak in Saguaro National Park West, but then the [government shutdown](https://en.wikipedia.org/wiki/United_States_federal_government_shutdown_of_2013) happened and all national parks were closed, so the location was changed to Catalina State Park.
That's fine!
The Congress did not shutdown the moon, so that I can still enjoy all the benefits of a moonlight park, even in a different park.
The benefits, as advertised, include:

> Traveling by moonlight will provide an interesting perspective of the desert as well as offer a unique view of the city lights of Tucson.
> Quiet hikers may hear the nocturnal song of the coyote or sight a tarantula on an evening hunt.

We watched the colors of a desert sunset, navigated our way in the trail under the bright moonlight enhanced by headlamps, and watched the stars in the sky while chewing apples.
It was truly an interesting perspective that differs from what I would see during daytime.

![Catalina State Park in moonlight, 16OCT2013](moonlight.jpg)

A year later, on Oct 06, 2014, I joined another moonlight hike.
One of the trip leaders happened to [Jen Dempsey](https://www.facebook.com/jen.dempsey.5) from computer science department, and she advertised the trip on computer science's mailing list:

> Outdoor Adventure through the Rec Center is offering a moonlight hike next Monday to Wasson Peak in the Tucson Mountains.
> I heard the trip leader for this particular hike is phenomenal so you should look into it!

[Without fear of another government shutdown](https://en.wikipedia.org/wiki/Continuing_Appropriations_Resolution,_2015) closing national parks again, I signed up for the trip.
We went to Saguaro National Park West as planned.
The trail was long, and steep at some sections.
I was sweating even without [@TheTucsonHeat](/t/2017/heat-and-monsoon/).
On the other hand, there were few rocks that could attack me, so I was probably safe.

![Wasson Peak in moonlight, 06OCT2014](WassonPeak.jpg)

## The Paved Hills

Jen organized a few computer science department hikes after the moonlight hike, but I wasn't able to join any because they all conflicted with my conference trips.
The next time I met Jen was on a "hike" in Tumamoc Hill, on Apr 29, 2015.
The word "hike" is in quotes, because the "trail" on Tumamoc Hill is a paved walking path, so it is not a real hiking area.
It was a good workout, though, because the walking path has a steeper climb than most real trails.
I [returned to Tumamoc Hill on Jul 03, 2017](https://youtu.be/r0Na-24MS4w).

![Tumamoc Hill walking path, 29APR2015](Tumamoc.jpg)

"A" Mountain, the hill on which a giant "A" letter representing University of Arizona was located, is also a paved hill.
There is a driveway to a parking lot on the top of the hill which is also open to bikes and pedestrians.
I went up "A" Mountain many times [by rental car](/t/2017/behind-the-wheel/), in addition to [two unsuccessful attempts by bike](/t/2017/on-two-wheels/).
From the parking lot on top, there are two real (unpaved) but very short trails: one goes to the summit, the other goes to the letter "A".
I've hiked both, and I witnessed a family releasing a balloon carrying a slip of paper containing their wishes from the summit at sunset.

![A Mountain, 20JAN2015](A.jpg)

## Wading in the River

My third hike with Outdoor Rec (OR) was Aravaipa Canyon on Sep 19, 2015.
The uniqueness of this hike was that you have to get wet!
Aravaipa Canyon is a wilderness, with no designated trail.
Wading in knee-deep water is a requirement, with the only alternative being bushwhacking through dense vegetation.
In Seven Falls hike, you have the option of keeping your shoes dry, but in Aravaipa Canyon, your shoes will get wet.
[#KeepCalmAndGetDrenched](https://twitter.com/hashtag/KeepCalmAndGetDrenched), peace.

![wading in Aravaipa Canyon, 19SEP2015](Aravaipa-wade.jpg)

This was also my first time having a taste of OR's full lunch (moonlight hikes only provide snacks).
The food, which is said to account for a major portion of trip prices, was disappointing.
It includes tortilla chips, two cheeses, hummus spread, and some fruits.
I'd rather have a Safeway sandwich.

![picnic in Aravaipa Canyon, 19SEP2016](Aravaipa-picnic.jpg)

Unlike hiking in the mountains, there were no moving rocks that could attack me.
However, we saw a snake on the river banks, but we stayed away from it.

![snake in Aravaipa Canyon, 19SEP2016](Aravaipa-snake.jpg)

## I Hiked Alone

I generally hike with friends, because I do not own a car so I cannot access the trailhead by myself.
However, I hiked alone once.

It was Christmas 2016.
Many Tucsonans were going to Sabino Canyon, and the parking lots at Tucson's favorite hiking area were overflowing.
Pima Country Department of Transportation started [a trial program to run an hourly shuttle to bring visitors to Sabino Canyon from Udall Park](https://www.kold.com/story/34133078/electric-bus-to-sabino-canyon-part-of-new-pilot-program/).
I was delighted to hear this news, because my Christmas week had been boring, and Udall Park is reachable by city bus.

On Dec 30, 2016, I took a morning bus to Udall Park, and caught the shuttle up to Sabino.
Being alone, I chose a safe hike: see the Sabino Dam.
This is a short 30-minute hike on mostly flat terrain, which minimizes the risk of injury without a friend by my side.
With the help of a selfie stick, I took a beautiful picture of myself:

![selfie in front of Sabino Dam, 30DEC2016](SabinoDam.jpg)

This picture has been [retweeted by @VisitTucsonAZ](https://twitter.com/yoursunny/status/814883446820577282), the city's official tourism Twitter account.
My friends later commented that this is one of a few pictures in which I was smiling.
I guess this was influenced by the cute "Grin and Bear Down" shirt.

Following the [geocaching map](/t/2017/start-geocaching/), I hiked to several other places.
I safely hugged a cactus that does not have thorns:

![keep calm and hug a cactus, 30DEC2016](Sabino-hug.jpg)

I also viewed [the cactus trio](https://coord.info/GC4N97P) near the tram road:

![cactus trio, 30DEC2016](Sabino-trio.jpg)

Overall, hiking alone is a different experience than group hikes.
When I hike alone, there would be no peer pressure to move on, so I could take my time searching for geocaches, taking the best selfie, or just enjoying the environment.
However, I would not be able to start a conversation to learn the lives of people in my group, I could not hold a girl's hand when she's crossing a river, and I might get in trouble in the unlikely case of an injury.

## Hiking with Mom

My mom came to visit me during spring break 2017.
I rented a car for a whole week, and took her on four hikes.

![Airport Mesa in Sedono, 14MAR2017](AirportMesa.jpg)

We watched sunset on top of "A" Mountain, while eating Sonoran hotdogs from [El Güero Canelo](/t/2017/Tucson-restaurants/).
We viewed Hole-in-the-Rock in Phoenix's Papago Park, and watched sunset again on Papago Buttes.
We walked under Bell Rock in Sedona, and watched sunset a third time on Airport Mesa.
We didn't forget Tucson's own Sabino Canyon, where we took the $10 tram to the end, and also played in a pool.

![Phoneline Trail, 17MAR2017](Phoneline.jpg)

## Return to Mt Lemmon

I started writing my dissertation in Spring 2017 semester.
Dissertation writing is stressful, but it also gives me plenty of free time because I found myself unable to focus if I were working long hours.
Thus, I would go for a hike whenever I have an opportunity.

I joined Outdoor Rec for a hike to Mount Bigelow on Apr 15, 2017.
The hike started with the Butterfly Trail in Mt Lemmon, which I still remembered despite the last visit was five years ago.
Then, we continued ascending 780 feet to the summit.

The trip leader, Patrick, was carrying a big backpack, which contained eight liters of water and three jackets "in case people are freezing".
These jackets were not needed.
Despite there was snow somewhere along the trail, most of us were in short-sleeved shirts due to strenuous physical activity.
The water, on the other hand, was very useful.
The recommendation was three liters per person and I carried four liters myself, but I still needed two more liters from Patrick's pack.
However, I believe I could get away with my four liters, because I increased my water intake when Patrick revealed he had the extra water and suggested us to "off-load" some weight in his pack.

During a rest stop, I tried to carry Patrick's big backpack, which weighed about 20KG at that moment.
While I was able to stand up with the pack, walking with it was difficult.
Patrick pointed to the hip straps of his pack, which could reduce pressure on shoulders and place weight on legs; this reinforces the importance of [leg days](/t/2017/heavy-iron-stuff/).

![Mt Bigelow summit, 15APR2017](Bigelow.jpg)

Just a month later, I returned to Mt Lemmon with a group of computer science students.
They ambitiously planned a 7.8-mile hike along Wilderness of Rocks loop trail.
However, the departure was 30 minutes late, and the drive took longer than it should because imprecise destination coordinates led us to the wrong trailhead.
When we finally started the hike at 11:00, I knew we would not be able to complete the planned long hike if we were to exit the trail before sunset.
Sometime later, we voted to decide that we would turn back from wherever we could reach at 13:30.
I saw some rocks on this hike, but the forest was more appealing to me.

![Marshall Gulch Trail, 20MAY2017](MarshallGulch.jpg)

## One Last Hike

Spring semester ended shortly after the Wilderness of Rocks hike, and nobody was organizing more hikes because [@TheTucsonHeat is known to kill hikers](/t/2017/heat-and-monsoon/).
I enrolled several trips at Outdoor Rec including camping and canoeing, but none of these trips happened because too few people signed up.
Three refunds later, I received confirmation that a Butterfly Trail hike would be running on Aug 27, 2017, my [final weekend in Tucson](/t/2017/last-month-in-Tucson/).
I signed up for this trip.
Strangely, I did not receive an email from the trip leader detailing the exact departure time, but the website says the trip would depart at 09:00, so I went to Outdoor Rec at 08:40.
I was shocked to find the gates locked and nobody inside.
After a few phone calls, the manager of Campus Rec informed me that the trip had departed at 08:30 without me.
I was devastated because my final weekend in Tucson was about to be ruined.

Two other participants also missed the trip departure due to the same reason.
When the manager broke the bad news to us, I proposed to take them onto a different hike, if either of them had a vehicle.
[Marissa](https://www.facebook.com/marissa.salazar.900) reluctantly agreed to my proposal, and drove three of us in her old truck to Sabino Canyon; we also each picked up a sandwich at Safeway as lunch.
I guided them on a hike to Seven Falls, a trail that I was so familiar with that I could navigate without a map.
For them, it was their first time on this trail, and they were both excited.

![get hydrated on the hike to Seven Falls, 27AUG2017](SevenFalls-drink.jpg)

Having a small group, we were able to have some extensive conversations.
Marissa is a theatre arts major and on her way to become a theatrical actor, and she was delighted when I mentioned the [All in the Timing](https://uanews.arizona.edu/calendar/75631-play-all-timing) performance at their college.
[Soon](https://www.facebook.com/assss.3) is an applied math major, so I talked with him about infinite decimal numbers.
Those conversions distracted them from constantly asking, ["are we there yet"](https://www.urbandictionary.com/define.php?term=are%20we%20there%20yet%3F), which I could not answer because I know the route but not the waypoints.

We reached the falls eventually.
After lunch, I offered Soon an chance to "push me into the water".
He climbed onto the top of a small waterfall while trying to keep his feet dry; I swam to the bottom of this waterfall and asked him to drag me up.
Not knowing this was a trap, he gave me his hand.
Before he could realize, he was soaked in the pool, and I was laughing; remember that I enjoy seeing people getting wet?
Seconds later, he started sinking to the bottom of the pool, and I confidently pulled him out of the 2-meter-deep water using my "excellent" swim skills.
After we got to the shore, he revealed that he knew he would end up in the water when I warned him to remove all electronic devices and he was okay with that, but he didn't expect the water to be so deep, as he doesn't know how to swim.
I grinned: "nobody has ever died on my trips".

It was an impromptu trip to save our weekends from being ruined by Outdoor Rec.
All three of us agreed that we had a good time.
This was also my last hike before leaving Tucson.

## Conclusion

The desert is unforgiving.
[@TheTucsonHeat](/t/2017/heat-and-monsoon/) can kill people.
However, as long as I'm careful, hiking in Arizona is enjoyable.
I will definitely miss those good times.
