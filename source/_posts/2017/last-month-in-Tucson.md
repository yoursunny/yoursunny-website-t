---
title: My Last Month in Tucson
lang: en
date: 2017-10-26
tags:
- life
- Arizona
- food
image: UArms.jpg
---

I [began my life as a Tucsonan](/t/2017/first-days-in-Tucson/) on Aug 04, 2011, [spent six years](/t/2017/six-years-in-arizona/) as a PhD student at The University of Arizona.
However, everything has an end, and my life in Tucson ended on Aug 31, 2017, after I accepted a research job in Gaithersburg, Maryland.

## Move to Sahara Apartments

My apartment lease at [University Arms Apartments](/t/2017/roommates-or-not/) ended on Aug 01, 2017.
I needed one more month in order to finish editing my dissertation.
I chose [Sahara Apartments](https://saharaapartments.com) for my short-term accommodation.

On Jul 15, I went to Sahara Apartments to "apply for a room".
I received a tour promptly, but I was told to apply online, because they do everything online.
Despite being a small community, Sahara had a complicated website for all the business procedures, including new resident application, rent payment, maintenance requests, shuttle reservation, and policy violation reporting.

I put in my online application a day later, and received an invoice within a few hours.
The invoice indicates that paying by credit card would incur a 4% convenience fee, and paying by personal check would incur a five-day delay to make sure the check clears.
WTF?

A week before my move-in date, I visited the leasing office again to submit my check.
The owner and manager of Sahara Apartments, Ted Mehr, greeted me.
He accepted my check, and told me that I could pay with the checking account number through ACH debit, which clears immediately and avoids the convenience fee.

![packing up at University Arms](UArms.jpg)

I rented a car on Jul 31, and moved from University Arms to Sahara.
I was assigned room 3316 on the third floor.
Although there is an elevator, moving everything from the car to the third floor was a tough task that resulted in several sweat-soaked shirts.

## Life at Sahara

Short-term stays are considered "hotel accommodation" at Sahara Apartments.
Rooms are furnished with bed sheets, towels, dinner plates, and kitchen utensils.
This means I could donate my own pans, pots, and plates to Goodwill, as I don't plan to bring them to Maryland with great effort only to have them smashed on the way.

Sahara's amenities are comparable to NorthPointe Apartments.
There is a swimming pool, a hot tub, a little fitness center, a games room, a lounge, and a small movie theater that I never figured out how to use.
A dozen on-size laundry rooms are open 24/7, and the price is $1.75 per load, half of the price at University Arms.
I took advantage of the cheap pricing, and washed all my clothes in the machines during the month.

![gas grill at Sahara Apartments](grill.jpg)

My favorite amenity is the gas grill, which isn't available at University Arms.
I can [cook steaks](https://youtu.be/DtYyA_mlNGU), salmon, leek, onion, carrots, and broccoli on the grill.
I can also [take a dip in the pool](https://youtu.be/Uz3-H5xA-4w) while my red meat is cooking.

The ugly part of Sahara Apartments is [a colony of ants](https://youtu.be/8C7oKh0TiSI).
Whenever I have fish in my room, thousands of ants would find their way in, in an attempt to remove nutrition from the fish bones.
Apartment management would not do anything until I have filled a form online; then, the maintenance guy would show up with a spray bottle.

## "One Last Time"s

August is my final month in Tucson, so that I wanted to relive some of the good experiences by visiting my favorite places, eating my favorite local food, and participating my favorite activities "one last time".

[![getting wet at Brandi Fenton splash pad](splash.jpg)](https://youtu.be/JKVyfGOpC6s)

I [rode a bike](/t/2017/on-two-wheels/) along The Loop, found [more geocaches](/t/2017/start-geocaching/), and [played in the mud](https://youtu.be/57Fi2oLuVUA).
I went to [2nd Saturdays](/t/2017/Tucson-festivals/) and Tucson Improv Movement.
I [ate at](/t/2017/Tucson-restaurants/) El Güero Canelo, Zemam's, Yoshimatsu, and Tucson Tamales.
I continued to enjoy [the heat, the monsoon](/t/2017/heat-and-monsoon/), and [one last hike](/t/2017/hiking-Arizona/).

![squat at Lohse Family YMCA](ymca.jpg)

There were also some "first time"s, for the activities that I always thought I could do "next time" until realizing that I wouldn't have more time.
This includes a visit to [Arizona State Museum](/t/2017/Tucson-museums/), and [a few workouts](/t/2017/heavy-iron-stuff/) at downtown YMCA.

## The End

I moved all my stuff to CubeSmart Self Storage, and departed from Tucson on Aug 31, 2017.
Although I could [return to Tucson in the future](/t/2017/cross-country-move/), it would be unlikely for me to spend more than a few days in Tucson.
Thus, I have officially left Tucson, the city I know and love.
