---
title: Festivals in Tucson
lang: en
date: 2017-08-20
tags:
- life
- Arizona
- food
image: TFOB-Alexa.jpg
---

Tucson has [over 50 festivals](https://southernarizonaguide.com/annual-events-festivals-southern-arizona/) every year.
During my [six years in University of Arizona](/t/2017/six-years-in-arizona/), I have attended many local festivals, and here are my top picks.

## 7. San Ysidro Festival

[San Ysidro Festival](https://www.yelp.com/events/tucson-san-ysidro-festival-at-mission-gardens) at [Mission Garden](https://www.missiongarden.org/) is a revival of an old Tucson tradition that dates back to the early days of Spanish settlement in the southern Arizona region.

There are demonstrations of traditional farming practices:

![horse at San Ysidro Festival 2016](SanYsidro-horse.jpg)

I even had the chance to get my hands dirty and collect some wheat straws:

![collecting wheat straw at San Ysidro Festival 2016](SanYsidro-wheat.jpg)

## 6. Tucson Rodeo

[Tucson Rodeo](http://tucsonrodeo.com/) is a celebration of cowboys.
Professional and junior cowboys and cowgirls try to ride a bucking horse or bull.
They just need to stay on the horse for eight seconds, although sometimes they would fall off miserably.

![bareback riding at Tucson Rodeo, Feb 25, 2015](Rodeo-bareback.jpg)

[Tucson Rodeo Parade](https://www.tucsonrodeoparade.com/) happens once a year during the Rodeo week.
It is the largest non-motorized parade in the country, where most floats are drawn by horses.

![Wells Fargo float at Tucson Rodeo Parade 2016](Rodeo-parade.jpg)

## 5. Tucson Festival of Books

[Tucson Festival of Books](https://tucsonfestivalofbooks.org/) is a community-wide celebration of literature.
As the name suggests, the festival is mainly about books.
There are rows and rows of publisher's booths where you can purchase books.
I rarely read books, so they are irrelevant to me.

Television is an important form of media, and is part of "literature".
I joined a tour of Arizona Public Media to learn how a TV station operates:

![Arizona Public Media control room](TFOB-azpm.jpg)

I also met Alexa Liacko, one of my favorite TV reporters:

![me with Alexa Liacko at Tucson Festival of Books 2016](TFOB-Alexa.jpg)

My favorite section is [Science City](https://sciencecity.arizona.edu/), which allows the public to explore UA’s science-rich campus and experience the exciting worlds of science and technology.
I got to see [how a volcano erupts](https://youtu.be/k92GJiFKsIo), and a live gila monster presented by the Desert Museum:

![gila monster at Science City 2016](TFOB-gilamonster.jpg)

## 4. Winterhaven Festival of Lights

[Winterhaven Festival of Lights](http://winterhavenfestival.org/) is Tucson's holiday tradition.
Homeowners in the Winterhaven neighbor decorates their front yards with thousands of lights, as a gift to all.

![zoo exhibit at Winterhaven Festival of Lights 2014](Winterhaven-zoo.jpg)

One homeowner even performed live:

![A Christmas Carol at Winterhaven Festival of Lights 2015](Winterhaven-carol.jpg)

My favorite yard is the music fountain:

[![music fountain at Winterhaven Festival of Lights 2014](Winterhaven-fountain.jpg)](https://youtu.be/R2yG91zQi2I)

## 3. Cyclovia Tucson

[Cyclovia Tucson](https://www.cycloviatucson.org/) is when they temporarily close the streets to motor vehicles, so people can bike, walk, and play in the streets.

![bicycles on the street at Cyclovia Tucson spring 2015](Cyclovia-street.jpg)

Cyclovia is not a bike race.
Although most people participate in Cyclovia Tucson [on two wheels](/t/2017/on-two-wheels/), I once walked half of the route when the route was right in front of my apartment.

There is usually a zipline, which people would wait for an hour in exchange for 15 seconds of awesomeness:

![zipline at Cyclovia Tucson spring 2016](Cyclovia-zipline.jpg)

Another usual activity is a rock climbing wall, which I attempted three times but couldn't get past 90%:

![rock climbing at Cyclovia Tucson fall 2016](Cyclovia-climb.jpg)

Cyclovia has an activity map, which typically includes street games and information booths.
I tasted a piece of sausage cooked in solar oven:

![solar oven at Cyclovia Tucson fall 2015](Cyclovia-solar.jpg)

The garden hose is never published in the activity map, but usually appears somewhere along the route to you can cool off from [the Tucson heat](/t/2017/heat-and-monsoon/):

![garden hose at Cyclovia Tucson spring 2017](Cyclovia-hose.jpg)

## 2. 2nd Saturdays Downtown

[2nd Saturdays Downtown](http://www.2ndsaturdaysdowntown.com/) is a monthly outdoor entertainment event happening in downtown Tucson.
Each month, there are three musicians or bands playing on the main stage.

![Eric Schaffer & the Other Troublemakers on 2nd Saturdays main stage, Jun 11, 2016](2ndSat-stage.jpg)

I never liked the guitar, so I usually wander off to see the sideshows elsewhere on the streets.
My favorite was the two boys playing music with pots and buckets, who received my donation twice:

[![music with pots and buckets at 2nd Saturdays, Jul 11, 2015](2ndSat-pots.jpg)](https://youtu.be/qQ2ej0ABx1U)

Food is an important part of 2nd Saturdays experiences.
There are dozens of food trucks and food booths to choose from, and you can watch closely how your food was made:

[![Fiamme Pizza tossing at 2nd Saturdays, May 14, 2016](2ndSat-pizza.jpg)](https://youtu.be/iVctbHbZcR8)

2nd Saturdays occur more often than other festivals, and the attendance highly depends on weather.
Some months, there could be hundreds of Tucsonans in front of the main stage, and I could [play a little prank](/t/2017/freewifi/) with those people.
Other times, when the wind picks up and thunder is roaring, the seats would be almost empty, and the vendor who [paid $175](https://twitter.com/yoursunny/status/751985721020407808) for an opportunity to sell you a sandwich would be crying.

## 1. Tucson Meet Yourself

[Tucson Meet Yourself](https://tucsonmeetyourself.org/) is a "folklife" festival.
In this 3-day festival, I have the chance to learn the culture, arts, and living traditions of various ethnic communities represented in southern Arizona, through the demonstrations, exhibits, and performances by people from those communities.

Performers are singing, dancing, and playing instruments:

[![Mariachi Herencia De Cuco Del Cid at Tucson Meet Yourself 2016](TMY-mariachi.jpg)](https://youtu.be/dMBuIN6p2aI)

![street dance at Tucson Meet Yourself 2012](TMY-StreetDance.jpg)

[![Capoeira Mandinga martial arts at Tucson Meet Yourself 2016](TMY-CapoeiraMandinga.jpg)](https://youtu.be/gl3PHp8I7Og)

Indigenous artists are showing their artwork:

![Tohono O'odham basketary at Tucson Meet Yourself 2015](TMY-basketary.jpg)

Most importantly, Tucson Meet Yourself is nicknamed **Tucson Eat Yourself**, and there are lots of food!

![what I ate at Tucson Meet Yourself 2016](TMY-food.jpg)

Sometimes your food purchase comes with a performance, too:

[![Turkish ice cream at Tucson Meet Yourself 2016](TMY-Turkish.jpg)](https://youtu.be/Q6J91icvk3g)

## Celebrate Every Day

When I am not at a festival but still want some celebration, I would look up [National Day Calendar](https://nationaldaycalendar.com/).
With nearly 1500 national days, national weeks, national months to choose from, I would never run out of excuses for celebrations.
I cut up old credit cards on national buy nothing day, drank Eegee's on national piña colada day, [flossed my teeth with a Waterpik](https://amzn.to/2vR8xN3) on national fresh breath day, and took a selfie in my #GrinAndBearDown shirt on national selfie day.

![celebrating national selfie day on Jun 21, 2016](NationalSelfieDay.jpg)

Festivals made my life more colorful and helped my survive the otherwise boring grad school.
