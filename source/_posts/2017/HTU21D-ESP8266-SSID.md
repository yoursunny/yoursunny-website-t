---
title: Show Temperature and Humidity as WiFi SSID with ESP8266 and HTU21D Sensor
lang: en
date: 2017-07-10
tags:
- ESP8266
image: SSID.gif
---

Since I started playing with ESP8266 WiFi microcontroller in 2016, I had a [TMP36](https://www.adafruit.com/product/165) temperature sensor.
TMP36 is an analog sensor: it uses a voltage between 0.00V and 1.75V to represent a temperature reading between -50℃ and 125℃.
The ESP8266 has an analog-to-digital converter (ADC) capable of reading voltages up to 1.00V.
To read a temperature from TMP36 into ESP8266, I need to use a pair of resistors as a [voltage divider](https://learn.sparkfun.com/tutorials/voltage-dividers), so that the resulting voltage does not exceed ADC's limit.
However, I feel the temperature reading is very inaccurate: is my room really [19℃](https://twitter.com/yoursunny/status/739549963173040128) when [@TheTucsonHeat](https://twitter.com/TheTucsonHeat) is in town?
I once replaced the carbon resistors with metal film resistors, and the temperature reading instantly [changed by as much as 8℃](https://twitter.com/yoursunny/status/739509298275848193).

I need an upgrade to the temperature sensor!
The new sensor must output digital signal, so that my lousy resistors wouldn't affect its accuracy.
After comparing [DHT11, DHT22](https://learn.adafruit.com/dht/overview), [DS18B20](https://www.maximintegrated.com/en/products/analog/sensors-and-sensor-interface/DS18B20.html), and several others, I eventually chose [HTU21D-F](https://learn.adafruit.com/adafruit-htu21d-f-temperature-humidity-sensor/overview) because it has an I<sup>2</sup>C interface, which can be added directly to my [LCD kit](/t/2017/ESP8266-LCD-count-up-timer/).

The **HTU21D** sensor arrives in the mail a few weeks later.
It needs a bit of soldering to add the pin headers onto the breakout board; [ImmodderNation has a soldering video](https://www.youtube.com/watch?v=d7a6bvualp0) to get you started.
Afterwards, wiring is simple: just add it into the existing I<sup>2</sup>C bus!
As long as you don't short the wires, you won't [burn down the office](https://twitter.com/yoursunny/status/590291057495117824).

![HTU21D on I2C bus of ESP8266](wiring.jpg)

A quick test with [Adafruit's library](https://learn.adafruit.com/adafruit-htu21d-f-temperature-humidity-sensor/wiring-and-test) confirms the HTU21D sensor is working correctly.
Temperature and humidity readings are showing up on the serial console.
However, I don't want to tug around the laptop to measure temperature around the house, and don't want to program the LCD or connect to Losant or NDN right away.
I thought up a quick and easy way: let's **create a Wi-Fi hotspot from the ESP8266**, and show temperature and humidity as the WiFi network name (SSID)!

![WiFi SSID shows temperature and humidity](SSID.gif)

## Arduino sketch: show HTU21D temperature/humidity as WiFi SSID

```arduino
#include <Adafruit_HTU21DF.h>
#include <ESP8266WiFi.h>
#include <PString.h>
#include <Streaming.h>

Adafruit_HTU21DF htu;

void
setup()
{
  WiFi.disconnect();
  WiFi.persistent(false);
  WiFi.mode(WIFI_AP);

  htu.begin();
}

void
loop()
{
  char buf[32];
  PString out(buf, sizeof(buf));
  out << htu.readTemperature() << "C " << htu.readHumidity() << "%";
  WiFi.softAP(out);
  delay(10000);
}
```

This code requires [ESP8266 Arduino core](https://github.com/esp8266/Arduino), [Adafruit HTU21DF library](https://github.com/adafruit/Adafruit_HTU21DF_Library), [PString](http://arduiniana.org/libraries/pstring/), and [Streaming](http://arduiniana.org/libraries/streaming/).
It establishes a WiFi hotspot access point (AP), takes temperature and humidity measurements from the HTU21D-F sensor every 10 seconds, and renames its WiFi SSID to show the temperature and humidity.

**Publishing sensor data via WiFi SSID** is an unconventional method, but it has the benefit of not requiring an Internet connection and not requiring even a WiFi association.
I can pull down the WiFi menu on my phone anytime and look at the temperature.
