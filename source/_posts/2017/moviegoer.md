---
title: I am a Moviegoer
lang: en
date: 2017-08-15
tags:
- life
- Arizona
---

I watched over 150 movies during my [six years at University of Arizona](/t/2017/six-years-in-arizona/).

## What did I Watch

My favorite genre is [**action and adventure**](https://www.amazon.com/gp/search?ie=UTF8&tag=yoursunny-20&linkCode=ur2&linkId=9a29d467563e84e44c134a05e1099174&camp=1789&creative=9325&index=instant-video&keywords=Action%20and%20Adventure).
I love seeing superheroes save the world, because they bring hope to our world.
My favorite character is [Spider-Man](https://www.amazon.com/gp/search?ie=UTF8&tag=yoursunny-20&linkCode=ur2&linkId=7758271a143d68fbbf6cfea6569dc0b0&camp=1789&creative=9325&index=instant-video&keywords=spiderman), because he only does good things and is full of positive energy.

I also frequently watch [**science fiction**](https://www.amazon.com/gp/search?ie=UTF8&tag=yoursunny-20&linkCode=ur2&linkId=284b0784dd615541f10a9f1c34427700&camp=1789&creative=9325&index=instant-video&keywords=science%20fiction) films.
I particularly enjoy films set in the outer space, such as [The Space Between Us](https://amzn.to/2vClqdK) and [Passengers](https://amzn.to/2i62HTc), because the weightless shots are breathtaking.

My third favorite genre is [**drama**](https://www.amazon.com/gp/search?ie=UTF8&tag=yoursunny-20&linkCode=ur2&linkId=ec4e391e14ae7029504ac041a5282144&camp=1789&creative=9325&index=instant-video&keywords=drama).
Some of my favorites are [National Lampoon's Christmas Vacation](https://amzn.to/2vHeV7T), [Nebraska](https://amzn.to/2fK8RY7), and [Instructions Not Included](https://amzn.to/2vH4DEP).
I can have a good laugh out of them.

I also enjoy films in which kids and teens play a significant role.
I have watched [Alexander and the Terrible, Horrible, No Good, Very Bad Day](https://amzn.to/2fJHLAD), [Captain Fantastic](https://amzn.to/2vH5aGB), and the [Maze Runner](https://www.amazon.com/gp/search?ie=UTF8&tag=yoursunny-20&linkCode=ur2&linkId=b8c5143891b2663a5609d5efbc06ddd4&camp=1789&creative=9325&index=instant-video&keywords=maze%20runner) series.

I find new films from trailers, especially those shown at the movie theater before a feature presentation.
I pay attention to the trailers, and take a note of trailers that interest me in my smartphone.
Once the movie is released, I would check the IMDB scores, re-watch the trailer on YouTube, and make a decision on whether to watch it.

## Where did I Watch

A *moviegoer* needs to *go* to watch movies.
I watched most movies in movie theaters.

My favorite movie theater is [AMC Loews Foothills 15](https://www.amctheatres.com/movie-theatres/tucson/amc-foothills-15).
They have the only IMAX screen in Tucson, where I watched [Gravity](https://amzn.to/2vHeiLl).
However, I did not go to AMC often, because it is a 40-minute bus ride from [NorthPointe](/t/2017/roommates-or-not/), and even further from University Arms.
The theater where I watched the most movies is [Century 20 El Con and XD](https://www.cinemark.com/theatres/az-tucson/century-20-el-con-and-xd), because of its convenient location from University Arms.
I can bike there in 20 minutes, and I can call a SafeRide or [ride a bus](/t/2017/SunTran/) to get there in less than 10 minutes.

The most unique cinema is [RoadHouse Cinemas](https://roadhousecinemas.com/movie-theater/tucson).
It allows me to do two things at a time: I can simultaneously watch a movie and eat a meal.
However, my experience at this cinema has been mixed.
I went there five times, but only two visits were trouble-free.
I was forced to switch seats when watching [Gone Girl](https://amzn.to/2w0Rqcs), because they sold the same seat twice.
I was starving when watching [Blackhat](https://amzn.to/2vCpZ7O), because the server forgot to put in my food order.
I was turned away for [Maze Runner: The Scorch Trials](https://amzn.to/2vCmuOx), because all 68 seats were sold out; they have so few seats in each auditorium, because they are recliner seats, not the regular ones.

I also went to [Century Gateway 12](https://cinemark.com/arizona/century-gateway-12), [Grand Cinemas Oracle View 6](https://tucson.com/business/local/thrifty-moviegoers-haven-the-oracle-view-goes-dark/article_8720850a-dccc-56cc-b02d-b1809c1c67d7.html), and UA's very own [Gallagher Theater](https://www.union.arizona.edu/involvement/gallagher/).
They screen movies that are a few months old, at a cheaper price.
[Loft Cinema](https://loftcinema.org/) screens independent films, and I watched [Boyhood](https://amzn.to/2vH9fdT) and [WADJDA](https://amzn.to/2wOLKj8) there.

When it gets too hot, I would stay in and watch a movie on my computer or tablet.
Pima County Public Library has a [DVD collection](https://www.library.pima.gov/movies/) which I could borrow for free, and I watched Siri's recommendation [2001: A Space Odyssey](https://amzn.to/2vCrfb8) from there; similarly, I borrowed a few DVDs from NorthPointe leasing office.
[Google Play](https://play.google.com/store/movies) offers movie rentals sometimes as cheap as $2; although this price is higher than a movie ticket at Century Gateway 12, I could save more in bus tickets by renting from Google Play.
Finally, [Amazon Prime Video](https://www.amazon.com/tryprimefree?ref_=assoc_tag_ph_1427739975520&_encoding=UTF8&camp=1789&creative=9325&linkCode=pf4&tag=yoursunny-20&linkId=dc68412e6623601f4d03fa754c840187) allowed me to watch thousands of movies for free, and unlike a rental, there is no time limit in finishing a movie.

## How Movies Changed Me

I watched all but one film alone.
I do not feel lonely without a girl by my side, because during the movie, my mind would be in the storyline completely.
They allowed me to relax, and leave worries behind.
I have spent 325 hours of my life watching movies, estimated from 130-minute average runtime of today's films.
I consider those hours worthwhile.

There was one film that changed my actions.
That film is [Plastic Is Forever](https://www.indiegogo.com/projects/plastic-is-forever-kids-environment), a short documentary about ocean plastic pollution.
I watched this movie during [Arizona International Film Festival](http://www.filmfestivalarizona.com/) at [The Screening Room](http://screeningroomtucson.com/events.shtml).
It caused me to rethink my environmental footprint in relation to plastic consumption.
Although it is impractical for me to stop buying single-use plastic containers, I started to ask for [no plastic straw](https://twitter.com/yoursunny/status/883396590085390337) when [restaurant](/t/2017/Tucson-restaurants/) serves my water.
This is a small action with a minor effect on the environment, but I wouldn't have thought about it without watching this film.

## Will I Watch More Films?

Definitely, if I still have time.
