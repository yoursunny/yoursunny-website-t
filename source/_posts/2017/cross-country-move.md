---
title: My Epic Cross-Country Move on Amtrak
lang: en
date: 2017-11-12
tags:
- life
- Arizona
- Maryland
image: observation.jpg
---

I recently moved across the United States from Tucson, Arizona to Gaithersburg, Maryland, 1940 miles apart.
Tucson is in the southwest; Gaithersburg is on the east coast just north of Washington, DC.
The move was a long and complicated process, as well as an epic journey.

## Choosing the Train

Most Americans would drive across the country.
When my uncle graduated from University of Maryland, he spent two weeks driving from Maryland to California, and visited Yellowstone National Park and many other places during his trip.
Driving across the country would allow me to have a good look at the country, and [seek geocaches](/t/2017/start-geocaching/) in many states.
All my clothes, computers, and other toys can be packed in the car.

While this option was attractive, I decided against it because of its high cost.
Unlike my uncle, I did not purchase a vehicle during college, but relied on [rental cars](/t/2017/behind-the-wheel/).
Renting a car is inexpensive, but only if you return the vehicle to the same place.
If I rent a car in Arizona and return it in Maryland, they are going to charge a "one way fee" that is approximately $1000, on top of the normal rental and insurance charges.
Additionally, I have to spend on motels and meals.
In total, the driving option would cost me between $2000 and $3000.

Google Maps estimated a driving distance of 2290 miles, or 33 hours non-stop.
I am not accustomed to long drives.
I felt exhausting on the [3-hour drive from Tucson to Yuma](/t/2018/YumaMega/), and I couldn't imagine a 11x longer drive.
This is another important factor for me to decide not to drive.

Another popular option is flying.
American Airlines would take me from Tucson to DC in less than nine hours for a low price under $300.
Luggage is limited to four pieces, and they'll cost an extra $410.
This is the most cost-effective option, but I would be missing all the sights I could see if driving.
I disliked it.

Then I thought about an unpopular option: Amtrak.
Amtrak is America's one and only long-distance passenger railway service.
Taking the train is slow and expensive, but it offers a middleground between driving and flying.
I don't have to endure the exhausting long drive, but I can still see the sights where the train goes by.
However, there aren't many opportunities to explore the places in more depth, or to find geocaches.

A train trip from Tucson to Washington, the nearest major train station, needs four days and three nights.
A ticket for a seat costs $232, but sitting in a recliner for four days would be crazy, so that I decided to upgrade to a sleeper, priced at $1073.
Baggage allowance is generous: I can check in four pieces, and carry on two large bags and two small "personal items".
However, there are many limitations in what can go into those bags: you can't transport anything that can break (such as pots and plates), and you can't check anything with a plug (so that computers must go into the carry-on).
This means some things must be shipped through some other means.

[Weighing all factors](/t/2020/thought-provoker-3/), I chose the train over the two more popular options.

## Storage Cube and Goodwill

I was required to apply for a new visa before taking the new research job.
It is best to do visa application in my home country, which maximizes the probability of getting approved.
This means I must go back to China.

I would not have housing in Tucson when I'm applying for the visa in China, but my property needs to go somewhere.
Thus, I started exploring self storage facilities in August.
I chose CubeSmart Self Storage at 2424 N Oracle Rd because of their great rates.
On Aug 30, the day before leaving, I moved everything to my storage cube, protected by a [Master disc lock](https://amzn.to/2zdUZey) I purchased off Amazon.

![my storage cube](cube.jpg)

I gave away many bulky or fragile items that I don't plan to bring on the train.
I donated some of my bedding and all those ceramic plates to Goodwill Donation Center.
For excessive clothing, the better ones went to the Salvation Army, and others were placed in a park next to a group of homeless people.
Broken electronics and other metal things were recycled at [RISE Equipment Recycling Center](http://www.riseequipmentrecycling.org/).
I also gave my microwave and vacuum cleaner to a classmate, in exchange of asking him to receive my mails when I'm away.

## To China and Back

Until they build [the China-Russia-Canada-America railway](https://www.theguardian.com/world/2014/may/08/chinese-experts-discussions-high-speed-beijing-american-railway), the only option of going back to China is still flying.
I bought a round-trip ticket departing Tucson on Aug 31, and returning on Oct 15.
It was my first time flying on a Boeing 787 Dreamliner aircraft, and I was fascinated by the smart glass window that changes opacity in response to a set of buttons.

![Dreamliner windows at low opacity](window1.jpg) ![Dreamliner windows at medium opacity](window2.jpg) ![Dreamliner windows at high opacity](window3.jpg)

My top priority in China was the visa application.
The interview went smoothly, but I was "checked" and it took three weeks to receive a clearance.
Other than that, I attended two weddings, travelled to Xiamen and Nanjing, [lifted in a Chinese gym](/t/2017/Chinese-gym-vs-American-gym/), and [witnessed some shocking changes in China](/t/2017/shocking-changes-China/).

![downtown Dallas](dallas.jpg)

I reentered the United States on a J-1 visa on Oct 15.
The international flight arrived early, so I had two hours to walk around downtown Dallas and (obviously) grab a geocache.
Later that day, I settled in a hotel near Tucson International Airport.

## The Sweet Overtime

My Amtrak ticket, booked right after receiving the visa, was on Oct 23, because all sleepers before that were sold out.
Between the Oct 15 flight and the Oct 23 train, I have an extra week in Tucson.

I lived in Sahara Apartments, same place as [my last full month in Tucson](/t/2017/last-month-in-Tucson/).
Ted Mehr the manager was surprised to see me coming back, but he arranged a room immediately.
He let me take a room "reserved for someone coming tonight", without realizing that I made an online reservation and that room was prepared for me in the first place.

I had rental cars on Oct 16 and Oct 22.
I retrieved my stuff from CubeSmart on Oct 16.
During the week, I shuffled things between bags and boxes so that every bag conforms to Amtrak's strict 50lb weight limit.
On Oct 22, I drove three checked bags to Tucson Amtrak station, and sent a box of pots and pans via FedEx Ground.

![pasta, turkey, and green beans](cooked.jpg)

I cooked half of the meals during that week, using the groceries I bought on Oct 16.
I had limited spice (salt and red chili, no oil), which makes cooking challenging.
I managed to make microwaved ground turkey, steamed green beans, grilled steak, grilled salmon, baked potato, and boiled pasta.
I ate the other half of the meals at familiar and unfamiliar restaurants.

![Comedy Corner performs on stage](comedy.jpg)

I went to University of Arizona three times during the week.
On Wednesday, I enjoyed an improv comedy performance brought by Comedy Corner, and sadly discovered that my CatCard could no longer open the library doors.
On Friday, I checked out my old office that has been renovated, and did one last presentation to my research group.
On Saturday, I went to Campus Rec for a work out while checking out the fancy new locker rooms.

![Gould Simpson 718 office](office.jpg)

The extra sweetener of the week was [a girl](https://www.facebook.com/VanessaaBailonc) I met at Sahara Apartments.
I got hugs and kisses, plus a massage on my sore muscles after a workout.
Did I fall in love?

## The Train is Departing

The train journey started on Oct 23 morning.
Tucson station is located in downtown, across the street from the bus terminal.
Since I don't have much stuff with me, I took a bus to the station.

![Sunset Limited at Tucson](TUS.jpg)

The train pulled out of Tucson Depot at 08:15.
At this time, I have officially left Tucson, the city I know and love, without knowing when I would return.
An exciting and unique adventure awaited!

## Double Doors

Right after I found my room and put down my carry-on luggage, the sleeping car attendant informed me that the dining car is serving breakfast.
To reach the dining car, it is unnecessary to exit the train and walk along the platform, because this path would not be possible unless the train is stopped at a station.
Instead, I should use the walkway on the upper level of the train, which connects all cars accessible to passengers.
There are double doors between adjacent cars, and the dining car is three cars ahead of my sleeping car.

![Superliner end door](double-door.jpg)

Unlike China's quiet CRH trains, it is very noisy between those double doors.
Although the walkway is fully enclosed and you won't fall off the train, the enclosure is not sound proof, and you can hear all the noise from the tracks.
To make the situation scarier, the coupling point seems to be bumpier than other parts of the train.
I do not like this spot, but throughout my journey, I had to walk through them many times.

## Rolling Food

![scrambled eggs with bacon and roasted potatoes](scramble.jpg)
![black bean burger](burger.jpg)
![land and sea, aka steak and crab cake](steak.jpg)

Everybody needs food, including when the train is rolling.
My sleeper ticket includes "all" meals during the trip.
Well, not really: the dining car is open for three meals per day, but my body needs four or five meals daily, so I have to fill the rest with protein bars and the like.

![pancake trio with turkey sausage](pancake.jpg)
![pecan tart](pecan.jpg)
![seared salmon with wild rice pilaf](salmon.jpg)

Quality of dining car meals are decent.
Most entrees are cooked as well as a land-based restaurant of similar price point.
I especially like the pancakes, seared salmon, and the "land & sea" option that has the highest list price of $39.

![dining car order slip](order.jpg)

Service is decent as well.
Whenever I show up at the dining car, the dining car attendant arranges myself and three other patrons onto a four-person table.
Then, she gives everyone an order slip; however, we are not supposed to fill out the order, but only needs to write down the sleeper room number and a signature.
Whenever we are ready, she takes orders and writes them on the slip, and then passes the slips to the kitchen at the lower level of the dining car.
Food comes upstairs through one of those little elevators, and the attendant serves the food.
As the four of us finish eating our entrees, the attendant takes away the plates, and offers desserts at lunch and dinner.

![kitchen elevator](kitchen.jpg)

Food is served on disposable plastic plates, which I dislike, because [plastic is forever](https://www.indiegogo.com/projects/plastic-is-forever-kids-environment).
Forks and knives, on the other hand, are metal.
Napkins, to my surprise, are cloth rather than paper.

Amtrak practices "communal seating".
When a party of one, i.e. myself, shows up at the double doors of the dining car, the attendant always pairs me with others to share a four-person table.
This not only maximizes seat utilization and allows more passengers to eat at the same time, but also encourages socialization among passengers, which is part of the Amtrak culture.
I prepared a long sentence to introduce myself: "I am from Shanghai China, but I studied computer networking at University of Arizona in Tucson Arizona for six years, and I'm now moving to Gaithersburg Maryland just north of Washington DC to start a research job".
I also ask others about where they are from, and what industry they work in.
I met a British citizen, who told stories about how an American customs officer lent money to him to enter the United States.
I met a female US Army soldier stationed in San Antonio, who shared her experience in Afghanistan.
I met the VP of a honey company, who encourages everyone to melt honey in hot water as an alternate to coffee.
I also met an Allstate insurance agent, a Dell Computer employee, an obese man trying to lose weight, and several elders that are either going to or returning from weddings or family reunions.

![dining car communal seating](dining.jpg)

Another way to avoid an overcrowded dining car is the reservation system.
Just after I returned from my first breakfast, the sleeping car attendant showed up at my room and asked when I'd like to have lunch and dinner.
I chose a 1PM lunch and a 5PM dinner, so he wrote those times on a paper slip.
Then I realized the Sunset Limited train travels east and there would be time zone changes during the trip, so I asked about the time zone, and the answer was: lunch is on Pacific Daylight Time, and dinner is on Mountain Daylight time.
Thus, my lunch and dinner would be only three hours apart, but it's too late to change because he had walked away.

Breakfast does not require a reservation, because people naturally wake up at different times.
One time the dining car was full when I showed up for breakfast, so the dining car attendant wrote down my name on a waitlist, and later called me back through the announcement system.

## The Roomette

Apart from meals, I spent most of my time in my sleeping accommodation, the roomette.
The English suffix [-ette](https://en.wiktionary.org/wiki/-ette) forms nouns meaning a smaller form of something.
Thus, "roomette" denotes a smaller room.
How small it is?
It is 198cm wide and 107cm deep, about half of the [smallest hotel room I ever stayed in](/t/2021/Beijing2/).
There is a [fantastic YouTube video tour of the Superliner roomette](https://www.youtube.com/watch?v=rn1aRrfODpw), which I watched before the trip so I knew what to expect.

The roomette has a "daytime configuration" that provides two recliner chairs, and a "nighttime configuration" that provides two beds.
The sleeping car attendant would do the transformation, but I figured out the switches quickly so I didn't need his help.
I spent most of the day lying on one of the recliner chairs, resting my feet on the other chair, and reading from my laptop placed on a tray table just above the chair (more on this later).
The upper berth, located above the window, is perfect for afternoon napping.
At night, I slept on the lower berth that is 11cm wider than the upper berth.
Although this bed is still narrower than the 75cm bed I used during my childhood, I found its size sufficient for a good sleep.

![napping on the upper berth](upper.jpg)

## Showering at 80MPH

There are four lavatories in the sleeping car.
The equipment is similar to lavatories on airliners, except that the water is labelled as portable (and [EPA is watching](https://www.epa.gov/dc/epa-Amtrak-agreement-helps-ensure-drinking-water-safety-trains)).

![Texas Eagle shower](shower.jpg)

The more fanciful thing, however, is the shower.
Taking a shower while moving at 80mph is a unique experience.
Since the train keeps swaying from side to side, I must keep my balance with just my feet, when my left hand is holding the soap and my right hand is grabbing the showerhead.
Luckily, I have mastered this feet-only balancing art in the Shanghai subway, so that I can enjoy the hot shower without worrying about tripping over.

## Observation

As the station agent in Tucson said to me: it will get there, it just takes longer.
When the train slowly rolls across the country, I have plenty of time to see what's outside the window.

On the first day, the landscape was mostly desert:

![Arizona desert](desert.jpg)

Cattles were the common sights on the second day when we passed through Texas:

![Texas cattle farm](cattle.jpg)

I saw wheat planted all over Illinois on the third day:

![Illinois wheat farm](wheat.jpg)

The last day's journey is mostly along Shenandoah River and Potomac River:

![Potomac River near Harpers Ferry](HFY.jpg)

I can see sunset:

![sunset over Texas](sunset.jpg)

Sunrise, if I get up early:

![sunrise over Missouri](sunrise.jpg)

I can see mountains:

![mountains in New Mexico](mountains.jpg)

Rivers:

![Dallas, TX](DAL.jpg)

Large cities:

![St Louis, MO](STL.jpg)

Small villages:

![Bartlett, MO](Bartlett.jpg)

Colorful buildings in Mexico:

![Mexico border near El Paso](ELP.jpg)

Rail yards:

![a rail yard in Missouri](railyard.jpg)

And of course, stations:

![Alton, IL](ALN.jpg)

Every train has an observation car.
It not only has windows opening to the side, but also windows toward the top, so that passengers can better enjoy the view.
Unlike a similar concept in China's trains, Amtrak's observation car is open to all passengers on a first-come basis, and you don't have to buy a more expensive ticket for sitting in the observation car.

![observation car](observation.jpg)

## WiFi Stops & The Chicago Bean

The bad news is, there is no WiFi on Amtrak trains.
After I sent a tweet out of Tucson station, I decided that I'm not going to use cellular data during the trip, and would access Internet only if I get WiFi.
I only got WiFi four times before reaching Washington, DC.

The train needed some coupling operations in San Antonio, Texas on Tuesday morning.
We arrived at San Antonio just after 05:00.
At this stop, the last two cars, including my sleeping car, were to be detached from Sunset Limited, and attached onto a new train.
Sunset Limited would then continue its way east, while the new Texas Eagle goes north.
This gave me two hours to use the WiFi in the station, and grab [a geocache nearby](https://coord.info/GC7AQ6R).

![Texas Eagle (left) and Sunset Limited (right)](SAS.jpg)

There were brief 15-minute stops at Fort Worth on Tuesday afternoon and St Louis on Wednesday morning.

![Forth Worth, TX](FTW.jpg)

The longest stop, however, was Chicago Union Station.
It is the terminus of Texas Eagle, and I need to transfer to Capitol Limited.
Despite Texas Eagle was one hour late, I still had four hours to make the transfer.
Thus, I was planning to visit "The Bean", a world-famous public sculpture in Chicago.
Google Maps indicates "The Bean" is located in Millennium Park, a half-hour walk from the train station.
However, since I'm transferring in Chicago, I had to bring along all my carry-on luggage, including a backpack, a duffle, and a full-size roller bag.
It would be nearly impossible to walk one mile with so many bags.
A police officer informed me that there is no luggage storage in Chicago Union Station, and advised me to ["download the Uber app"](https://www.uber.com/invite/junxiaos1ui) so I can get there with all my luggage.
I figured I'd have some snacks before trying to find an Uber, so I walked toward Amtrak's Metropolitan Lounge, exclusive for sleeper and first class passengers.
I was delighted to find a small luggage storage room in the Metropolitan Lounge, and the attendant confirmed that I'm permitted to leave the station with my luggage stored there.
Thus, I put down the duffle and the roller bag, and walked out of the station without getting a snack.

![Cloud Gate aka The Bean in Chicago](cloud-gate.jpg)

Chicago is the third largest city of the nation, with many high-rise buildings.
I identified the correct direction, and found The Bean without much trouble.
I also saw the Crown Fountain, although I did not [#KeepCalmAndGetDrenched](https://twitter.com/hashtag/KeepCalmAndGetDrenched) because it's cold out there.

![Crown Fountain in Chicago](crown.jpg)

After viewing the Chicago skyline, I rushed back to Chicago Union Station, ran into Amtrak Metropolitan Lounge for a soda and some dried bananas, and boarded my next train just in time.

![Chicago skyline](skyline.jpg)

Other than those four station stops, I came prepared for the life without WiFi.
I downloaded Amtrak's [route journey guides](https://www.Amtrak.com/route-guides) and timetables onto my laptop and phones, so that I can read about the towns and other sights along the tracks.
I downloaded offline maps of every state in [HERE WeGo](https://here.com) app, so that I can identify the train's location via GPS, and also decide which side of the dining car would have a better view when I'm offered a choice.
I also read research papers related to what I'm going to work on.
Unlike [a camping trip](/t/2016/offline/), there was no concern about battery power, because there are power outlets throughout the train, including one in my roomette where I can keep my laptop and phones fully charged at all times.

## Arrival

Capitol Limited arrived at Washington Union Station at 13:30 on Thursday.
The dining car didn't serve lunch, because "express lunch" is served only if the train is more than two hours late.
My three checked bags came out shortly after, and I suddenly have four big roller bags.

![my checked bag being unloaded at Washington Union Station](WAS.jpg)

I booked a rental car to avoid paying a hefty sum for a taxi.
What I didn't know is that, there are no luggage carts in Washington Union Station, and rental cars are parked two floors up.
It took me 30 minutes to drag all those bags to the rental car.

![dragging luggage to the rental car](bags.jpg)

I drove one and a half hours to Gaithersburg, Maryland, reached a hotel, and moved my luggage upstairs with a cart.
The box sent via FedEx Ground arrived a day later, and I picked it up at a FedEx Office store.
This concluded my epic cross-country move.

## How Much Did I Spend

Here's an incomplete list of how much I spent for this cross-country move:

* Sun Tran bus for rental car pickup: $1.50
* Enterprise rental car on Aug 30-31, for CubeSmart move-in and going to airport: $34.39 + $40 voucher redeemed with $20 [Discover cashback](https://bit.ly/itapply) + fuel
* CubeSmart storage, two months: $76.27 + $15.10 purchasing a [Master disc lock](https://amzn.to/2zdUZey)
* roundtrip American Airlines flight from Tucson to Shanghai: $652.66
* DART light rail to downtown Dallas: $5
* Quality Inn Tucson Airport, 1 night: $62.20
* Avis rental car on Aug 16-17, for CubeSmart move-out and moving from Quality Inn to Sahara: $59.03 + fuel
* Sahara Apartments, 7 nights: $354.06
* Sun Tran bus for rental car pickup: $3.75
* Dollar rental car on Aug 21-22, delivering luggage to Amtrak station and FedEx: $59.40 + fuel
* FedEx Ground shipping: $77.30
* Sun Tran bus after rental car return: $1.50
* Sun Tran bus going to train station: $1.75
* one-way Amtrak from Tucson to Washington: $1073
* tips to sleeping car attendants: $30
* tips to dining car attendants: $19
* received [Lyft voucher](https://lyft.com/ida/JUNXIAO404540) because Chicago driver couldn't find me and kept taking wrong turns: -$5
* Budget rental car on Oct 26-Nov 02, for going from DC to Gaithersburg and touring apartments: $405.83 + fuel
* Extended Stay America Gaithersburg South, 3 nights: $208.53
* Ride On bus after rental car return: $2

TOTAL: $3157.27
