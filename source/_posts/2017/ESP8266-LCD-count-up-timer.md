---
title: Count-Up Timer on ESP8266 and I2C LCD
lang: en
date: 2017-05-09
tags:
- ESP8266
image: photo.jpg
---

I need a count-up timer on the desk so that I can do a presentation without turning my head to the wall clock.
So I wrote one with ESP8266 and I<sup>2</sup>C-connected LCD unit.

![photo of LCD count-up timer using ESP8266](photo.jpg)

```arduino
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x3F, 16, 2);

void
setup()
{
  lcd.begin(16, 2);
  lcd.init();
  lcd.backlight();
}

void
loop()
{
  int seconds = millis() / 1000;
  int minutes = seconds / 60;
  seconds %= 60;

  lcd.clear();
  lcd.print(minutes);
  lcd.print(':');
  if (seconds < 10) {
    lcd.print('0');
  }
  lcd.print(seconds);
  delay(100);
}
```

Hardware is [Losant LCD Kit](https://store.losant.com/products/lcd-kit).
I'm using [Marco Schwartz's LiquidCrystal\_I2C library](https://github.com/marcoschwartz/LiquidCrystal_I2C).
