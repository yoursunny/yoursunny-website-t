---
title: ndnping Jewelry on ESP8266
lang: en
date: 2017-05-10
tags:
- NDN
- ESP8266
---

I was wearing a unique piece of jewelry at NDN community meeting, Mar 2017: a pair of ESP8266 units that communicate with each other over the NDN testbed.
They are ugly, but it is a nice way to demonstrate my creation in a Named Data Networking community meeting.

<iframe width="560" height="315" src="https://www.youtube.com/embed/HkdRj3FhfQI" frameborder="0" allowfullscreen></iframe>

Two Witty Cloud boards are tied to my wrists, and powered by a USB powerbank in my pocket.
One of them runs a *ndnping* client, and the other runs a *ndnping* server.
The client sends Interests to a router in Arizona, the Interests (under a multicast prefix) are flooded through the testbed, and reach the server which is connected to a router in Memphis.

## Arduino Code

Library: [esp8266ndn](https://github.com/yoursunny/esp8266ndn/tree/10cc607285fa7961baf546aea8f6a6121523652b)

Client sketch `PingClient.ino`:

```arduino
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Streaming.h>
#include <esp8266ndn.h>

const char* WIFI_SSID = "ssid";
const char* WIFI_PASS = "passw0rd";
const char* NDN_ROUTER_HOST = "hobo.cs.arizona.edu";
const uint16_t NDN_ROUTER_PORT = 6363;

const int LED = 15;
char PREFIX[] = "/ndn/multicast/esp8266ndn-demo/ping";

WiFiUDP g_udp;
ndn::UnicastUdpTransport g_transport(g_udp);
ndn::Face g_face(g_transport);

ndn_NameComponent g_comps[8];
ndn::InterestLite g_interest(g_comps, 8, nullptr, 0, nullptr, 0);
ndn::PingClient g_client(g_face, g_interest, 5000);

void
processData(void*, const ndn::DataLite& data, uint64_t)
{
  g_client.processData(data);
}

void
ndnpingEvent(void*, ndn::PingClient::Event evt, uint64_t seq)
{
  switch (evt) {
    case ndn::PingClient::Event::PROBE:
      digitalWrite(LED, HIGH);
      break;
    default:
      digitalWrite(LED, LOW);
      break;
  }
}

void
setup()
{
  Serial.begin(115200);
  Serial.println();

  WiFi.begin(WIFI_SSID, WIFI_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  delay(1000);

  IPAddress routerIp;
  if (!WiFi.hostByName(NDN_ROUTER_HOST, routerIp)) {
    Serial.println("cannot resolve router IP");
    ESP.restart();
  }
  g_transport.begin(routerIp, NDN_ROUTER_PORT, 6363);
  g_face.onData(&processData, nullptr);

  pinMode(LED, OUTPUT);

  ndn::parseNameFromUri(g_interest.getName(), PREFIX);

  g_client.onEvent(&ndnpingEvent, nullptr);
}

void
loop()
{
  g_face.loop();
  g_client.loop();
  delay(10);
}
```

Server sketch `PingServer.ino`:

```arduino
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <esp8266ndn.h>

const char* WIFI_SSID = "ssid";
const char* WIFI_PASS = "passw0rd";
const char* NDN_ROUTER_HOST = "titan.cs.memphis.edu";
const uint16_t NDN_ROUTER_PORT = 6363;
const uint8_t NDN_HMAC_KEY[] = {
  0xaf, 0x4a, 0xb1, 0xd2, 0x52, 0x02, 0x7d, 0x67, 0x7d, 0x85, 0x14, 0x31, 0xf1, 0x0e, 0x0e, 0x1d,
  0x92, 0xa9, 0xd4, 0x0a, 0x0f, 0xf4, 0x49, 0x90, 0x06, 0x7e, 0xf6, 0x50, 0xc8, 0x50, 0x2c, 0x6b,
  0x1e, 0xbe, 0x00, 0x2d, 0x5c, 0xaf, 0xd9, 0xe1, 0xd3, 0xa5, 0x25, 0xe2, 0x72, 0xfb, 0xa7, 0xa7,
  0xe4, 0xb0, 0xc9, 0x00, 0xc2, 0xfe, 0x58, 0xb4, 0x9f, 0x38, 0x0b, 0x45, 0xc9, 0x30, 0xfe, 0x26
};
const int LED = 12;
char PREFIX[] = "/ndn/multicast/esp8266ndn-demo/ping";

WiFiUDP g_udp;
ndn::UnicastUdpTransport g_transport(g_udp);
ndn::Face g_face(g_transport);

ndn_NameComponent g_comps[8];
ndn::NameLite g_prefix(g_comps, 8);
ndn::PingServer g_server(g_face, g_prefix);

void
processInterest(void*, const ndn::InterestLite& interest, uint64_t)
{
  g_server.processInterest(interest);
}

void
onProbe(void*, const ndn::InterestLite&, uint8_t* payloadBuf, size_t* payloadSize)
{
  digitalWrite(LED, HIGH);
  delay(10);
  digitalWrite(LED, LOW);
}

void
setup()
{
  Serial.begin(115200);
  Serial.println();

  WiFi.begin(WIFI_SSID, WIFI_PASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  delay(1000);

  IPAddress routerIp;
  if (!WiFi.hostByName(NDN_ROUTER_HOST, routerIp)) {
    Serial.println("cannot resolve router IP");
    ESP.restart();
  }
  g_transport.begin(routerIp, NDN_ROUTER_PORT, 6363);
  g_face.onInterest(&processInterest, nullptr);
  g_face.setHmacKey(NDN_HMAC_KEY, sizeof(NDN_HMAC_KEY));

  pinMode(LED, OUTPUT);

  ndn::parseNameFromUri(g_prefix, PREFIX);

  g_server.onProbe(&onProbe, nullptr);

  // express one Interest to create face on router
  ndn::InterestLite interest(nullptr, 0, nullptr, 0, nullptr, 0);
  g_face.sendInterest(interest);
}

void
loop()
{
  g_face.loop();
  delay(10);
}
```
