---
title: "reboot-into.sh: Fast Operating System Switch for BerryBoot"
lang: en
date: 2017-06-17
tags:
- RaspberryPi
- bash
image: reboot.gif
---

[BerryBoot](http://www.berryterminal.com/doku.php/berryboot) is a bootloader for Raspberry Pi, allowing multiple operating system images to be placed on a single microSD card.
It displays a menu upon system boot, so that the user can choose which OS to load.

I use a Raspberry Pi 3 as my primary desktop computer.
It loads Ubuntu Mate 16.04 by default, in which I can code, read, and write dissertation.
The same computer is also equipped with RetroPie, as my gaming machine playing FreeDoom.

One problem I'm frequently facing is: in order to switch from work mode to game mode, I must reboot the machine.
Shutting down Ubuntu Mate can take as little as 10 seconds, or as much as 3 minutes, depending on luck.
**I hate to stay with the machine while it's rebooting, but if I walk away, I may miss the 10-second window in which I should select RetroPie from the BerryBoot menu**, before it loads the default, Ubuntu Mate, automatically.

A less known feature of BerryBoot is its [`runonce` file](https://rvalbuena.blogspot.com/2013/02/changing-berryboot-selected-os-on.html).
You may instruct BerryBoot to load a specific image at next boot by writing the image name to `data/runonce` file in BerryBoot partition.
This works particularly well if the Raspberry Pi is headless and does not have a keyboard, but it requires 5 steps and requires typing the full image name in the `runonce` file.

To simplify this process and quickly switch to another operating system in BerryBoot, I wrote a little script:

```bash
#!/bin/bash
WANTOS=$1

if [[ -z $WANTOS ]]; then
  echo Usage: ./reboot-into.sh osname
  exit 2
fi

gvfs-mount -d /dev/mmcblk0p2
cd /media/$(whoami)/berryboot/images
IMGNAME=$(ls *.img* | grep -i $WANTOS | head -1)

if [[ -z $IMGNAME ]]; then
  echo OS not found. Available images:
  ls *.img*
  cd
  gvfs-mount -u /media/$(whoami)/berryboot
  exit 3
fi

echo $IMGNAME | sudo tee ../data/runonce

echo -n 'Rebooting into '$IMGNAME' '
for I in $(seq 5); do sleep 1; echo -n .; done

cd
gvfs-mount -u /media/$(whoami)/berryboot
sudo shutdown -r now
```

This script accepts a single argument, which can be any substring of the desired OS image name.
It mounts the berryboot partition in order to find the full image name, writes the `runonce` file, and triggers a reboot.

I use `gvfs-mount` instead of `mount`, so that I can delay `sudo` until after the full image name is determined, and therefore a prompt for entering password is a reminder that the system is about to be rebooted.
The script also displays a "progress" of five dots, giving the user a final chance to cancel the reboot by pressing CTRL+C if the chosen OS is wrong.

## Screenshots

![OS image not found](notfound.gif)

![reboot into another OS](reboot.gif)
