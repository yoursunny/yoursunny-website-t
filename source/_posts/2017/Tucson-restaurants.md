---
title: My Favorite Tucson Restaurants
lang: en
date: 2017-08-26
tags:
- life
- Arizona
- food
image: ElGueroCanelo-hotdog.jpg
---

During my [six years living in Tucson](/t/2017/six-years-in-arizona/), I visited hundreds of local restaurants.
Here are my top picks.
Those restaurants are all good, and their order within this article does not indicate my preference.

## Best on campus: IQ Fresh

![crispy fish wrap from IQ Fresh](IQ-crispy-fish.jpg)

Located in the main student union, [IQ Fresh](https://union.arizona.edu/dining/sumc/iq) is my usual place for a quick lunch between classes.
They mainly serve wraps: shredded meats and veggies rolled inside a piece of tortilla.
They offer 14 different meats and veggies, and 10 tortilla flavors, giving 140 choices.

My favorite is the **crispy fish wrap** in spinach wrapper, with a side of sweet potato fries.
Sadly, this particular item has been discontinued in 2015, and I stopped going there altogether.

## Best in Main Gate Square: Pasco Kitchen & Lounge

If student union is University of Arizona's living room, [Main Gate Square](https://www.maingatesquare.com/) is UA's front yard.
When I need a change from student union's fast food, I would grab a better meal in Main Gate Square.

![grassfed burger from Pasco](Pasco-grassfed-burger.jpg)

My favorite restaurant in Main Gate Square is [Pasco](http://pascokitchen.com/), which has nice decoration, friendly servers, and creative cuisines.
When I have a big exam coming, I would go to Pasco and have a **grassfed burger**, to ensure I have enough energy pushing through the exam.

## Best Indian: Kababeque Indian Grill

![goat biryani from Kababeque](Kababeque-biryani.jpg)

My other favorite in Main Gate Square is [Kababeque Indian Grill](http://www.kababeque.net/).
I only order from their "curry in a hurry" menu, in which food comes out fast in about 10 minutes.
I can choose one of 6 proteins and one of 10 curry sauces.
I haven't tried all combinations, but so far I like **goat biryani** the most.
However, I dislike that the food is packaged in foam lunch boxes, because [plastic is forever](https://www.indiegogo.com/projects/plastic-is-forever).

## Best Vietnamese: Thuận Kiều Vietnamese Cuisine

Thuận Kiều is located in the [Lee Lee International Market](http://leeleesupermarket.com/), an Asian grocery store which I occasionally visit to stock on hard-to-find Chinese spices.
Lee Lee is very far from [my apartments](/t/2017/roommates-or-not/), but they used to offer a pickup service so that I can go there in their van.
However, I often had to wait an hour or more for Lee Lee's pickup service, which means a shopping trip is several hours, and I would be hungry.
Thus, I included a lunch or dinner at Thuận Kiều during most Lee Lee shopping trips.

![rice vermicelli patties from Thuận Kiều](ThuanKieu-vermicelli.jpg)

Thuận Kiều offers **rice vermicelli patties with meats**.
They are tasty, and are not served in most other Vietnamese restaurants.
However, I feel the manager at Thuận Kiều is "rude", possibly because I cannot understand Vietnamese language.

## Best Chinese: Dragon View

![Peking duck from Dragon View](DragonView-duck.jpg)

I come from China and I can cook most Chinese food, but there are always several entrees that I could not make due to lack of ingredients or equipment.
One of them is the **Peking Duck**, so I set out to find the best Peking Duck in Tucson.
I sampled Peking Duck at [Old Peking Chinese Restaurant](https://oldpekingchinesetucson.com/) and [Dragon View Chinese Restaurant](https://www.dragonviewrestaurant.com/), and I preferred the duck from Dragon View.

## Best Japanese: Yoshimatsu Japanese Eatery

I discovered [Yoshimatsu](http://yoshimatsuaz.com/) through [ISA's international dinner series](/t/2017/ISA/).
Since sushi is commodity (served in every Japanese restaurant), I chose an unique item on my first visit: seafood okonomi yaki.
I was hooked to this Japanese pancake immediately, and had several more during the later years.

![okonomi yaki from Yoshimatsu](Yoshimatsu-okonomiyaki.jpg)

Tip: okonomi yaki is best with seafood.
The pork variant just doesn't taste good.

## Best vegetarian: Govinda's Natural Foods Buffet

![my plate at Govinda's buffet](Govinda.jpg)

[Kayla](https://www.facebook.com/kayla.stack) brought me and half of ISA to [Govinda's](https://www.govindasoftucson.com/).
They serve a buffer of vegetarian and vegan food.
Some are hot food from Indian cuisine, while others are salad, salad, salad.
Unlike Sweet Tomatoes or [Las Vegas buffets](/t/2020/LasVegas/), there aren't many options at Govinda's, but as Kayla said, "the options are all good options".
I agreed with her after trying this restaurant.

## Best hotdog: El Güero Canelo

![Sonoran hotdog from El Güero Canelo](ElGueroCanelo-hotdog.jpg)

[El Güero Canelo](https://www.elguerocanelo.com/) is a restaurant that I visited often but could not remember its name.
I typically refer to it as "the Sonoran hotdog place on Oracle".
My favorite dish is, obviously, the Sonoran hotdog, or "estilo Sonora" as it used to appear on the Spanish-language receipt.
It is incredibly cheap (about $3), which serves as my mid-afternoon snack.

## Best frozen: Tucson Tamale Company

![tamale in rice cooker](TucsonTamale.jpg)

American Express's [ShopSmall](http://shopsmall.com/) campaign brought me to [Tucson Tamale](https://tucsontamale.com/).
They sell tamales, a Mexican food made with corn masa filled with meats and chilies, wrapped in (non-edible) cornhusks.
Weighed in at 200 calories per piece, tamales were included in many of my meals for additional flavors.
The steaming rack on top of my rice cooker is the rightful place for a tamale from Tucson Tamale Company.

## Best breakfast: Nook

![outdoor seating at Nook](Nook.jpg)

I usually eat bread and avocado for breakfast in my room, and do not go out for breakfast very often.
Among about a dozen breakfast restaurants I've tried, my vote goes to [Nook](https://www.nookdowntown.com/).
Unlike most "greasy spoon" places, Nook is very clean.
I especially liked their outdoor seating.

## Best unique experience: Zemam's

<iframe width="560" height="315" src="https://www.youtube.com/embed/veJA0qMpYaw" frameborder="0" allowfullscreen></iframe>

[Zemam's](https://www.zemamsrestaurants.com/) is the first and best Ethiopian restaurant in Tucson.
When I visited Zeman's for the first time, I was astonished that my plate came with neither fork-and-knife like in a American restaurant, nor chopsticks like in a Chinese restaurant, and those items were nowhere to be found in the dining room.
After observing what other customers were doing, I learned a unique way for eating Ethiopian food: with my hand and injera bread.
Eating with hand is actually a common food consumption method in many regions of the world, but Zemam's is the only restaurant that earnestly promotes this part of their culture.
I was very glad to experience their culture, and make myself adorable (as a YouTube comment said) in the process.
