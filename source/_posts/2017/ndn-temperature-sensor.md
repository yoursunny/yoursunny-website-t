---
title: How I Put a Temperature Sensor on the NDN Testbed
lang: en
date: 2017-07-24
tags:
- NDN
- ESP8266
- Losant
image: sensor.jpg
---

The frontpage of my [recently renovated](/t/2017/yoursunny-com-git/) website shows a room temperature reading of my apartment.
This is not your average IoT project, but a showcase of how an IoT device can communicate directly over Named Data Networking (NDN).
This article explains where does this temperature reading come from, how is it turned into NDN Data packets, how the sensor appears on the NDN testbed, and how the temperature reading shows up on the website.

![room temperature display on yoursunny.com frontpage](frontpage.gif)

## Sensor Hardware

If you have been reading my posts, you may have guessed that the temperature reading comes from an ESP8266.
Of course!
It is actually my very first ESP8266, which I received from Losant IoT Inc.
It comes with a TMP36 analog temperature sensor, and has been reporting my room temperature to Losant platform [since May 2016](https://twitter.com/yoursunny/status/727564826147311617).

![Losant Builder Kit with all the additions](sensor.jpg)

## esp8266ndn Library

Soon after I setup the sensor, I had a new idea: I should make the sensor dual-stack, reporting temperature to both Losant and NDN!
To make this happen, the ESP8266 must speak NDN protocol.
There isn't an NDN library for the ESP8266, but ndn-cpp-lite, a lightweight NDN client library offered by UCLA REMAP, [supports the Arduino Yun](https://github.com/named-data/ndn-cpp/tree/bf398050b52faa618f09f49d4ac5b4029ae9f720/examples/arduino/analog-reading).
While their Arduino Yun example is not pretty because it is not a proper Arduino library but requires a non-standard compilation procedure, it illustrates the key points of porting ndn-cpp-lite to Arduino IDE: keep the C and C++ files you need, and delete the others.
Following the example, I packaged ndn-cpp-lite into [esp8266ndn](https://github.com/yoursunny/esp8266ndn), an Arduino library for the ESP8266 microcontroller to speak NDN protocol.
I also added a pair of ndnping client and server into the esp8266ndn library, which I once [wore as a jewelry](/t/2017/ESP8266-ndnping-jewelry/).

Having the esp8266ndn library, the ESP8266 can then create NDN Data packets with temperature readings.
To minimize programming, I decide to integrate temperature readings into the ndnping server: Data produced by ndnping server carries a temperature reading and some other information in the payload.
Links to this and other source code are at the end of this article.

The producer code can respond to ndnping normally:

![ndnping to the ESP8266](ndnping.gif)

At the same time, the Data payload includes temperature, WiFi SSID, local IP address, and device uptime:

![Data packet from the ESP8266 as seen by ndnpeek and ndn-dissect](ndnpeek.gif)

Since the sensor is dual-stack, it was also necessary to change the Losant side to use an asynchronous MQTT library, so that the sensor can simultaneously send updates to Losant's MQTT broker and respond to NDN Interests.

## Testbed Connection and Prefix Registration

I implemented a "face" in the esp8266ndn library that connects the NDN testbed via UDP tunnel.
This is the easy part.

The hard part is how to register a prefix on the NDN testbed.
The standard method of obtaining a "back route" (a route from the testbed to an end host) is through [NFD's auto prefix propagation](/t/2016/nfd-prefix/), but this isn't feasible for the ESP8266 because ESP8266 doesn't have the horsepower to run NFD.
But I have a trick: [sign prefix registration commands on a server](/t/2017/homecam-NDN-prefix/), download a signed command to the ESP8266, and send it to the testbed router.
With this trick, the ESP8266 shows up on the NDN testbed:

![hobo RIB showing /ndn/edu/arizona/cs/shijunxiao/temperature-sensor prefix](hobo-rib.png)

I wish the ESP8266 can sign a prefix registration command by itself, which is probably possible via [micro-ecc](https://github.com/kmackay/micro-ecc), but [nobody worked on my hackathon project](https://github.com/4th-ndn-hackathon/4th-ndn-hackathon.github.io/blob/a64958f52f0d6e27e7ef3ad24b54783cb1511f99/assets/esp8266.pdf) so this isn't happening yet.

## Temperature Retrieval on Website

The final step is to retrieve temperature readings from the NDN testbed and display it on the website.
Using the NDN-JS library, the script sends an Interest to retrieve a Data freshly produced by the sensor, extracts the temperature field, and displays it on the frontpage.
Links to this and other source code are at the end of this article.

## Conclusion

This article describes the inner workings of the temperature reading feature on yoursunny.com frontpage.
An ESP8266 in my apartment reports the temperature to Losant platform and at the same time registers itself as a producer on the NDN testbed.
The website uses NDN-JS to retrieve the temperature reading directly from the ESP8266 sensor device, which doubles as a ndnping server and includes the temperature reading the payload of NDN Data packets.

Links to source code:
[esp8266ndn](https://github.com/yoursunny/esp8266ndn/tree/ae38a10248d5ee92e0e94715efaed7b1553e38ce),
[ndnping server payload](https://github.com/yoursunny/LosantBuilderKit/blob/cb4115a4cb71a585a4186a51e40a75a99236a41c/LosantBuilderKit.ino#L117-L126),
[asynchronous Losant MQTT client](https://github.com/yoursunny/LosantBuilderKit/blob/be9f9891807a6d6d6b013f4c3c2ddd1543d33e64/LosantDeviceAsync.cpp),
[prefix registration](https://github.com/yoursunny/LosantBuilderKit/blob/cb4115a4cb71a585a4186a51e40a75a99236a41c/NdnPrefixRegistration.cpp),
[website](https://bitbucket.org/yoursunny/yoursunny-website/src/cc43f7df541901a6a5850a8d400bde9e14a43380/www/index.php#index.php-95:113)
