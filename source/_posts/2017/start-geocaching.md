---
title: How I Started Geocaching
lang: en
date: 2017-08-14
tags:
- Arizona
- geocaching
image: GC5WHZV.jpg
---

[Geocaching](https://www.geocaching.com/) is an outdoor treasure hunt game.
Participants, or "geocachers", use a Global Positioning System (GPS) receiver to hide and seek containers, called "geocaches" or "caches", at specific locations marked by coordinates all over the world.
I learned about geocaching by accident in Jul 2013, and I was immediately hooked to this game.
Since then, Geocaching had a strong influence in my weekend activities, gadget purchases, and travel choices.

![selfie with GC5WHZV container](GC5WHZV.jpg)

## It All Started from Jenny

Grocery stores offer discounts with a loyalty card, but I forgot to bring mine on Jul 12, 2013.
I [complained on Facebook](https://www.facebook.com/yoursunny/posts/10151700144103827), and my friends told me that I could give my phone number to the cashier, and they would be able to apply the discounts.
[Daniel](https://www.facebook.com/daniel.harms)'s answer mentioned that I could enter "area code + 867-5309" as the phone number, and there is usually a loyalty card associated with this phone number.
I did not understand what's special about this phone number, so I started online searching.

![Facebook post about forgotten grocery loyalty card](8675309.png)

The search brought me to [5208675309.com](http://web.archive.org/web/20090228025112/http://5208675309.com/), in which "520" is Tucson's area code.
I had no clue what this website is about, but it points me to [www.geocaching.com](https://www.geocaching.com/), so I curiously followed the link.
I read the description and watched the video about this treasure hunt game, and I thought it matches my interests:
First, I need more activities, because summer is boring when all my friends are out of town.
Second, my phone has a GPS, and I haven't been using it much.

![5208675309.com](5208675309.png)

I set out to [find my first geocache](https://coord.info/GLBH1FX6) on Jul 15, 2013.
It was [gopro25's BIG bucket cache](https://coord.info/GC30ZDD), which I carefully selected from the list recommended for beginners.
The cache is located in a neighborhood across the street from [NorthPointe](/t/2017/roommates-or-not/).
I wrote down the GPS coordinates from the website, entered it into Google Maps app on my [T-Mobile Comet](/t/2017/first-days-in-Tucson/), and followed the blue dot as I walked toward the cache.
Without much difficulty, I uncovered a 5-gallon bucket half buried on the ground.
Following [the instructions I read on the website](https://www.geocaching.com/guide/), I signed the logbook, and traded a paddle ball toy with a shoe cleaning product.

## What is "LPC"?

After my first find, I continued searching for more geocaches in the vicinity area.
Every evening, I would write down the coordinates of caches I would like to find.
In the morning, I would take a walk for up to three hours, and return to NorthPointe before 10:00 when it would become too hot.
I found 14 caches in Jul 2013, and 15 in Aug 2013.

There were an equally large number of searches that resulted in DNF (Did Not Find).
Most geocache containers are much smaller than a 5-gallon bucket: it could have the size of a lunch box, or even as small as a fingertip.
Although the GPS receiver could bring me to the location, I must figure out where the container is before I could claim I have found the cache.

During these two months, I've seen fingertip-sized magnetic containers attached to metal artworks, mint boxes stuck into guardrails, and flashlight-sized bottles hanging from trees.
There were one kind of geocaches that I had trouble with: the coordinates point to the middle of a big parking lot, and the only object was a lamppost!
Where could a container be hidden in a lamppost?

I had my first major breakthrough in geocaching skills at [LPC PNG Campbell/ Limberlost](https://coord.info/GLBKY9G2).
I noticed a crack in the base of the lamppost.
I poked my head near the crack, and found a mint box inside!
Bingo!

I was a bit scared when I lifted the base of a lamppost for the first time.
It made a noise, and I was concerned that someone would stop me and kick me off their property.
However, nobody paid attention, and I successfully signed the log.
After this find, I understood what "LPC" is: "lamp post cache".

In the next few months, I continued finding more traditional geocaches, and gained more experience on what the containers look like and where they might be hidden.
There were several half-day or full-day bike rides on weekends, where I would choose an area in the city, search for geocaches, and buy some groceries on the way back.

## Attending Event Caches

Most geocaches are physical containers hidden somewhere in the world, waiting for you to find and log.
The minority is an "event cache" category, which is a special kind of geocaches for geocachers to meet and socialize with each other.
Location of the event is represented as coordinates, although there would be no trouble finding the "cache" once you arrive.

I [attended my first geocaching event](https://coord.info/GLD5Q8FY) in a taco restaurant on Jan 16, 2014.
I introduced myself to many people.
One of the new friends I met that day was *GeoSpinnerUA*.
The next weekend, on Jan 25, 2014, she brought me out in her van and took me to attend a series of three "flash mob" events.

![GC4WG2Q group photo, courtesy of Raven Art Studio](flashmob.jpg)

Since then, I have attended over 25 geocaching events, most in [restaurants](/t/2017/Tucson-restaurants/).
Although I'm usually limited by [where the bus could reach](/t/2017/SunTran/), I was attending events more frequently than those with cars.
Occasionally I would [drive a Zipcar](/t/2017/behind-the-wheel/) to attend a geocaching event, and I wasn't hesitant in showing off my Zipcard when they asked how I could get there.
During the events, I enjoyed hearing stories about their epic geocaching journeys and flipping through "travel bugs".

![The Snowdog tells his Mt Lemmon stories at GC75BFE](GC75BFE.jpg)

Events are where I could have a look on the demography of local geocachers.
It seems that most geocachers are elderly people.
They have retired and have plenty of spare time, and they would sometimes organize an out-of-state road trip, just for geocaching.
Another category is families with kids, and most commonly a father spending quality time geocaching with his teenager son.
College students like myself are seen less often.

![people at GC69BEQ](GC69BEQ.jpg)

## FTF!

First To Find, or FTF, is an honor that every geocacher wants.
When a geocache is published, if you get there first and sign the log before all other geocachers, you are the FTF.
Since you have to be the first, there would be only one FTF for each geocache.

I can rarely get a FTF, because I did not pay for a premium membership on Geocaching.com.
Premium members would receive an email notification whenever a new geocache is published in the area, but I do not have access to this benefit.
However, I managed to get two FTFs on the same day.

It was Dec 09, 2014, a boring Tuesday morning.
I was flipping through various apps in my phone, and I noticed two new geocaches popping out on the map that have never been logged.
I ran to the university, borrowed a bike, and rode to the location.
I got FTF on [both](https://coord.info/GLG8D635) of [them](https://coord.info/GLG8D9C9).
😃!

![view from GC5HJEZ](GC5HJEZ.jpg)

I also had five "relaxed" FTFs.
These were traditional caches published on the same day when an event was hosted.
Owners of these caches specifically indicate that everyone who found the cache on the same day as the event would be considered co-FTFs.

I was the ["real" FTF](https://coord.info/GLM611FV) for one of these five caches, as I signed the log first:

![GC68APT logbook](GC68APT.jpg)

I aptly [received "congrats"](https://coord.info/GLM61QPQ) for winning the race:

![GC68APT "congrats"](congrats.png)

## Up Next

This article is the first part of my stories with geocaching.
As I became a more experienced geocacher, I found [more challenging geocaches](/t/2019/geocaching-comfort/), moved and released travel bugs, hid my own geocaches for others to find, invited friends to join the game, and [drove out of town to attend a mega event](/t/2018/YumaMega/).
I will write about all these in future articles.
