---
title: Shocking Changes in China
lang: en
date: 2017-10-16
tags:
- life
- China
image: bikes.jpg
---

After [six years in Arizona](/t/2017/six-years-in-arizona/), I graduated from The University of Arizona, and returned to China on Sep 01, 2017.
My previous returning to China was in 2014, and China has changed greatly during my three years of absence.

## Swipe Your Face

Fraud is a serious problem in China.
The "standard" countermeasure is two-factor authentication, i.e. a random code sent to your phone.
However, fraudsters have been able to convince the victim to reveal the random code on their phone.
A stronger countermeasure is doing all transaction in person: you have to show up at the bank or phone company, and present your ID card.
However, there are usually long lines in those places and thus it's not a pleasant experience.

The regulators invented a new way: "swipe your face".
I bought a SIM card for my smartphone online.
To activate my account, I must upload a picture of my ID card, and record a video of me acting according to a series of random instructions.
The instructions could be: "blink your eyes, open your mouth, rotate your head to the left".
The system would then analyze the video to confirm that I am alive and am the same person as shown on the ID card.

I believe video authentication provides much stronger authentication than asking for "mother's maiden name" and "last four digits of social security number" as most companies do in the United States.
It is surely less convenient than answering a few questions, but this is a trade-off for China where fraud is more common.

## WeChat Pay

In the United States, almost every business accepts credit cards.
Point-of-Sale (POS) machines cost thousands of dollars, but payment providers such as [Square](https://squareup.com) have been offering inexpensive credit card readers that attach to smartphones, which allow smaller merchants such as food trucks and farmer's market vendors to take credit cards.

China has jumped one-step forward, and eliminated the hardware credit card reader altogether.
Whenever I attempt to use a credit card, most merchants would refer me to use "WeChat Pay".
WeChat Pay is a payment feature built into WeChat, the most popular social networking app in China.
After entering my credit card number into the app once, when I want to make a payment, I just need to open the app, display a barcode on my smartphone screen, and let the vendor scan this barcode using his phone.

Some merchants use a different procedure: they post a barcode on their wall or window.
To make a payment, I must scan their barcode with my phone, and enter the purchase amount myself.
However, I didn't have success in using this procedure, and had to pay cash at those vendors.

During my 6-week stay in China, I made nine purchases with WeChat Pay, with the largest transaction exceeding $600.
In contrast, I only swiped the credit card three times in the same period.

## Mobike

I [enjoy riding bikes](/t/2017/on-two-wheels/).
I used to own a bike in Shanghai, but my family decided to get rid of it because I'm in Arizona and nobody else was riding or maintaining the bike.
I didn't ride when I returned to China in the past few years, but this time, I can ride again without buying a new bike: "shared bikes" are littering Chinese streets.

![a row of shared bikes outside a subway station in Nanjing](bikes.jpg)

[Mobike](https://www.mobike.com) is a leading provider of shared bikes.
Each of their bike, colored in orange, has a barcode printed on the handlebar as well as the rear fender.
I can rent any bike by scanning its barcode with the Mobike app downloaded into my smartphone.

I obtained a Mobike trial account via WeChat Pay.
It allows five rides per week, where each ride is limited to two hours.
The rides do cost money, but they have been fairly cheap due to the many promotions they were running.
I completed 8 rides and I only spent ¥5.

Mobikes are widely available.
Most of the time, I was able to locate a bike within a quarter mile.
However, I had mixed experience in unlocking the bikes.
Renting a bike requires an active Internet connection on my phone.
It takes about half a minute where the connection is good; when the cellular signal becomes spotty, I would have to scan the barcode many times in order to unlock a bike, and that is very frustrating.
The bikes are also of poor design and maintenance: the saddle is way too low and cannot be adjusted, which severely affects my top speed and causes sore quads quickly; half of the bikes I rented have broken bells/handlebar/brakes/etc.

## Grocery Delivery

[Amazon Fresh](https://www.amazon.com/b?ie=UTF8&node=10329849011&tag=yoursunny-20&ref_=assoc_tag_ph_1507065544167&_encoding=UTF8&camp=1789&creative=9325&linkCode=pf4&linkId=18bc4fa1902b364e2a42e1836d341036) offers a grocery delivery service in major US cities.
They do not consider Tucson as a major city, so I never experienced this service.

Shanghai is the largest city in the world by population, so it surely has a grocery delivery service, called "食行生鲜" (34580.com).
Unlike Amazon Fresh, 34580.com does not drop the package on your doorstep, because most Chinese homes are apartments and packages get stolen easily.
Instead, they have refrigerated lockers in each neighborhood.
When you place an order in the 34580.com app, you can select a delivery date as early as the next day.
In the evening on that day, you can swipe a membership card at the locker facility, and the locker containing your package would open.

![34580.com grocery locker](locker.jpg)

The produce, meats, and fish sold through 34580.com are of high quality.
Pricing is comparable to local grocery stores, and next-day delivery is free.
Thus, my family enjoys purchasing from them.
The only downside is that each member is limited to purchasing only one discount item each day, and one fish or 300 grams of beans isn't going to feed four people.
