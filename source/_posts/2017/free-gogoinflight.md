---
title: How to Access Gogo Inflight WiFi on American Airlines for Free?
lang: en
date: 2017-06-21
tags:
- WiFi
- security
image: bypassmode.gif
---

[Gogo](https://www.gogoair.com/) offers WiFi Internet access on American Airlines domestic flights within United States.
The Internet services are provided through [Air-To-Ground (ATG)](https://en.wikipedia.org/w/index.php?title=Gogo_Inflight_Internet&oldid=785672822#Air-To-Ground_.28ATG.29) technology: a cellular radio network of over 200 towers in North America points their antennas to the sky; a Gogo-equipped airliner connects to one of these cellular towers, and provides Internet to passengers via WiFi.
Pricing for Internet access via Gogo in-flight WiFi starts at USD 4.95 for 30 minutes.

![gogoinflight airborne splash page](splash.gif)

American Airlines and Gogo also provide inflight personal device entertainment through the same `gogoinflight` Wi-Fi signal.
This service is free of charge, and allows passengers to watch movies and TV episodes on their own smartphones and tablets.
Presumably, contents are pre-downloaded to a server located inside the aircraft, and therefore accessing those contents do not consume radio bandwidth.

![Rogue One: A Star Wars Story movie provided on gogoinflight](movie.gif)

While watching videos are permitted during the flight, Gogo does not want passengers to download these copyrighted movies for watching later at home.
Therefore, users must have the *Gogo Entertainment* app on their mobile devices in order to watch movies.
The app is compatible with the Digital Rights Management technology used in the entertainment server, to ensure that the movies can be watched for free, but cannot be (easily) downloaded.
But what if the device does not already have the Gogo Entertainment app?

![gogoinflight entertainment requires an app](compatibility.gif)

Gogo is kind enough to allow passengers to download the Gogo Entertainment app during the flight, on their WiFi.
Downloading an app to an Android device is easy without Internet: every Android app can be packaged into an APK file; installing from an APK package requires toggling some security settings in the Android system, but it is otherwise doable.
iOS devices, on the other hand, are more locked down.
Most iOS devices do not support installing apps from files, but can only install apps by downloading from Apple's Appstore.
In order to allow iPhone and iPad users to install the Gogo Entertainment app, Gogo provides a "bypass mode":

![gogoinflight bypass mode](bypassmode.gif)

You will be asked to solve an CAPTCHA, after which you are redirected to Apple Appstore:

![Gogo Entertainment listing on Appstore](appstore.gif)

At this moment, if you quit the Appstore, you'll find that, you have connected to the Internet for free.
The connection goes through AS11167, AirCell LLC.

![AirCell LLC](bgp.gif)

This "free" gogoinflight WiFi lasts about 10 minutes, enough for fetching emails, downloading some reading materials, or checking maps.
It is also useful for testing the speeds and latency of Gogo Inflight WiFi before paying $4.95 or more to purchase the service.
The bandwidth I get is less than 20KB and the latency is almost 900ms, so I think it would not be a good purchase.

**How could Gogo plug this hole?**
They could apply a whitelist in "bypass mode" to only allow connections to Apple servers.
Or, they could disable the in-flight app downloading altogether and ask passengers to download the Gogo Entertainment app before departure, similar to what Southwest Airlines does.
