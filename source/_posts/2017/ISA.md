---
title: Meet the World at International Student Association
lang: en
date: 2017-08-17
tags:
- life
- Arizona
image: TrailDustTown.jpg
---

Most of my social life during my [six years at University of Arizona](/t/2017/six-years-in-arizona/) was at the [International Student Association (ISA)](https://www.facebook.com/groups/mtwua/).
I made friends, made frenemies, and had fun through events and gatherings organized by ISA.

![ISA Trail Dust Town visit, Apr 04, 2013](TrailDustTown.jpg)

## The Very First Event

Every incoming international student is required to attend the International Student Orientation, organized by [International Student Programs and Services (ISPS)](http://web.archive.org/web/20110719140111/http://internationalstudents.arizona.edu/home).
In the 2011 orientation, ISPS recruited a group of student workers, called [Peer Integrators (PIs)](http://web.archive.org/web/20110717020644/http://internationalstudents.arizona.edu:80/peerintegrator), to assist with the orientation, and to plan and participate in orientation social events for new international students.
One of the social events was a **campus race** on Aug 14, 2011, organized by [Morris Zhou](https://www.facebook.com/morris.zhou), a PI from China.
I participated in this event because its name sounds like my favorite TV show [The Amazing Race](http://www.cbs.com/shows/amazing_race/).

The goal of this event was to make students familiarize with the UA campus.
Each team of two or three students were given a sheet of hints, each referring to a location on campus.
Within one hour, the team must visit as many locations as they can, and take pictures of the team members in front of those locations.
The team that gets the largest number of correct answers would be awarded the winner.

![Campus Race hint sheet](CampusRace.png)

I entered the race with two other Chinese students that I met in [Grant Inn](/t/2017/first-days-in-Tucson/).
None of us had any idea about most of the hints.
My strategy was to spend five minutes at a public computer in the student union to lookup information online; this helped solve hints such as "turquoise seats" and "pottery exhibits".
Then, it was a foot race across the entire campus, where we [took pictures](https://www.facebook.com/media/set/?set=oa.178449022226660&type=1) of what we believed to be the correct answers.
Despite all the running, my team did not win the race.

![my team members in front of a blue emergency phone](emergency.jpg)

At the end of the event, Morris announced that he and a few other PIs are starting a new student organization called **Meet The World**.
It would be a student club mainly for international students to make friends and socialize.
He encouraged us to join the [Facebook group](https://www.facebook.com/groups/mtwua/) to keep updated with upcoming events.

## Meet The World => International Student Association

Student clubs at University of Arizona are managed by [Associated Students of the University of Arizona (ASUA)](http://asua.arizona.edu).
While it is not required for a club to be recognized by ASUA, an official recognition has many benefits including on-campus promotion opportunities.
Therefore, Morris drafted the club constitution, and worked toward getting the club recognized.
He would be the president, and [Jessica Borders](https://www.facebook.com/708676285) would serve as the treasurer.

During the recognition process, Morris noticed that there was a previous club called "International Student Association" but they were not renewing their registration.
He asked Meet The World members, dubbed "MTWer's", whether we wanted to register the club as "International Student Association".
While I personally preferred "Meet The World", MTWer's eventually voted in favor of "International Student Association" as the official club name, while keeping "Meet The World" as a slogan.

![club name poll](club-name.gif)

On Sep 16, 2011, **International Student Association** (ISA) was founded and officially recognized.
The goals of ISA are:

* To foster cultural diversity by building awareness of the international cultures represented in the University of Arizona community.
* To promote the celebration of diverse traditions through various social events and activities.
* To encourage exploration of local/regional culture and customs.
* To model respect for differences and the desire to learn from each other in this community.

## Worldly Desserts

In America, money is the ticket to everything.
During ISA's first two years of presence, many events were revolved around "making money", or "fundraising" if that sounds better.

In 2012, ISA did a bake sale around Valentine's Day, and operated a food booth at Wildcat World Fair.
I personally disliked these events, because I thought social events would be more fun.
Nevertheless, I helped designed a poster for the food booth titled **A Taste Of The World**.

In 2013, ISA ambitiously took on a bigger event: Tucson Meet Yourself.
ISA operated a food booth at Tucson Meet Yourself 2013, specializing in desserts, and aptly named **Worldly Desserts**.
$800 license fees must be paid before the food booth would be approved, but ISA did not have much money at the time.
I loaned $200 to ISA for paying the fees; it was a risky investment, but I decided to take a risk for my beloved club.
I was not legally allowed to receive an interest over the loan, so I asked for $3 worth of food voucher at our booth as a compensation.

![Worldly Desserts food booth at Tucson Meet Yourself 2013](WorldlyDesserts.jpg)

The Worldly Desserts food booth was a success, because this was ISA's last "making money" event that I could remember.
Lee Lee International Market sponsored some of the ingredients, and volunteers devoted their time to prepare and sell the food.
The earning from this food booth was never published, but it is widely believed that a food booth at Tucson Meet Yourself can earn at least $3000 minus $1000 in costs and fees.
I redeemed my voucher for a few pieces of unidentified objects made from chocolates, and got my $200 back after the festival.

## Online Presence

ISA's initial online presence was the [Facebook group](https://www.facebook.com/groups/mtwua/), created by Morris.
The group identifier, `mtwua`, means "Meet The World at University of Arizona".

Morris soon resigned from his president role, and [Li Jiang](https://www.facebook.com/li.jiang.9) took over his position.
On Dec 09, 2011, she invited me to become an officer:

> we are honored to invite you as the Tech Person (the title has not been addressed yet, it will though!). This position is about editing Facebook (such as create events and post whatever we need to) and also the Orgsync.

I kindly accepted this position, and asked for the title of **Chief Technology Officer** (CTO) which was approved.
I was responsible for approving new members into the Facebook Group and deleting undesirable posts in the Group.

Since I am an experience web developer and have made [more than 60 websites](/t/2011/10year-websites/), I proposed to create a website for ISA.
A few weeks later, [UAISA.org](http://web.archive.org/web/20130428201044/http://www.uaisa.org/) was born.
The website contained an event calendar, ISA introduction, past events, and meeting minutes.
The last category, meeting minutes, was difficult to obtain: the "secretary" officer was responsible for writing the meeting minutes, but sometimes they forgot to write them, and I had to send dozens of email reminders; I was persistent in sending the reminders, because I had them automated.
Along with the website, I also created a Facebook *Page* to store event photos for integration into the website, because photos posted in ISA's Facebook *Group* could not be displayed on the website due to API restrictions.

I served as CTO for two years, the maximum term of an officer role allowed by ISA constitution, until the end of 2013.
After my retirement, the website was closed for more than a year, and the Page was abandoned and eventually became a source of confusion because newcomers did not know why there were both a Page and a Group.

## Downtown History Walk

While my primary responsibility of the CTO was managing the Facebook Group and editing the website, these were not my only responsibility.
I also organized two events: a downtown history walk, and a visit to Trail Dust Town.

The **downtown history walk** was a walking tour along the Turquoise Trail (formerly called the Presidio Trail) in Downtown Tucson.
This trail is a 2.5-mile loop through downtown Tucson that highlights structures and sites of historic interest and is marked by a turquoise stripe on the sidewalk.
During this trip, I would lead a group of students along the trail, and point out various locations for them.
Most of the group would gather on campus, and ride a [city bus](/t/2017/SunTran/) to downtown before starting the walk.

The first time I led this trip was on Aug 14, 2012.
The then ISA president, Li Jiang, ordered me to shorten the route to no more than 1.5 miles given the hot weather, and thus we only walked on the south section of the trail.
To prepare for the trip, I walked alone to familiarize myself with the route, and requested 20 maps from Tucson Visitor Center under the condition that unused maps must be returned because "maps cost money to produce".
This event had a great turnout of 29 participants.
I had [Gaurav Singh](https://www.facebook.com/787924987) assist me in leading the way and ensuring nobody gets lost.
I also brought students into a Thai restaurant, where Li Jiang made a deal that they would donate 10% of sales to ISA.

![route of first Downtown History Walk](presidio.jpg)

I repeated the event twice in 2013.
8 guests showed up on Jan 06, and 21 showed up on Aug 31.
While the turnout was less than the first walk, we walked the entire route.

Event planners after me developed the downtown history walk into **Discover Tucson**, a scavenger hunt event just like ISA's very first campus race.
The race was hosted three times, with decreasing number of participants.
I won the race once, and the prize was a Navajo dream catcher.

![Navajo dream catch from Discover Tucson on Aug 22, 2014](DreamCatcher.jpg)

## Jan and Debbie's Era

[Jan Rydzak](https://www.facebook.com/jan.rydzak) and [Debbie Schmidt](https://www.facebook.com/Shigure), a pair of Polish dancers, became the next two ISA presidents.
They were influential and sociable, contributed greatly to ISA's culture, and made everyone happy.

![Jan Rydzak wanted to tandem skydive with me](Jan.jpg)

In addition to ISA's traditional "international dinner" series where students gather and [eat at a restaurant](/t/2017/Tucson-restaurants/), and the downtown trips, Jan and Debbie introduced several new lines of events:

* LanguageLive!, which invites native speakers of exotic languages to teach everyone their languages.
* Movie nights, where 10~15 of us watch a movie in the library, complete with (cheap) popcorns and soda.
* White elephant gift exchange on Christmas, which allowed me to give rid of some useless junk around the house, in exchange for some other useless junk.
* Potlucks, where each student brings a dish to share; chips and soda are forbidden, so everyone brings desserts or salad, and I'm sometimes the only one bringing the protein.
* Karaoke.
* [Hikes!](/t/2017/hiking-Arizona/)

Turnout of those events varied from more than 40 to less than 5, but having a variety of different events is definitely better than just a dinner every month.
However, since Jan and Debbie were gone, turnout of events was constantly decreasing, and ISA seems to have lost its glory.

**TRIVIA**: Debbie's title before becoming the president was *Supreme Queen of the Universe*, as seen in [meeting minutes](http://web.archive.org/web/20131211193235/http://www.uaisa.org/meeting/20131122/).

## Conclusion

ISA has been an important part of my social life during my six years attending University of Arizona.
Likewise, I had significant influence on ISA's history, because I am the longest-term member of ISA since its beginning.
Although I could not resurrect ISA from its declining state, I wish ISA would have a strong leader in the future and find back its glory.
