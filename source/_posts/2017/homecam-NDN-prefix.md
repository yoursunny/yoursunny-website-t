---
title: How HomeCam Registers NDN Prefixes?
lang: en
date: 2017-07-17
tags:
- NDN
- JavaScript
---

[HomeCam](/p/homecam/), a browser-based home surveillance camera, is one of my major side projects during the past few years.
My [last post](/t/2017/how-homecam-works/) gave an overview on the overall architecture and various components of HomeCam.
I mentioned that HomeCam delivers pictures via [Named Data Networking (NDN)](https://named-data.net/), which provides better scalability because the camera only needs to serve each picture once even if there are multiple viewers, and the network takes care of the distribution.

Both HomeCam viewers and cameras connect to the global NDN testbed network via WebSockets.
A viewer sends Interests to the network to request a picture from a specific camera identified by part of the Interest name.
The network forwards the Interests to the named camera, and the camera responds with Data packets that contain the picture.
But how does the network know where the camera is?

The **camera must register its prefix** onto the NDN testbed network.

For a regular end host running the NDN Forwarding Daemon (NFD), you may [let the world reach your NFD](/t/2016/nfd-prefix/) via auto prefix propagation.
This procedure involves sending a prefix registration command to the network, which causes the router to add a route toward the end host.
A requirement is that the prefix registration command must be signed by a key trusted by the network.

HomeCam camera is a browser-based application that directly connects to a testbed router, and there is no NFD running locally to do auto prefix propagation for us.
However, HomeCam can still have a route added toward the camera by sending a prefix registration command.

The main obstacle is how to generate a prefix registration command and sign it with a network-trusted certificate.
While it is possible to [issue your own NDN certificates](/t/2016/ndncert/) and have the network trust them, it is difficult to securely store and use these certificates in the browser.

HomeCam's solution is to generate and sign the prefix registration commands on the webserver.
Although HomeCam uses NDN as an underlying transport mechanism, it is not a [pure NDN website](/t/2011/ndn-web/) but is deployed as a regular HTTPS website, where [same-origin policy](https://developer.mozilla.org/en-US/docs/Web/Security/Same-origin_policy) applies.
Since the JavaScript web application completely trusts the origin server, signing the NDN prefix registration command on the server is as secure as signing on the client, while avoiding many [oddities of attempting to do cryptography in JavaScript](https://web.archive.org/web/20111127053119/http://www.matasano.com/articles/javascript-cryptography/).

This solution is implemented in four steps:

1. An NDN signing key is made available to the webserver.
   nginx is running as `www-data` and its home directory is `/var/www`, so the certificate should be placed there.
   You may request or import a certificate to this location by prefixing every `ndnsec` command with `sudo HOME=/var/www -u www-data`.
   If you can see the certificate with `sudo HOME=/var/www -u www-data ndnsec list -c`, it is installed correctly.
2. The HomeCam camera web app asks the web server via AJAX for a signed prefix registration command.
   A PHP script on the web server invokes `register-prefix-cmd` command with appropriate arguments.
3. The [register-prefix-cmd](https://github.com/yoursunny/ndn6-tools/blob/main/register-prefix-cmd.md) program generates a prefix registration command with specified name prefix and other parameters, and prints the signed command to stdout, which is [`passthru`](https://www.php.net/manual/en/function.passthru.php)'ed to the browser.
4. The web app sends the signed command via NDN-JS WebSocket face to the router.
   If all goes well, this shall bring back a successful response, and the router would add a route toward the WebSocket face where the camera may receive Interests from viewers.

I have been running a camera 24x7 for several weeks, and this "sign elsewhere" prefix registration solution works well most of the time.
Same idea can be used in other applications where the app itself is unable to sign its prefix registration commands due to difficulty in key management or limitation in computational resources, but there exists a "secure channel" between the app and a server.
